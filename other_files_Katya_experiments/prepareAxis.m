function [X,Y,Z,B]=prepareAxis(postProcessedPerSubArray,sector_image_plane,centersSubArrays,rotation_angle,inputOpt)
global plot_debug

inputOpt = parser(struct('filterPixels1', -60, ...
                        'imgSize',       256, ...
                        'sizeOfArray',   4, ...
                        'sizeOfPatch',   16), inputOpt);
                    
if size(postProcessedPerSubArray.Bmode,2)>1
    [X,Z]=meshgrid(postProcessedPerSubArray.disp_az_lims.*1000, postProcessedPerSubArray.disp_dep_lims.*1000);
    switch sector_image_plane
        case 'azimuth'
            Y=centersSubArrays(2).*1000.*ones(size(X));
            position=[X(:) Y(:) Z(:)];
        case 'elevation'
            Y=centersSubArrays(1).*1000.*ones(size(X));
            position=[Y(:) X(:) Z(:)];
    end
else
    position(:,1)= postProcessedPerSubArray.disp_az_lims.*ones(size(postProcessedPerSubArray.disp_dep_lims)).*1000;
    position(:,3)= postProcessedPerSubArray.disp_dep_lims.*1000;
    position(:,2)= centersSubArrays(2).*1000.*ones(size(postProcessedPerSubArray.disp_dep_lims));
end
[position]=rotate_2Dimage(position,-rotation_angle,centersSubArrays(:)'.*1000);
if size(postProcessedPerSubArray.Bmode,2)>1
    X=reshape(position(:,1), [inputOpt.imgSize inputOpt.imgSize])./10^3;
    Z=reshape(position(:,2), [inputOpt.imgSize inputOpt.imgSize])./10^3;
    Y=reshape(position(:,3), [inputOpt.imgSize inputOpt.imgSize])./10^3;
else
    X=position(:,1)./10^3;
    Z=position(:,2)./10^3;
    Y=position(:,3)./10^3;
end
if plot_debug
    figure(2); hold on;
    if size(postProcessedPerSubArray.Bmode,2)>1
        p=warp(X.*10^3,Y.*10^3,Z.*10^3,postProcessedPerSubArray.Bmode); caxis([-60 0]);
    else
        scatter3(X.*10^3,Y.*10^3,Z.*10^3,5.0,postProcessedPerSubArray.Bmode, 'filled');
    end
    view(-130,85);
end


ind=find(postProcessedPerSubArray.Bmode(:) >inputOpt.filterPixels1);
B=postProcessedPerSubArray.Bmode(ind);
X=X(ind);
Y=Y(ind);
Z=Z(ind);