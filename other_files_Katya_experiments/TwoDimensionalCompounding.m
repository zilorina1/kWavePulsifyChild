function [AzAxisPerSubArray,ElAxisPerSubArray, BmodePerSubArray]=TwoDimensionalCompounding(j,postProcessedPerSubArray, sector_image_plane,input, cmpdImg)
      global plot_debug
      
      ElAxisPerSubArray=cellfun(@(x) x.disp_dep_lims, postProcessedPerSubArray,'UniformOutput',false);
      AzAxisPerSubArray=cellfun(@(x) x.disp_az_lims, postProcessedPerSubArray,'UniformOutput',false);
      BmodePerSubArray=cellfun(@(x) x.Bmode, postProcessedPerSubArray,'UniformOutput',false);
      
      if j>1 %start the compounding from the 2d image
                    %% In the depth axis, we add the background pixels for the cropped pictures, so that all pics have the same zAxis limits  
                    minZ=min(cell2mat(cellfun(@(x) min(x(:)),ElAxisPerSubArray(j-1:j),'UniformOutput',false)));
                    maxZ=max(cell2mat(cellfun(@(x) max(x(:)),ElAxisPerSubArray(j-1:j),'UniformOutput',false))); 
                    is_needed_to_add = cellfun(@(x) (min(x)>minZ)|(max(x)<maxZ),ElAxisPerSubArray(j-1:j),'UniformOutput',false);
                    min_adding_values=cellfun(@(x) minZ:x(2)-x(1):min(x)-(x(2)-x(1)), ElAxisPerSubArray(j-1:j),'UniformOutput',false); 
                    max_adding_values=cellfun(@(x) max(x)+(x(2)-x(1)):x(2)-x(1):maxZ, ElAxisPerSubArray(j-1:j),'UniformOutput',false);
                    ElAxisPerSubArray(j-1:j)=cellfun(@(x,y,z) [y x z],ElAxisPerSubArray(j-1:j), min_adding_values, max_adding_values,'UniformOutput',false);
                    BmodePerSubArray(j-1:j)= cellfun(@(x,y,z) cat(1,min(x(:)).*ones(numel(y),size(x,2)),x,min(x(:)).*ones(numel(z),size(x,2))),...
                                                                                                      BmodePerSubArray(j-1:j),min_adding_values,max_adding_values,'UniformOutput',false);                     
                     %general Axis
                     minX=min(cell2mat(cellfun(@(x) min(x(:)),AzAxisPerSubArray(j-1:j),'UniformOutput',false)));%X limits for each image
                     maxX=max(cell2mat(cellfun(@(x) max(x(:)),AzAxisPerSubArray(j-1:j),'UniformOutput',false)));%X limits for each image
                     minZ=max(cell2mat(cellfun(@(x) min(x),ElAxisPerSubArray(j-1:j),'UniformOutput',false)));%intersection for depth
                     maxZ=min(cell2mat(cellfun(@(x) max(x),ElAxisPerSubArray(j-1:j),'UniformOutput',false)));%intersection for depth
                     zAxis=ElAxisPerSubArray(j-1:j); 
                     xAxis=AzAxisPerSubArray(j-1:j);
 
                     indZ=cellfun(@(x) ((minZ<=x)|(x<=maxZ)),zAxis,'UniformOutput',false);
                     croppedZaxis=cellfun(@(x,indz) x(indz),zAxis,indZ,'UniformOutput',false);

                    if (max(AzAxisPerSubArray{j-1}(:))>min(AzAxisPerSubArray{j}(:)))||(min(AzAxisPerSubArray{j-1}(:))>max(AzAxisPerSubArray{j}(:))) %if current images intersect 
                          %intersection X
                          minIntX=max(cell2mat(cellfun(@(x) min(x(:)),AzAxisPerSubArray(j-1:j),'UniformOutput',false)));
                          maxIntX=min(cell2mat(cellfun(@(x) max(x(:)),AzAxisPerSubArray(j-1:j),'UniformOutput',false)));
                                                        
                          %{
                          figure(4); imagesc(xAxis{1}, zAxis{1}, BmodePerSubArray{j-1}); axis tight equal ij;
                          colormap(gray); caxis([-250 0]);
                          
                          figure(5); imagesc(xAxis{2}, zAxis{2}, BmodePerSubArray{j}); axis tight equal ij;
                          colormap(gray); caxis([-250 0]);
                          %}
                          %% Calculate the Intersection part
                          indX=cellfun(@(x) find((minIntX<=x)&(x<=maxIntX)),xAxis,'UniformOutput',false);
                          croppedXaxis=cellfun(@(x,indz) x(indz),xAxis,indX,'UniformOutput',false);
                          [Xq,Yq]=meshgrid(linspace(minIntX, maxIntX, input.outImgSize(1)),linspace(minZ, maxZ, input.outImgSize(2)));
                          [GridX, GridY]=cellfun(@(x,y) meshgrid(x,y),croppedXaxis,croppedZaxis,'UniformOutput',false);
                          %part of images that intersect and make them one
                          %of one size
                          IntersectBmodebySubArray=cellfun(@(x,indz,indx,y,z) interp2(y,z,x(indx, indz),Xq,Yq),BmodePerSubArray(j-1:j),indX, indZ,GridX,GridY,'UniformOutput',false);
         
                          %substitute background with other image (so that we will not average with the background)
                          %ind=cellfun(@(x) x==min(x(:)),IntersectBmodebySubArray,'UniformOutput',false);
                          ind=cellfun(@(x) x<=cmpdImg.filterPixels1,IntersectBmodebySubArray,'UniformOutput',false);
                          IntersectBmodebySubArray{1}(ind{1})=arrayfun(@(x,y,N) compare_intersect(x,y,N),IntersectBmodebySubArray{1}(ind{1}),IntersectBmodebySubArray{2}(ind{1}),...
                                                                                                                                     ones(numel(IntersectBmodebySubArray{1}(ind{1})),1).*cmpdImg.filterPixels2);
                          IntersectBmodebySubArray{2}(ind{2})=arrayfun(@(x,y,N) compare_intersect(x,y,N),IntersectBmodebySubArray{1}(ind{2}),IntersectBmodebySubArray{2}(ind{2}),...
                                                                                                                                     ones(numel(IntersectBmodebySubArray{1}(ind{2})),1).*cmpdImg.filterPixels2);
                          %calculate the averaged intersection
                          IntersectionBmode = sum(cat(3,IntersectBmodebySubArray{:}),3)./numel(IntersectBmodebySubArray);
           
                          %% Keep the left and right parts of the image (for further averaging and total image)      
                          indX=cellfun(@(x) find((x<minIntX)|(x>maxIntX)),xAxis,'UniformOutput',false);
                          if ~isempty(indX)
                           ind=~cellfun(@isempty, indX);  
                           img=BmodePerSubArray(j-1:j);
                           if find(ind==0)
                               indX=indX(ind);
                               indX{2}= indX{1}(find(diff(indX{1})>1)+1:end);
                               indX{1}=indX{1}(1:find(diff(indX{1})>1));
                               xAxis=repmat(xAxis(ind),1,2);
                               indZ=repmat(indZ(ind),1,2);
                               croppedZaxis=repmat(croppedZaxis(ind),1,2);
                               img=BmodePerSubArray(j-1:j);
                               img=repmat(img(ind),1,2);
                           end      
                           croppedXaxis=cellfun(@(x,indz) x(indz),xAxis,indX,'UniformOutput',false);
                           Xq=cell(1,2);Yq=cell(1,2);
                           if find(cellfun(@(x) numel(x), indX)>1)
                              [GridX, GridY]=cellfun(@(x,y) meshgrid(x,y),croppedXaxis,croppedZaxis,'UniformOutput',false); 
                              [Xq,Yq]=cellfun(@(x) meshgrid(linspace(min(x), max(x), input.outImgSize(1)),linspace(minZ, maxZ, input.outImgSize(2))),croppedXaxis,'UniformOutput',false);
                              ind=(1:2).* (cellfun(@(x) numel(x), indX)>1);
                              ind=ind(ind~=0);
                              LeftRightBmodebySubArray(ind)=...
                                    cellfun(@(x,indz,indx,y,z,xq,yq) interp2(y,z,x(indx, indz),xq,yq),img(ind),indX(ind), indZ(ind),...
                                                                                      GridX(ind),GridY(ind),Xq(ind),Yq(ind),'UniformOutput',false);                                                         
                           end
                           if find(cellfun(@(x) numel(x), indX)==1)
                             ind=(1:2).* (cellfun(@(x) numel(x), indX)==1);
                             ind=ind(ind~=0);
                             Yq(ind)=cellfun(@(x) linspace(minZ, maxZ, input.outImgSize(2)),Yq(ind),'UniformOutput',false);
                             Xq(ind)=croppedXaxis(ind);
                             LeftRightBmodebySubArray(ind)=...
                                    cellfun(@(x,indz,indx,z1,z2) interp1(z1,x(indx, indz),z2)',img(ind),...
                                    indX(ind), indZ(ind),croppedZaxis(ind),Yq(ind),...
                                                                                 'UniformOutput',false);                                                                                   
                           end
                           %% Glue together the intersection part and left and right parts of the image  
                           if max(Xq{1}(1,:))<min( Xq{2}(1,:))
                              finalBmode=cat(2,LeftRightBmodebySubArray{1},IntersectionBmode,LeftRightBmodebySubArray{2});
                              axX=[Xq{1}(1,:) linspace(minIntX, maxIntX, input.outImgSize(1)) Xq{2}(1,:)];
                              axY=linspace(minZ, maxZ, input.outImgSize(2));
                           else
                              finalBmode=cat(2,LeftRightBmodebySubArray{2},IntersectionBmode,LeftRightBmodebySubArray{1});
                              axX=[Xq{2}(1,:) linspace(minIntX, maxIntX, input.outImgSize(1)) Xq{1}(1,:)];
                              axY=linspace(minZ, maxZ, input.outImgSize(2));
                           end    
                          else
                           finalBmode=IntersectionBmode;
                           axX=linspace(minIntX, maxIntX, input.outImgSize(1));
                           axY=linspace(minZ, maxZ, input.outImgSize(2));
                          end
                    else %if the images do not intersect in X axis
                         [GridX, GridY]=cellfun(@(x,y) meshgrid(x,y),xAxis,croppedZaxis,'UniformOutput',false); 
                         [Xq,Yq]=cellfun(@(x) meshgrid(linspace(min(x), max(x), input.outImgSize(1)),linspace(minZ, maxZ, input.outImgSize(2))),xAxis,'UniformOutput',false);        
                          LeftRightBmodebySubArray=cellfun(@(x,indx,y,z,xq,yq) interp2(y,z,x(indx,:),xq,yq),BmodePerSubArray(j-1:j),indZ,GridX,GridY,Xq,Yq,'UniformOutput',false);
                          finalBmode=cat(2,LeftRightBmodebySubArray{1},LeftRightBmodebySubArray{2});
                          axX=[Xq{1}(1,:) Xq{2}(1,:)];
                          axY=linspace(minZ, maxZ, input.outImgSize(2));
                    end
  
         
                    %% Interpolate the image to the original size
                    [xF,yF]=meshgrid(axX,axY);
                    [Xq,Yq]=meshgrid(linspace(min(axX), max(axX), input.outImgSize(1)),linspace(min(axY), max(axY), input.outImgSize(2)));
                    ind=isnan(finalBmode(round(size(finalBmode,1)/2),:)); %clean any NaNs
                    xF(:,ind)=[];yF(:,ind)=[];finalBmode(:,ind)=[];
                    finalBmode=(interp2(xF,yF,finalBmode,Xq,Yq));
                    if plot_debug
                       figure(2); clf;
                       imagesc(linspace(min(axX), max(axX), input.outImgSize(1)), linspace(min(axY), max(axY), input.outImgSize(2)),finalBmode);
                       colormap(gray);
                       set(gca,'YDir','Reverse');
                       axis equal tight;
                       colorbar;
                    end
                    
                    %% Update the left image with the intersected one
                    AzAxisPerSubArray{j}=linspace(min(axX), max(axX), input.outImgSize(1)); 
                    ElAxisPerSubArray{j}=linspace(min(axY), max(axY), input.outImgSize(2));
                    BmodePerSubArray{j}=finalBmode;
      end
end