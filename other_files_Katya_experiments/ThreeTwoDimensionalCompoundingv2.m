function [BmodeGlobal, pointsDistance]=ThreeTwoDimensionalCompoundingv2 (Axis, Bmode3D, mesh3D, BmodeGlobal, pointsDistance)
iX_local=round((Axis(:,1)-mesh3D.globalMin(1))./mesh3D.stepGlobal(1))+1;
iY_local=round((Axis(:,2)-mesh3D.globalMin(2))./mesh3D.stepGlobal(2))+1;
iZ_local=round((Axis(:,3)-mesh3D.globalMin(3))./mesh3D.stepGlobal(3))+1;
if (~isempty(find(iX_local<=0)))||(~isempty(find(iY_local<=0)))||(~isempty(find(iZ_local<=0)))
    display('Redefine 3D mesh...');
end
meshUniform=[iX_local iY_local iZ_local];
[uniqueMeshUniform,ib,id]=unique(meshUniform, 'rows');

radius=2;%1;
[p1,p2,p3]=meshgrid(-radius:radius,-radius:radius,-radius:radius);
p=[p1(:) p2(:) p3(:)];
fprintf('Compounding of all subArrays...');

                         %% Parallel Toolbox - parfor
                          %delete(gcp('nocreate')); 
                          %parpool();%(22);
                          parpool(36);
                          v=cell(1,size(uniqueMeshUniform,1)); 
                          v1=cell(1,size(uniqueMeshUniform,1)); 
                          i1=cell(1,size(uniqueMeshUniform,1)); 
                          i2=cell(1,size(uniqueMeshUniform,1)); 
                          i3=cell(1,size(uniqueMeshUniform,1));
                         % distcomp.feature( 'LocalUseMpiexec',false);
                          %parpool(36); % define how many processes, this is for the computational computer from the lab, on the local machine, use a small number 
                          mesh3D_lin1=mesh3D.linGlobal{1};
                          mesh3D_lin2=mesh3D.linGlobal{2};
                          mesh3D_lin3=mesh3D.linGlobal{3};

                          WaitMessage = parfor_wait(size(uniqueMeshUniform,1), 'Waitbar', true);
                          parfor ik=1:size(uniqueMeshUniform,1)
                                 idx=find(id==ik);
                                 d_around=reshape(cell2mat(arrayfun(@(ikk) cell2mat(arrayfun(@(row) (mesh3D_lin1(iX_local(ikk)+p(row,1))-Axis(ikk,1))^2+...
                                            (mesh3D_lin2(iY_local(ikk)+p(row,2))-Axis(ikk,2))^2+...
                                           (mesh3D_lin3(iZ_local(ikk)+p(row,3))-Axis(ikk,3))^2,...
                                            1:size(p,1),'UniformOutput', false)'), idx,'UniformOutput', false)), [size(p,1) numel(idx)]);
                                 Bmode3D_current=reshape(cell2mat(arrayfun(@(ikk) Bmode3D(ikk).*ones(size(p,1),1), idx, 'UniformOutput', false)),[size(p,1) numel(idx)]);
                                 Bmode3D_current=cell2mat(arrayfun(@(row)  sum(1./d_around(row,:).*Bmode3D_current(row,:)), 1:size(p,1),'UniformOutput', false))';
                                 indy=cell2mat(arrayfun(@(row)  iY_local(idx(1))+p(row,2), 1:size(p,1),'UniformOutput', false))';
                                 indx=cell2mat(arrayfun(@(row)  iX_local(idx(1))+p(row,1), 1:size(p,1),'UniformOutput', false))';
                                 indz=cell2mat(arrayfun(@(row)  iZ_local(idx(1))+p(row,3), 1:size(p,1),'UniformOutput', false))';                                                                 
                                 v{ik}=cell2mat(arrayfun(@(row)  sum(1./d_around(row,:)), 1:size(p,1),'UniformOutput', false))';
                                 v1{ik}=Bmode3D_current;
                                 i1{ik}=indy;
                                 i2{ik}=indx;
                                 i3{ik}=indz;
                                 WaitMessage.Send;
                                 
                         end
                         WaitMessage.Destroy
                         i1=cell2mat(i1');
                         i2=cell2mat(i2');
                         i3=cell2mat(i3');
                         indexes=[i1 i2 i3];
                         [unique_indexes,ib,id]=unique(indexes, 'rows');
                         v=cell2mat(v'); v1=cell2mat(v1');
                         WaitMessage = parfor_wait(size(unique_indexes,1), 'Waitbar', true);
                         parfor ik=1:size(unique_indexes,1)
                                  idx=find(id==ik);
                                  bmode(ik)=sum(v1(idx));
                                   %sum(v1(find(ismember(indexes,indexes(ib(ik),:), 'rows'))));
                                  pointsdistance(ik)=sum(v(idx));
                                  %sum(v(find(ismember(indexes,indexes(ib(ik),:), 'rows'))));
                                  WaitMessage.Send;
                         end
                         WaitMessage.Destroy;
                         BmodeGlobal(sub2ind(size(BmodeGlobal),unique_indexes(:,1),unique_indexes(:,2),unique_indexes(:,3)))=bmode;
                                          %arrayfun(@(x) sum(v1(find(ismember(indexes,indexes(x,:), 'rows')))), ib);
                         pointsDistance(sub2ind(size(BmodeGlobal),unique_indexes(:,1),unique_indexes(:,2),unique_indexes(:,3)))=pointsdistance;
                          %delete(gcp('nocreate')); 
                                          
%% Regular for (uncomment in case there is no Parallel Toolbox, and comment parfor section)
% h_w = waitbar(0,['Compounding of all SubArrays...']);
% 
% for ik=1:size(uniqueMeshUniform,1)
%     idx=find(id==ik);
% %     d=cell2mat(arrayfun(@(ikk) (mesh3D.linGlobal{1}(iX_local(ikk))-Axis(ikk,1))^2+...
% %         (mesh3D.linGlobal{2}(iY_local(ikk))-Axis(ikk,2))^2+...
% %         (mesh3D.linGlobal{3}(iZ_local(ikk))-Axis(ikk,3))^2,...
% %         idx,'UniformOutput', false));
% %     pointsDistance(iY_local(idx(1)), iX_local(idx(1)),iZ_local(idx(1)))=sum(1./d);
% %     BmodeGlobal(iY_local(idx(1)), iX_local(idx(1)),iZ_local(idx(1)))=...
% %          sum(1./d.*Bmode3D(idx));
%     d_around=reshape(cell2mat(arrayfun(@(ikk) cell2mat(arrayfun(@(row) (mesh3D.linGlobal{1}(iX_local(ikk)+p(row,1))-Axis(ikk,1))^2+...
%         (mesh3D.linGlobal{2}(iY_local(ikk)+p(row,2))-Axis(ikk,2))^2+...
%         (mesh3D.linGlobal{3}(iZ_local(ikk)+p(row,3))-Axis(ikk,3))^2,...
%          1:size(p,1),'UniformOutput', false)'), idx,'UniformOutput', false)), [size(p,1) numel(idx)]);
%     % d_around (node number, to pixels contributing)
%     Bmode3D_current=reshape(cell2mat(arrayfun(@(ikk) Bmode3D(ikk).*ones(size(p,1),1), idx, 'UniformOutput', false)),[size(p,1) numel(idx)]);
%     Bmode3D_current=cell2mat(arrayfun(@(row)  sum(1./d_around(row,:).*Bmode3D_current(row,:)), 1:size(p,1),'UniformOutput', false))';
%     indy=cell2mat(arrayfun(@(row)  iY_local(idx(1))+p(row,2), 1:size(p,1),'UniformOutput', false))';
%     indx=cell2mat(arrayfun(@(row)  iX_local(idx(1))+p(row,1), 1:size(p,1),'UniformOutput', false))';
%     indz=cell2mat(arrayfun(@(row)  iZ_local(idx(1))+p(row,3), 1:size(p,1),'UniformOutput', false))';
%     array=BmodeGlobal(sub2ind(size(BmodeGlobal),indy,indx,indz));
%     array(isnan(array))=0;
%     array=Bmode3D_current+array;
%     BmodeGlobal(sub2ind(size(BmodeGlobal),indy,indx,indz))=array;
%     array=pointsDistance(sub2ind(size(BmodeGlobal),indy,indx,indz));
%     array(isnan(array))=0;
%     array=cell2mat(arrayfun(@(row)  sum(1./d_around(row,:)), 1:size(p,1),'UniformOutput', false))'+array;
%     pointsDistance(sub2ind(size(BmodeGlobal),indy,indx,indz))=array;
%     waitbar((ik)/(size(uniqueMeshUniform,1)),h_w);
%  end
% delete(h_w);




end