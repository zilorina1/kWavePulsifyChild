classdef kwaveSimulation
    properties
            DATA_CAST       = 'single';     % set to 'single' or 'gpuArray-single' to speed up computations
            RUN_SIMULATION  = true;         % set to false to reload previous results instead of running simulation
            PLOT_SIM      =   false;
            DATARECAST    =   true;
            
            points_per_wavelength_x=4;        % to create dx in kwavegrid     
            points_per_wavelength_y=4;        % to create dy in kwavegrid     
            points_per_wavelength_z=4;        % to create dz in kwavegrid      
            
            rho0 = 900;                    % [kg/m^3]
            c0 =1500;
            alpha_coeff = 0.75;      % [dB/(MHz^y cm)]
            alpha_power = 1.5;
            BonA = 6;
            
            source_strength = 1e6;          % [Pa]
            tone_burst_freq = f0;     % [Hz]
            tone_burst_cycles = 4;
            
            pml_x_size = 10;                % [grid points]
            pml_y_size = 10;                % [grid points]
            pml_z_size= 10;


    end
    properties (Access = private)
            pPatch                 %PulsifyPatch
    end    
    properties (Dependent)
              dx                       %step in kwave grid in x-direction
              dy                       %step in kwave grid in y-direction   
              dz                       %step in kwave grid in z-direction
              Nx                       %number of elements in the grid in x-direction
              Ny                       %number of elements in the grid in y-direction
              Nz                       %number of elements in the grid in z-direction
              kgrid                    %Kwave grid
              medium                   % phantom with density, speed of sound etc
              KwavePatch               % patch projected on kgrid
              input_signal
              input_args
    end
    
    properties (Constant)
                    gap=0.01;       %[m] in get.krid, the gap after phantom, before patch etc
    end
    
    methods
        function obj = kWaveSimulation_Setup(inOptions)
            % probe - Probe constructor
            % Syntax:  obj = probe(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify probe object
            global plot_debug
            if nargin > 0
                %% parser
                names  = fieldnames(inOptions);
                values = struct2cell(inOptions);
                
                for ii = 1:length(values)
                    if ~isprop(obj, names{ii})
                        error(['Probe: This object does not contain this property: ', names{ii}]);
                    end
                    
                    obj.(names{ii}) = values{ii};
                end
            end
            
            %% assertions
            assert(any(strcmp(obj.apodization, {'rectwin'; 'hanning'; 'taylorwin'})), ...
                'Probe: No apodization with this name')
            assert(any(strcmp(obj.shape, {'flat'; 'pca'; 'pca_normalized'; '2D'; 'custom'})), ...
                'Probe: No shape with this name')
            
            %% Pre-loaded center of element
            if strcmp(obj.shape, 'flat')
                obj.shapeNumber = 1;
                obj.preLoadedElementPos{1} = obj.getElementPositionForFlatPatch;
            elseif strfind(obj.shape, 'pca_normalized')
                load('pca_patch_normalized', 'pca_patch');
                obj.preLoadedElementPos = pca_patch;
            elseif strfind(obj.shape, 'pca')
                load('pca_patch', 'pca_patch');
                obj.preLoadedElementPos = pca_patch;

            elseif strfind(obj.shape, '2D')
                load('twoDpatch', 'twoDpatch')
                obj.preLoadedElementPos{1} = twoDpatch;
            elseif strcmp(obj.shape, 'custom')
                load('custom_shape', 'custom')
                if obj.nElements_x<size(custom.centers,1)
                   indx1=round(size(custom.centers,1)/2)-obj.nElements_x/2;
                   indx2=round(size(custom.centers,1)/2)+obj.nElements_x/2;
                else
                    indx1=1; indx2=size(custom.centers,1);
                end  
                
                if obj.nElements_y<size(custom.centers,2)
                   indy1=round(size(custom.centers,2)/2)-obj.nElements_y/2;
                   indy2=round(size(custom.centers,2)/2)+obj.nElements_y/2;
                else
                    indy1=1; indy2=size(custom.centers,2);
                end    
                obj.preLoadedElementPos = custom.centers(indx1:indx2, indy1:indy2);
                obj.preLoadedEdges = custom.edges(indx1:indx2, indy1:indy2);
                obj.preLoadedElementPos=cellfun(@(x) x-[-0.0385    0.0300    0.0124], obj.preLoadedElementPos, 'UniformOutput', false);
                obj.preLoadedEdges =cellfun(@(x) x-repmat([-0.0385    0.0300    0.0124], [4,1]), obj.preLoadedEdges,'UniformOutput', false);
            end
            
            if plot_debug
                figure(1);
                scatter3(obj.centerOfAperture(1)*1000, obj.centerOfAperture(3)*1000, obj.centerOfAperture(2)*1000, 20.0, 'filled', 'yellow');
            end
            
        end

       function input_args = get.input_args(obj)            
            input_args = {...
                 'PMLInside', false, 'PMLSize', [obj.pml_x_size, obj.pml_y_size, obj.pml_z_size], ...
                'DataCast', obj.DATA_CAST, 'DataRecast', obj.DATARECAST, 'PlotSim',obj.PLOT_SIM};
       end  
        
        function input_signal = get.input_signal(obj)
            global c f0
            % create the input signal using toneBurst 
            input_signal = toneBurst(1/obj.kgrid.dt, obj.tone_burst_freq, obj.tone_burst_cycles);

            % scale the source magnitude by the source_strength divided by the
            % impedance (the source is assigned to the particle velocity)
            input_signal = (obj.source_strength ./ (c * obj.rho0)) .* input_signal;
        end 
        
        
        function dx = get.dx(obj)
            global c f0
            dx=c/(obj.points_per_wavelength_x*f0);            
        end 
        
        
        function dy = get.dy(obj)
            global c f0
            dy=c/(obj.points_per_wavelength_y*f0);            
        end  
        
        
        function dz = get.dz(obj)
            global c f0
            dz=c/(obj.points_per_wavelength_z*f0);            
        end 
        
       function Nx = get.Nx(obj)
            x1=max(max(obj.pPatch.phantom.position(:,1)), max(max(cellfun(@(x) x(1),obj.pPatch.Tx.elementPos))))+obj.gap;
            x2=min(min(obj.pPatch.phantom.position(:,1)), ...
                                                                      min(min(cellfun(@(x) x(1),obj.pPatch.Tx.elementPos))))-obj.gap;
            x_size=abs(x1-x2);
            Nx = round(x_size/obj.dx);     % [grid points]           
       end
       
       function Ny = get.Ny(obj)
            y1=max(max(obj.pPatch.phantom.position(:,2)),max(max(cellfun(@(x) x(2),obj.pPatch.Tx.elementPos))))+obj.gap;
            y2=min(min(obj.pPatch.phantom.position(:,2)),...
                                                                    min(min(cellfun(@(x) x(2),obj.pPatch.Tx.elementPos))))-obj.gap;
            y_size=abs(y1-y2);
            Ny = round(y_size/obj.dx);     % [grid points]
       end 
       
       function Nz = get.Nz(obj)
            z1=max(max(obj.pPatch.phantom.position(:,3)),max(max(cellfun(@(x) x(3),obj.pPatch.Tx.elementPos))))+obj.gap;
            z2=min(min(obj.pPatch.phantom.position(:,3)), min(min(cellfun(@(x) x(3),obj.pPatch.Tx.elementPos))))-obj.gap;
            z_size=abs(z1-z2);
            Nz = round(z_size/obj.dx);     % [grid points]
       end 
       function kgrid=get.kgrid(obj)
            kgrid = kWaveGrid(obj.Nx, obj.dx, obj.Ny, obj.dy, obj.Nz, obj.dz);
            kgrid.makeTime(c);
            %{
             figure(2); hold on; 
             scatter3(obj.phantom.position(:,1).*10^3,obj.phantom.position(:,3).*10^3,obj.phantom.position(:,2).*10^3,50.0,'filled', 'MarkerEdgeColor', 'r'); 
             axis equal tight;
             scatter3(kgrid.x(1:10:end).*10^3,kgrid.z(1:10:end).*10^3,kgrid.y(1:10:end).*10^3,5.0,'filled', 'MarkerFaceColor', 'b'); 
            %}
       end
       function medium=get.medium(obj)
            medium.density=obj.rho0.*ones(obj.kgrid.Nx,obj.kgrid.Ny,obj.kgrid.Nz);
            medium.sound_speed=obj.c0.*ones(obj.kgrid.Nx,obj.kgrid.Ny,obj.kgrid.Nz);

            ind=cellfun(@(x) find(strcmp(x, 'LV, wall')), obj.phantom.segmentationClasses, 'UniformOutput', false);
            ind=cell2mat(ind(~cellfun(@isempty, ind)));
            ind=obj.phantom.segmentationIndex{ind};
            [medium.density, medium.sound_speed] =obj.project_phantom_on_kWavegrid(obj.kgrid,obj.phantom.position,ind,[1080 1585],medium.density, medium.sound_speed);
            
            ind=find(medium.density==1080);
            X=kgrid.x(ind);Y=kgrid.y(ind); Z=kgrid.z(ind); D=medium.density(ind);
            figure(2); hold on; scatter3(X(:).*10^3,Z(:).*10^3,Y(:).*10^3,5.0, D(:), 'filled', 'MarkerEdgeColor', 'r'); 
            axis equal tight;%colorbar;
       end
       function [varargout]=project_phantom_on_kWavegrid(obj, kgrid,data,ind,val,varargin)      
              [meshPhantomUnique]=project_points_on_kWavegrid(obj, kgrid,data,ind);   
              varargout{1}=varargin{1}; varargout{2}=varargin{2};        
              for ik=1:size(meshPhantomUnique,1)
                     varargout{1}(meshPhantomUnique(ik,1), meshPhantomUnique(ik,2), meshPhantomUnique(ik,3))=val(1);
                     varargout{2}(meshPhantomUnique(ik,1), meshPhantomUnique(ik,2), meshPhantomUnique(ik,3))=val(2);
              end
       end
       
       function KwavePatch=get.KwavePatch(obj)
           elementEdges=obj.pPatch.Tx.elementEdges;
           
           AllPos=[]; %positions of all elements
           KWelementPos=zeros(obj.kgrid.Nx,obj.kgrid.Ny,obj.kgrid.Nz);
           KWelementIndexRow=zeros(obj.kgrid.Nx,obj.kgrid.Ny,obj.kgrid.Nz);
           KWelementIndexCol=zeros(obj.kgrid.Nx,obj.kgrid.Ny,obj.kgrid.Nz);
           %% go around each element of the patch
           for i=1:size(elementEdges,1)
              for j=1:size(elementEdges,2)
                    
                    corners=elementEdges{i,j};    
                    P1=corners(1,:);
                    P2=corners(2,:);
                    P3=corners(3,:);
                    P4=corners(4,:);
 
                    elementSizeError=[0 0];
                    gp=obj.kgrid.dx/2; 
                    kol=1;
                    
                    %% define the plane by edges (rectangle of the size of the element)
                    vec1=P1+linspace(-gp,obj.pPatch.Tx.width+gp,10*(obj.pPatch.Tx.width+gp)/obj.kgrid.dx)'*(P2-P1)./norm(P2-P1);
                    vec2=P1+linspace(-gp,obj.pPatch.Tx.width+gp,10*(obj.pPatch.Tx.width+gp)/obj.kgrid.dx)'*(P4-P1)./norm(P4-P1);
                    plane=[vec1; vec2];
                    for jj=2:size(vec2,1)
                       plane=[plane; vec2(jj,:)+linspace(0,norm(P2-P1),5*norm(P2-P1)/obj.kgrid.dx)'*(P2-P1)./norm(P2-P1)];
                    end
                    %scatter3(plane(:,1).*10^3,plane(:,3).*10^3,plane(:,2).*10^3,5.0, 'g','filled');
                     
                    %% project the plane on k-wave grid 
                    [mesh]=project_points_on_kWavegrid(obj,obj.kgrid,plane,1:size(plane,1)); 
                    [mesh_un,ib,id]=unique(mesh(:,1:2), 'rows'); %get only unique values
                    mesh=[mesh_un mesh(ib,3)];
                    Pos=[];
                    for ik=1:size(mesh,1)
                       P=[obj.kgrid.x_vec(mesh(ik,1)) obj.kgrid.y_vec(mesh(ik,2)) obj.kgrid.z_vec(mesh(ik,3))];
                       % check if the point on the grid is already taken by
                       % other eleement
                       if ~isempty(AllPos)
                                ExistP=find(ismember(AllPos(:,1:2),P(1:2),'rows')~=0);   
                       else
                                ExistP=[];
                       end  
                       
                       if isempty(ExistP) %if the point is not yet on the grid, we add it
                             Pos=[Pos; P];
                             KWelementPos(mesh(ik,1), mesh(ik,2), mesh(ik,3))=1;
                             KWelementIndexRow(mesh(ik,1), mesh(ik,2), mesh(ik,3))=i;
                             KWelementIndexCol(mesh(ik,1), mesh(ik,2), mesh(ik,3))=j;
                       end
                    end
                    
                    elementPos{i,j}=Pos; %grid points belonging to the current element
                     
                    %% find the element center
                    center=[mean(Pos(:,1)) mean(Pos(:,2)) mean(Pos(:,3))];
                    [~,t]=min(pdist2(Pos,center));
                    elementPosCenters{i,j}=Pos(t,:); %grid point corresponsing to the center of the element
                    
                    % for improving the precision later
%                     elementSize{i,j}=[max(Pos(:,1))-min(Pos(:,1)) max(Pos(:,2))-min(Pos(:,2))];
%                     elementSizeError=abs(elementSize{i,j}-obj.Tx.width)<kgrid.dx;
%                     gap=kgrid.dx/2*kol; kol=kol+1;    
                    
                    AllPos=[AllPos; Pos]; %add new grid points
              end
           end
            
                     
           %% plot whatever we have defined
           figure(2); hold on;
           for i=1:size(elementPos,1)
              for j=1:size(elementPos,2)
                   E=elementPos{i,j};
                   X = E(:,1)*1000;
                   Y = E(:,2)*1000;
                   Z = E(:,3)*1000;
                   cc=rand(1,3);
                   scatter3(X,Z,Y, 5.0, 'filled', 'MarkerEdgeColor', cc);
                   scatter3(elementPosCenters{i,j}(1).*10^3,elementPosCenters{i,j}(3).*10^3,elementPosCenters{i,j}(2).*10^3, 10.0, 'filled', 'MarkerEdgeColor', 'k');
              end
           end             
           view(3); axis equal tight;
           
%            figure(3);
%            ind=find( KWelementPos==1);
%            X=kgrid.x(ind);Y=kgrid.y(ind); Z=kgrid.z(ind); D= KWelementPos(ind);
%            scatter3(X(:).*10^3,Z(:).*10^3,Y(:).*10^3,5.0, D(:), 'filled', 'MarkerEdgeColor', 'r'); axis equal tight;
%            
           
           %% SubArrays:
           cut = elementPosCenters(obj.pPatch.Tx.subArraySize/2:end-obj.pPatch.Tx.subArraySize/2+1,obj.pPatch.Tx.subArraySize/2:end-obj.pPatch.Tx.subArraySize/2+1);
           cutMat = cell2mat(cut);           
           %% 2D filter obtains the average position of 4 neighboring elements
           subArrayPos = filter2([1 0 0 1; 1 0 0 1], cutMat, 'valid')/4;
           subArrayPos = mat2cell(subArrayPos, ones(size(subArrayPos,1),1), 3*ones(1,size(subArrayPos,2)/3));
           figure(2); hold on;
           ind=obj.pPatch.Tx.subArraySize/2:size(elementPosCenters,1)-obj.pPatch.Tx.subArraySize/2+1;
           for i=1:size(subArrayPos ,1)
              for j=1:size(subArrayPos,2)
                  [mesh]=project_points_on_kWavegrid(obj, obj.kgrid,subArrayPos{i,j},1:size(subArrayPos{i,j},1)); 
                  subArrayPosK{i,j}=[obj.kgrid.x_vec(mesh(1)) obj.kgrid.y_vec(mesh(2)) obj.kgrid.z_vec(mesh(3))];
                  subArrayElementsPosIndex{i,j}=[ind(i)-obj.pPatch.Tx.subArraySize/2+1:ind(i)+obj.pPatch.Tx.subArraySize/2;...
                      ind(j)-obj.pPatch.Tx.subArraySize/2+1:ind(j)+obj.pPatch.Tx.subArraySize/2];                  
                  %scatter3(subArrayPosK{i,j}(1).*10^3,subArrayPosK{i,j}(3).*10^3,subArrayPosK{i,j}(2).*10^3, 50.0, 'filled', 'MarkerEdgeColor', 'r','MarkerFaceColor', 'k');
              end    
           end        
           
            %% SubArray Curvature           
            centerSA = subArrayPosK;          
            %% shift
            centerSA_shiftRight = circshift(centerSA, 1, 2);
            centerSA_shiftDown  = circshift(centerSA, 1, 1);           
            %% difference to between the original and shifted matrices gives the argument to the atan function
            diffAz = cellfun(@(X,Y) X-Y, centerSA_shiftRight, centerSA, 'UniformOutput', false);
            diffAz(:,1) = diffAz(:,2);
            diffEl = cellfun(@(X,Y) X-Y, centerSA_shiftDown , centerSA, 'UniformOutput', false);
            diffEl(1,:) = diffEl(2,:);
            
            azRot = cellfun(@(x) atan(x(3)/x(1)), diffAz);
            elRot = cellfun(@(x) atan(x(3)/x(2)), diffEl);
            
            subArrayCurvature = num2cell(azRot + 1j*elRot);
            subArrayCurvature = cellfun(@(x) [real(x), imag(x)], subArrayCurvature, 'UniformOutput', false);


           
           KwavePatch.elementPosCenters=elementPosCenters;
           KwavePatch.elementPos=elementPos;
           KwavePatch.subArrayPosCenters=subArrayPosK;
           KwavePatch.subArrayElementsPosIndex=subArrayElementsPosIndex;
           KwavePatch.KWelementPos=KWelementPos;
           KwavePatch.KWelementIndexRow=KWelementIndexRow;
           KwavePatch.KWelementIndexCol=KWelementIndexCol;
           KwavePatch.subArrayCurvature=subArrayCurvature;
           
           clear elementPosCenters elementPos subArrayPosK subArrayElementsPosIndex KWelementPos KWelementIndex subArrayCurvature

       end     
    end
end