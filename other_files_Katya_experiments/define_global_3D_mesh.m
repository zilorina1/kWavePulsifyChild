function [mesh3D, BmodeGlobal, pointsDistance]=define_global_3D_mesh(Axis,step)
 gap=0.0001;
 mesh3D.globalMin(1)=min(Axis(:,1))-gap;
 mesh3D.globalMax(1)=max(Axis(:,1))+gap;
 mesh3D.globalMin(2)=min(Axis(:,2))-gap;
 mesh3D.globalMax(2)=max(Axis(:,2))+gap;
 mesh3D.globalMin(3)=min(Axis(:,3))-gap;
 mesh3D.globalMax(3)=max(Axis(:,3))+gap;
%  
%  mesh3D.linGlobal{1}=gpuArray(mesh3D.globalMin(1):step:mesh3D.globalMax(1));
%  mesh3D.linGlobal{2}=gpuArray(mesh3D.globalMin(2):step:mesh3D.globalMax(2));
%  mesh3D.linGlobal{3}=gpuArray(mesh3D.globalMin(3):step:mesh3D.globalMax(3));
 mesh3D.linGlobal{1}=mesh3D.globalMin(1):step:mesh3D.globalMax(1);
 mesh3D.linGlobal{2}=mesh3D.globalMin(2):step:mesh3D.globalMax(2);
 mesh3D.linGlobal{3}=mesh3D.globalMin(3):step:mesh3D.globalMax(3);

 mesh3D.stepGlobal(1)=step;
 mesh3D.stepGlobal(2)=step;
 mesh3D.stepGlobal(3)=step;
 [X_global_mesh, Y_global_mesh, Z_global_mesh]=meshgrid(mesh3D.linGlobal{1},mesh3D.linGlobal{2},mesh3D.linGlobal{3});
 mesh3D.X_global_mesh=X_global_mesh;
 mesh3D.Y_global_mesh=Y_global_mesh;
 mesh3D.Z_global_mesh=Z_global_mesh;
%  BmodeGlobal=gpuArray(NaN(size(X_global_mesh)));
%  pointsDistance=gpuArray(NaN(size(X_global_mesh)));
 BmodeGlobal=NaN(size(X_global_mesh));
 pointsDistance=NaN(size(X_global_mesh));

end