function [position]=rotate_2Dimage(position, angle, center) %added by Kate to rotate and translate phantom for curved arrays
%debug plots to check the transformation of coordinates to
%be okay
                %{
                figure(1); hold on;
                 plot3(position(:,1), position(:,3), position(:,2), 'r', 'Linewidth', 4);
                scatter3( subArray_center(:,1), subArray_center(:,3),  subArray_center(:,2),100.0, 'b','filled');
%}
phantom_centered_position=position-ones(size(position,1),1)*mean(position,1); %Centering coordinates
subArray_center=center-mean(position,1); %Centering coordinates
E=[angle(2) -angle(1) 0]; %Euler angles for X,Y,Z-axis rotations
Rx=[1 0  0;0  cos(E(1)) -sin(E(1)); 0  sin(E(1)) cos(E(1))]; %X-Axis rotation
Ry=[cos(E(2)) 0  sin(E(2)); 0  1  0; -sin(E(2)) 0 cos(E(2))]; %Y-axis rotation
Rz=[cos(E(3))  -sin(E(3)) 0; sin(E(3))  cos(E(3))  0; 0  0  1]; %Z-axis rotation
R=Rx*Ry*Rz; %Rotation matrix
subArray_center=[R*subArray_center']'+mean(position,1);%
position=[R*phantom_centered_position']'+ones(size(position,1),1)*mean(position,1);

position=position-(subArray_center-center);
%position(:,2)=position(:,2)-(subArray_center(2)-center(2));
end
