function [h, t] = pulsify_calc_hh(probe_tx, probe_rx, phantom_position, phantom_amp, indexTx, indexRx)

global fs

if nargin == 4
    indexTx = 1;
    indexRx = 1;
end

centerTx = probe_tx.center;
centerRx = probe_rx.center;

[h1, t1] = pulsify_calc_h(probe_tx, phantom_position - centerTx{indexTx});
[h2, t2] = pulsify_calc_h(probe_rx, phantom_position - centerRx{indexRx});

h = conv(h1, h2)*phantom_amp;
t = (0:(length(h)-1))/fs + round((t1(1)+t2(1))*fs)/fs; 



