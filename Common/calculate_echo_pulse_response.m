function [V, t] = calculate_echo_pulse_response(TX, RX, PH)

global fs

for indexRx_x = 1:RX.nElements_x
    for indexRx_y = 1:RX.nElements_y
        for indexTx_x = 1:TX.nElements_x
            for indexTx_y = 1:TX.nElements_y
                
                %% v is indexed with a linear index
                index = sub2ind([ TX.nElements_x TX.nElements_y RX.nElements_x RX.nElements_y], indexTx_y, indexTx_x, indexRx_y, indexRx_x );
                
                indexTx = sub2ind([TX.nElements_x TX.nElements_y], indexTx_y, indexTx_x );
                indexRx = sub2ind([RX.nElements_x RX.nElements_y], indexRx_y, indexRx_x );
                
                for ii = 1:length(PH.amplitude)
                    
                    %% calculate two-way impulse response
                    [hh, th] = pulsify_calc_hh(TX, RX, PH.position(ii,:), PH.amplitude(ii), indexTx, indexRx);
                    
                    % column vector
                    vii = (conv(conv(TX.impulseResponse, hh), RX.impulseResponse)*1e-32).';
                   
                    t1   = round(th(1)*fs);
                    tEnd = (length(vii)-1) + t1;
                    
                    %% aligning signals from different point sources
                    % Allocaing zeros for a new TX-RX pair
                    if index == 1 && ii == 1
                        V = zeros(length(vii),1);
                        tMin = t1;
                        tMax = tEnd;
                    elseif index > 1 && ii == 1
                        V = [V zeros(size(V,1),1)];
                    end
                    
                    %  Aligning the signals from different scatterers for
                    %  the same TX-RX pair
                    if tMin <= t1
                        endPad1 = max(tEnd-tMax, 0);
                        endPad2 = max(tMax-tEnd, 0);
                        iniPad = t1-tMin;
                        V = [V(:,1:index); zeros(endPad1, size(V,2))];
                        V  = V + [zeros(size(V,1), size(V,2)-1) [(zeros(iniPad,1)); vii; zeros(endPad2, 1)] ];
                    else
                        iniPad  = tMin-t1;
                        iniPad2 = max(tEnd-tMax, 0);
                        endPad  = max(tMax-tEnd, 0);
                        V = [zeros(iniPad, size(V,2)); V];
                        V  = V + [zeros(size(V,1), size(V,2)-1) [zeros(iniPad2,1); vii; zeros(endPad, 1)] ];
                    end
                    
                    tMin = min(tMin, round(t1));
                    tMax = max(tMax, round(tEnd));
                    
                end
            end
        end
    end
end

t = (tMin:tMax)'/fs;