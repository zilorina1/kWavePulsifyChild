function [delay_line] = computeRxDelayLinePost(tx_focus, img_ranges, rx_angles, rx_apex, crystal_pos, c0, tx_angle)
% computeRxDelayLine creates the two-way beamforming delays for focused, diverging or plane waves.
% [DELAY_LINE] = computeRxDelayLine(TX_FOCUS, RX_RANGES, RX_ANGLES, RX_APEX, ELEMENT_POS, C0, DW_SLIDING, TX_ANGLE)
%
% Inputs:  TX_FOCUS coordinates of the transmit focal point [x y z] [m]
%          RX_RANGES array with depths of the receive points [m]
%          RX_ANGLES receive angles [Angle_az; Angle_el] [deg]
%          RX_APEX coordinates of the receive apex [x y z] [m]
%          ELEMENT_POS coordinates of the elements [N_az N_el x|y|z] [m]
%          C0 speed of sound [m/s]
%          DWSlidingTx: TRUE for a DW sequence sliding the transmit verticies, FALSE tor tilting approach
%          TX_ANGLE: angle of the transmit beam [Angle_az; Angle_el] [deg]
%
% Outputs: DELAY_LINE: two-way delays [n_ranges n_channels] [s]
%
% Pedro Santos (KUL, 2016)


crystal_pos = reshape(crystal_pos, [], 3); % [nAz*nEl x|y|z]
crystal_pos = shiftdim(crystal_pos', -1);   % [1  x|y|z  N]
    
if tx_focus(1, 3) > 0 % focused
%     if ~all(rx_apex ==0)
%         warndlg('rx_apex has not been tested yet')
%     end
        
    rxFoc      = PP_Utils.beam2cart(rx_angles(1), rx_angles(2), img_ranges); % coordinates of the rx line to reconstruct
    rxOrig2Foc = bsxfun(@minus, rxFoc, rx_apex);
    rxOrig2Foc = sqrt(sum(rxOrig2Foc .* rxOrig2Foc, 2)); % distance from rx origin to rx line positions
    
    elem2Foc   = bsxfun(@minus, rxFoc, crystal_pos);
    elem2Foc   = sqrt(sum(elem2Foc .* elem2Foc, 2)); % (faster than elem2Foc.^2)

    delay_line = bsxfun(@plus, rxOrig2Foc,  elem2Foc(:,1,:)) / c0;  % d = sqrt(|Orig2Foc|^2 + |Elem2Foc|^2)
    delay_line  = permute(delay_line, [1 3 2]); % remove empty dim
    
else
    
    % RX Delays
    rxFoc      = PP_Utils.beam2cart(rx_angles(1), rx_angles(2), img_ranges); % coordinates of the rx line to reconstruct
    elem2Foc   = bsxfun(@minus, rxFoc, crystal_pos);
    elem2Foc   = sqrt(sum(elem2Foc .* elem2Foc, 2));
    delay_line = elem2Foc(:,1,:);  % d = sqrt(|Orig2Foc|^2 + |Elem2Foc|^2)
    rxDelay  = permute(delay_line, [1 3 2]); % remove empty dim
    
    
    % TX Delays
    if tx_focus(1, 3) < 0 % DW
        rx2VFoc = bsxfun(@minus, rxFoc, tx_focus);
        rx2VFoc = sqrt(sum(rx2VFoc .* rx2VFoc, 2));     % distance from rx origin to rx line positions
        r = norm(tx_focus);  % faster
        %       r = sqrt(tx_focus(1).^2 + tx_focus(2).^2 + tx_focus(3).^2);
        txDelay = rx2VFoc - r;
        
    else % PW
        txDelay = sqrt(sum(rxFoc .* rxFoc, 2) .* abs(cosd(tx_angle(1) - rx_angles(1)))); % projection of RxPosition into TxAngle
    end
    
    % Two way delay computation
    delay_line = bsxfun(@plus, txDelay, rxDelay) ./ c0;
    

end

end
