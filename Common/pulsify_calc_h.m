function [h, t] = pulsify_calc_h(probe, phantom_position)

global c fs

width    = probe.width;
heigth   = probe.height;
nSub     = probe.nSub;

widthSubArray  = width/nSub;
heigthSubArray = heigth/nSub;

%% center of each sub-division of the aperture
subCenter_x = ((-width/2+width/(2*nSub)) :widthSubArray :(width/2-width/(2*nSub)));
subCenter_y = (-heigth/2+heigth/(2*nSub)):heigthSubArray:(heigth/2-heigth/(2*nSub));

subCenter   = [reshape(repmat(subCenter_x, nSub, 1), nSub^2, 1), reshape(repmat(subCenter_y', 1, nSub), nSub^2, 1), zeros(nSub^2,1)];

% Phantom is offset by center of each sub-division
phantom_position = repmat(phantom_position, nSub^2, 1) - subCenter;

distance      = realsqrt(sum(realpow(phantom_position.',2)))';
position_norm = bsxfun(@rdivide, phantom_position,  distance);

deltaT1 = min(heigthSubArray*position_norm(:,2), widthSubArray*position_norm(:,1))/c;
deltaT2 = max(heigthSubArray*position_norm(:,2), widthSubArray*position_norm(:,1))/c;

T_in = zeros(nSub^2, 4);

%% x-axis indexes of trapezium
T_in(:,1) = distance/c - (deltaT1+deltaT2)/2;
T_in(:,2) = T_in(:,1) + deltaT1;
T_in(:,3) = T_in(:,1) + deltaT2;
T_in(:,4) = T_in(:,1) + deltaT1 + deltaT2;

T_in = sort(T_in,2);

%% top of trapezium
A = heigthSubArray*widthSubArray./(2*pi*distance);
trapz_h = 2*A./(T_in(:,3)-T_in(:,2) + T_in(:,4)-T_in(:,1));

%% time variables
t = (round(min(min(T_in))*fs):1:round(max(max(T_in))*fs))'/fs;
T_out = repmat(t', nSub^2, 1);

[M, N] = size(T_out);

%% auxiliary variables
HH    = repmat(trapz_h, 1, N);
T1    = repmat(T_in(:,1), 1, N);
T3    = repmat(T_in(:,3), 1, N);

% output
H_out = zeros(M,N);

%% Top of trapezium
indTopOfTrapezium = bsxfun(@le, T_in(:,2), t') &  bsxfun(@ge, T_in(:,3), t'); 
H_out(indTopOfTrapezium) = HH(indTopOfTrapezium);

%% rise and fall of trapezium
indRiseTrapezium  =  bsxfun(@lt, T_in(:,1), t') &  bsxfun(@gt, T_in(:,2), t'); 
indFallTrapezium  =  bsxfun(@lt, T_in(:,3), t') &  bsxfun(@ge, T_in(:,4), t'); 

H_aux = repmat(trapz_h./(T_in(:,2) - T_in(:,1)), 1, N);
H_out(indRiseTrapezium) = H_aux(indRiseTrapezium).*(T_out(indRiseTrapezium) - T1(indRiseTrapezium));

H_aux = repmat(trapz_h./(T_in(:,4) - T_in(:,3)), 1, size(H_out, 2));
H_out(indFallTrapezium) = HH(indFallTrapezium) - H_aux(indFallTrapezium).*(T_out(indFallTrapezium) - T3(indFallTrapezium));

% out
h = sum(H_out);



