function [x_circle, y_circle] = circle(x,y,r)
% hold on
th = 0:pi/100e3:2*pi;
x_circle = r * cos(th) + x;
y_circle = r * sin(th) + y;
% plot(x_circle, y_circle);
% hold off
end
