function structOut = parser(structDefault, structIn) 
% parser - Compares two structs and outputs another struct. The output struct  
% has the same fields as the default struct. In the comparison, the default 
% struct always takes precedence, in the sense that all missing fields on the input struct 
% assume the value in the default struct. 
% 
% Syntax:  structOut = parser(structDefault, structIn) 
% 
% Inputs: 
%    structDefault - default struct 
%    structIn - input struct 
% 
% Outputs: 
%    structOut - has the same fields as the default struct 
% 
% Example:  
%    struct_default = struct('a', 1, 'b', 2, 'c', 3); 
%    structOut = parser(struct_default, struct('a', -1)) 
%    structOut =  
%   struct with fields: 
%     a: -1 
%     b: 2 
%     c: 3 
 
namesDefault  = fieldnames(structDefault); 
structOut = structDefault; 
 
for ii = 1:length(namesDefault) 
     
    if isfield(structIn, namesDefault{ii})
        % field must be the same type
        class1 = class(structDefault.(namesDefault{ii})); 
        class2 = class(structIn.(namesDefault{ii})); 
        if strcmp(class1, class2) 
            structOut.(namesDefault{ii}) =  structIn.(namesDefault{ii});
        elseif strcmp(class1, 'logical') && strcmp(class2, 'double') && (structIn.(namesDefault{ii}) == 1 || structIn.(namesDefault{ii}) == 0)
            % exception is made for flags, where 1 and 0 (doubles) are
            % converted to logical 
            structOut.(namesDefault{ii}) =  logical(structIn.(namesDefault{ii}));
        else 
            error(['parser. Input structure must have the same type of variable as default structure: ', namesDefault{ii}]); 
        end 
    end 
end 