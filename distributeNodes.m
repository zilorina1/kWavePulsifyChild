function []=distributeNodes(active_path, current_path, N_nodes, choice)
softwareOpt=PP_scanSequencer();
load([active_path 'data_for_kWave.mat'], 'softwareOpt');

disp(['############################# Distribute the tasks for ' choice  ' ##############################################################'])
if strcmp(choice, 'Subarrays')
  N_tasks=size(softwareOpt.shotIndex,1);
else
  N_tasks=softwareOpt.nTxAz*softwareOpt.nTxEl;
end  
  
shotsPerNode=[];  
N_nodes=str2num(N_nodes);
shotsPerNode(1:N_nodes)=fix(N_tasks/N_nodes);
remainedShots=N_tasks-shotsPerNode(1)*N_nodes;
if remainedShots~=0
    for i=1:remainedShots
      shotsPerNode(i)=shotsPerNode(i)+1;
    end    
end
  
val=1;
for i=1:N_nodes
   taskPerNode{i}=val:shotsPerNode(i)+val-1;
   val=shotsPerNode(i)+val;
end
save([active_path 'nodesDistribution.mat'], 'taskPerNode', 'choice');
disp('############################# Saved tasks distribution between the nodes ####################################################')

disp('############################# Creating job files ####################################################')
filename = 'jobMain2D.pbs';
fid = fopen(fullfile(current_path, filename),'r') ;
f=fread(fid,'*char')';
fclose(fid);
for i=1:N_nodes
    temp = strrep(f,'./mainKatyaCurved2D "$savedir" "$kWaveDir" "$PBS_O_WORKDIR" 1 ' ,['./mainKatyaCurved2D "$savedir" "$kWaveDir" "$PBS_O_WORKDIR" ' num2str(i) ' ']);
    temp1 = strrep(temp,'#PBS -o output3D.txt',['#PBS -o output3D_' num2str(i) '.txt']);
    fullfile(current_path,['jobMain2D_' num2str(i) '.pbs'])
    fid  = fopen(fullfile(current_path,['jobMain2D_' num2str(i) '.pbs']),'w');
    fprintf(fid,'%s',temp1);
    fclose(fid);
end
disp('############################# Created all job files ####################################################')
end