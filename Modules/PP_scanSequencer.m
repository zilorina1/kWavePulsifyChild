classdef PP_scanSequencer < dynamicprops
    % PP_scanSequencer -- this class contains the programming of the patch for
    % transmission
    
    properties
        scanMode           = 'zero'                                   % determines the indexes of the pre-programmed readings of the patch. 
                                                                      % Options are zero, custom, single, full, azimuthSlice, elevationSlice, etc.
        initialPosition    = [1 1]                                    % 2x1 integers. starting position for the scan. For scanMode = single, the only position to shoot
        azimuthDelta       = 1                                        % integer, 'jump' in the scan in the azimuth direction
        elevationDelta     = 1                                        % integer, 'jump' in the scan in teh elevation direction
        shotIndex          = []                                       % Nx2 integers. indexes of positions of where to shoot on the patch
        shotIndexRx        = []                                       % Nx2 integers. indexes of positions of where to shoot on the patch -- RX side
        hadamard           = false                                    % boolean
        txFocalDepth       = 0.05                                     % depth of focus
        nTxAz              = 179                                      % integer, number of transmits on azimuth direction
        nTxEl              = 1                                        % integer, number of transmits on elevation direction
        txEvents           = []                                       % struct with focusing information: tx angles, focus point and rx angles
        openingAngle       = [178 0]                                  % angle sweep on az. and elevation
        SectorTilt         = [0 0]                                    % center angle for the sweep
        nMLAs              = [1 1];                                   % multiline aquisition
        maxDepth           = []                                       % ignore depth beyond maxDepth
        coleAmpThreshold   = 0                                        % specific for COLE algorithm
        maxIndexOfsubArray = 151                                      
    end
    
    properties (Access = protected)
        is_initialized          = 0;    % Used by the set methods
        tx_angles_az
        tx_angles_el
        MLA_spread_az
        MLA_spread_el
    end
    
    properties (Dependent = true)
        ImageSector
        nScanLines
    end
    
    methods
        function obj = PP_scanSequencer(inOptions)
            % transOptions - software options contructor
            % Syntax:  obj = transOptions(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify transOptions object
            if nargin > 0
                %% parser
                names  = fieldnames(inOptions);
                values = struct2cell(inOptions);
                
                for ii = 1:length(values)
                    if ~isprop(obj, names{ii})
                        error(['ScanSequencer: This object does not contain this property: ', names{ii}]);
                    end
                    
                    obj.(names{ii}) = values{ii};
                end
            end
                       
            obj = validateSequence(obj);
            obj = setupShotPosition(obj);
            obj = setupTxAngles(obj);
            obj = setupRxAngles(obj);
            
            % Adapt sequence
            obj.is_initialized = 1;
            
        end % END scanSequencer
        
        
        function plot_sequence(obj, fig_h)
            % plot_sequence - plot focus points for TX and RX events
            % Syntax:  plot_sequence(obj, fig_h)
            
            if ~exist('fig_h', 'var') || isempty(fig_h), fig_h = figure; end
            
            rx_max_depth = .06; % maximum depth for Rx lines % TODO: take if from maximum scanning depth
            n_rx_beams   = prod(1);
            if isfield(obj, 'nMLTs') || isprop(obj, 'nMLTs')
                n_tx_beams = prod(obj.nMLTs);
            else
                n_tx_beams = 1;
            end
            
            figure(fig_h), hold all
            axis auto, axis equal tight, set(gca, 'zdir', 'reverse')
            co = get(0, 'defaultAxesColorOrder'); % take color order to plot Tx/Rx in the same color for a given txEvent
            nco = size(co, 1);
            xlabel('Azimuth [mm]'), ylabel('Elevation [mm]'), zlabel('Range [mm]')
            for tx_i = 1:numel(obj.txEvents)
                
                % Transmit focus
                tx_focus = obj.txEvents(tx_i).txFocus * 1000';
                plot3([zeros(1, n_tx_beams); tx_focus(:,1)'], [zeros(1, n_tx_beams); tx_focus(:,2)'], [zeros(1, n_tx_beams); tx_focus(:,3)'], '-x', 'color', co(mod(tx_i-1, nco)+1, :))
                %                 scatter(tx_focus(:,1)', tx_focus(:,2)', 40, repmat(co(tx_i, :), size(tx_focus, 1), 1), 'filled') % top view
                
                % Receive focus
                rx_angles_event = obj.txEvents(tx_i).rxAngles';
                rx_focus        = PP_Utils.beam2cart(rx_angles_event(:,1), rx_angles_event(:,2), rx_max_depth*1000);
                plot3([zeros(1, n_rx_beams); rx_focus(:,1)'], [zeros(1, n_rx_beams); rx_focus(:,2)'], [zeros(1, n_rx_beams); rx_focus(:,3)'], '--o', 'color', co(mod(tx_i-1, nco)+1, :))
                
                if any(tx_focus(:,2))
                    view([0 90]), axis equal tight
                else
                    view([0 0]), axis equal tight
                end
                title(sprintf('Tx %g/%g', tx_i, numel(obj.txEvents)))
                pause(0.1)
            end
            hold off
            legend('Tx beam', 'Rx beam', 'location', 'NorthEast')
            
            
            
        end % END plot
    end
    
    methods (Access = protected)
        function obj = recreateTxRxSequence(obj)
            obj = setupTxAngles(obj);
            obj = setupRxAngles(obj);
            obj = setupCompounding(obj);
        end
        
        function obj = recreateRxSequence(obj)
            obj = setupRxAngles(obj);
        end
        
        
        %% Main sequencer
        function obj = setupTxAngles(obj)
            % setupTxAngles - sets up focus angles for TX event
            %
            % Syntax: setupTxAngles(obj, HDP_meta)
            
            obj.tx_angles_az  = linspace(-obj.ImageSector(1)/2, obj.ImageSector(1)/2, obj.nTxAz) + obj.SectorTilt(1);
            obj.tx_angles_el  = linspace(-obj.ImageSector(2)/2, obj.ImageSector(2)/2, obj.nTxEl) + obj.SectorTilt(2);
            
            %% Construct Tx events
            for tx_el = 1 : obj.nTxEl
                for tx_az = 1 : obj.nTxAz
                    
                    tx_i = (tx_el-1)*obj.nTxAz + tx_az; % current Tx event number
                    
                    % Tx Angle
                    obj.txEvents(tx_i).txAngle = [obj.tx_angles_az(tx_az); obj.tx_angles_el(tx_el)];
                    
                    % Tx Focus
                    
                    obj.txEvents(tx_i).txFocus = PP_Utils.beam2cart(obj.tx_angles_az(tx_az), obj.tx_angles_el(tx_el), obj.txFocalDepth);
                    obj.txEvents(tx_i).txAngle = [obj.tx_angles_az(tx_az); obj.tx_angles_el(tx_el)];
                    
                end %END tx_az
            end % END tx_el
            obj.txEvents(tx_i+1:end) = []; % make sure we delete remaining events
        end % END setupTxAngles
        
        function obj = setupRxAngles(obj)
            % setupRxAngles - sets up focus angles for RX event
            %
            % Syntax: setupRxAngles(obj)
            
            delta_Tx(1)        = PP_Utils.iif(numel(obj.tx_angles_az) > 1, mean(diff(obj.tx_angles_az)), obj.ImageSector(1));
            delta_Tx(2)        = PP_Utils.iif(numel(obj.tx_angles_el) > 1, mean(diff(obj.tx_angles_el)), obj.ImageSector(2));
            delta_Rx           = delta_Tx ./ obj.nMLAs;
            obj.MLA_spread_az  = linspace(-(delta_Tx(1)/2 - delta_Rx(1)/2), delta_Tx(1)/2 - delta_Rx(1)/2, obj.nMLAs(1));
            obj.MLA_spread_el  = linspace(-(delta_Tx(2)/2 - delta_Rx(2)/2), delta_Tx(2)/2 - delta_Rx(2)/2, obj.nMLAs(2));
            
            
            %% Setup receive events
            for tx_el = 1 : obj.nTxEl
                for tx_az = 1 : obj.nTxAz
                    tx_i = (tx_el-1)*obj.nTxAz + tx_az;              % current Tx event number
                    for mla_el = 1:obj.nMLAs(2)
                        for mla_az = 1:obj.nMLAs(1)
                            
                            rx_i = (mla_el-1)*obj.nMLAs(1) + mla_az; % current Rx line number
                            
                            obj.txEvents(tx_i).rxAngles(:, rx_i) = obj.txEvents(tx_i).txAngle + [obj.MLA_spread_az(mla_az); obj.MLA_spread_el(mla_el)];
                        end % END mla_az
                    end % END mla_el
                    obj.txEvents(tx_i).rxAngles(:, rx_i+1:end) = []; % make sure we delete remaining events
                end % END tx_az
            end % END tx_el
        end % END setupRxAngles
        
        function obj = validateSequence(obj)
            % validateSequence - sanity check to software options
            %
            % Syntax:  [output1,output2] = function_name(input1,input2,input3)
            
            assert(~(obj.nTxEl > 1 && (obj.nScanLines(2) == 1 || numel(obj.ImageSector) == 1 || obj.ImageSector(2) == 0)),...
                'PP_scanSequeces: For elevation scanning, the following is required:\n\t\t* more than 1 Rx elevation line\n\t\t* positive opening angle in elevation.\n')
            assert(any(strcmp(obj.scanMode, {'full', 'zero', 'single', 'azimuthSlice', 'elevationSlice', 'custom'})), 'Error in PP_scanSequencer\nUnrecognized scanMode %s', obj.scanMode)
            assert(~(strcmp(obj.scanMode, 'custom') && isempty(obj.shotIndex)), 'Error in PP_scanSequencer\nFor custom scanMode, provide shotIndex %s', obj.scanMode)
            
            if numel(obj.ImageSector) == 1
                obj.ImageSector = [obj.ImageSector 0]; % elevation opening angle null (2D scan)
            end
            
            if obj.nTxEl == 1 && obj.ImageSector(2) > 0
                disp('*** WARNING: Imaging opening angle in elevation set to zero!')
                obj.ImageSector(2) = 0;
            end
        end
        
        %%
        function obj = setupShotPosition(obj)
            % setupShotPosition - sets up shooting positions on teh patch.
            % Thre are some preprogrammed scans. They include 'full',
            % 'azimuthSlice', 'elevationSlice' , 'zero', 'single' and
            % 'custom'
            %
            % shotIndex is a matrix Nx2 of integers (row, col) that determines the
            % position of each shot on the patch. Position (1,1)
            % corresponds to the top left corner of the patch. Position
            % (76,76) is the center. Position (1,151) corresponds to the
            % top right corner.
            
            % shotIndex oncly activates a sub-array. The combination of
            % these indexes plus the shape of the patch gives the shot
            % position in 3D space
            %
            % Syntax: setupShotPosition(obj)
            
            %% pre-programmed scan modes
            switch obj.scanMode
                case 'full'
                    X = obj.initialPosition(1):obj.elevationDelta:obj.maxIndexOfsubArray;
                    Y = obj.initialPosition(2):obj.azimuthDelta:obj.maxIndexOfsubArray;
                case 'zero'
                    X = 76;
                    Y = 76;
                case 'single'
                    X = obj.initialPosition(1);
                    Y = obj.initialPosition(2);
                case 'azimuthSlice'
                    X = obj.initialPosition(1);
                    Y = obj.initialPosition(2):obj.azimuthDelta:obj.maxIndexOfsubArray;
                case 'elevationSlice'
                    X = obj.initialPosition(1):obj.elevationDelta:obj.maxIndexOfsubArray;
                    Y = obj.initialPosition(2);
                case 'custom'
                    X = unique((obj.shotIndex(:,1)));
                    Y = unique((obj.shotIndex(:,2)));
            end
            
             [XX, YY] = meshgrid(X,Y);
             obj.shotIndex = [XX(:) YY(:)];
             obj.shotIndexRx = obj.shotIndex;
            
        end
    end
    
    %% set/get methods
    methods
        
        function set.nTxAz(obj, val_in)
            assert(isreal(val_in) && isscalar(val_in), 'Value must be a real scalar.')
            obj.nTxAz = val_in;
            if obj.is_initialized
                obj = recreateTxRxSequence(obj);
            end
        end
        function set.nTxEl(obj, val_in)
            assert(isreal(val_in) && isscalar(val_in), 'Value must be a real scalar.')
            obj.nTxEl = val_in;
            if obj.is_initialized
                obj = recreateTxRxSequence(obj);
            end
        end
        function set.nMLAs(obj, val_in)
            assert(isreal(val_in) && (numel(val_in)==1 || numel(val_in)==2) , 'Value must be a real scalar (Az) or 2-element vector (Az,El).')
            if numel(val_in) == 1, val_in = [val_in 1]; end
            obj.nMLAs = val_in;
            if obj.is_initialized
                obj = recreateRxSequence(obj);
            end
        end
        function set.openingAngle(obj, val_in)
            assert(isreal(val_in) && (numel(val_in)==1 || numel(val_in)==2) , 'Value must be a real scalar (Az) or 2-element vector (Az,El).')
            if numel(val_in) == 1, val_in = [val_in 0]; end
            obj.openingAngle = val_in;
        end
        function set.SectorTilt(obj, val_in)
            assert(isreal(val_in) && (numel(val_in)==1 || numel(val_in)==2) , 'Value must be a real scalar (Az) or 2-element vector (Az,El).')
            if numel(val_in) == 1, val_in = [val_in 0]; end
            obj.SectorTilt = val_in;
        end
        function im_sector = get.ImageSector(obj)
            im_sector = obj.openingAngle + obj.SectorTilt; %Changes by
            %Kate, to be able to tilt on a certain  degree
            %im_sector = obj.openingAngle;
        end
        function n_lines = get.nScanLines(obj)
            %n_lines = [(obj.nTxAz-1) obj.nTxEl] .* obj.nMLAs .* obj.nMLTs;
            n_lines = [obj.nTxAz obj.nTxEl];
        end
    end
end

