function [coordsPerEl,meanElPos,elAngle,subApMean,FCP] = createCurvedArrayAbaq(probeMidPoint,probeRot,focusDepth)

%Literals
dx = 37.5e-6;               %Spatial stepsize
Ne = 192;                   %Amount of elements in curved array
desElLen = 300e-6 - dx;       %Desired element lenght [m]
Nx = 4056; Ny = 4076;
radius = (41E-3)/dx; % Got this value (41mm) via Louis

%Make entire circle with radius = radius of curvature of probe
circle = makeCircle(Nx, Ny, Nx/2, Ny/2, radius, 2*pi, false);
[cirRow,cirCol] = find(circle==1);
clearvars circle

%Sort x- and y-coordinates according to counterclockwise angle wrt midpoint
arc_angles = atan2((cirCol-Ny/2),(cirRow-Nx/2));
[arc_angles,I] = sort(arc_angles);
cirCol = cirCol(I); cirRow = cirRow(I);

%Rearrange array, so that probe midpoint is the first entry
[~,pos] = min(abs(arc_angles-probeRot));
arc_angles = circshift(arc_angles,-(pos-1));
cirCol = circshift(cirCol,-(pos-1)); 
cirRow = circshift(cirRow,-(pos-1));

startBw = length(cirRow); %Start of first element in the "backward" progression
startFw = 2; %Start of first element in the "forward" progression

elComp = cell(Ne,1);
meanElPos = zeros(Ne,2);
elAngle = zeros(Ne,1);
elSize = zeros(Ne,1);

%Backward loop: make Ne/2 elements from probe midpoint in clockwise manner
for elId = Ne/2:-1:1
    prevSize = 0;
    endBw = 1;
    rightSize = false;
    while rightSize ~= true
        curSize = (sqrt( (cirRow(startBw-endBw)-cirRow(startBw))^2 + (cirCol(startBw-endBw)-cirCol(startBw))^2 ) +1 )*dx;
        if abs(curSize-desElLen) > abs(prevSize-desElLen)
            %Mean that previous combination was better
            rightSize = true;
        else 
            %Look further
            prevSize = curSize;
            endBw = endBw+1;
        end
    end
    
    pointSet = (startBw-(endBw-1)):startBw; %Improve code clarity
    elComp{elId} = pointSet;
    elSize(elId) = length(pointSet);
    meanElPos(elId,:) = [mean(cirRow(pointSet)) mean(cirCol(pointSet))];
    elAngle(elId) = mean(arc_angles(pointSet));
    
    startBw = startBw - endBw - 1; %Leave one position open as a kerf
end

%Forward loop: make elements;from probe midpoint in counterclockwise manner
for elId = (1:Ne/2)+Ne/2
    prevSize = 0;
    endFw = 1;
    rightSize = false;
    while rightSize ~= true
        curSize = (sqrt( (cirRow(startFw+endFw)-cirRow(startFw))^2 + (cirCol(startFw+endFw)-cirCol(startFw))^2 ) +1 )*dx;
        if abs(curSize-desElLen) > abs(prevSize-desElLen)
            %Mean that previous combination was better
            rightSize = true;
        else 
            %Look further
            prevSize = curSize;
            endFw = endFw+1;
        end
    end
    
    pointSet = startFw:(startFw+endFw-1); %Improve code clarity
    elComp{elId} = pointSet;
    elSize(elId) = length(pointSet);
    meanElPos(elId,:) = [mean(cirRow(pointSet)) mean(cirCol(pointSet))];
    elAngle(elId) = mean(arc_angles(pointSet));
    
    startFw = startFw + endFw + 1; %Leave one position open as a kerf
end

%Create matrix with [Row coordinates, column coordinates, elemente numbers]
coordsPerEl = zeros(sum(elSize),3);
posId = 1;
for elId = 1:Ne
    coordsPerEl(posId:posId+elSize(elId)-1,:) = [cirRow(elComp{elId}) cirCol(elComp{elId}) ones(elSize(elId),1)*elId];
    posId = posId+elSize(elId);
end

%Calculate mean positions of each subaperture and their focus point
subApMean = zeros(161,2);
FCP= zeros(161,2);
for lineId = 1:161
    subApMean(lineId,1:2) = [mean(meanElPos(lineId:lineId+31,1)),mean(meanElPos(lineId:lineId+31,2))]; %[row,column]
    FCP(lineId,1:2) = [Nx/2+(subApMean(lineId,1)-Nx/2)*(focusDepth+(41e-3))/(41e-3),Ny/2+(subApMean(lineId,2)-Ny/2)*(focusDepth+(41e-3))/(41e-3)];
end

rowShift = probeMidPoint(2) - cirRow(1);
colShift = probeMidPoint(1) - cirCol(1);

coordsPerEl(:,1) = coordsPerEl(:,1) + rowShift;
coordsPerEl(:,2) = coordsPerEl(:,2) + colShift;

meanElPos(:,1) = meanElPos(:,1) + rowShift;
meanElPos(:,2) = meanElPos(:,2) + colShift;

subApMean(:,1) = subApMean(:,1) + rowShift;
subApMean(:,2) = subApMean(:,2) + colShift;

FCP(:,1) = FCP(:,1) + rowShift;
FCP(:,2) = FCP(:,2) + colShift;
[coordsPerEl,~] = sortrows(coordsPerEl,[2 1]);

%{
figure,
scatter(coordsPerEl(:,2),coordsPerEl(:,1),15,'filled');
hold on
set(gca,'yDir','reverse');
axis equal
%}
end

