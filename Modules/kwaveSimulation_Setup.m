classdef kwaveSimulation_Setup
    properties
            DATA_CAST       = 'gpuArray-single';%'single';    ns
            RUN_SIMULATION  = true;         
            PLOT_SIM      =   false;
            DATARECAST    =   true;
            
            points_per_wavelength_x=8; %6-was fine, 4       % to create dx in kwavegrid     
            points_per_wavelength_y=8; %6-was fine, 4       % to create dy in kwavegrid     
            points_per_wavelength_z=8; %6-was fine, 4       % to create dz in kwavegrid      
            
            rho0 = 1000;               % background density [kg/m^3]
            c0 =1500;                  % background speed of sound [m/s];
            alpha_coeff = 0.75;        % attenuation coefficient [dB/(MHz^y cm)]
            alpha_power = 1.5;         % attenuation coefficient power
            BonA = 6;                  % nonlinear coefficient from Westervelt equation
            
            source_strength = 1e6;     % strength of the source [Pa]
            tone_burst_cycles = 3;     % number of cycles of the pulse
            
            pml_x_size = 20;           % PML size [grid points]
            pml_y_size = 20;           % PML size [grid points]
            pml_z_size= 20;            % PML size [grid points]

            pPatch                     % Defined previously PulsifyPatch
    end    
    properties (Dependent)
              dx                       %step in kwave grid in x-direction
              dy                       %step in kwave grid in y-direction   
              dz                       %step in kwave grid in z-direction
              Nx                       %number of elements in the grid in x-direction
              Ny                       %number of elements in the grid in y-direction
              Nz                       %number of elements in the grid in z-direction
              kgrid                    %Kwave grid
              medium                   %phantom with density, speed of sound etc
              KwavePatch               %patch projected on kgrid
              input_signal             %input signal
              input_args               %kWave simulation input_args (see run_transmission in pulsifyPatch, kWave option)
    end
    
    properties (Constant)
                    %gap=0.007;       %[m] in get.krid, the gap after phantom, before patch etc
                    gap=0.001;%0.002;
    end
    
    methods
        function [obj] = kwaveSimulation_Setup(inOptions)
            % probe - Probe constructor
            % Syntax:  obj = probe(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify probe object

            if nargin > 0
                %% parser
                names  = fieldnames(inOptions);
                values = struct2cell(inOptions);
                
                for ii = 1:length(values)
                    if ~isprop(obj, names{ii})
                        error(['Probe: This object does not contain this property: ', names{ii}]);
                    end
                    
                    obj.(names{ii}) = values{ii};
                end
            end
        end

       function input_args = get.input_args(obj)            
            input_args = {...
                 'PMLInside', false, 'PMLSize', [obj.pml_x_size, obj.pml_y_size, obj.pml_z_size], ...
                'DataCast', obj.DATA_CAST, 'DataRecast', obj.DATARECAST, 'PlotSim',obj.PLOT_SIM};%,...
                %'Smooth', [true true true]};
       end  
        
        function input_signal = get.input_signal(obj)
            global c f0
            tone_burst_freq = f0;     % [Hz]
            % create the input signal using toneBurst 
            input_signal = toneBurst(1/obj.kgrid.dt, tone_burst_freq, obj.tone_burst_cycles);

            % scale the source magnitude by the source_strength divided by the
            % impedance (the source is assigned to the particle velocity)
            input_signal = (obj.source_strength ./ (c * obj.rho0)) .* input_signal;
        end 
        
        
        function dx = get.dx(obj)
            global c f0
            dx=c/(obj.points_per_wavelength_x*f0);            
        end 
        
        
        function dy = get.dy(obj)
            global c f0
            dy=c/(obj.points_per_wavelength_y*f0);            
        end  
        
        
        function dz = get.dz(obj)
            global c f0
            dz=c/(obj.points_per_wavelength_z*f0);            
        end 
        
       function Nx = get.Nx(obj)
            x1=max(max(obj.pPatch.phantom.position(:,1)), max(max(cellfun(@(x) x(1),obj.pPatch.Tx.elementPos))))+obj.gap;
            x2=min(min(obj.pPatch.phantom.position(:,1)), ...
                                                                      min(min(cellfun(@(x) x(1),obj.pPatch.Tx.elementPos))))-obj.gap;
            x_size=abs(x1-x2);
            Nx = round(x_size/obj.dx);     % [grid points] 
            Nx=obj.fit_factor(Nx+2*obj.pml_x_size, 100)-2*obj.pml_x_size;
       end
       
       function Nx=fit_factor(obj, Nx, gap)
            [a2,a3,a5,a7]=checkFactors(Nx,Nx+gap);
            p=[a2 a3 a5 a7];
            [~,ind]=min(p-Nx);
            Nx=p(ind);
%             if ~isempty(a2)
%                [~,ind]=min(a2-Nx);
%                Nx=a2(ind);
%             elseif ~isempty(a3)
%                [~,ind]=min(a3-Nx);
%                Nx=a3(ind);                            
%             elseif ~isempty(a5)
%                [~,ind]=min(a5-Nx);
%                Nx=a5(ind);   
%             elseif ~isempty(a7)
%                [~,ind]=min(a7-Nx);
%                Nx=a7(ind);      
%             end       
       end    
       
       function Ny = get.Ny(obj)
            y1=max(max(obj.pPatch.phantom.position(:,2)),max(max(cellfun(@(x) x(2),obj.pPatch.Tx.elementPos))))+obj.gap;
            y2=min(min(obj.pPatch.phantom.position(:,2)),...
                                                                    min(min(cellfun(@(x) x(2),obj.pPatch.Tx.elementPos))))-obj.gap;
            y_size=abs(y1-y2);
            Ny =round(y_size/obj.dx);     % [grid points]
            Ny=obj.fit_factor(Ny+2*obj.pml_y_size, 100)-2*obj.pml_y_size;
       end 
       
       function Nz = get.Nz(obj)
            z1=max(max(obj.pPatch.phantom.position(:,3)),max(max(cellfun(@(x) x(3),obj.pPatch.Tx.elementPos))))+obj.gap;
            z2=min(min(obj.pPatch.phantom.position(:,3)), min(min(cellfun(@(x) x(3),obj.pPatch.Tx.elementPos))))-obj.gap;
            z_size=abs(z1-z2);
            Nz =round(z_size/obj.dx);     % [grid points]
            Nz=obj.fit_factor(Nz+2*obj.pml_z_size, 100)-2*obj.pml_z_size;
       end 
       function kgrid=get.kgrid(obj)
            global c
            kgrid = kWaveGrid(obj.Nx, obj.dx, obj.Ny, obj.dy, obj.Nz, obj.dz);
            %t_end = (obj.Nx * obj.dx) * 2.2 / c;   % [s]
            kgrid.t_array =kgrid.makeTime(obj.c0);
            %{
             figure(1); hold on; 
             scatter3(obj.pPatch.phantom.position(1:10:end,1).*10^3,obj.pPatch.phantom.position(1:10:end,3).*10^3,obj.pPatch.phantom.position(1:10:end,2).*10^3,50.0,'filled', 'MarkerEdgeColor', 'r'); 
             axis equal tight;
             scatter3(kgrid.x(1:100:end).*10^3,kgrid.z(1:100:end).*10^3,kgrid.y(1:100:end).*10^3,5.0,'filled', 'MarkerFaceColor', 'b'); 
            %}
       end
       function medium=get.medium(obj)
            kgrid=obj.kgrid;
           
            medium.density=obj.rho0.*ones(kgrid.Nx,kgrid.Ny,kgrid.Nz);
            medium.sound_speed=obj.c0.*ones(kgrid.Nx,kgrid.Ny,kgrid.Nz);
            
            if strcmp(obj.pPatch.phantom.predefined, 'ellipsoid')||strcmp(obj.pPatch.phantom.predefined, 'ellipsoid_cropped')  
               [medium]=define_medium_for_certain_part(obj, medium, 'LV, wall', kgrid, [1543 2815],0);
               [medium]=define_medium_for_certain_part(obj,  medium, 'LV, interior', kgrid, [1543 2815],0);   
            end  
            if strcmp(obj.pPatch.phantom.predefined, 'knee_ligaments')|| strcmp(obj.pPatch.phantom.predefined, 'knee_ligaments_cropped')   
               [medium]=define_medium_for_certain_part(obj, medium, 'Femur', kgrid, [1543 2815],0); %first density, then speed of sound
               [medium]=define_medium_for_certain_part(obj,  medium, 'Fibula', kgrid, [1543 2815],0);
               [medium]=define_medium_for_certain_part(obj, medium, 'Tibia', kgrid, [1543 2815],0);
               [medium]=define_medium_for_certain_part(obj, medium, 'Patella', kgrid, [1543 2815],0);
               [medium]=define_medium_for_certain_part(obj, medium, 'tibCart1', kgrid, [1099 1639.6],0.95);
               [medium]=define_medium_for_certain_part(obj, medium, 'tibCart2', kgrid, [1099 1639.6],0.95);
               [medium]=define_medium_for_certain_part(obj, medium,'patCart', kgrid, [1099 1639.6],0.95);
               [medium]=define_medium_for_certain_part(obj,  medium,'FemCart', kgrid, [1099 1639.6],0.95);    
               [medium]=define_medium_for_certain_part(obj,  medium,'MCL', kgrid,[1142 1750],0.95);
               [medium]=define_medium_for_certain_part(obj,  medium,'LCL', kgrid,[1142 1750],0.95);
            end
            %figure; imagesc(squeeze(medium.density(200,:,:)));colorbar;
            medium.density = smooth(medium.density, [], [], 0.999);%0.16);
            %figure; imagesc(squeeze(d1(200,:,:)));colorbar;
            medium.sound_speed = smooth(medium.sound_speed, [], [], 0.999);%0.16);   
            if strcmp(obj.pPatch.phantom.predefined, 'knee_ligaments')|| strcmp(obj.pPatch.phantom.predefined, 'knee_ligaments_cropped')
                ind=cellfun(@(x) find(contains(x, 'fiber')), obj.pPatch.phantom.segmentationClasses, 'UniformOutput', false);
                ind=find(~cellfun(@isempty, ind));
                for i=1:numel(ind) 
                  [medium]=define_medium_for_certain_part(obj,  medium,obj.pPatch.phantom.segmentationClasses{ind(i)}, kgrid,[1250 1750],0.95);
                end                    
            end    
            clear kgrid
       end
       function [medium]=define_medium_for_certain_part(obj, medium, str, kgrid, val, variance)
             ind=cellfun(@(x) find(strcmp(x, str)), obj.pPatch.phantom.segmentationClasses, 'UniformOutput', false);
             ind=find(~cellfun(@isempty, ind));
             temp=obj.pPatch.phantom.segmentationIndex(ind);
             ind=cell2mat(temp(~cellfun(@isempty,obj.pPatch.phantom.segmentationIndex(ind))));
             if ~isempty(ind)
                 [medium.density, medium.sound_speed] =obj.project_phantom_on_kWavegrid(kgrid,obj.pPatch.phantom.position,ind,val,variance,medium.density, medium.sound_speed);
             end
       end    
       
       function [varargout]=project_phantom_on_kWavegrid(obj, kgrid,data,ind, val,variance,varargin)      
              [meshPhantomUnique]=project_phantom_points_on_kWavegrid(obj, kgrid,data,ind);   
              varargout{1}=varargin{1}; varargout{2}=varargin{2};        
              for ik=1:size(meshPhantomUnique,1)
                     varargout{1}(meshPhantomUnique(ik,1), meshPhantomUnique(ik,2), meshPhantomUnique(ik,3))=...
					                                                   sqrt(variance*val(1)).*randn(1,1) + val(1);%val(1);
                     varargout{2}(meshPhantomUnique(ik,1), meshPhantomUnique(ik,2), meshPhantomUnique(ik,3))=...
					                                                   sqrt(variance*val(2)).*randn(1,1) + val(2);%val(2);
              end
       end
       
       function [meshPhantomUnique]=project_phantom_points_on_kWavegrid(obj, kgrid, data,ind)    
              iX_local=round((data(ind,1)-min(kgrid.x_vec))./kgrid.dx)+1;
              iY_local=round((data(ind,2)-min(kgrid.y_vec))./kgrid.dy)+1;
              iZ_local=round((data(ind,3)-min(kgrid.z_vec))./kgrid.dz)+1;
              
              FV.faces=boundary(data(ind, :));
              FV.vertices=data(ind,:);
              x=kgrid.x_vec(min(iX_local)):kgrid.dx:kgrid.x_vec(max(iX_local));
              y=kgrid.y_vec(min(iY_local)):kgrid.dy:kgrid.y_vec(max(iY_local));
              z=kgrid.z_vec(min(iZ_local)):kgrid.dz:kgrid.z_vec(max(iZ_local));
              in = inpolyhedron(FV,x,y,z);
              [X,Y,Z]=meshgrid(x,y,z); x=X(in); y=Y(in); z=Z(in);

              iX_local=round((x-min(kgrid.x_vec))./kgrid.dx)+1;
              iY_local=round((y-min(kgrid.y_vec))./kgrid.dy)+1;
              iZ_local=round((z-min(kgrid.z_vec))./kgrid.dz)+1;
              
%               figure(2); hold on; s=10;x=kgrid.x_vec(iX_local);y=kgrid.y_vec(iY_local); z=kgrid.z_vec(iZ_local);
%               scatter3(x(1:s:end).*10^3, z(1:s:end).*10^3, y(1:s:end).*10^3, 2.0, 'b', 'filled');

              if (min(iX_local)<1) || (max(iX_local)>numel(kgrid.x_vec))||(min(iY_local)<1) || (max(iY_local)>numel(kgrid.y_vec))||...
                                (min(iZ_local)<1) || (max(iZ_local)>numel(kgrid.z_vec))
                              error ('Something is wrong with kgrid, it does not cover the entire region');           
              end              
              meshPhantom=[iX_local iY_local iZ_local];
              meshPhantomUnique=unique(meshPhantom, 'rows');   
       end
       function [meshPhantomUnique]=project_points_on_kWavegrid(obj, kgrid, data,ind)
           iX_local=round((data(ind,1)-min(kgrid.x_vec))./kgrid.dx)+1;
           iY_local=round((data(ind,2)-min(kgrid.y_vec))./kgrid.dy)+1;
           iZ_local=round((data(ind,3)-min(kgrid.z_vec))./kgrid.dz)+1;
           if (min(iX_local)<1) || (max(iX_local)>numel(kgrid.x_vec))||(min(iY_local)<1) || (max(iY_local)>numel(kgrid.y_vec))||...
                   (min(iZ_local)<1) ||(max(iZ_local)>numel(kgrid.z_vec))
               error ('Something is wrong with kgrid, it does not cover the entire region');
           end
           meshPhantom=[iX_local iY_local iZ_local];
           meshPhantomUnique=unique(meshPhantom, 'rows');
       end
        
       function KwavePatch=get.KwavePatch(obj)
           elementEdges=obj.pPatch.Tx.elementEdges;
           kgrid=obj.kgrid;
           
           AllPos=[]; %positions of all elements
           KWelementPos=zeros(kgrid.Nx,kgrid.Ny,kgrid.Nz);
           KWelementIndexRow=zeros(kgrid.Nx,kgrid.Ny,kgrid.Nz);
           KWelementIndexCol=zeros(kgrid.Nx,kgrid.Ny,kgrid.Nz);
           %figure(2);hold on;
           %% go around each element of the patch
           for i=1:size(elementEdges,1)
              for j=1:size(elementEdges,2)
                    
                    corners=elementEdges{i,j};    
                    P1=corners(1,:);
                    P2=corners(2,:);
                    P3=corners(3,:);
                    P4=corners(4,:);
    %                     scatter3(P1(1).*10^3,P1(3).*10^3,P1(2).*10^3,10.0, 'g','filled');
%                     scatter3(P2(1).*10^3,P2(3).*10^3,P2(2).*10^3,10.0, 'b','filled');
%                     scatter3(P3(1).*10^3,P3(3).*10^3,P3(2).*10^3,10.0, 'r','filled');
%                     scatter3(P4(1).*10^3,P4(3).*10^3,P4(2).*10^3,10.0, 'k','filled');

                    
                    
                    elementSizeError=[0 0];
                    gp=0;%kgrid.dx/2; 
                    kol=1;
                    
                    %% define the plane by edges (rectangle of the size of the element)
                    vec1=P1+linspace(-gp,obj.pPatch.Tx.width+gp,10*(obj.pPatch.Tx.width+gp)/kgrid.dx)'*(P2-P1)./norm(P2-P1);
                    vec2=P1+linspace(-gp,obj.pPatch.Tx.width+gp,10*(obj.pPatch.Tx.width+gp)/kgrid.dx)'*(P4-P1)./norm(P4-P1);
                    plane=[vec1; vec2];
                    for jj=2:size(vec2,1)
                       plane=[plane; vec2(jj,:)+linspace(0,norm(P2-P1),5*norm(P2-P1)/kgrid.dx)'*(P2-P1)./norm(P2-P1)];
                    end
%                     scatter3(plane(:,1).*10^3,plane(:,3).*10^3,plane(:,2).*10^3,5.0, 'g','filled');
%                     scatter3(vec1(:,1).*10^3,vec1(:,3).*10^3,vec1(:,2).*10^3,5.0, 'r','filled');
%                     scatter3(vec2(:,1).*10^3,vec2(:,3).*10^3,vec2(:,2).*10^3,5.0, 'b','filled');
                    %% project the plane on k-wave grid 
                    [mesh]=project_points_on_kWavegrid(obj,kgrid,plane,1:size(plane,1)); 
                    [mesh_un,ib,id]=unique(mesh(:,1:2), 'rows'); %get only unique values
                    mesh=[mesh_un mesh(ib,3)];
                    Pos=[];
                    for ik=1:size(mesh,1)
                       P=[kgrid.x_vec(mesh(ik,1)) kgrid.y_vec(mesh(ik,2)) kgrid.z_vec(mesh(ik,3))];
                       % check if the point on the grid is already taken by
                       % other eleement
                       if ~isempty(AllPos)
                                ExistP=find(ismember(AllPos(:,1:2),P(1:2),'rows')~=0);   
                       else
                                ExistP=[];
                       end  
                       
                       if isempty(ExistP) %if the point is not yet on the grid, we add it
                             Pos=[Pos; P];
                             KWelementPos(mesh(ik,1), mesh(ik,2), mesh(ik,3))=1;
                             KWelementIndexRow(mesh(ik,1), mesh(ik,2), mesh(ik,3))=i;
                             KWelementIndexCol(mesh(ik,1), mesh(ik,2), mesh(ik,3))=j;
                      end
                    end
                    
                    elementPos{i,j}=Pos; %grid points belonging to the current element
                     
                    %% find the element center
                    center=[mean(Pos(:,1)) mean(Pos(:,2)) mean(Pos(:,3))];
                    [~,t]=min(pdist2(Pos,center));
                    elementPosCenters{i,j}=Pos(t,:); %grid point corresponsing to the center of the element
                    
                    % for improving the precision later
%                     elementSize{i,j}=[max(Pos(:,1))-min(Pos(:,1)) max(Pos(:,2))-min(Pos(:,2))];
%                     elementSizeError=abs(elementSize{i,j}-obj.Tx.width)<kgrid.dx;
%                     gap=kgrid.dx/2*kol; kol=kol+1;    
                    
                    AllPos=[AllPos; Pos]; %add new grid points
              end
           end
            
                     
%            
           
           %% SubArrays:
           if (size(elementPosCenters,1)>1)&&(size(elementPosCenters,2)>1)
             cut = elementPosCenters(obj.pPatch.Tx.subArraySize(1)/2:end-obj.pPatch.Tx.subArraySize(1)/2+1,obj.pPatch.Tx.subArraySize(2)/2:end-obj.pPatch.Tx.subArraySize(2)/2+1);
             cutMat = cell2mat(cut);
             subArrayPos = filter2([1 0 0 1; 1 0 0 1], cutMat, 'valid')/4;
             subArrayPos = mat2cell(subArrayPos, ones(size(subArrayPos,1),1), 3*ones(1,size(subArrayPos,2)/3));
             ind1=obj.pPatch.Tx.subArraySize(1)/2:size(elementPosCenters,1)-obj.pPatch.Tx.subArraySize(1)/2+1;
             ind2=obj.pPatch.Tx.subArraySize(2)/2:size(elementPosCenters,2)-obj.pPatch.Tx.subArraySize(2)/2+1;
           elseif size(elementPosCenters,1)==1
             subArrayPos = elementPosCenters(1,obj.pPatch.Tx.subArraySize(2)/2:end-obj.pPatch.Tx.subArraySize(2)/2);
             ind1=1;
             ind2=obj.pPatch.Tx.subArraySize(2)/2:size(elementPosCenters,2)-obj.pPatch.Tx.subArraySize(2)/2+1;
           elseif size(elementPosCenters,2)==1
             subArrayPos = elementPosCenters(obj.pPatch.Tx.subArraySize(1)/2:end-obj.pPatch.Tx.subArraySize(1)/2,1); 
             ind2=1; ind1=obj.pPatch.Tx.subArraySize(1)/2:size(elementPosCenters,1)-obj.pPatch.Tx.subArraySize(1)/2+1;
           end    
                      
           %% 2D filter obtains the average position of 4 neighboring elements
           figure(1); hold on;
           currentElemPosCenters=cell(obj.pPatch.Tx.subArraySize(1),obj.pPatch.Tx.subArraySize(2));
           for i=1:size(subArrayPos ,1)
              for j=1:size(subArrayPos,2)
                  if (size(elementPosCenters,1)>1)&&(size(elementPosCenters,2)>1)
                      subArrayElementsPosIndex{i,j}={ind1(i)-obj.pPatch.Tx.subArraySize(1)/2+1:ind1(i)+obj.pPatch.Tx.subArraySize(1)/2;...
                          ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2};
                      currentElemPosCenters=elementPosCenters(ind1(i)-obj.pPatch.Tx.subArraySize(1)/2+1:ind1(i)+obj.pPatch.Tx.subArraySize(1)/2,...
                          ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2);
                      subArrayPosK{i,j}=elementPosCenters(round(mean(ind1(i)-obj.pPatch.Tx.subArraySize(1)/2+1:ind1(i)+obj.pPatch.Tx.subArraySize(1)/2)), round(mean(ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2)));
                      subArrayPosK{i,j}=subArrayPosK{i,j}{1};
                      subArrayCurvature{i,j}=calculateProbeRotationCurvature(obj,currentElemPosCenters);
                  elseif size(elementPosCenters,1)==1
                      subArrayElementsPosIndex{i,j}={1,...
                          ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2};
                      %ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2
                      currentElemPosCenters=elementPosCenters(1,...
                          ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2);
                      subArrayPosK{i,j}=elementPosCenters(1, round(mean(ind2(j)-obj.pPatch.Tx.subArraySize(2)/2+1:ind2(j)+obj.pPatch.Tx.subArraySize(2)/2)));
                      subArrayPosK{i,j}=subArrayPosK{i,j}{1};
                      subArrayCurvature{i,j}=calculateProbeRotationCurvature(obj,currentElemPosCenters);
                  elseif size(elementPosCenters,2)==1
                      subArrayElementsPosIndex{i,j}={ind1(i)-obj.pPatch.Tx.subArraySize(1)/2+1:ind1(i)+obj.pPatch.Tx.subArraySize(1)/2;...
                          1};
                      currentElemPosCenters=elementPosCenters(ind1(i)-obj.pPatch.Tx.subArraySize(1)/2+1:ind1(i)+obj.pPatch.Tx.subArraySize(1)/2,...
                          1);
                      subArrayPosK{i,j}=elementPosCenters(round(mean(ind1(i)-obj.pPatch.Tx.subArraySize(1)/2+1:ind1(i)+obj.pPatch.Tx.subArraySize(1)/2)), 1);
                      subArrayPosK{i,j}=subArrayPosK{i,j}{1};
                      subArrayCurvature{i,j}=calculateProbeRotationCurvature(obj,currentElemPosCenters)';                                    
                  end    
                  %scatter3(subArrayPosK{i,j}(1).*10^3,subArrayPosK{i,j}(3).*10^3,subArrayPosK{i,j}(2).*10^3, 100.0, 'filled', 'MarkerEdgeColor', 'r','MarkerFaceColor', 'k');
              end    
           end        
           
            %% SubArray Curvature                     

           KwavePatch.elementPosCenters=elementPosCenters;
           KwavePatch.elementPos=elementPos;
           KwavePatch.subArrayPosCenters=subArrayPosK;
           KwavePatch.subArrayElementsPosIndex=subArrayElementsPosIndex;
           KwavePatch.KWelementPos=KWelementPos;
           KwavePatch.KWelementIndexRow=KWelementIndexRow;
           KwavePatch.KWelementIndexCol=KWelementIndexCol;
           KwavePatch.subArrayCurvature=subArrayCurvature;
           
           clear elementPosCenters elementPos subArrayPosK subArrayElementsPosIndex KWelementPos KWelementIndex subArrayCurvature kgrid

       end   
       
      
        
        function subArrayCurvature=calculateProbeRotationCurvature(obj, centersT)
            %created by Kate for rotation calculation
            % calculate rotation angle
            %azimuth rotation
            N1=obj.pPatch.Tx.subArraySize(1);
            N2=obj.pPatch.Tx.subArraySize(2);
            
            if size(centersT,2)~=1
                point1=[mean([centersT{1,1}(:,1) centersT{N1,1}(:,1)]) mean([centersT{1,1}(:,2) centersT{N1,1}(:,2)]) mean([centersT{1,1}(:,3) centersT{N1,1}(:,3)])];
                point2=[mean([centersT{N1,N2}(:,1) centersT{1,N2}(:,1)]) mean([centersT{N1,N2}(:,2) centersT{1,N2}(:,2)]) mean([centersT{N1,N2}(:,3) centersT{1,N2}(:,3)])];
                subArrayCurvature(1)=sign((point1(:,3)-point2(:,3)))*abs((atan(abs(point1(:,3)-point2(:,3))/abs(point1(:,1)-point2(:,1)))));
            else
                subArrayCurvature(1)=0;
            end
            
%             figure(2); hold on;
%             centers=cell2mat(reshape(centersT, [N*N 1]));
%             scatter3(centers(:,1).*10^3,centers(:,3).*10^3, centers(:,2).*10^3, 50.0, 'k', 'filled');
%             scatter3(point1(1)*10^3,point1(3)*10^3, point1(2)*10^3, 200.0, 'r', 'filled');
%             scatter3(point2(1)*10^3,point2(3)*10^3, point2(2)*10^3, 200.0, 'g', 'filled');
            
            %elevation rotation
            if size(centersT,1)~=1
                point1=[mean([centersT{1,1}(:,1) centersT{1,N2}(:,1)]) mean([centersT{1,1}(:,2) centersT{1,N2}(:,2)]) mean([centersT{1,1}(:,3) centersT{1,N2}(:,3)])];
                point2=[mean([centersT{N1,N2}(:,1) centersT{N1,1}(:,1)]) mean([centersT{N1,N2}(:,2) centersT{N1,1}(:,2)]) mean([centersT{N1,N2}(:,3) centersT{N1,1}(:,3)])];
                subArrayCurvature(2)=-sign((point1(:,3)-point2(:,3)))*abs((atan(abs(point1(:,3)-point2(:,3))/abs(point1(:,2)-point2(:,2)))));
            else
                subArrayCurvature(2)=0;
            end
%             scatter3(point1(1)*10^3,point1(3)*10^3, point1(2)*10^3, 200.0, 'k', 'filled');
%             scatter3(point2(1)*10^3,point2(3)*10^3, point2(2)*10^3, 200.0, 'k', 'filled');
            
            
        end
       
          
      function plot_geometry(obj,inOptions, varargin) 
           
          if nargin==5
             KwavePatch=varargin{1};
             kgrid=varargin{2};
             medium=varargin{3}; 
          else
             KwavePatch=obj.KwavePatch; kgrid=obj.kgrid; medium=obj.medium; 
          end   
          
          inOptions = parser(struct('plot_kgrid', 0, ...
                        'plot_subArrays',   0, 'plot_patch', 0), inOptions);
                    
          %% plot the patch
           figure(3); set(gcf, 'Position',[ 500 100 800 800]) 
           hold on;
           if inOptions.plot_patch
               for i=1:size(KwavePatch.elementPos,1)
                   for j=1:size(KwavePatch.elementPos,2)
                       E=KwavePatch.elementPos{i,j};
                       X = E(:,1)*1000;
                       Y = E(:,2)*1000;
                       Z = E(:,3)*1000;
                       cc=rand(1,3);
                       scatter3(X,Z,Y, 5.0, 'filled', 'MarkerEdgeColor', cc);
                       scatter3(KwavePatch.elementPosCenters{i,j}(1).*10^3,KwavePatch.elementPosCenters{i,j}(3).*10^3,...
                           KwavePatch.elementPosCenters{i,j}(2).*10^3, 10.0, 'filled', 'MarkerEdgeColor', 'k');
                   end
               end
               view(3); axis equal tight;
           end
           %% plot the phantom
           figure(3); hold on;
           %val=unique(round(medium.density(:)));
           %val=val(val~=mode(round(medium.density(:))));
           %val=val(val>920);
           s=10;
           ind=find(medium.density(:)>mean(medium.density(:)));
%            
%            s=1;
%            [~, ind1]=min(abs(kgrid.z(:)-2/10^3));%mean(kgrid.z(:))));
%            ind=find((medium.density(:)>mean(medium.density(:)))&(kgrid.z(:)==kgrid.z(ind1)));
%            X=kgrid.x(ind);Y=kgrid.y(ind); Z=kgrid.z(ind); D=medium.density(ind);
%            scatter3(X(1:s:end).*10^3,Z(1:s:end).*10^3,Y(1:s:end).*10^3,2.0, D(1:s:end), 'filled','MarkerEdgeColor', 'none'); 
%            [~, ind1]=min(abs(kgrid.y(:)-(-6/10^3)));
%            ind=find((medium.density(:)>mean(medium.density(:)))&(kgrid.y(:)==kgrid.y(ind1)));
           X=kgrid.x(ind);Y=kgrid.y(ind); Z=kgrid.z(ind); D=medium.density(ind);
           scatter3(X(1:s:end).*10^3,Z(1:s:end).*10^3,Y(1:s:end).*10^3,2.0, D(1:s:end), 'filled','MarkerEdgeColor', 'none'); 

%            for i=1:10:numel(val)
%             cc=rand(1,3);
%             ind=find(abs(medium.density-val(i))<1);
%             X=kgrid.x(ind);Y=kgrid.y(ind); Z=kgrid.z(ind); D=medium.density(ind);
%             scatter3(X(1:s:end).*10^3,Z(1:s:end).*10^3,Y(1:s:end).*10^3,2.0, D(1:s:end), 'filled', 'MarkerFaceColor', cc,'MarkerEdgeColor', 'none'); 
%            end
%             
           axis equal tight;colorbar;
           caxis([1000 1543]);
           title('k-Wave Geometry', 'Fontsize', 13);
           
           %% the grid
           if inOptions.plot_kgrid
                s=20;
%                 x=kgrid.x(1:s:end, 1:s:end, 1:s:end).*10^3;
%                 y=kgrid.y(1:s:end,1:s:end,1:s:end).*10^3;
%                 z=kgrid.z(1:s:end,1:s:end,1:s:end).*10^3; 
                figure(3); hold on;
                %scatter3(x(:), y(:), z(:), 2.0, 'b', 'filled');
                
                x=kgrid.x(1, :, :).*10^3;
                y=kgrid.y(1,:, :).*10^3;
                z=kgrid.z(1, :, :).*10^3; 
                scatter3(x(:), z(:), y(:), 2.0, 'r', 'filled');
 
                x=kgrid.x(:,1,:).*10^3;
                y=kgrid.y(:,1,:).*10^3;
                z=kgrid.z(:,1, :).*10^3; 
                scatter3(x(:), z(:), y(:), 2.0, 'r', 'filled');

                x=kgrid.x(:,:,1).*10^3;
                y=kgrid.y(:,:,1).*10^3;
                z=kgrid.z(:,:,1).*10^3; 
                scatter3(x(:), z(:), y(:), 2.0, 'r', 'filled');

                
                x=kgrid.x(end,:,:).*10^3;
                y=kgrid.y(end,:,:).*10^3;
                z=kgrid.z(end,:,:).*10^3;
                scatter3(x(:), z(:), y(:), 2.0, 'r', 'filled');
                
                x=kgrid.x(:,end,:).*10^3;
                y=kgrid.y(:,end,:).*10^3;
                z=kgrid.z(:,end, :).*10^3;
                scatter3(x(:), z(:), y(:), 2.0, 'r', 'filled');
                
                x=kgrid.x(:,:,end).*10^3;
                y=kgrid.y(:,:,end).*10^3;
                z=kgrid.z(:,:,end).*10^3;
                scatter3(x(:), z(:), y(:), 2.0, 'r', 'filled');

           end
            
           %% subArrays
           if inOptions.plot_subArrays
               for i=1:size(KwavePatch.subArrayPosCenters ,1)
                   for j=1:size(KwavePatch.subArrayPosCenters,2)
                        scatter3(KwavePatch.subArrayPosCenters{i,j}(1).*10^3,KwavePatch.subArrayPosCenters{i,j}(3).*10^3,KwavePatch.subArrayPosCenters{i,j}(2).*10^3, 100.0, 'filled', 'MarkerEdgeColor', 'r','MarkerFaceColor', 'k');
                   end
               end
           end    
           
      end
    end
end