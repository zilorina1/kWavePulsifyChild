
% if ~exist('rawData','var')||isempty(rawData)
%      clearvars
% else    
%     clearvars -except rawData
% end
% 
% profile clear
% profile on
function []=startKatya(active_path,kWavepath)
% F = findall(0,'type','figure','tag','TMWWaitbar');
% delete(F);

% [active_path,~,~] = fileparts(which('start.m'));
% active_path=[active_path '\'];

%% Preliminaries
global c fs f0
f0    = 6e6; % Transducer center frequency [Hz] 2.5e6
fs    = 100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]'; for COLE and FieldII

%%
tx_focal_depth  = 0.01; % metres
alpha           = 0.0; % db/cm/MHz attenuation for TGC
imageSize       = [512 512];
Y_level          = 0.01;

% set_field ('att',  alpha*100/1e6*f0);
% set_field ('c', c);
% set_field ('fs',  fs);
% set_field ('use_att',1);
% set_field ('use_rectangles',1);

%% patch and software options

pPatch = pulsifyPatch(struct(...
   'Tx_shape',                'flat', ...
    'Tx_nElements_x',             4,... %azimuth
    'Tx_nElements_y',            25, ....%elevation
     'Tx_subArraySize',              [4 4],...%[el az]
      'Tx_width',       120e-6,...
      'Tx_height',       120e-6,...
      'Tx_kerf',       94e-6,...
      'nPoints',                 1e6, ...
     'predefined',     'knee_ligaments', ...
      'center',          [0  -0.02  -0.04], ...
      'rotationAngle',  [20 60 10],...
      'Tx_centerOfAperture',  [ 0.0059   -0.0141   -0.051]));


%X = 1:stepX:130; %along elevation direction, from up to downX=[1];%[50];
X=1;
stepY=1;
Y =[1:2];%[1:4:55];%along azimuth direction, from left to right
[XX, YY] = meshgrid(X,Y);
shotPosition = [XX(:) YY(:)];

%% software options
softwareOpt = PP_scanSequencer(struct(...
'scanMode',         'custom', ...
    'shotIndex',     shotPosition, ...
    'initialPosition',  [1, 1], ...
    'txFocalDepth',     tx_focal_depth, ...
    'nTxAz'         ,   1, ...
    'nTxEl'         ,   11, ...
    'maxDepth'      ,   0.13, ...
    'coleAmpThreshold', 1e-28, ...
    'openingAngle'  ,   [0, abs(-5)+abs(5)]));

if softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end    
    
%% hardware options
% hardwareOpt = transOptions(struct('mode_tx', 'tx_on_rows',...
%     'mode_rx',     'rx_on_rows',...
%     'COLE',        true));
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'KWave',        true));
% hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
%     'mode_rx',     'full_control',...
%     'COLE',        true));

if strcmp(hardwareOpt.mode_tx,'tx_on_rows')&strcmp(hardwareOpt.mode_rx,'rx_on_rows')&(softwareOpt.nTxAz>1)
    error('For row-row, we can do sectorial imaging only in the elevation plane');
end   
if strcmp(hardwareOpt.mode_tx,'tx_on_cols')&strcmp(hardwareOpt.mode_rx,'rx_on_cols')&(softwareOpt.nTxEl>1)
    error('For col-col, we can do sectorial imaging only in the azimuth plane');
end

if hardwareOpt.COLE
%% load LUT data once
% I save it to rawData variable, so if you want to load different LUTs, you
% would need to clean rawData variable (in the beginning of the file all variables are clened except it)
%index=round(cmpdImg.number_of_images_in_secondary_direction/2);
if (~exist('rawData','var'))
   if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
      folderName = strcat(pwd, '\Dataset\COLE\', 'tx_on_rows', '_', 'rx_on_rows', '\3D\'); 
   else    
      folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '\3D\');
   end
   h_w = waitbar(0,['I will load LUT tables forever...']);
   fprintf('I will load LUT tables forever...');
   t0 = tic; 
   for ii = 1:length(softwareOpt.txEvents)
                 if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '.mat');
                 else    
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '.mat');
                 end
                 if ~exist(filename, 'file')
                        error('COLE: LUT not found');
                 else
                        load(filename, 'echoData', 'impulseResponse');
                        rawData.echoData{ii}=echoData;
                        rawData.impulseResponse{ii}=impulseResponse;
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                            rawData.echoData{ii}.data = permute(rawData.echoData{ii}.data, [1 3 2]);
                        end    
                 end
                 waitbar((ii)/(length(softwareOpt.txEvents)),h_w);        
   end
   delete(h_w), drawnow
end
end


if ~exist([active_path 'geometry.png'])
    inOptions.plotsubArray=0;
    inOptions.plotSequence=0;
    inOptions.newFigure=1;
    pPatch.plot_geometry(inOptions, softwareOpt);
    saveas(gca, [active_path 'geometry.png']);
    close all;
end
% hold on;
% scatter3(pPatch.Tx.elementPos{1,1}(1)*10^3, pPatch.Tx.elementPos{1,1}(3)*10^3, pPatch.Tx.elementPos{1,1}(2)*10^3, 5.0, 'r', 'filled');
% scatter3(pPatch.Tx.elementPos{4,1}(1)*10^3, pPatch.Tx.elementPos{4,1}(3)*10^3, pPatch.Tx.elementPos{4,1}(2)*10^3, 5.0, 'r', 'filled');

%% crop the phantom
a=pPatch.convert_cell_to_Fii_mat(pPatch.Tx.elementPos);
gap_el=0.001;%0.002
gap_az=0.0005;
ind=find(((pPatch.phantom.position(:,2)<max(a(:,2))+gap_el)&(pPatch.phantom.position(:,2)>min(a(:,2))-gap_el)&...
         (pPatch.phantom.position(:,1)>min(a(:,1))-gap_az)&(pPatch.phantom.position(:,1)<max(a(:,1))+gap_az)&...
         (pPatch.phantom.position(:,3)<mean(a(:,3))+1.5*tx_focal_depth)));
          %(pPatch.phantom.position(:,3)<mean(a(:,3))+1.8*tx_focal_depth)));
ind_new=zeros(1,numel(pPatch.phantom.position));      
ind_new(ind)=1:numel(ind);
pPatch.phantom.segmentationIndex=cellfun(@(x) ind_new(intersect(x,ind)),pPatch.phantom.segmentationIndex, 'UniformOutput', false);    
center=mean([pPatch.phantom.position(ind, :); pPatch.convert_cell_to_Fii_mat(pPatch.Tx.elementPos)]);
arr=[pPatch.phantom.position(ind, :); pPatch.convert_cell_to_Fii_mat(pPatch.Tx.elementPos)];
center=(max(arr)+min(arr))/2;
pPatch.phantom.position=pPatch.phantom.position(ind, :)-center;
pPatch.phantom.positionForCalc=pPatch.phantom.position;
pPatch.phantom.amplitude=pPatch.phantom.amplitude(ind);
phantomSaved=pPatch.phantom;
save('currently_cropped_phantom.mat', 'phantomSaved');
save([active_path 'currently_cropped_phantom.mat'], 'phantomSaved');

pPatch = pulsifyPatch(struct(...
   'Tx_shape',                'flat', ...
    'Tx_nElements_x',           pPatch.Tx.nElements_x,...
    'Tx_nElements_y',           pPatch.Tx.nElements_y, ....
    'Tx_subArraySize',          pPatch.Tx.subArraySize,...
     'Tx_width',       pPatch.Tx.width,...
      'Tx_height',     pPatch.Tx.height,...
      'Tx_kerf',       pPatch.Tx.kerf,...
   'predefined',     'knee_ligaments_cropped', ...
    'nPoints',                 5e5,...%5e5,...%1e4, ...
      'Tx_centerOfAperture', pPatch.Tx.centerOfAperture-center));      
%save([active_path 'cropped_field_patch_knee_ligaments.mat'], 'pPatch', '-v7.3');
%load('cropped_field_patch_knee_ligaments.mat', 'pPatch');
if ~exist([active_path 'geometry_zoom.png'])
    inOptions.plotsubArray=0;
    inOptions.plotSequence=0;
    inOptions.newFigure=1;
    pPatch.plot_geometry(inOptions, softwareOpt);
    saveas(gca, [active_path 'geometry_zoom.png']);
end
% hold on;
% scatter3(pPatch.Tx.elementPos{1,1}(1)*10^3, pPatch.Tx.elementPos{1,1}(3)*10^3, pPatch.Tx.elementPos{1,1}(2)*10^3, 5.0, 'r', 'filled');
% scatter3(pPatch.Tx.elementPos{4,1}(1)*10^3, pPatch.Tx.elementPos{4,1}(3)*10^3, pPatch.Tx.elementPos{4,1}(2)*10^3, 5.0, 'r', 'filled');


if hardwareOpt.KWave
    kWaveSetup=kwaveSimulation_Setup(struct('pPatch',   pPatch));
    kwaveInput.kgrid=kWaveSetup.kgrid;
    kwaveInput.medium=kWaveSetup.medium;
    kwaveInput.KwavePatch=kWaveSetup.KwavePatch;
    kwaveInput.input_signal=kWaveSetup.input_signal;
    kwaveInput.input_args=kWaveSetup.input_args;
    fs    = 1/kwaveInput.kgrid.dt; % Sampling frequency [Hz]
    c= kWaveSetup.c0; %background speed of sound
    %InOptions.plot_kgrid=0;InOptions.plot_subArrays=0;InOptions.plot_patch=1;
    %kWaveSetup.plot_geometry(InOptions, kwaveInput.KwavePatch,kwaveInput.kgrid,kwaveInput.medium);
    %  hold on;
    %  scatter3(kwaveInput.KwavePatch.elementPosCenters{1,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(2)*10^3, 10.0, 'r', 'filled');
    %  scatter3(kwaveInput.KwavePatch.elementPosCenters{4,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(2)*10^3, 10.0, 'r', 'filled');
    
    BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, [], kwaveInput, active_path, kWavepath);
else
    BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData,[],[],[]);
end

BfData_temp = BfData_raw(~all(cellfun(@isempty, BfData_raw),2),:);
maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
clear BfData_temp;

%% PostProcessing and compounding
Axis    = [];
Bmode3D = [];

outOptions = struct('filterBmode',     0, ...
    'slice',           'elevation', ...
    'outImgSize',      [imageSize(1) imageSize(2)], ...
    'alpha',           0, ... % attenuation
    'debug_plots',     false, ...
    'use_post_pad',    false, ...
    'save_fig',        0, ...
    'save_png',        0, ...
    'outFolder',       active_path, ...
    'maxBfData',       double(maxBfData));

if hardwareOpt.KWave
 temp=cell2mat(reshape(kwaveInput.KwavePatch.elementPosCenters, [1 size(kwaveInput.KwavePatch.elementPosCenters,1)*size(kwaveInput.KwavePatch.elementPosCenters,2)])');
 kwaveInput.KwavePatch.Tx.apex=mean(temp);
 kwaveInput.KwavePatch.Tx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.Tx.pitch=pPatch.Tx.pitch;
 kwaveInput.KwavePatch.Rx.apex=mean(temp);
 kwaveInput.KwavePatch.Rx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.compoundingProbe.subArrayPos=kwaveInput.KwavePatch.subArrayPosCenters;
 kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature=cellfun(@(x) -x, kwaveInput.KwavePatch.subArrayCurvature, 'UniformOutput', false);
 clear temp;
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, kwaveInput.KwavePatch, softwareOpt, outOptions);
else
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, pPatch, softwareOpt, outOptions);   
end

% for sectorial 2D compounding
% To better filter the background noise outside the object (non-essential, for not bright pixels <-60 and -40)
cmpdImg.filterPixels1=-60;%-60;
cmpdImg.filterPixels2=-30;%-40;

AzAxisPerSubArray=cell(1,numel(postProcessedPerSubArray));
ElAxisPerSubArray=cell(1,numel(postProcessedPerSubArray));
BmodePerSubArray=cell(1,numel(postProcessedPerSubArray));
for ii=1:numel(postProcessedPerSubArray)
    %                               figure(3); hold on;
    %                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
    %                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
    %                               p.AlphaData=0.7.*ones(size(bmode)); p.AlphaData((bmode==min(bmode(:))))=0;
    %                               set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
    %                               axis equal tight ij; colormap(gray);
    %                               saveas(gca, [active_path 'bmode1.png']);
    % %
    %                               figure(4);
    %                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
    %                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
    %                               axis equal tight ij; colormap(gray); caxis([-60 0]);colorbar;
    %                               set(gca, 'XDir','Normal');set(gca, 'YDir','Reverse');
    %                               saveas(gca, [active_path 'bmode2.png']);
    %
    
    if (size(postProcessedPerSubArray{1,1}.Bmode,1)>1)&(size(postProcessedPerSubArray{1,1}.Bmode,2)>1)
        [AzAxisPerSubArray,ElAxisPerSubArray, BmodePerSubArray]=TwoDimensionalCompounding(ii,postProcessedPerSubArray(1:ii),sector_image_plane,...
            outOptions, cmpdImg);
        postProcessedPerSubArray{ii}.Bmode=BmodePerSubArray{end};
        postProcessedPerSubArray{ii}.disp_az_lims=AzAxisPerSubArray{end};
        postProcessedPerSubArray{ii}.disp_dep_lims=ElAxisPerSubArray{end};
    end
    
    
end

if (size(postProcessedPerSubArray{1,1}.Bmode,1)>1)&(size(postProcessedPerSubArray{1,1}.Bmode,2)>1)
    figure(4); clf;
    postProcessed.disp_az_lims=AzAxisPerSubArray{end};
    postProcessed.disp_dep_lims=ElAxisPerSubArray{end};
    postProcessed.Bmode=BmodePerSubArray{end};%.*maxN;
    imagesc(postProcessed.disp_az_lims, postProcessed.disp_dep_lims,postProcessed.Bmode);
    colormap(gray);
    set(gca,'YDir','Reverse');
    axis equal tight;colorbar;
    caxis([-60 0]);
    saveas(gca, [active_path '_bmode_flat_compounded.png']);
end
if hardwareOpt.KWave                              
 CENTER = kwaveInput.KwavePatch.compoundingProbe.subArrayPos;
 ALPHA  = kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature;
else
 CENTER = pPatch.compoundingProbe.subArrayPos;
 ALPHA  = pPatch.compoundingProbe.subArrayCurvature;   
end    

compPosition = zeros(length(BfData_raw),3);
global plot_debug
plot_debug=1;
for ss = 1:length(BfData_raw)
    
    saIndex = softwareOpt.shotIndex(ss,:);
    compPosition(ss,:) = CENTER{saIndex(2), saIndex(1)};
    saAngle    = -ALPHA {saIndex(2), saIndex(1)};
    
    if size(BfData_raw{ss}.data,2)>1 %sectorial imaging
            switch sector_image_plane %ignore non-primary curvature for 2D image
                      case 'azimuth'
                               saAngle=[0 saAngle(2)];
                      case 'elevation'
                               saAngle=[saAngle(1) 0];
            end
    end
    inputOpt.imgSize=imageSize(2);
    inputOpt.sizeOfArray=[pPatch.Tx.subArraySize(2) pPatch.Tx.subArraySize(1)];
    inputOpt.sizeOfPatch=[pPatch.Tx.nElements_x pPatch.Tx.nElements_y];
    [xt,yt,zt,Bt]=prepareAxis(postProcessedPerSubArray{ss}, sector_image_plane, compPosition(ss,:), saAngle, inputOpt);
    Axis    = [Axis; [xt yt zt]];
    Bmode3D = [Bmode3D; Bt];
end

%% plotting
if hardwareOpt.KWave 
  step_3D_mesh=kwaveInput.kgrid.dx;%10^(-3)/2;%how precise the uniform 3D mesh is [m]
else
 step_3D_mesh=10^(-3)/2;   
end    

[mesh3D, BmodeGlobal, pointsDistance] = define_global_3D_mesh(Axis,step_3D_mesh);
[BmodeGlobal, pointsDistance]         = ThreeTwoDimensionalCompoundingv2 (Axis, Bmode3D, mesh3D, BmodeGlobal, pointsDistance);
BmodeGlobal(~isnan(BmodeGlobal))      = BmodeGlobal(~isnan(BmodeGlobal))./pointsDistance((~isnan(BmodeGlobal)));
BmodeGlobal(isnan(BmodeGlobal))   = -80;

% plot 3inOptions.plotsubArray=0;
% inOptions.plotSequence=0;
% inOptions.newFigure=1;
% pPatch.plot_geometry(inOptions, softwareOpt);

threshold=-60;
ind=find(BmodeGlobal>threshold);
figure(1); hold on;
scatter3(mesh3D.X_global_mesh(ind).*10^3,mesh3D.Y_global_mesh(ind).*10^3,mesh3D.Z_global_mesh(ind).*10^3, 2.0,BmodeGlobal(ind), 'filled');
%scatter3(Y_global_mesh(ind).*10^3,X_global_mesh(ind).*10^3,Z_global_mesh(ind).*10^3, 50.0,BmodeGlobal(ind), 'filled');
%colormap(flipud(hot)); 
colormap(gray); 
colorbar;
axis equal tight;colorbar;
caxis([threshold 0]); view(3);
zlabel('z'); ylabel('y'); xlabel('x');
saveas(gca, [active_path 'bmode_3Dview.png']);


%% projection Elevation
threshold=-90;ind=find(BmodeGlobal(:)>threshold);
[~,r]=min(abs(mesh3D.X_global_mesh(:)-mean(mesh3D.X_global_mesh(ind))));
ind=find((mesh3D.X_global_mesh==mesh3D.X_global_mesh(r)));
x=mesh3D.Z_global_mesh(ind);
y=mesh3D.Y_global_mesh(ind);
Bmode_Az_Proj=BmodeGlobal(ind);
ind=find(Bmode_Az_Proj(:)>threshold);
x=x(ind); y=y(ind); Bmode_Az_Proj=Bmode_Az_Proj(ind);

[X,Y]=meshgrid(linspace(min(x),max(x),1024),linspace(min(y),max(y),1024));  
F = scatteredInterpolant(x,y,Bmode_Az_Proj);
Bmode=F(X,Y);     
figure; 
imagesc(linspace(min(x),max(x),1024),linspace(min(y),max(y),1024),Bmode); 
axis equal tight ij; colormap(gray);
caxis([-60 0]);colorbar;
saveas(gca, [active_path 'bmode_projectionEl.png']);

%% projection Azimuth
threshold=-90;ind=find(BmodeGlobal(:)>threshold);
[~,r]=min(abs(mesh3D.Z_global_mesh(:)-mean(mesh3D.Z_global_mesh(ind))));
ind=find((mesh3D.Z_global_mesh==mesh3D.Z_global_mesh(r)));
x=mesh3D.X_global_mesh(ind);
y=mesh3D.Y_global_mesh(ind);
Bmode_Az_Proj=BmodeGlobal(ind);
ind=find(Bmode_Az_Proj(:)>threshold);
x=x(ind); y=y(ind); Bmode_Az_Proj=Bmode_Az_Proj(ind);

[X,Y]=meshgrid(linspace(min(x(Bmode_Az_Proj>threshold)),max(x(Bmode_Az_Proj>threshold)),1024),linspace(min(y(Bmode_Az_Proj>threshold)),max(y(Bmode_Az_Proj>threshold)),1024));  
F = scatteredInterpolant(x,y,Bmode_Az_Proj);
Bmode=F(X,Y);     
figure; 
imagesc(linspace(min(x),max(x),1024),linspace(min(y),max(y),1024),Bmode); 
axis equal tight ij; colormap(gray);
caxis([-60 0]);colorbar;
saveas(gca, [active_path 'bmode_projectionAz.png']);
end