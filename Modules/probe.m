classdef probe < handle
    % probe -- this class contains all the information and physical
    % dimensions of the probe (TX or RX).
    properties
        % all measures in meters
        shape            = 'flat'                                                % pre-programmed shapes for the patch.
        % options are 'flat', 'pca', 'pca_normalized', '2D' and 'custom'
        shapeNumber      = 1                                                     % for the when the pre-programed shape is indexes by a number
        nElements_x      = 166                                                   % elements in the x-axis
        nElements_y      = 166                                                   % elements in the y-axis
        width            =  534e-6                                                % width of element
        height           =  534e-6                                                % height of element
        kerf             =  66e-6
        name             = 'transmitter';
        apodization      = 'hanning'
        excitation       = 1
        impulseResponse  = gauspuls(-2/2.5e6:1/100e6:2/2.5e6, 2.5e6, 0.5)
        focus            = [0,0,0]
        centerOfAperture = [0,0,0]                                              % center point of the whole aperture
        nSub             = 32                                                   % For FieldII. No of sub-divisions of one elements

        elementPos                                                              % cell matrix. Position of the center of each element in 3D space
        subArrayPos                                                             % cell matrix. Position of the center of each subArray in 3D space
        subArrayCurvature                                                       % cell matrix. curvature of the patch at each subArray center
        elementEdges                                                            % cell matrix. Position of the edges of each element in 3D space
        elementCurvature                                                        % cell matrix. curvature of the patch at each element position
        centralElementPos                                                       % cell matrix. position of the elements for the central subarray
        subArraySize
    end
    
    properties (Access = private)
        preLoadedElementPos                                                     % pre-loaded shapes to speed up processing
        preLoadedEdges
    end
    
    properties (Dependent)
        apoMat                                                                  % apodization matrix
        FiiPointer                                                              % FieldII pointer, corresponds to the central sub-array
        FiiPointer_rectangles                                                   % FieldII pointer, can correspond to a non-central sub-array
        pitch                                                                   % kerf + width
        centralElementEdges                                                     % cell matrix. position of the edges the elements for the central subarray
    end
    
%     properties (Constant)
%         subArraySize  = 16
%     end
%     
    methods
        function set.elementPos(obj,val)   
            obj.elementPos=val;            
        end
       function set.subArrayPos(obj,val)            
           obj.subArrayPos=val;            
       end
       function set.subArrayCurvature(obj,val)  
            obj.subArrayCurvature=val;            
       end
       function set.elementEdges(obj,val)
            obj.elementEdges=val;            
       end
      function  set.elementCurvature (obj,val)            
          obj.elementCurvature =val;            
      end
      function set.centralElementPos  (obj,val)            
          obj.centralElementPos  =val;            
        end
        function obj = probe(inOptions)
            % probe - Probe constructor
            % Syntax:  obj = probe(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify probe object
            global plot_debug newStream
            if nargin > 0
                %% parser
                names  = fieldnames(inOptions);
                values = struct2cell(inOptions);
                
                for ii = 1:length(values)
                    if ~isprop(obj, names{ii})
                        error(['Probe: This object does not contain this property: ', names{ii}]);
                    end
                    
                    obj.(names{ii}) = values{ii};
                end
            end
            
            %% assertions
            assert(any(strcmp(obj.apodization, {'rectwin'; 'hanning'; 'taylorwin'})), ...
                'Probe: No apodization with this name')
            assert(any(strcmp(obj.shape, {'flat'; 'pca'; 'pca_normalized'; '2D'; 'custom'; 'subArray'; 'MCLpatch'; 'LCLpatch'})), ...
                'Probe: No shape with this name')
            
            %% Pre-loaded center of element
            if strcmp(obj.shape, 'flat')
                obj.shapeNumber = 1;
                obj.preLoadedElementPos{1} = obj.getElementPositionForFlatPatch;
            elseif strfind(obj.shape, 'pca_normalized')
                load('pca_patch_normalized', 'pca_patch');
                obj.preLoadedElementPos = pca_patch;
            elseif strfind(obj.shape, 'pca')
                load('pca_patch', 'pca_patch');
                obj.preLoadedElementPos = pca_patch;

            elseif strfind(obj.shape, '2D')
                load('twoDpatch', 'twoDpatch')
                obj.preLoadedElementPos{1} = twoDpatch;
            elseif strcmp(obj.shape, 'custom')||strcmp(obj.shape, 'MCLpatch')||strcmp(obj.shape, 'LCLpatch')
                switch obj.shape
                    case 'custom' 
                          load('custom_shape', 'custom');
                    case 'MCLpatch'     
% %                            load('skinMCL_remeshed_transducer_in_the_right_order.mat', 'custom');
% %                            custom.centers=cell2mat(custom.centers);
% %                            figure; hold on; s=10;
% %                            scatter3(custom.centers(1:s:end,1).*10^3,...
% %                                custom.centers(1:s:end,2).*10^3,...
% %                                custom.centers(1:s:end,3).*10^3, 2.0, 'b', 'filled');
% %                            scatter3(custom.centers(1,1)*10^3, custom.centers(1,2)*10^3, custom.centers(1,3)*10^3, 50.0, 'g', 'filled')
%                           load('skinMCL_remeshed_transducer_patch_120_94.mat', 'skinMCL_remeshed');                           
%                           custom.edges=cellfun(@(x)  skinMCL_remeshed.vertices(skinMCL_remeshed.faces(x,:),:)./10^3,num2cell(1:size(skinMCL_remeshed.faces,1)), 'UniformOutput', false)';
%                           custom.edges=rot90(reshape(custom.edges, [374 396]))';
%                           custom.centers=cellfun(@(x) mean(x), custom.edges, 'UniformOutput', false);
% 
% 
%                            %custom.edges=reshape(cellfun(@(x) [x(:,1) x(:,3) x(:,2)],custom.edges, 'UniformOutput', false), [187 198]);
%                           %custom.centers=reshape(cellfun(@(x) [x(1) x(3) x(2)],custom.centers,'UniformOutput', false), [187 198]);
%                           custom.edges=reshape(cellfun(@(x) [x(:,1) x(:,3) x(:,2)],custom.edges, 'UniformOutput', false), [374 396]);
%                           custom.centers=reshape(cellfun(@(x) [x(1) x(3) x(2)],custom.centers,'UniformOutput', false), [374 396]);
%                           
% %                           figure; hold on; s=10; pp=cell2mat(custom.centers(:));
% %                           scatter3(pp(1:s:end,1).*10^3,...
% %                                pp(1:s:end,3).*10^3,...
% %                                pp(1:s:end,2).*10^3, 2.0, 'r', 'filled');
% %                           scatter3(custom.centers{1,1}(1)*10^3, custom.centers{1,1}(3)*10^3, custom.centers{1,1}(2)*10^3, 50.0, 'g', 'filled');
% %                           scatter3(custom.centers{374,1}(1)*10^3, custom.centers{374,1}(3)*10^3, custom.centers{374,1}(2)*10^3, 50.0, 'g', 'filled');   
% %                           scatter3(custom.centers{1,396}(1)*10^3, custom.centers{1,396}(3)*10^3, custom.centers{1,396}(2)*10^3, 50.0, 'g', 'filled');   
% %                           scatter3(custom.centers{374,396}(1)*10^3, custom.centers{374,396}(3)*10^3, custom.centers{374,396}(2)*10^3, 50.0, 'g', 'filled');  
% %              
%                           
%                           custom.centers= cellfun(@(x) x+[-0.0435    0.0201    0.0577], custom.centers, 'UniformOutput', false); %to move with the phantom
%                           custom.edges= cellfun(@(x) x+[-0.0435    0.0201    0.0577], custom.edges, 'UniformOutput', false); %to move with the phantom
%                           custom.centers= cellfun(@(x) (...
%                              (PP_Utils.rotz(0*pi/180)*...
%                               PP_Utils.roty(60*pi/180)*...
%                               PP_Utils.rotx(0*pi/180)) * x')'+[0 -0.02 -0.04], custom.centers, 'UniformOutput', false);
%                           custom.edges= cellfun(@(x) (...
%                              (PP_Utils.rotz(0*pi/180)*...
%                               PP_Utils.roty(60*pi/180)*...
%                               PP_Utils.rotx(0*pi/180)) * x')'+[0 -0.02 -0.04], custom.edges, 'UniformOutput', false);      
%                           save('skinMCL_remeshed_transducer_in_the_right_order_ready_to_use_in_the_simulator_120_94.mat', 'custom');
                          load('skinMCL_remeshed_transducer_in_the_right_order_ready_to_use_in_the_simulator_120_94.mat', 'custom');
                          %figure; hold on;
                          %cellfun(@(x) patch('XData', x(:,1).*10^3,'YData',x(:,3).*10^3,'ZData',x(:,2).*10^3, 'FaceColor', 'b', 'EdgeColor', 'k', 'FaceAlpha', 0.5), custom.edges,'UniformOutput', false);
                          %load('skinMCL_remeshed_transducer_in_the_right_order_ready_to_use_in_the_simulator.mat', 'custom');
                          %cellfun(@(x) patch('XData', x(:,1).*10^3,'YData',x(:,3).*10^3,'ZData',x(:,2).*10^3, 'FaceColor', 'r', 'EdgeColor', 'k', 'FaceAlpha', 0.5), custom.edges,'UniformOutput', false);
                          %custom.centers=custom.centers'; custom.edges=custom.edges';
                    case 'LCLpatch'  
                          load('skinLCL_remeshed_transducer_in_the_right_order.mat', 'custom');
                          custom.edges=reshape(cellfun(@(x) [x(:,1) x(:,3) x(:,2)],custom.edges, 'UniformOutput', false), [166 189]);
                          custom.centers=reshape(cellfun(@(x) [x(1) x(3) x(2)],custom.centers,'UniformOutput', false), [166 189]);
                          custom.centers= cellfun(@(x) x+[-0.0435    0.0201    0.0577], custom.centers, 'UniformOutput', false);
                          custom.edges= cellfun(@(x) x+[-0.0435    0.0201    0.0577], custom.centers, 'UniformOutput', false);
                          custom.centers= cellfun(@(x) (...
                             (PP_Utils.rotz(0*pi/180)*...
                              PP_Utils.roty(90*pi/180)*...
                              PP_Utils.rotx(0*pi/180)) * x')'+[0 -0.02 -0.04], custom.centers, 'UniformOutput', false);
                          custom.edges= cellfun(@(x) (...
                             (PP_Utils.rotz(0*pi/180)*...
                              PP_Utils.roty(90*pi/180)*...
                              PP_Utils.rotx(0*pi/180)) * x')'+[0 -0.02 -0.04], custom.edges, 'UniformOutput', false);  
                          save('skinLCL_remeshed_transducer_in_the_right_order_ready_to_use_in_the_simulator.mat', 'custom');
                          load('skinLCL_remeshed_transducer_in_the_right_order_ready_to_use_in_the_simulator.mat', 'custom');
                end
                if obj.nElements_x<size(custom.centers,1)
                    indx1=round(size(custom.centers,1)/2)-obj.nElements_x/2;
                    indx2=round(size(custom.centers,1)/2)+obj.nElements_x/2-1; 
                    if strcmp(obj.shape,'MCLpatch'), indx1=indx1-2*18; indx2=indx2-2*18; end %indx1=indx1-18; indx2=indx2-18; end
                else
                   indx1=1; indx2=size(custom.centers,1);
                end  
                
                if (obj.nElements_x==1), indx2=round(indx1); indx1=round(indx1); end
                
                if obj.nElements_y<size(custom.centers,2)
                   indy1=round(size(custom.centers,2)/2)-obj.nElements_y/2;
                   indy2=round(size(custom.centers,2)/2)+obj.nElements_y/2-1;
                else
                   indy1=1; indy2=size(custom.centers,2);
                end
                
                if (obj.nElements_y==1), indy2=round(indy1); indy1=round(indy1); end
                
                obj.preLoadedElementPos = custom.centers(indx1:indx2, indy1:indy2)'; %THIS WAS CHANGED
                obj.preLoadedEdges = custom.edges(indx1:indx2, indy1:indy2)'; %THIS WAS CHANGED
                
                if strcmp(obj.shape, 'custom') %for knee ligaments patch
                  obj.preLoadedElementPos=cellfun(@(x) x-[-0.0385    0.0300    0.0124], obj.preLoadedElementPos, 'UniformOutput', false);
                  obj.preLoadedEdges =cellfun(@(x) x-repmat([-0.0385    0.0300    0.0124], [4,1]), obj.preLoadedEdges,'UniformOutput', false);
                  clear custom
                end 
                if strcmp(obj.shape, 'MCLpatch')||strcmp(obj.shape, 'LCLpatch')
                    centerAperture=mean(cell2mat(reshape(obj.preLoadedElementPos, [size(obj.preLoadedElementPos,1)*size(obj.preLoadedElementPos,2) 1])));
                    clear custom
                end
                %{
                 figure; hold on;
                 cellfun(@(x) patch('XData', x(:,1),'YData',x(:,3),'ZData',x(:,2), 'FaceColor', 'b', 'EdgeColor', 'k'), obj.preLoadedEdges,'UniformOutput', false);
                 cellfun(@(x) scatter3(x(1),x(3),x(2),2.0, 'r', 'filled'), obj.preLoadedElementPos,'UniformOutput', false);
                %}
            elseif strfind(obj.shape, 'subArray')
                obj.preLoadedElementPos = inOptions.elementPos;
            end
            
            if plot_debug
                figure(1);
                scatter3(obj.centerOfAperture(1)*1000, obj.centerOfAperture(3)*1000, obj.centerOfAperture(2)*1000, 20.0, 'filled', 'yellow');
            end
            
        end
        
        function pitch = get.pitch(obj)
            % get.picth - returns picth in meters
            %
            % Syntax:  elementEdges = get.elementEdges(obj)
            
            pitch = obj.width + obj.kerf;
            
        end
        
        function elementPos = get.elementPos(obj)
            % get.subArrayPos - returns cells with all element
            % positions. elementPos is a cell matrix. Each element is
            % a 1x3 vector
            %
            % Syntax:  elementPos = get.elementPos(obj)
            if isempty(obj.elementPos)
                %% load from preLoadedElementPos when possible
                if isempty(obj.preLoadedElementPos)
                    if strcmp(obj.shape, 'flat')
                        elementPos = obj.getElementPositionForFlatPatch;
                    elseif strfind(obj.shape, 'pca')
                        load('pca_patch', 'pca_patch');
                        elementPos = pca_patch{obj.shapeNumber};
                    elseif strfind(obj.shape, 'pca_normalized')
                        load('pca_normalized', 'pca_patch');
                        elementPos = pca_patch{obj.shapeNumber};
                    elseif strfind('2D')
                        load('twoDpatch', 'twoDpatch')
                        obj.preLoadedElementPos{1} = twoDpatch;
                    else
                        obj.shape = 'custom';
                        load('custom_shape', 'custom')
                        elementPos{1} = custom{obj.shapeNumber};
                    end
                elseif strcmp(obj.shape, 'custom')||strcmp(obj.shape, 'subArray')||strcmp(obj.shape, 'MCLpatch')||strcmp(obj.shape, 'LCLpatch')
                    elementPos = obj.preLoadedElementPos;
                else
                    elementPos = obj.preLoadedElementPos{obj.shapeNumber};
                end
                
            if ~strcmp(obj.shape, 'subArray')
             aux = obj.convert_cell_to_Fii_mat(elementPos);   
             elementPos = cellfun(@(x) x-mean(aux)+obj.centerOfAperture, elementPos, 'UniformOutput', false);
            end
          else
            elementPos = obj.elementPos;        
          end    
        end
        
        function subArrayPos = get.subArrayPos(obj)
            % get.subArrayPos - returns cells with all possible
            % sub-array positions. subArrayPos is a smaller cell matrix
            % than elementPos. Because subArraySize = 16, element and
            % sub-array positions do not coincide
            %
            % Syntax:  subArrayPos = get.subArrayPos(obj)
           if isempty(obj.subArrayPos) 
            if (obj.subArraySize(1)>1)&(obj.subArraySize(2)>1)
               cut = obj.elementPos(obj.subArraySize(1)/2:end-obj.subArraySize(1)/2+1,obj.subArraySize(2)/2:end-obj.subArraySize(2)/2+1);
               cutMat = cell2mat(cut);
               %% 2D filter obtains the average position of 4 neighboring elements
               subArrayPos = filter2([1 0 0 1; 1 0 0 1], cutMat, 'valid')/4;
               subArrayPos = mat2cell(subArrayPos, ones(size(subArrayPos,1),1), 3*ones(1,size(subArrayPos,2)/3));
            elseif obj.subArraySize(1)==1
               %subArrayPos = obj.elementPos(1,obj.subArraySize(2)/2:end-obj.subArraySize(2)/2+1);
               subArrayPos = obj.elementPos(1,obj.subArraySize(2)/2:end-obj.subArraySize(2)/2);
            elseif obj.subArraySize(2)==1
               %subArrayPos = obj.elementPos(obj.subArraySize(1)/2:end-obj.subArraySize(1)/2+1,1);
               subArrayPos = obj.elementPos(obj.subArraySize(1)/2:end-obj.subArraySize(1)/2,1);
            end   
           else
            subArrayPos=obj.subArrayPos;   
           end    
        end
        
        function centralElementPos = get.centralElementPos(obj)
            % get.elementPos - returns center of the element of the central sub-array in meters
            %
            % Syntax:  centralElementPos = get.centralElementPos(obj)
            %
            % Outputs:
            % centralElementPos - 16x16 cell matrix. Element positions of the central sub-array
            centralElementPos=obj.elementPos;
%             if mod(obj.nElements_x,2) == 0
%                 x = obj.pitch*(-obj.subArraySize/2:obj.subArraySize/2-1) + obj.pitch/2;
%             else
%                 x = obj.pitch*(-(obj.subArraySize-1)/2:(obj.subArraySize-1)/2);
%             end
%             
%             if mod(obj.nElements_y,2) == 0
%                 y = obj.pitch*(-obj.subArraySize/2:obj.subArraySize/2-1) + obj.pitch/2;
%             else
%                 y = obj.pitch*(-(obj.subArraySize-1)/2:(obj.subArraySize-1)/2);
%             end
%             
%             YY = repmat(y(end:-1:1), length(x), 1);
%             XX = repmat(x, 1, length(y));
%             NN = length(XX);
%             centralElementPos =  reshape(mat2cell([XX(:) YY(:) zeros(NN,1)] + obj.centerOfAperture, ones(NN,1), 3), realsqrt(NN), realsqrt(NN))';
            
        end
        
        function subArrayCurvature = get.subArrayCurvature(obj)
            % get.subArrayCurvature - returns cells with the curvature of the patch on the subarray positions.
            %
            % Syntax:  subArrayCurvature = get.subArrayCurvature(obj)
            if (isempty(obj.subArrayCurvature))
                if (obj.subArraySize==[obj.nElements_y obj.nElements_x])
                    subArrayCurvature{1,1} = [0 0];
                else
                    centerSA = obj.subArrayPos;
                    
                    %% shift
                    centerSA_shiftRight = circshift(centerSA, 1, 2);
                    centerSA_shiftDown  = circshift(centerSA, 1, 1);
                    
                    %% difference to between the original and shifted matrices gives the argument to the atan function
                    diffAz = cellfun(@(X,Y) X-Y, centerSA_shiftRight, centerSA, 'UniformOutput', false);
                    diffAz(:,1) = diffAz(:,2);
                    diffEl = cellfun(@(X,Y) X-Y, centerSA_shiftDown , centerSA, 'UniformOutput', false);
                    diffEl(1,:) = diffEl(2,:);
                    
                    azRot = cellfun(@(x) atan(x(3)/x(1)), diffAz);
                    elRot = cellfun(@(x) atan(x(3)/x(2)), diffEl);
                    
                    subArrayCurvature = num2cell(azRot + 1j*elRot);
                    subArrayCurvature = cellfun(@(x) [real(x), imag(x)], subArrayCurvature, 'UniformOutput', false);
                end
            else
                if ~iscell(obj.subArrayCurvature) 
                    subArrayCurvature{1}=obj.subArrayCurvature; 
                else
                    subArrayCurvature=obj.subArrayCurvature;    
                end
            end
        end
        
        function elementCurvature = get.elementCurvature(obj)
            % get.subArrayCurvature - returns cells with the curvature of the patch on the position of the elements.
            %
            % Syntax:  elementCurvature = get.elementCurvature(obj)
            
            centerSA = obj.elementPos;
            
            %% shift
            centerSA_shiftRight = circshift(centerSA, 1, 2);
            centerSA_shiftDown  = circshift(centerSA, 1, 1);
            
            %% difference to between the original and shifted matrices gives the argument to the atan function
            if (size(centerSA,2)>1)
             diffAz = cellfun(@(X,Y) X-Y, centerSA_shiftRight, centerSA, 'UniformOutput', false);
             diffAz(:,1) = diffAz(:,2);
             azRot = cellfun(@(x) atan(x(3)/x(1)), diffAz);
            else
             azRot=0;   
            end
            if (size(centerSA,1)>1)
             diffEl = cellfun(@(X,Y) X-Y, centerSA_shiftDown , centerSA, 'UniformOutput', false);
             diffEl(1,:) = diffEl(2,:);
             elRot = cellfun(@(x) atan(x(3)/x(2)), diffEl);
            else
             elRot=0;    
            end
            
            elementCurvature = num2cell(azRot + 1j*elRot);
            elementCurvature = cellfun(@(x) [real(x), imag(x)], elementCurvature, 'UniformOutput', false);
            
        end
        
        function elementEdges = get.elementEdges(obj)
            % get.elementEdges - returns cells with edges of each element in meters
            %
            % Syntax:  elementEdges = get.elementEdges(obj)
            %
            % Outputs:
            % elementEdges - cell with edges of elements, starting from lower left,
            % moving right and up. Each cell is a 4x3 matrix
            
            %% elementEdges of elements, starting from lower left, running counterclockwise

            if strcmp(obj.shape, 'flat')
                C     = obj.elementPos;
                ALPHA = obj.elementCurvature;
                
                M = [-obj.width/2 -obj.height/2 0; ...
                    +obj.width/2 -obj.height/2 0; ...
                    +obj.width/2 +obj.height/2 0; ...
                    -obj.width/2 +obj.height/2 0];
                
                elementEdges = cellfun(@(x,alpha) x +  (PP_Utils.rotx(-alpha(2))*PP_Utils.roty(-alpha(1))*M')', C, ALPHA, 'UniformOutput', false);
            else
                elementEdges=obj.preLoadedEdges;
                aux = obj.convert_cell_to_Fii_mat(obj.preLoadedElementPos);
                elementEdges = cellfun(@(x) x-mean(aux)+obj.centerOfAperture, elementEdges, 'UniformOutput', false);
                
            end
        end
        
%         function centralElementEdges = get.centralElementEdges(obj)
%             % get.centralElementEdges - returns cells with edges of the elements of the 
%             % central sub-array in meters
%             %
%             % Syntax:  elementEdges = get.elementEdges(obj)
%             %
%             % Outputs:
%             % centralElementEdges - cell with edges of elements, starting from lower left,
%             % moving right and up. Each cell is a 4x3 matrix
%             
%             %% elementEdges of elements, starting from lower left, running counterclockwise
%             % Elements are arranged starting from lower left and moving right and then moving
%             % up
%             C     = obj.centralElementPos;
%             M = [-obj.width/2 -obj.height/2 0; ...
%                 +obj.width/2 -obj.height/2 0; ...
%                 +obj.width/2 +obj.height/2 0; ...
%                 -obj.width/2 +obj.height/2 0];
%             
%             centralElementEdges = cellfun(@(x) x + M, C, 'UniformOutput', false);
%             
%         end
        
        function FiiPointer = get.FiiPointer(obj)
            % get.FiiPointer - returns Field II pointer for a subarray
            % centered at (0,0,0)
            %
            % Syntax:  FiiPointer = get.FiiPointer(obj)
            
            nElementsX = obj.subArraySize(2);
            nElementsY = obj.subArraySize(1);
            
            FiiPointer = xdc_2d_array(nElementsX, nElementsY, obj.width, obj.height, obj.kerf, obj.kerf, ones(nElementsX ,nElementsY), obj.nSub, obj.nSub, obj.focus);
            
            %% Set the impulse response and excitation of the emit aperture
            xdc_impulse(FiiPointer, obj.impulseResponse);
            xdc_excitation(FiiPointer, obj.excitation);
            
        end
        

        function FiiPointer_rectangles = get.FiiPointer_rectangles(obj)
            %created by Kate based on M arcus's function
            %createCustomFieldIIApertures
            nElementsX = obj.subArraySize(1);
            nElementsY = obj.subArraySize(2);
            
            rectangles = zeros(nElementsX*nElementsY,19);
            rectangles(:,1)  = 1:nElementsX*nElementsY;        % line number
            rectangles(:,14) = ones(1,nElementsX*nElementsY);  % apodization
            rectangles(:,15) = obj.width;
            rectangles(:,16) = obj.height;
            
            E = obj.centralElementEdges;
            
            rectangles(:,2:4)   = cell2mat(cellfun(@(x) x(1,:), E(:), 'UniformOutput', false)); %first corners
            rectangles(:,5:7)   = cell2mat(cellfun(@(x) x(2,:), E(:), 'UniformOutput', false)); %second corners
            rectangles(:,8:10)  = cell2mat(cellfun(@(x) x(3,:), E(:), 'UniformOutput', false)); %third corners
            rectangles(:,11:13) = cell2mat(cellfun(@(x) x(4,:), E(:), 'UniformOutput', false)); %fourth corners
            
            rectangles(:,17:19) = cell2mat(cellfun(@(x) x, obj.centralElementPos(:),'UniformOutput',false)); %rectangles centers
            
            FiiPointer_rectangles = xdc_rectangles(rectangles, rectangles(:,17:19), obj.focus);
            
            %% Set the impulse response and excitation of the aperture
            xdc_impulse(FiiPointer_rectangles, obj.impulseResponse);
            xdc_excitation(FiiPointer_rectangles, obj.excitation);
        end
        
        function elementPos = getElementPositionForFlatPatch(obj)
            % getElementPositionForFlatPatch - returns center of each element in
            % meters for a flat patch
            %
            % Syntax:  elementPos = getElementPositionForFlatPatch(obj)
            %
            % Outputs:
            % elementPos - center of elements. In 'matrix' form. Starts form the
            % from upper left, moving  right and down
            % sign(elementPos{1,1} = [- +])
            % sign(elementPos{1,end} = [+ +])
            % sign(elementPos{end,1} = [- -])
            % sign(elementPos{end,end} = [+ -])
            
            if (mod(obj.nElements_x,2) == 0) &&(obj.nElements_x>1)
                x = ((obj.pitch)*(-obj.nElements_x/2:obj.nElements_x/2-1)) + (obj.pitch)/2;
            elseif (obj.nElements_x>1)
                x = (obj.pitch)*(-(obj.nElements_x-1)/2:(obj.nElements_x-1)/2);
            else
                x = 0;
            end
            
            if (mod(obj.nElements_y,2) == 0) && (obj.nElements_y>1)
                y = ((obj.pitch)*(-obj.nElements_y/2:obj.nElements_y/2-1)) + (obj.pitch)/2;
            elseif  (obj.nElements_y>1)
                y = (obj.pitch)*(-(obj.nElements_y-1)/2:(obj.nElements_y-1)/2);
            else
                y = 0;
            end
            
            if (obj.nElements_x==obj.nElements_y)
                Y = repmat(y(end:-1:1), length(x), 1);
                X = repmat(x, 1, length(y));
                NN = length(X);
                elementPos = reshape(mat2cell([X(:) Y(:) zeros(NN,1)], ones(NN,1), 3), realsqrt(NN), realsqrt(NN))';
            else
                Y = reshape(repmat(y(end:-1:1), length(x), 1), 1,length(y)*length(x));
                X = repmat(x,1,length(y));
                NN= length(X);
                elementPos = reshape(mat2cell([X(:) Y(:) zeros(NN,1)], ones(NN,1), 3), length(x), length(y))'; 
            end    
        end
        
        function  apoMat_new = getApoMat(obj, hardwareOpt)
            % getApoMat - returns apodization matrix
            %
            % Syntax:  apoMat_new = getApoMat(obj, hardwareOpt)
            %
            % Inputs:
            % hardwareOpt -hardware options
            %
            % Outputs:
            % apoMat_new - nElements_xxnElements_y matrix with apodization
            % values
            
            nX = obj.subArraySize(2)/(1*~hardwareOpt.PMUT_level + sqrt(hardwareOpt.PMUTPerElement)*hardwareOpt.PMUT_level);
            nY = obj.subArraySize(1)/(1*~hardwareOpt.PMUT_level + sqrt(hardwareOpt.PMUTPerElement)*hardwareOpt.PMUT_level);
            
            if strcmp(obj.name, 'transmitter') && strcmp(hardwareOpt.mode_tx, 'tx_on_rows')
                nX = 1;
            end
            
            if strcmp(obj.name, 'receiver')
                if strcmp(hardwareOpt.mode_rx, 'rx_on_cols')
                    nY = 1;
                elseif strcmp(hardwareOpt.mode_rx, 'rx_on_cols_v2')
                    nY = 2;
                elseif strcmp(hardwareOpt.mode_rx, 'rx_on_rows')
                    nX = 1;
                end
            end
            
            %% probe apodization
            switch obj.apodization
                case 'rectwin'
                    apoMat_new = ones(nY, nX);
                case 'taylorwin'
                    vec1 = taylorwin(nX);
                    vec2 = taylorwin(nY);
                    apoMat_new = vec2*vec1';
                case 'hanning'
                    vec1 = hanning(nX);
                    vec2 = hanning(nY);
                    apoMat_new = vec2*vec1';
            end
            
            apoMat_new = flipud(apoMat_new)/max(apoMat_new(:));
            
            if strcmp(obj.name, 'transmitter') && hardwareOpt.hadamard
                W = hadamard(obj.subArraySize);
                apoMat_new = diag(apoMat_new)*W;
            end
            
            if strcmp(obj.name, 'receiver') && strcmp(hardwareOpt.mode_rx, 'rx_on_cols_v2')
                apoMat_new = apoMat_new';
            end
            
            % FIXME: use containsstr
            if (strcmp(obj.name, 'transmitter') && ~isempty(strfind(hardwareOpt.mode_tx, 'rows'))) || (strcmp(obj.name, 'receiver') && ~isempty(strfind(hardwareOpt.mode_rx, 'rows')))
                apoMat_new = kron(apoMat_new, ones(nY*nX, 1));
            elseif (strcmp(obj.name, 'receiver') && ~isempty(strfind(hardwareOpt.mode_rx, 'cols')))
                apoMat_new = repmat(apoMat_new(:), nX/nY, 1);
            end
            
            if hardwareOpt.PMUT_level
                apoMat_new = kron(apoMat_new, ones(hardwareOpt.PMUTPerElement,1));
            end
            
        end
        
        function [delay_line] = computeTxDelayLine(obj, tx_angle, tx_focus, crystal_pos, varargin)
            % computeTxDelayLine creates the transmit beamforming delays for focused, diverging or plane waves.
            % [DELAY_LINE] = computeTxDelayLine(TX_RANGE, TX_FOCUS, TX_APEX, ELEMENT_POS, c, DW_SLIDING)
            %
            % Inputs:  TX_ANGLES transmit angles [Angle_az; Angle_el] [deg]
            %          TX_FOCUS coordinates of the transmit focal point [x y z] [m]
            %          TX_APEX coordinates of the transmit (virtual) apex [x y z] [m]
            %          crystal_pos coordinates of the elements [N_az N_el x|y|z] [m]
            %          c speed of sound [m/s]
            %          DWSlidingTx: TRUE for a DW sequence sliding the transmit verticies, FALSE for tilting approach
            %
            % Outputs: DELAY_LINE: two-way delays [n_ranges n_channels] [s]
            %
            % See also: COMPUTERXDELAYLINE
            %
            % Pedro Santos (KUL, 2018)
            if isempty(varargin)
              global c
            else
			   c=varargin{1};
			end

            crystal_pos = reshape(crystal_pos, [], 3); % [nAz*nEl x|y|z]
            crystal_pos = shiftdim(crystal_pos', -1);   % [1  x|y|z  N]
            
            tx_apex = obj.centerOfAperture;
            %scatter3(tx_apex(1).*10^3, tx_apex(3).*10^3, tx_apex(2).*10^3, 50.0, 'r', 'filled');
            %scatter3(tx_focus(1).*10^3, tx_focus(3).*10^3, tx_focus(2).*10^3, 100.0, 'k', 'filled');
            %crystal_pos=squeeze(crystal_pos)';
            %scatter3(crystal_pos(:,1).*10^3, crystal_pos(:,3).*10^3, crystal_pos(:,2).*10^3, 50.0, 'g', 'filled');
            if ~isinf(tx_focus(3)) && tx_focus(3) ~= 0 	% Focused & Diverging beams
                
                orig2Foc   = bsxfun(@minus, tx_focus, tx_apex);
                orig2Foc   = sqrt(sum(orig2Foc .* orig2Foc, 2));              % distance from tx origin to tx focus
                elem2Foc   = bsxfun(@minus, tx_focus, crystal_pos);
                elem2Foc   = sqrt(sum(elem2Foc .* elem2Foc, 2));              % distance from elements to tx focus
                
                delay_line = sign(tx_focus(3)) * (orig2Foc - elem2Foc(:,1,:))/c;  % d = |Orig2Foc| - |Elem2Foc| & account for DW delays if needed
                delay_line = permute(delay_line, [1 3 2]);                    % remove empty dim
                
            else % Plane wave
                
                tx_norm    = PP_Utils.beam2cart(tx_angle(1), tx_angle(2), 100);    % Plane normal vector
                elem2Plane = bsxfun(@minus, crystal_pos, tx_norm);              % Distance vector from the element to the plane
                elem2Plane = sqrt(sum(elem2Plane .* elem2Plane, 2));
                
                delay_line = (1 - elem2Plane)/c; % subtract the norm of tx_norm (== 1)
                
            end
        end
        
        function [delay_line] = computeRxDelayLine(obj, tx_focus, img_ranges, rx_angles, crystal_pos, tx_angle,varargin)
            % computeRxDelayLine creates the two-way beamforming delays for focused, diverging or plane waves.
            % [DELAY_LINE] = computeRxDelayLine(TX_FOCUS, RX_RANGES, RX_ANGLES,  ELEMENT_POS, c,  TX_ANGLE)
            %
            % Inputs:  TX_FOCUS coordinates of the transmit focal point [x y z] [m]
            %          img_ranges array with depths of the receive points [m]
            %          RX_ANGLES receive angles [Angle_az; Angle_el] [deg]
            %          crystal_pos coordinates of the elements [N_az N_el x|y|z] [m]
            %          TX_ANGLE: angle of the transmit beam [Angle_az; Angle_el] [deg]
            %
            % Outputs: DELAY_LINE: two-way delays [n_ranges n_channels] [s]
            %
            % Pedro Santos (KUL, 2016)
            
            if isempty(varargin)
                 global c
            else
		         c=varargin{1};
	        end

            global plot_debug

            %scatter3(crystal_pos(:,1).*10^3, crystal_pos(:,3).*10^3,crystal_pos(:,2).*10^3, 10.0, 'g', 'filled');
            crystal_pos = reshape(crystal_pos, [], 3); % [nAz*nEl x|y|z]
            crystal_pos = shiftdim(crystal_pos', -1);   % [1  x|y|z  N]
            if (tx_focus(1, 3) > 0)||(obj.centerOfAperture(3)<tx_focus(1, 3)) % focused
                %     if ~all(rx_apex ==0)
                %         warndlg('rx_apex has not been tested yet')
                %     end
                if iscell(obj.subArrayCurvature), 
                    curvature=obj.subArrayCurvature{1}; 
                else
                    curvature=obj.subArrayCurvature;
                end
                rxFoc      = PP_Utils.beam2cart(rx_angles(1)+rad2deg(curvature(1)), rx_angles(2)+rad2deg(curvature(2)), img_ranges); % coordinates of the rx line to reconstruct
                %scatter3(obj.centerOfAperture(1).*10^3, obj.centerOfAperture(3).*10^3,obj.centerOfAperture(2).*10^3, 50.0, 'r', 'filled');
                rxFoc=rxFoc+reshape(obj.centerOfAperture, [1 3]); 
                if plot_debug
                    figure(1);
                    hold on;scatter3(rxFoc(:,1).*1000,rxFoc(:,3).*1000,rxFoc(:,2).*1000,5.0, 'r', 'filled');
                end
                %hold on;scatter3(rxFoc(:,1).*1000,rxFoc(:,3).*1000,rxFoc(:,2).*1000,5.0, 'magenta', 'filled');
                rxOrig2Foc = bsxfun(@minus, rxFoc, reshape(obj.centerOfAperture, [1 3]));
                rxOrig2Foc = sqrt(sum(rxOrig2Foc .* rxOrig2Foc, 2)); % distance from rx origin to rx line positions
                
                elem2Foc   = bsxfun(@minus, rxFoc, crystal_pos);
                elem2Foc   = sqrt(sum(elem2Foc .* elem2Foc, 2)); % (faster than elem2Foc.^2)
                
                delay_line = bsxfun(@plus, rxOrig2Foc,  elem2Foc(:,1,:)) / c;  % d = sqrt(|Orig2Foc|^2 + |Elem2Foc|^2)
                delay_line  = permute(delay_line, [1 3 2]); % remove empty dim
                
            else
                
                % RX Delays
                rxFoc      = PP_Utils.beam2cart(rx_angles(1), rx_angles(2), img_ranges); % coordinates of the rx line to reconstruct
                elem2Foc   = bsxfun(@minus, rxFoc, crystal_pos);
                elem2Foc   = sqrt(sum(elem2Foc .* elem2Foc, 2));
                delay_line = elem2Foc(:,1,:);  % d = sqrt(|Orig2Foc|^2 + |Elem2Foc|^2)
                rxDelay  = permute(delay_line, [1 3 2]); % remove empty dim
                
                % TX Delays
                if tx_focus(1, 3) < 0 % DW
                    rx2VFoc = bsxfun(@minus, rxFoc, tx_focus);
                    rx2VFoc = sqrt(sum(rx2VFoc .* rx2VFoc, 2));     % distance from rx origin to rx line positions
                    r = norm(tx_focus);  % faster
                    txDelay = rx2VFoc - r;
                    
                else % PW
                    txDelay = sqrt(sum(rxFoc .* rxFoc, 2) .* abs(cosd(tx_angle(1) - rx_angles(1)))); % projection of RxPosition into TxAngle
                end
                
                % Two way delay computation
                delay_line = bsxfun(@plus, txDelay, rxDelay) ./ c;
            end
            
        end
    end
    
    methods(Static)
        function mat = convert_cell_to_Fii_mat(cell_mat)
            mat = cell_mat; %flipud(cell_mat)';
            mat = cell2mat(reshape(cell_mat', [size(cell_mat,1)*size(cell_mat,2) 1]));
        end 
    end    
end

