classdef phantom
    % phantom -- this class contains all the information on the phantom
    properties
        % all measures in meters
        nPoints    = 1                                      % number of scatterers                                                      
        position   = [0, 0, 0.05]                           % scatterer positions
        positionForCalc = [0,0,0.05]
        amplitude  = 1;                                     % scatterer amplitudes
        predefined = 'none'                                 % predefined phantom
        radius = [0.01,0]                                   % radius of phantom; active for some and different for each predefined phantom
        rotationAngle = [0, 0, 0]                           % rotation of phantom onx, y and z-axis
        center = [0,0,0]                                    % center of phantom
        frame  = 0                                          % used to index for phantom data in the time domain
        ellipsoidData = [1, 3, 0.01]                        % for ellipsoid, [y_stretch1, y_stretch2, cut]
        segmentationIndex   = {};                           % for segmented data; cell; index for position and amplitude for one class
    end
    
    properties (GetAccess = 'public' , SetAccess = 'private')
        isSegmented          = 0;                           % for segmented data
        segmentationClasses = {};                           % for segmented data, string with classes
        ribs                                                % by Katya: for saving not only 3D points, but also faces, since ribs are the triangulated surface
    end
    
    properties (Dependent)
        volume_ml                          
        position_at_origin_mean
        position_at_origin_minmax
    end
    
     properties(Hidden, Constant)
        defaultIndex = [];
    end
    
    methods
        function obj = phantom(varArgIn)
            % phantom - phantom constructor
            % Syntax:  obj = phantom(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify phantom object
            
            if nargin > 0
                %% parser
                names  = fieldnames(varArgIn);
                values = struct2cell(varArgIn);
                
                for ii = 1:length(values)
                    if ~isprop(obj, names{ii})
                        error(['Phantom: This object does not contain this property: ', names{ii}])
                    end
                    
                    obj.(names{ii}) = values{ii};
                end
            end
            
            %% validation
            obj = validatePhantom(obj);
            
            switch obj.predefined
                case 'none'
                     'do notthing, dude'; 
                case 'random3dSphere'
                    rvals = 2*rand(obj.nPoints,1)-1;
                    elevation = asin(rvals);
                    azimuth = 2*pi*rand(obj.nPoints,1);
                    radii = obj.radius(1)*(rand(obj.nPoints,1).^(1/3));
                    
                    [x, y, z] = sph2cart(azimuth, elevation, radii);
                    obj.position = [x, y, z];
                    obj.amplitude = ones(obj.nPoints,1);
                case 'grid2D'
                    
                    az_locations  = -30 : 30 : 30;
                    el_locations  = 0;      % 0 for central elevation plane
                    dep_locations = 40 : 10 : 60;
                    
                    [x, y, z]    = ndgrid(az_locations, el_locations, dep_locations);
                    obj.position = [x(:) y(:) z(:)]/1000;
                    
                    obj.amplitude = ones(size(obj.position, 1), 1);
                    
                case 'grid3D'
                    
                    az_locations  = -30 : 30 : 30;
                    el_locations  = 0;      % 0 for central elevation plane
                    dep_locations = 40 : 10 : 60;
                    
                    [x, y, z]    = ndgrid(az_locations, el_locations, dep_locations);
                    
                    yyy = [-5; 0; 5];
                    obj.position = [];
                    for jj = 1:length(yyy)
                        obj.position = [obj.position; x(:)+2*yyy(jj), yyy(jj)*ones(numel(y),1), z(:)];
                    end
                    
                    obj.position = obj.position/1000;
                    obj.amplitude = ones(size(obj.position, 1), 1);
                case 'slab_azimuth'
                    az_angle  = (-10 : 0.25 : 10)';
                    el_angle  = 0;      % 0 for central elevation plane
                    range     = (47.5 : 0.25 : 52.5)'/1000;
                    
                    az_angle = az_angle .* (pi/180);
                    el_angle = el_angle .* (pi/180);
                    
                    % Works similarly to sph2cart. But x/z and y/x axis are swapped
                    x = bsxfun(@times, range,  bsxfun(@times, cos(el_angle), sin(az_angle))');
                    y = zeros(size(x));
                    z = bsxfun(@times, range,  bsxfun(@times, cos(el_angle), cos(az_angle))');
                    
                    obj.position = [x(:) y(:) z(:)];
                    obj.amplitude = ones(size(obj.position, 1), 1);
                case 'slab_elevation'
                    az_angle  = 0;
                    el_angle  = (-10 : 0.25 : 10)';
                    range     = (47.5 : 0.25 : 52.5)'/1000;
                    
                    az_angle = az_angle .* (pi/180);
                    el_angle = el_angle .* (pi/180);
                    
                    % Works similarly to sph2cart. But x/z and y/x axis are swapped
                    x = bsxfun(@times, range,  bsxfun(@times, cos(el_angle), sin(az_angle))');
                    y = bsxfun(@times, range,  sin(el_angle)');
                    z = bsxfun(@times, range,  bsxfun(@times, cos(el_angle), cos(az_angle))');
                    
                    obj.position = [x(:) y(:) z(:)];
                    obj.amplitude = ones(size(obj.position, 1), 1);
                case 'cyst_yz'
                    obj.position  = [zeros(obj.nPoints,1) 0.04*rand(obj.nPoints,1)-0.02 0.07*rand(obj.nPoints,1)-0.035];
                    obj.amplitude = ones(obj.nPoints, 1);
                    
                    ind = sqrt(sum((obj.position).^2,2)) >  obj.radius;
                    
                    obj.position  = obj.position(ind,:);
                    obj.amplitude = obj.amplitude(ind);
                case 'cyst_xz'
                    obj.position  = [0.04*rand(obj.nPoints,1)-0.02  zeros(obj.nPoints,1)  0.07*rand(obj.nPoints,1)-0.035];
                    obj.amplitude = ones(obj.nPoints, 1);
                    
                    ind = sqrt(sum((obj.position).^2,2)) >  obj.radius;
                    
                    obj.position  = obj.position(ind,:);
                    obj.amplitude = obj.amplitude(ind);
                case 'cyst'
                    N = obj.nPoints;
                    x_size = 50/1000;   %  Width of phantom [mm]
                    y_size = 0/1000;   %  Transverse width of phantom [mm]
                    z_size = 60/1000;   %  Height of phantom [mm]
                    z_start = 10/1000;  %  Start of phantom surface [mm];
                    
                    %  Create the general scatterers
                    
                    x = (rand (N,1)-0.5)*x_size;
                    y = (rand (N,1)-0.5)*y_size;
                    z = rand (N,1)*z_size + z_start;
                    
                    %  Generate the amplitudes with a Gaussian distribution
                    
                    obj.amplitude = sqrt(0.05)*randn(N,1);
                    
                    %  Make the cyst and set the amplitudes to zero inside
                    
                    %  6 mm cyst
                    r=6/2/1000;      %  Radius of cyst [mm]
                    xc=10/1000;     %  Place of cyst [mm]
                    zc=10/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2);
                    obj.amplitude = obj.amplitude .* (1-inside);
                    
                    %  5 mm cyst
                    r=5/2/1000;      %  Radius of cyst [mm]
                    zc=20/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2);
                    obj.amplitude = obj.amplitude .* (1-inside);
                    
                    %  4 mm cyst
                    r=4/2/1000;      %  Radius of cyst [mm]
                    zc=30/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2);
                    obj.amplitude = obj.amplitude .* (1-inside);
                    
                    %  3 mm cyst
                    r=3/2/1000;      %  Radius of cyst [mm]
                    zc=40/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2);
                    obj.amplitude = obj.amplitude .* (1-inside);
                    
                    %  2 mm cyst
                    r=2/2/1000;      %  Radius of cyst [mm]
                    zc=50/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2);
                    obj.amplitude = obj.amplitude .* (1-inside);
                    
                    %  Make the high scattering region and set the amplitudes to 10 times inside
                    
                    %  6 mm region
                    r=5/2/1000;       %  Radius of cyst [mm]
                    xc=-5/1000;     %  Place of cyst [mm]
                    zc=50/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;
                    obj.amplitude = obj.amplitude .* (1-inside) + 10* inside;
                    
                    %  5 mm region
                    r=4/2/1000;       %  Radius of cyst [mm]
                    zc=40/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;
                    obj.amplitude = obj.amplitude .* (1-inside) + 10* inside;
                    
                    %  4 mm region
                    r=3/2/1000;       %  Radius of cyst [mm]
                    zc=30/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;
                    obj.amplitude = obj.amplitude .* (1-inside) + 10* inside;
                    
                    %  3 mm region
                    r=2/2/1000;       %  Radius of cyst [mm]
                    zc=20/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;
                    obj.amplitude = obj.amplitude .* (1-inside) + 10* inside;
                    
                    %  2 mm region
                    r=1/2/1000;       %  Radius of cyst [mm]
                    zc=10/1000+z_start;
                    
                    inside = ( ((x-xc).^2 + (z-zc).^2) < r^2) ;
                    obj.amplitude = obj.amplitude .* (1-inside) + 10* inside;
                    
                    %  Place the point scatterers in the phantom
                    
                    for i=N-5:N
                        x(i) = -15/1000;
                        y(i) = 0;
                        z(i) = z_start + (10+5*10)/1000 + (i-N)*10/1000;
                        obj.amplitude(i) = 20;
                    end
                    
                    %  Return the variables
                    obj.position = [x y z];
                case 'ellipsoid'
                    %% create random sphere
                    rvals = 2*rand(obj.nPoints,1)-1;
                    elevation = asin(rvals);
                    azimuth = 2*pi*rand(obj.nPoints,1);
                    radii = obj.radius(1)*(rand(obj.nPoints,1).^(1/3));
                    
                    %% create hollow shpere
                    [x, y, z] = sph2cart(azimuth, elevation, radii);
                    ind = ((x.^2 +y.^2/obj.ellipsoidData(1) +z.^2) > obj.radius(2)^2);
                    x1 = x(ind); 
                    y1 = obj.ellipsoidData(2)*y(ind); % ellipsoid stretch
                    z1 = z(ind);
                    
                    %% cut top
                    ind2 = y1 < obj.ellipsoidData(3); % cut top
                    
                    %% wall
                    obj.position = [x1(ind2), y1(ind2), z1(ind2)];
                    indexWall = 1:size(obj.position,1);
                    
                    %% interior
                    x1 = x(~ind);
                    y1 = obj.ellipsoidData(2)*y(~ind); % ellipsoid stretch
                    z1 = z(~ind);
                    
                    %% cut top
                    ind2 = y1 < obj.ellipsoidData(3); % cut top
                    obj.position = [obj.position; x1(ind2), y1(ind2), z1(ind2)];
                                       
                    %% center on origin, min-max
                    obj.position  = obj.position_at_origin_minmax;
                    obj.amplitude = [ones(length(indexWall),1); zeros(size(obj.position,1)-length(indexWall),1)];
                    
                    %% segmentation
                    obj.isSegmented = true;
                    obj.segmentationClasses = {'LV, wall', 'LV, interior'};
                    obj.segmentationIndex{1} = indexWall;
                    obj.segmentationIndex{2} = indexWall(end)+1:size(obj.position,1);
                    
                case 'ring'
                    %% create random sphere
                    rvals = 2*rand(obj.nPoints,1)-1;
                    elevation = asin(rvals);
                    azimuth = 2*pi*rand(obj.nPoints,1);
                    radii = obj.radius(1)*(rand(obj.nPoints,1).^(1/3));
                    
                    %% create hollow shpere
                    [x, y, z] = sph2cart(azimuth, elevation, radii);
                    ind = ((x.^2 +y.^2/obj.ellipsoidData(1) +z.^2) > obj.radius(2)^2);
                    x1 = x(ind); 
                    y1 = obj.ellipsoidData(2)*y(ind); % ellipsoid stretch
                    z1 = z(ind);
                    
                    %% cut top
                    ind2 = y1 < obj.ellipsoidData(3); % cut top
                    
                    %% wall
                    obj.position = [x1(ind2), y1(ind2), z1(ind2)];
                    indexWall = 1:size(obj.position,1);
                    
                    %% interior
                    x1 = x(~ind);
                    y1 = obj.ellipsoidData(2)*y(~ind); % ellipsoid stretch
                    z1 = z(~ind);
                    
                    %% cut top
                    ind2 = y1 < obj.ellipsoidData(3); % cut top
                    obj.position = [obj.position; x1(ind2), y1(ind2), z1(ind2)];
                                       
                    %% center on origin, min-max
                    obj.position  = obj.position_at_origin_minmax;
                    obj.amplitude = [ones(length(indexWall),1); zeros(size(obj.position,1)-length(indexWall),1)];
                    
                    index = abs(obj.position(:,2)) < 0.005;
                    obj.position  = obj.position (index,:);
                    obj.position(:,2)  = 0;
                    obj.amplitude = obj.amplitude(index);
                    
                  
                case 'half_moon'
                    %% create random sphere
                    th = linspace(0, 2*pi, obj.nPoints)';
                    x = obj.radius(1)*cos(th);
                    z = min(obj.radius(1)*sin(th), 0);
                    
                    obj.position  = [x, zeros(length(x),1), z];
                    obj.amplitude = ones(obj.nPoints, 1);
                                       
                case 'physical_004'
                    fileName = strcat(pwd, '/Dataset\heart_model\2020.18.05 - from_daniel/ct_train_1004_label.nii');
                    
                    %% segmented data
%                     info       = niftiinfo(fileName);
%                     threedData = niftiread(fileName);
                    % Kate: only reads in Matlab 2019 (uncomment the above)
                    load(strcat(pwd, '\Dataset\heart_model\2020.18.05 - from_daniel\correctly_read.mat'), 'info', 'threedData');
                    
                    %% add walls to the data
                    labelsNum= [420 550 600 820 850]; %LA RA RV AO PA
                    % For the training data sets, we provide manual segmentation of the seven whole heart substructures [1,2],
                    % (1) the left ventricle blood cavity (label value 500);
                    % (2) the right ventricle blood cavity (label value 600);
                    % (3) the left atrium blood cavity (label value 420);
                    % (4) the right atrium blood cavity (label value 550);
                    % (5) the myocardium of the left ventricle (label value 205);
                    % (6) the ascending aorta (label value 820), which is defined as the aortic trunk from the aortic valve to the superior level of the atria; for example, the mean length of this trunk is about 41.9 mm as we measured in 25 healthy subjects;
                    % (7) the pulmonary artery (label value 850), which is defined as the beginning trunk between the pulmonary valve and the bifurcation point, for example, the mean length of this trunk is about 41.9 mm as we measured in 25 healthy subjects.
                    
                    % Dilate 5mm for RV, 2mm for the rest
                    for k=1:numel(labelsNum)
                        tempMask1 = threedData==labelsNum(k);
                        
                        if(k==3)
                            rx=round(7/info.PixelDimensions(1))-1;
                            ry=round(7/info.PixelDimensions(2))-1;
                            rz=round(7/info.PixelDimensions(3))-1;
                        else
                            rx=round(5/info.PixelDimensions(1))-1;
                            ry=round(5/info.PixelDimensions(2))-1;
                            rz=round(5/info.PixelDimensions(3))-1;
                        end
                        
                        tempMask2 = imdilate(tempMask1, ones([rx ry rz]))-tempMask1;
                        threedData(tempMask1) = 0;
                        equalsZero = threedData == 0;
                        threedData(equalsZero) = threedData(equalsZero) + uint16(tempMask2(equalsZero)*labelsNum(k));
                    end
                    
%                     Commented by Kate, new ribs and cartilage have been
%                     introduced from another CT scans
%                     %% add ribs
%                     fileName = strcat(pwd, '/Dataset\heart_model\2020.18.05 - from_daniel/ribs.mat');
%                     load(fileName, 'segMask');
%                     ribs = segMask;
%                     threedData = threedData + flip(uint16(ribs),2);
                    
                    %% the following places random point on a cube and rounds its to amplitude to the amplitude of the closest point on the mesh 
                    xAxis = (-info.ImageSize(1)/2:(info.ImageSize(1)/2-1))*info.PixelDimensions(1)/1e3;
                    yAxis = (-info.ImageSize(2)/2:(info.ImageSize(2)/2-1))*info.PixelDimensions(2)/1e3;
                    zAxis = (-info.ImageSize(3)/2:(info.ImageSize(3)/2-1))*info.PixelDimensions(3)/1e3;
                    
                    x =  min(xAxis) + (max(xAxis)-min(xAxis)).*rand(obj.nPoints,1);
                    y =  min(yAxis) + (max(yAxis)-min(yAxis)).*rand(obj.nPoints,1);
                    z =  min(zAxis) + (max(zAxis)-min(zAxis)).*rand(obj.nPoints,1);
                    
                    %% rounding x-axis
                    a = (info.ImageSize(1) - 1)/(max(xAxis)-min(xAxis));
                    b = 1 - a*min(xAxis);
                    indexX = round( (a*x+b));
                    
                    %% rounding y-axis
                    a = (info.ImageSize(2) - 1)/(max(yAxis)-min(yAxis));
                    b = 1 - a*min(yAxis);
                    indexY = round( (a*y+b));
                    
                    %% rounding z-axis
                    a = (info.ImageSize(3) - 1)/(max(zAxis)-min(zAxis));
                    b = 1 - a*min(zAxis);
                    indexZ = round( (a*z+b));
                    
                    linearIndex = sub2ind(info.ImageSize, indexX, indexY, indexZ);
                    
                    %% rounding amplitude to closest point on the mesh
                    obj.amplitude = threedData(linearIndex);
                    
                    %% empty space has zero value on dataset
                    indexNonZero = obj.amplitude~=0;
                    obj.position = [x(indexNonZero) y(indexNonZero) z(indexNonZero)];
                    obj.amplitude = double(obj.amplitude(indexNonZero));
                    
                    %% segmentation
                    obj.isSegmented = true;
                    UNQ = unique(obj.amplitude);
                    obj.segmentationClasses = {'LV wall', 'LA', 'LV cavity', 'RA' , 'RV', 'A', 'PA' };
                    AMPs                    = [1, 1, 0, 1, 1, 1, 1];
                    STDs                    = 0.25*ones(1, length(AMPs));
                    
                    for jj = 1:length(UNQ)
                        obj.segmentationIndex{jj} = find(obj.amplitude == UNQ(jj));
                        obj.amplitude(obj.segmentationIndex{jj}) = abs(STDs(jj)*randn(length(obj.segmentationIndex{jj}), 1) + AMPs(jj))*(AMPs(jj)>0);
                    end
                    
                    load(strcat(pwd, '/Dataset\heart_model\2020.18.05 - from_daniel/ribs_realistic_phantom3.mat'), 'FV');
                    obj.segmentationClasses{numel(obj.segmentationClasses)+1}='ribs';
                    obj.segmentationIndex{numel(obj.segmentationIndex)+1}=(size(obj.position,1)+1:size(obj.position,1)+size(FV.vertices,1))';
                    obj.position=[obj.position; FV.vertices];
                    obj.amplitude=[obj.amplitude; zeros(size(FV.vertices,1),1)];
                    obj.ribs=FV.faces;
                    
                    obj.position = obj.position - (min(obj.position(obj.segmentationIndex{2},:))+max(obj.position(obj.segmentationIndex{2},:)))/2;
                    
                case 'KUL_heart_model'
                    fname = ['Dataset\heart_model\dotMats\frame_', num2str(obj.frame),'.mat'];
                    load(fname, 'position', 'amplitude');
                    
                    obj.position  = position;
                    obj.amplitude = amplitude;
                    obj.isSegmented = true;
                    UNQ = unique(obj.amplitude);
                    obj.segmentationClasses = {'LV cavity', 'Myocardium'};
                    AMPs                    = [0, 1];
                    STDs                    = 0.25*ones(1, length(AMPs));
                    
                    for jj = 1:length(UNQ)
                        obj.segmentationIndex{jj} = find(obj.amplitude == UNQ(jj));
                        obj.amplitude(obj.segmentationIndex{jj}) = abs(STDs(jj)*randn(length(obj.segmentationIndex{jj}), 1) + AMPs(jj))*(AMPs(jj)>0);
                    end
                    
                    % center the phantom on the interior of the LV
                    obj.position = obj.position - (min(obj.position(obj.segmentationIndex{1},:))+max(obj.position(obj.segmentationIndex{1},:)))/2;
                case 'knee_ligaments'  
                    load('femur_patch.mat', 'Femur');
                    load('fibula_patch.mat', 'Fibula');
                    load('MCL_patch.mat', 'MCL');
                    load('patella_patch.mat', 'Patella');
                    load('tibia_patch.mat', 'Tibia');
                    load('LCL_patch.mat', 'LCL');
                    load('TibCart_patch.mat', 'TibCart1', 'TibCart2');
                    load('PatCart_patch.mat', 'PatCart');
                    load('FemCart_patch.mat', 'FemCart');
                    load('MCLfibers_patch.mat', 'MCLfibers');
                    
                    obj.position=[];
                    
                    femurPos=obj.generate_points_within_the_patch (Femur,obj.nPoints);
                    obj.segmentationClasses{1} = 'Femur';
                    obj.segmentationIndex{1}=1:size(femurPos(:,1));
                    obj.position=[obj.position; femurPos];
                    
                    fibulaPos=obj.generate_points_within_the_patch (Fibula,obj.nPoints);
                    obj.segmentationClasses{2} = 'Fibula';
                    obj.segmentationIndex{2}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(fibulaPos(:,1));
                    obj.position=[obj.position; fibulaPos];
                    
                    MCLPos=obj.generate_points_within_the_patch(MCL,obj.nPoints/2); 
                    obj.segmentationClasses{3} = 'MCL';
                    obj.segmentationIndex{3}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(MCLPos(:,1));
                    obj.position=[obj.position; MCLPos];
                     
                    
                    patellaPos=obj.generate_points_within_the_patch (Patella,obj.nPoints/2);
                    obj.segmentationClasses{4} = 'Patella';
                    obj.segmentationIndex{4}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(patellaPos(:,1));
                    obj.position=[obj.position;patellaPos];
                    
                    tibiaPos=obj.generate_points_within_the_patch (Tibia,obj.nPoints);
                    obj.segmentationClasses{5} = 'Tibia';
                    obj.segmentationIndex{5}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(tibiaPos(:,1));
                    obj.position=[obj.position; tibiaPos];
                    
                    
                    LCLPos=obj.generate_points_within_the_patch (LCL,obj.nPoints/2);
                    obj.segmentationClasses{6} = 'LCL';
                    obj.segmentationIndex{6}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(LCLPos(:,1));
                    obj.position=[obj.position; LCLPos];
                    
                    tibCart1Pos=obj.generate_points_within_the_patch (TibCart1,obj.nPoints/2);
                    obj.segmentationClasses{7} = 'tibCart1';
                    obj.segmentationIndex{7}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(tibCart1Pos(:,1));
                    obj.position=[obj.position; tibCart1Pos];                    
                    
                    tibCart2Pos=obj.generate_points_within_the_patch (TibCart2,obj.nPoints/2);
                    obj.segmentationClasses{8} = 'tibCart2';
                    obj.segmentationIndex{8}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(tibCart2Pos(:,1));
                    obj.position=[obj.position; tibCart2Pos];
                    
                    patCartPos=obj.generate_points_within_the_patch (PatCart,obj.nPoints/2);
                    obj.segmentationClasses{9} = 'patCart';
                    obj.segmentationIndex{9}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(patCartPos(:,1));
                    obj.position=[obj.position; patCartPos];
                    
                    femCartPos=obj.generate_points_within_the_patch (FemCart,obj.nPoints/2);
                    obj.segmentationClasses{10} = 'FemCart';
                    obj.segmentationIndex{10}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(femCartPos(:,1));
                    obj.position=[obj.position; femCartPos];
                    
                    
                    for i=1:numel(MCLfibers)
                        fiberPos=obj.generate_points_within_the_patch (MCLfibers{i},obj.nPoints/10);
                        obj.segmentationClasses{10+i} = ['fiber ' num2str(i)];
                        obj.segmentationIndex{10+i}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(fiberPos(:,1));
                        obj.position=[obj.position; fiberPos];
                    end   
                                     
                    amplFemur=1; amplFibula=1; amplMCL=0.8;
                    amplPatella=1; amplTibia=1; amplLCL=0.8; 
                    amplTibCart1=0.8; amplTibCart2=0.8; amplPatCart=0.8; amplFemCart=0.8;
                    
                    obj.amplitude=zeros(size(obj.position(:,1)));
                    obj.amplitude=[amplFemur.*ones(size(femurPos(:,1))); amplFibula.*ones(size(fibulaPos(:,1))); amplMCL.*ones(size(MCLPos(:,1))); amplPatella.*ones(size(patellaPos(:,1))); ...
                                amplTibia.*ones(size(tibiaPos(:,1))); amplLCL.*ones(size(LCLPos(:,1))); amplTibCart1.*ones(size(tibCart1Pos(:,1))); amplTibCart2.*ones(size(tibCart2Pos(:,1)));...
                                 amplPatCart.*ones(size(patCartPos(:,1))); amplFemCart.*ones(size(femCartPos(:,1)))];
                    obj.amplitude(size(obj.position(:,1))-size(obj.amplitude,1)+1:size(obj.position(:,1)))=amplMCL; %fibers         
                    obj.isSegmented = true;
                    obj.position=obj.position./1e3;
                    obj.position=[obj.position(:,1) obj.position(:,3) obj.position(:,2)];
                    load('skinMCL_remeshed_transducer_in_the_right_order.mat', 'custom');
                    custom.centers=cell2mat(reshape(reshape(cellfun(@(x) [x(1) x(3) x(2)],custom.centers,'UniformOutput', false), [187 198]), [187*198 1]));
                    mean([obj.position; custom.centers])
                    obj.position = obj.position - (min(obj.position(obj.segmentationIndex{3},:))+max(obj.position(obj.segmentationIndex{3},:)))/2;
                    
                    clear FemCart PatCart TibCart2 TibCart1 LCL Tibia Patella MCL Fibula Femur
                    clear femCartPos patCartPos tibCart2Pos tibCart1Pos LCLPos tibiaPos patellaPos MCLPos fibulaPos femurPos
                case {'knee_ligaments_cropped', 'ellipsoid_cropped'}
                    load('currently_cropped_phantom.mat', 'phantomSaved');
                    obj.center=mean(phantomSaved.position);
                    obj.isSegmented=phantomSaved.isSegmented;
                    obj.position=[]; kol=1; obj.amplitude=[];
                    

                    for kk=1:numel(phantomSaved.segmentationClasses)
                       FV.faces=[]; FV.vertices=[]; 
                       if (~isempty(phantomSaved.position(phantomSaved.segmentationIndex{kk},:)))&(numel(phantomSaved.segmentationIndex{kk})>3)
                        FV.faces=boundary(phantomSaved.position(phantomSaved.segmentationIndex{kk},:));
                        FV.vertices=phantomSaved.position(phantomSaved.segmentationIndex{kk},:);
                        if contains(phantomSaved.segmentationClasses{kk},'fiber')==0
                           fPos=obj.generate_points_within_the_patch (FV,obj.nPoints);
                        else
                           fPos=obj.generate_points_within_the_patch (FV,obj.nPoints/10); 
                        end    
                        obj.amplitude=[obj.amplitude; phantomSaved.amplitude(phantomSaved.segmentationIndex{kk}(1)).*ones(size(fPos(:,1)))];
                        obj.segmentationClasses{kol} =phantomSaved.segmentationClasses{kk};
                        if kol==1,    obj.segmentationIndex{kol}=1:size(fPos(:,1));
                        else obj.segmentationIndex{kol}=size(obj.position(:,1))+1:size(obj.position(:,1))+size(fPos(:,1)); end
                        obj.position=[obj.position; fPos];
                        kol=kol+1;
                       end 
                    end    
                otherwise
                    error(['Phantom: no such predefined phantom: ', obj.predefined])
            end
            
            if ~strcmp(obj.predefined, 'knee_ligaments_cropped')&&~strcmp(obj.predefined, 'ellipsoid_cropped')
                %% rotate and move to center
                obj.position = (...
                    (PP_Utils.rotz(obj.rotationAngle(3)*pi/180)*...
                    PP_Utils.roty(obj.rotationAngle(2)*pi/180)*...
                    PP_Utils.rotx(obj.rotationAngle(1)*pi/180)) * obj.position')' + obj.center;
            end
            obj.positionForCalc = obj.position;
        end
        
        function obj = validatePhantom(obj)
            % validatePhantom - some validation for the phantom
            %
            % Syntax:  obj = validatePhantom(obj)
            assert(size(obj.position,1) == length(obj.amplitude), 'Phantom: there should be as many positions as amplitudes for the phantom.')
            
            assert(length(obj.center) == 3, 'Phantom: problem with center of phantom.')
            assert(length(obj.rotationAngle) == 3, 'Phantom: you need 3 rotation angles.')
            assert(length(obj.ellipsoidData) == 3, 'Phantom: you need 3 values for ellipsoid data.')
            
        end
        
        function obj = add_point(obj, structIn)
            % add_point - add one point to  phantom
            %
            % Syntax:  obj = add_point(obj, structIn)
            %
            % Inputs:
            %    structIn - contains position and amplitude fields
            %
            % Outputs:
            %    obj - phantom with one extra point
            
            assert(isfield(structIn,{'position','amplitude'}), 'Phantom: structIn needs to have positions and amplitude fields')
            
            structIn = validatePhantom(structIn);
            
            obj.position  = [obj.position;  structIn.position];
            obj.amplitude = [obj.amplitude; structIn.amplitude];
        end
        
        function obj = remove_point(obj)
            % remove_point - remove last point to  phantom
            %
            % Syntax:  obj = remove_point(obj, structIn)
            %
            % Outputs:
            %    obj - phantom with one less point
            
            obj.position  = obj.position(1:end-1,:);
            obj.amplitude = obj.amplitude(1:end-1,:);
        end
        
        function p = get.position_at_origin_mean(obj)
            % position_at_origin - shifts phantom positions to origin [0,0,0]
            %
            % Syntax:  position_at_origin = get.position_at_origin(obj)
            %
            % Outputs:
            %    position_at_origin - Nx3 position matrix at origin
            p = obj.position - mean(obj.position);
        end
        
        function p = get.position_at_origin_minmax(obj)
            % position_at_origin - shifts phantom positions to origin [0,0,0]
            %
            % Syntax:  position_at_origin = get.position_at_origin(obj)
            %
            % Outputs:
            %    position_at_origin - Nx3 position matrix at origin
            p = obj.position - (min(obj.position)+max(obj.position))/2;
        end
        
        function volume_ml = get.volume_ml(obj)
            % calculate_colume - calculate volume of interior of LV for
            % segmented phantom. One of the segmented classes should be the
            % interior of the left ventricle
            %
            % Syntax:  position_at_origin = get.position_at_origin(obj)
            %
            % Outputs:
            %    v_ml - scalar; volume of the interiro of the LV in ml
            
            %% find class corresponding to the LV's interior
            volume_ml = 0;
            if obj.isSegmented
                found = 0;
                ii = 1;
                while found == 0
                    if ~isempty(strfind(obj.segmentationClasses{ii}, 'LV')) &&...
                            (~isempty(strfind(obj.segmentationClasses{ii}, 'interior')) || ~isempty(strfind(obj.segmentationClasses{ii}, 'cavity')))
                        found = ii;
                    else
                        ii = ii +1;
                    end
                end
                
                p_temp = obj.position(obj.segmentationIndex{found},:);
                
                [~, V] = convhull(p_temp(:,1), p_temp(:,2), p_temp(:,3)); % V in cubic meter
                volume_ml = V*1e6;
            end
             
        end   
        
        function points=generate_points_within_the_patch (obj,FV,N)
          x =  min(FV.vertices(:,1)) + (max(FV.vertices(:,1))-min(FV.vertices(:,1))).*rand(N,1);
          y =  min(FV.vertices(:,2)) + (max(FV.vertices(:,2))-min(FV.vertices(:,2))).*rand(N,1);
          z =  min(FV.vertices(:,3)) + (max(FV.vertices(:,3))-min(FV.vertices(:,3))).*rand(N,1);
          
          in = inpolyhedron(FV,[x y z]);
          points=[x(in) y(in) z(in)];
        end  

       
    end
end
