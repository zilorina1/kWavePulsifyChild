classdef pulsifyPatch < handle
    % Pulsify Patch -- this class contains all the information and physical
    % dimensions of the Pulsify Patch. It contains three objects: Tx, Rx
    % and phantom.
    
    properties
        % all measures in meters
        Tx                                              % Tx probe. See probe class for details
        Rx                                              % Rx probe
        compoundingProbe                                % compounding probe
        phantom                                         % phantom. For more info, see phantom class
    end
    
    properties (Access = private)
        rx_same_as_tx
    end
        
    
    methods
        function obj = pulsifyPatch(inOptions)
            % pulsifyPatch - Pulsify Patch constructor
            % Syntax:  obj = PulsifyPatch(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify patch object
            
            %% parser
            structDefault = obj.getstructDefault;
            if nargin == 0
                inOptions = [];
            end
            
            % if compounding shape is not specified, it is considered to be
            % the same as the probe shape
            if ~any(isfield(inOptions, {'compounding_shape', 'compounding_shapeNumber'}))
                if isfield(inOptions, 'Tx_shape')
                    inOptions.compounding_shape       = inOptions.Tx_shape;
                end
                if isfield(inOptions, 'Tx_shapeNumber')
                    inOptions.compounding_shapeNumber  = inOptions.Tx_shapeNumber;
                end
            end
            inOptions     = parser(structDefault, inOptions);
            
            %% internal properties
            obj.rx_same_as_tx = inOptions.rx_same_as_tx;
            
            %% TX
            obj.Tx  = probe(struct(...
                'name',             inOptions.Tx_name,...
                'width',            inOptions.Tx_width , ...
                'height',           inOptions.Tx_height, ...
                'kerf',             inOptions.Tx_kerf, ...
                'apodization',      inOptions.Tx_apodization, ...
                'impulseResponse',  inOptions.Tx_impulseResponse, ...
                'excitation',       inOptions.Tx_excitation, ...
                'nElements_x',      inOptions.Tx_nElements_x, ...
                'nElements_y',      inOptions.Tx_nElements_y, ...
                'focus',            inOptions.Tx_focus, ...
                'centerOfAperture', inOptions.Tx_centerOfAperture, ...
                'nSub',             inOptions.Tx_nSub, ...
                'shape',            inOptions.Tx_shape, ...
                'shapeNumber',      inOptions.Tx_shapeNumber,...
                'elementPos',       inOptions.Tx_elementPos,...
                'subArrayPos',       inOptions.Tx_subArrayPos,...
                'subArrayCurvature',       inOptions.Tx_subArrayCurvature));
            
            %% RX
            if inOptions.rx_same_as_tx
                obj.Rx       = obj.Tx;
                obj.Rx.name  = 'receiver';
                
            else
                obj.Rx  = probe(struct(...
                    'name',              inOptions.Rx_name,...
                    'width',             inOptions.Rx_width , ...
                    'height',            inOptions.Rx_height, ...
                    'kerf',              inOptions.Rx_kerf,   ...
                    'apodization',       inOptions.Rx_apodization, ...
                    'impulseResponse',   inOptions.Rx_impulseResponse, ...
                    'excitation',        inOptions.Rx_excitation, ...
                    'nElements_x',       inOptions.Rx_nElements_x, ...
                    'nElements_y',       inOptions.Rx_nElements_y, ...
                    'focus',             inOptions.Rx_focus, ...
                    'centerOfAperture',  inOptions.Rx_centerOfAperture, ...
                    'nSub',              inOptions.Rx_nSub, ...
                    'shape',             inOptions.Rx_shape, ...
                    'shapeNumber',       inOptions.Rx_shapeNumber)); %Changes by Kate, for defining elements of the probe for compounding ));
            end
            
            %% virtual compounding probe
            if strcmp(inOptions.compounding_shape, inOptions.Tx_shape) && inOptions.compounding_shapeNumber == inOptions.Tx_shapeNumber
                obj.compoundingProbe = obj.Tx;
                obj.compoundingProbe.name = 'compoundingProbe';
            else
                obj.compoundingProbe  = probe(struct(...
                    'name',             'compoundingProbe',...
                    'width',            inOptions.Tx_width , ...
                    'height',           inOptions.Tx_height, ...
                    'kerf',             inOptions.Tx_kerf, ...
                    'nElements_x',      inOptions.Tx_nElements_x, ...
                    'nElements_y',      inOptions.Tx_nElements_y, ...
                    'shape',            inOptions.compounding_shape, ...
                    'shapeNumber',      inOptions.compounding_shapeNumber));
            end
            
            %% Phantom
            obj.phantom = phantom(struct(...
                'predefined',       inOptions.predefined, ...
                'nPoints',          inOptions.nPoints, ...
                'position',         inOptions.position, ...
                'amplitude',        inOptions.amplitude, ...
                'radius',           inOptions.radius, ...
                'ellipsoidData',    inOptions.ellipsoidData, ...
                'frame',            inOptions.frame, ...
                'rotationAngle',    inOptions.rotationAngle, ...
                'center',           inOptions.center ));
            
        end
        
        function h_phantom = plot_geometry(obj, inOptions, softwareOpt)
            % plot_geometry - plots probe and phantom in 3D space.
            % Syntax:  plot_geometry(obj, inOptions)
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            %    softwareOpt - software options. To plot the scan sequence
            
            if nargin == 1
                inOptions.plotSequence = false;
            end
            
            %% parser
            inOptions = parser(struct(...
                'viewPoint',                    [-30, 30], ... % input to Matlab's 'view' function
                'newFigure',                    true, ...
                'plotSequence',                 true, ...
                'plotPhantom',                  true, ...
                'plotsubArray',                 false,...        %plot phantom or not, Kate
                'plotPhantomForCalc',           false),...        %plot phantom or not, Kate
                inOptions);
            
            global plot_debug
            
            if inOptions.newFigure
                figure;
                set(gcf, 'Position',[ 50 100 800 800])
            end
            
            hold on;
            
            %% plot patch
            E = cell2mat(obj.Tx.elementPos);
            X = E(:,(1:3:end))*1000;
            Y = E(:,(2:3:end))*1000;
            Z = E(:,(3:3:end))*1000;
            surf(X,Z,Y, 'FaceCOlor', 'b', 'FaceAlpha',0.25, 'LineStyle', ':')
            
            %% line at edges - top and bottom
            for ii = [1 size(E,1)]
                X = E(ii,(1:3:end))*1000;
                Y = E(ii ,(2:3:end))*1000;
                Z = E(ii,(3:3:end))*1000;
                plot3(X,Z,Y, 'k-', 'lineWidth', 2)
            end
            
            %% line at edges - left and right
            for ii = [0 size(E,2)-3]
                X = E(:,ii+1)*1000;
                Y = E(:,ii+2)*1000;
                Z = E(:,ii+3)*1000;
                plot3(X,Z,Y, 'k-', 'lineWidth', 2)
            end
            
            color = {'b', 'r'};
            if inOptions.plotsubArray
                for kk = 1:1+~(all(softwareOpt.shotIndex == softwareOpt.shotIndexRx))
                    if kk == 1
                        littleE = obj.Tx.subArrayPos;
                    else
                        littleE = obj.Rx.subArrayPos;
                    end
                   
                    shotPosition = (-kk+2)*softwareOpt.shotIndex +(kk-1)*softwareOpt.shotIndexRx;
                    
                    littleE = littleE(shotPosition(1)-8:shotPosition(1)+7, shotPosition(2)-8:shotPosition(2)+7);
                    littleE = cell2mat(littleE);
                    
                    %% line at edges - top and bottom
                    for ii = [1 size(littleE,1)]
                        X = littleE(ii,(1:3:end))*1000;
                        Y = littleE(ii ,(2:3:end))*1000;
                        Z = littleE(ii,(3:3:end))*1000;
                        plot3(X,Z,Y, 'Color', color{kk}, 'lineWidth', 3)
                    end
                    
                    %% line at edges - left and right
                    for ii = [0 size(littleE,2)-3]
                        X = littleE(:,ii+1)*1000;
                        Y = littleE(:,ii+2)*1000;
                        Z = littleE(:,ii+3)*1000;
                        plot3(X,Z,Y, 'Color', color{kk}, 'lineWidth', 3)
                    end
                end
            end
            
            xlabel('x [mm]')
            ylabel('z [mm]')
            zlabel('y [mm]')
            grid
            axis('image')
            
            h_phantom = [];
            
            if inOptions.plotPhantom
 
                ph_position = obj.phantom.position;
                if inOptions.plotPhantomForCalc
                    ph_position = obj.phantom.positionForCalc;
                end
                
                if (any((all(ph_position == 0, 1))) || strcmp(obj.phantom.predefined,'none')) 
                    % for single scatterers
                    if length(obj.phantom.amplitude) > 20
                        index = obj.phantom.amplitude ~= 0 & obj.phantom.amplitude < 20;
                        h_phantom(1)=scatter3(ph_position(index,1)*1000, ph_position(index,3)*1000,  ph_position(index,2)*1000, [], obj.phantom.amplitude(index),'.', 'DisplayName', 'scatterer');
                        
                        % for large scatterers
                        index = obj.phantom.amplitude >= 20;
                        if sum(index) >0
                            h_phantom(2)=scatter3(ph_position(index,1)*1000, ph_position(index,3)*1000,  ph_position(index,2)*1000, [], obj.phantom.amplitude(index),'o', 'MarkerFaceColor', 'r', 'large scatterer');
                        end
                    else
                        h_phantom=scatter3(ph_position(:,1)*1000, ph_position(:,3)*1000,  ph_position(:,2)*1000, [], obj.phantom.amplitude(:),'o', 'MarkerFaceColor', 'r', 'DisplayName', 'scatterer');
                    end
                elseif obj.phantom.isSegmented
                    % segmented phantom
                    colors = {'r', 'b', 'g', 'm', 'k','c', 'y'};
                    h_phantom = zeros(length(obj.phantom.segmentationClasses), 1);
                    % changed a bit by Kate, I think it is prettier like
                    % this (can always set back)
                    for kk = 1:length(obj.phantom.segmentationClasses)
                        
                        if strcmp(obj.phantom.segmentationClasses{kk}, 'ribs')
                            h_phantom(kk) =  trisurf(obj.phantom.ribs,obj.phantom.position(obj.phantom.segmentationIndex{kk},1).*1000,...
                              obj.phantom.position(obj.phantom.segmentationIndex{kk},3).*1000,...
                              obj.phantom.position(obj.phantom.segmentationIndex{kk},2).*1000,'Facecolor', [0.5 0.5 0.5], 'FaceAlpha',0.5,'EdgeColor', 'none', 'DisplayName', obj.phantom.segmentationClasses{kk}); 
                            %index = obj.phantom.segmentationIndex{kk};
                            %h_phantom = scatter3(ph_position(index,1)*1000, ph_position(index,3)*1000,  ph_position(index,2)*1000, [], obj.phantom.amplitude(index), ...
                            %    '.', 'DisplayName', 'ribs');
                        elseif strcmp(obj.phantom.segmentationClasses{kk}, 'LV wall')    
                            index = boundary(obj.phantom.position(obj.phantom.segmentationIndex{kk},:));
                            h_phantom(kk) = trisurf(index, obj.phantom.position(obj.phantom.segmentationIndex{kk},1)*1000, ...
                                   obj.phantom.position(obj.phantom.segmentationIndex{kk},3)*1000, ...
                                   obj.phantom.position(obj.phantom.segmentationIndex{kk},2)*1000, ...
                                  'Facecolor', colors{mod(kk-1,7)+1}, 'FaceAlpha',0.6, 'EdgeColor', 'none', 'DisplayName', obj.phantom.segmentationClasses{kk});                       
                        else    
                            index = boundary(ph_position(obj.phantom.segmentationIndex{kk},:));
                            h_phantom(kk) = trisurf(index, ph_position(obj.phantom.segmentationIndex{kk},1)*1000, ...
                                ph_position(obj.phantom.segmentationIndex{kk},3)*1000, ...
                                ph_position(obj.phantom.segmentationIndex{kk},2)*1000, ...
                                'Facecolor', colors{mod(kk-1,7)+1},...
                                'edgeColor', 'none', ...
                                'LineWidth', 0.25, 'LineStyle', ':', ...
                                'FaceAlpha',0.1, 'DisplayName', obj.phantom.segmentationClasses{kk});
                        end
                    end
                else
                    if length(obj.phantom.amplitude) < 100
                        h_phantom(1)=scatter3(ph_position(:,1)*1000, ph_position(:,3)*1000,  ph_position(:,2)*1000, [], obj.phantom.amplitude,'o', 'DisplayName', 'scatterer');
                    else
                        % another shape
                        index = boundary(ph_position);
                        h_phantom=trisurf(index,ph_position(:,1)*1000,ph_position(:,3)*1000,ph_position(:,2)*1000,...
                            'Facecolor','red', ...
                            'edgeColor', 'k', ...
                            'LineWidth', 0.25, 'LineStyle', '-', ...
                            'FaceAlpha',0.1, 'DisplayName', 'surface');
                    end
                end
                if plot_debug
                    hLegend = findobj(figure(1), 'Type', 'Legend');
                    hLegend.String(end)={'Phantom'};
                end
            end
            
            h_scan = [];
            if inOptions.plotSequence
                %% plot softwareOpt
                shotPositionMeters = zeros(size(softwareOpt.shotIndexRx,1),3);
                
                if size(shotPositionMeters, 1) == 1
                    h_scan(1) = plot3(shotPositionMeters(:,1)*1000, shotPositionMeters(:,3)*1000, shotPositionMeters(:,2)*1000, ...
                        'r.', 'DisplayName', 'scan sequence');
                    wedge = [ 0 0 0; cell2mat(arrayfun(@(x) x.txFocus, softwareOpt.txEvents, 'UniformOutput', false)')]*1000;
                    fill3(wedge(:,1), wedge(:,3), wedge(:,2), 'r', 'Facealpha', 0.25);
                                        
                else
                    CENTER = obj.Tx.subArrayPos;
                    for cc = 1:size(softwareOpt.shotIndexRx,1)
                        shotPositionMeters(cc,:) = CENTER{softwareOpt.shotIndexRx(cc,1), softwareOpt.shotIndexRx(cc,2)};
                    end
                    h_scan(1) = plot3(shotPositionMeters(:,1)*1000, shotPositionMeters(:,3)*1000, shotPositionMeters(:,2)*1000, ...
                        'r.', 'lineWidth', 1.5, 'DisplayName', 'scan sequence');
                    M = [1 0 0; 0 0 1; 0 1 0];
                    initialPoint = shotPositionMeters(1,:)*M*1000;
                    endPoint     = shotPositionMeters(15,:)*M*1000;
                    text(initialPoint(1)-3,initialPoint(2)-3,initialPoint(3)-3, sprintf('begin'), 'color', 'r', 'fontSize', 12);
                    mArrow3(initialPoint, endPoint, 'color', 'r');
                end
                
                legend([h_phantom(:); h_scan(:)]);

            end
            
            colormap(lines)
            view(inOptions.viewPoint)
            camlight
            axis equal
            title('COLE or FieldII Geometry', 'Fontsize', 13);
            hold off
        end
        
        function BfData_out  = run_transmission(obj, softwareOpt, hardwareOpt, rawData, kWaveSetup)
            % run_transmission - given the physical structure of the patch
            % object, simulates US transmission given hardware and software
            % options
            % Two main simulations options: Field II and COLE
            %
            % Syntax:  BfData  = run_transmission(obj, softwareOpt, hardwareOpt, rawData)
            % or BfData  = run_transmission(obj, softwareOpt, hardwareOpt)
            %
            % Inputs:
            %    softwareOpt - software options
            %    hardwareOpt - hardware options
            %    rawData - (optional) either LUT or FieldII raw data
            %    transmission
            %
            % Outputs:
            %    BfData - struct with fields data, timeAxis, saPosition
            %    and anglePosition
            %
            % Example: see singleTest.m
            
            global fs
            BfData_out = cell(size(softwareOpt.shotIndex,1),1);
            if ~hardwareOpt.KWave
                CENTER = obj.Tx.subArrayPos;
                ALPHA  = obj.Tx.subArrayCurvature;
            end               
            h_w = waitbar(0,['Calculating BfData...']);
                        
            for cc = 1:size(softwareOpt.shotIndex,1)
                saIndex = softwareOpt.shotIndex(cc,:);
                if ~hardwareOpt.KWave
                 %% shift and rotate phantom
                 saPosition = CENTER{saIndex(1), saIndex(2)};
                 saAngle    = ALPHA{saIndex(1), saIndex(2)};
                end
                
                if hardwareOpt.COLE
                    
                    %%
                    %% COLE
                    %%
                    obj = obj.rotate_translate_phantom(saPosition, saAngle);
                    if ~hardwareOpt.splitTxRx
                        if nargin == 4
                            BfData_out{cc} = obj.run_cole_unified_new(softwareOpt, hardwareOpt, rawData, saPosition);
                        else
                            BfData_out{cc} = obj.run_cole_unified_new(softwareOpt, hardwareOpt, saPosition);
                        end
                    else
                        ph_position_tx = obj.phantom.positionForCalc;
                        
                        saIndex = softwareOpt.shotIndexRx(cc,:);
                        saPosition_Rx = CENTER{saIndex(1), saIndex(2)};
                        saAngle_Rx    = ALPHA{saIndex(1), saIndex(2)};
                        obj = obj.rotate_translate_phantom(saPosition_Rx, saAngle_Rx);
                        ph_position_rx = obj.phantom.positionForCalc;
                        BfData_out{cc} = obj.run_cole_splitTxRX(ph_position_tx, ph_position_rx, softwareOpt, hardwareOpt);
                    end
                elseif hardwareOpt.KWave                   
                    [maskElementPos, maskElementIndexRow, maskElementIndexCol, currentElemPosCenters, maskElementPosStupidKwaveIndex]...
                                                                 =define_kwave_mask_for_current_subArray(obj,cc, kWaveSetup, softwareOpt);
                    number_of_used_grid_points= max(maskElementPosStupidKwaveIndex(:));
                    [ind]=get_all_elements_in_subArray(obj,kWaveSetup,softwareOpt.shotIndex(cc,1),softwareOpt.shotIndex(cc,2));
                    saElementsNumbers=reshape(arrayfun(@(x) ind(x,:),1:size(ind,1), 'UniformOutput', false), [obj.Tx.subArraySize obj.Tx.subArraySize])';
                    %{                                         
                    ind=find(maskElementPos==1);
                    X=kWaveSetup.kgrid.x(ind);Y=kWaveSetup.kgrid.y(ind); Z=kWaveSetup.kgrid.z(ind); D=maskElementPos(ind);
                    ck=rand(1,3);
                    figure(2); hold on; scatter3(X(:).*10^3,Z(:).*10^3,Y(:).*10^3,5.0, D(:), 'filled', 'MarkerEdgeColor', ck); 
                    axis equal tight;%colorbar;
                    %}
%                    currentElemPosCenters=cell2mat(reshape(currentElemPosCenters, [size(currentElemPosCenters,1)*size(currentElemPosCenters,2) 1])); 
%                     figure(2); hold on; scatter3(currentElemPosCenters(:,1).*10^3,currentElemPosCenters(:,3).*10^3,currentElemPosCenters(:,2).*10^3,50.0,'filled', 'MarkerEdgeColor', 'b'); 
 
                    saPosition=kWaveSetup.KwavePatch.subArrayPosCenters{softwareOpt.shotIndex(cc,1),softwareOpt.shotIndex(cc,2)};
                    saAngle= kWaveSetup.KwavePatch.subArrayCurvature{softwareOpt.shotIndex(cc,1),softwareOpt.shotIndex(cc,2)};

                    % maskElementPos -- kgrid with active grid points
                    % maskElementIndexRow -- kgrid with each grid point corresponding to the Row number of the element it belongs to
                    % maskElementIndexCol -- kgrid with each grid point corresponding to the Col number of the element it belongs to
                    % maskElementPosStupidKwaveIndex - kgrid with each grid point corresponding to its number in kwave (kwave manual p. 34)
                    
                    % currentElemPosCenters -- cell array of subArraysize*subArraysize, which contains current elements' centers
                    % saElementsNumbers -- cell array of subArraysize*subArraysize, which contains element numbers in current subArray saElementsNumber{1,1}=[3 4]
                    % saPosition -- scalar, center of the current SubArray
                    % saAngle -- tilting of the current subArray
                    
                    aux = obj.convert_cell_to_Fii_mat(currentElemPosCenters);
                    if strcmp(hardwareOpt.mode_tx, 'tx_on_rows')
                       txIndex = kron(eye(obj.Tx.subArraySize), ones( 1, obj.Tx.subArraySize));
                       elementPos  = kron(txIndex*aux./sum(txIndex,2), ones(obj.Tx.subArraySize,1));
                    else
                       elementPos  = aux;
                    end
                    
%                     pPatch=obj;
%                     pPatch.Rx=probe(struct(...
%                        'width',            obj.Tx.width , ...
%                        'height',           obj.Tx.height, ...
%                        'kerf',             obj.Tx.kerf, ...
%                        'nElements_x',     obj.Tx.subArraySize, ...
%                        'nElements_y',      obj.Tx.subArraySize, ...
%                        'focus',          obj.Tx.focus, ...
%                        'centerOfAperture', saPosition, ...
%                        'nSub',              obj.Tx.subArraySize, ...
%                        'elementPos',       currentElemPosCenters,...
%                        'subArrayPos',       saPosition,...
%                       'subArrayCurvature',      saAngle));
%                     pPatch.Tx=pPatch.Rx;

                   s=struct(...
                       'Tx_width',            obj.Tx.width , ...
                       'Tx_height',           obj.Tx.height, ...
                       'Tx_kerf',             obj.Tx.kerf, ...
                       'Tx_apodization',      obj.Tx.apodization,...
                       'Tx_impulseResponse',  obj.Tx.impulseResponse, ...
                       'Tx_excitation',       obj.Tx.excitation, ...
                       'Tx_nElements_x',     obj.Tx.subArraySize, ...
                       'Tx_nElements_y',      obj.Tx.subArraySize, ...
                       'Tx_focus',          obj.Tx.focus, ...
                       'Tx_centerOfAperture', saPosition, ...
                       'Tx_subArrayPos',       saPosition,...
                      'Tx_subArrayCurvature',      saAngle,...
                        'rx_same_as_tx', obj.rx_same_as_tx);
                   s.Tx_elementPos=currentElemPosCenters;
                   pPatch=subArrayPatch(s);
                    
                    %figure; hold on;
                    %load('sensor_data.mat', 'Y00');
                    ccc=rand(1,3);
                    if ~exist(['sensor_data_shot' num2str(cc) '.mat'])
                    for ii = 1:length(softwareOpt.txEvents)  
                     scatter3(elementPos(:,1).*10^3, elementPos(:,3).*10^3,elementPos(:,2).*10^3, 50.0, 'k', 'filled');
                     [xf,yf,zf] = PP_Utils.beam2cart(softwareOpt.txEvents(ii).txAngle(1)+rad2deg(saAngle(1)),...
                                   softwareOpt.txEvents(ii).txAngle(2)+rad2deg(saAngle(2)),softwareOpt.txFocalDepth-saPosition(3));
                     softwareOpt.txEvents(ii).txFocus(1)=saPosition(1)+xf;
                     softwareOpt.txEvents(ii).txFocus(2)=saPosition(2)+yf;
                     softwareOpt.txEvents(ii).txFocus(3)=zf+saPosition(3);
                     figure(2); hold on;  
                     plot3([saPosition(1)  softwareOpt.txEvents(ii).txFocus(1)].*10^3,...
                         [saPosition(3)  softwareOpt.txEvents(ii).txFocus(3)].*10^3,[saPosition(2)  softwareOpt.txEvents(ii).txFocus(2)].*10^3,'Color',ccc, 'Linewidth',1); 
                     
                     delays = reshape(pPatch.Tx.computeTxDelayLine(0, softwareOpt.txEvents(ii).txFocus, elementPos), [obj.Tx.subArraySize obj.Tx.subArraySize])';
                     delays= round((delays-min(delays(:)))/kWaveSetup.kgrid.dt);
                     apodization = obj.Tx.getApoMat(hardwareOpt);
                     source_pNt=max(delays(:))+length(kWaveSetup.input_signal)+1;
                     source_p = zeros(number_of_used_grid_points,source_pNt);
                     for iid = 1:size(saElementsNumbers,1)
                         for jjd=1:size(saElementsNumbers,2)
                             [r,c,v] = ind2sub(size(kWaveSetup.KwavePatch.KWelementIndexRow),...
                                find((kWaveSetup.KwavePatch.KWelementIndexRow==saElementsNumbers{iid,jjd}(1))&(kWaveSetup.KwavePatch.KWelementIndexCol==saElementsNumbers{iid,jjd}(2))));
                            KWaveIndices=unique(maskElementPosStupidKwaveIndex(r(:),c(:),v(:)));
                            source_p(KWaveIndices,:)= [zeros(1,delays(iid,jjd)), kWaveSetup.input_signal,...
                                zeros(1,source_pNt-(delays(iid,jjd)+length(kWaveSetup.input_signal)))].*apodization(iid,jjd).*ones(size(source_p(KWaveIndices,:)));
                            %cc=rand(1,3);
                            %plot(1:source_pNt,source_p(KWaveIndices(1),:),'Color', cc);
                         end   
                     end    
                     
                     
                    tempDelays{ii}=delays; 
                    sensor.record = {'p', 'p_max_all'};
                    source.p = 1; %Needed to avoid problems in parfor loop
                    thissensor = sensor;
                    thissource = source;
                    thissource.p = source_p;
                    thissource.p_mask=maskElementPos;
                    thissensor.mask = maskElementPos;
    
                    % run the simulation
                    sensor_data = kspaceFirstOrder3DC(kWaveSetup.kgrid, kWaveSetup.medium, thissource, thissensor, kWaveSetup.input_args{:});
                    %load('sensor_data.mat', 'sensor_data')
                    for iid = 1:size(saElementsNumbers,1)
                         for jjd=1:size(saElementsNumbers,2)
                             [r,c,v] = ind2sub(size(kWaveSetup.KwavePatch.KWelementIndexRow),...
                                find((kWaveSetup.KwavePatch.KWelementIndexRow==saElementsNumbers{iid,jjd}(1))&(kWaveSetup.KwavePatch.KWelementIndexCol==saElementsNumbers{iid,jjd}(2))));
                            KWaveIndices=unique(maskElementPosStupidKwaveIndex(r(:),c(:),v(:)));
                            element_p{iid,jjd}=mean(sensor_data.p(KWaveIndices,:),1);
                         end   
                    end
                   %{
                    s=10; 
                    X=kWaveSetup.kgrid.x(1:s:end,1:s:end,1:s:end);
                    Y=kWaveSetup.kgrid.y(1:s:end,1:s:end,1:s:end);
                    Z=kWaveSetup.kgrid.z(1:s:end,1:s:end,1:s:end);
                    %pp=sensor_data.p_max_all(1:s:end,1:s:end,1:s:end);
                    pp=
                    figure; scatter3(X(:),Y(:),Z(:), pp(:))
                    %}  
                      Y0=cell2mat(reshape(element_p',[1 obj.Tx.subArraySize*obj.Tx.subArraySize])');
                      %Y0(:,1:round((numel(kWaveSetup.input_signal)+max(delays(:))))*2)=zeros(size(Y0,1),round((numel(kWaveSetup.input_signal)+max(delays(:))))*2);
                      Y00(:,:,ii)=Y0';
                      Y00(1:500,:,ii)=zeros(500, size(Y00,2));
                      %figure; plot(Y00(:,1,1))
                      end
%                     scatter3(pPatch.Rx.centerOfAperture(:,1).*10^3, pPatch.Rx.centerOfAperture(:,3).*10^3,pPatch.Rx.centerOfAperture(:,2).*10^3, 50.0, 'g', 'filled');
%                     BfData_out{cc} = pPatch.offlineBeamforming(Y0', kWaveSetup.kgrid.t_array, softwareOpt, hardwareOpt);
%                      figure;plot(BfData_out{1}.data(150:end))
                    save(['sensor_data_shot' num2str(cc) '.mat'], 'Y00', '-v7.3');
                    else
                    load(['sensor_data_shot' num2str(cc) '.mat'], 'Y00');
                    end
                    BfData_out{cc}=pPatch.offlineBeamforming(Y00, kWaveSetup.kgrid.t_array, softwareOpt, hardwareOpt);
                    %figure; imagesc(BfData_out{cc}.data)
                    clear pPatch
                else %FieldII    
                    indexNonZero = obj.phantom.amplitude ~= 0;
                    
                    %%
                    %% Field II
                    %%
                    obj = obj.rotate_translate_phantom(saPosition, saAngle);
                    if nargin == 3
                        [Y_raw, tau] = calc_scat_all(obj.Tx.FiiPointer, obj.Rx.FiiPointer, obj.phantom.positionForCalc(indexNonZero,:), obj.phantom.amplitude(indexNonZero), 1);
                    else
                        Y_raw = rawData.Y;
                        tau   = rawData.tau;
                    end
                    
                    %% Tx beamforming
                    [Y0, tau] = obj.applyTxScanSequenceToFMC(Y_raw, tau, softwareOpt, hardwareOpt);
                    clear Y_raw
                    
                    pulseTxRx = conv(obj.Tx.impulseResponse,obj.Rx.impulseResponse);
                    max_ix = length(pulseTxRx)/2;
                    lagCorrection = max_ix / fs;
                    
                    timeAxis = (0:size(Y0,1)-1)/fs + tau - lagCorrection;
                    
                    %% RX beamforming
                    BfData_out{cc} = obj.offlineBeamforming(Y0, timeAxis, softwareOpt, hardwareOpt);
                end
                
                %% Digital beamforming, only one ADC
                if softwareOpt.hadamard
                    BfData_out{cc} = run_post_processing(obj, BfData_out{cc});
                end
                
                BfData_out{cc}.position   = saPosition;
                BfData_out{cc}.angle      = saAngle;
                if hardwareOpt.splitTxRx
                    BfData_out{cc}.position_Rx = saPosition_Rx;
                    BfData_out{cc}.angle_Rx    = saAngle_Rx;
                end
                waitbar((cc)/(size(softwareOpt.shotIndex,1)),h_w); 
            end
            delete(h_w), drawnow
        end
        
       function [meshPhantomUnique]=project_points_on_kWavegrid(obj, kgrid,data,ind)    
              iX_local=round((data(ind,1)-min(kgrid.x_vec))./kgrid.dx)+1;
              iY_local=round((data(ind,2)-min(kgrid.y_vec))./kgrid.dy)+1;
              iZ_local=round((data(ind,3)-min(kgrid.z_vec))./kgrid.dz)+1;
              if (min(iX_local)<1) || (max(iX_local)>numel(kgrid.x_vec))||(min(iY_local)<1) || (max(iY_local)>numel(kgrid.y_vec))||...
                                (min(iZ_local)<1) || (max(iZ_local)>numel(kgrid.z_vec))
                              error ('Something is wrong with kgrid, it does not cover the entire region');           
              end              
              meshPhantom=[iX_local iY_local iZ_local];
              meshPhantomUnique=unique(meshPhantom, 'rows');   
       end

       function [maskElementPos, maskElementIndexRow, maskElementIndexCol, currentElemPosCenters, maskElementPosStupidKwaveIndex]...
                                                                 =define_kwave_mask_for_current_subArray(obj, cc, kWaveSetup, softwareOpt)
                    maskElementPos=zeros(kWaveSetup.kgrid.Nx,kWaveSetup.kgrid.Ny,kWaveSetup.kgrid.Nz);
                    maskElementIndexRow=zeros(kWaveSetup.kgrid.Nx,kWaveSetup.kgrid.Ny,kWaveSetup.kgrid.Nz);
                    maskElementIndexCol=zeros(kWaveSetup.kgrid.Nx,kWaveSetup.kgrid.Ny,kWaveSetup.kgrid.Nz);
                    
                    currentElemPosCenters=cell(obj.Tx.subArraySize,obj.Tx.subArraySize);
                    [ind]=get_all_elements_in_subArray(obj,kWaveSetup,softwareOpt.shotIndex(cc,1),softwareOpt.shotIndex(cc,2));
                    for ik=1:size(ind,1)
                        [r,c,v] = ind2sub(size(kWaveSetup.KwavePatch.KWelementIndexRow),...
                             find((kWaveSetup.KwavePatch.KWelementIndexRow==ind(ik,1))&(kWaveSetup.KwavePatch.KWelementIndexCol==ind(ik,2))));
                        if mod(ik,obj.Tx.subArraySize)~=0
                            i1=fix(ik/obj.Tx.subArraySize)+1; i2=mod(ik,obj.Tx.subArraySize);
                        else
                            i1=fix((ik-1)/obj.Tx.subArraySize)+1; i2=obj.Tx.subArraySize;
                        end    
                        currentElemPosCenters{i1,i2}=kWaveSetup.KwavePatch.elementPosCenters{ind(ik,1),ind(ik,2)};
                        for jk=1:numel(r)
                             maskElementPos(r(jk), c(jk), v(jk))=1;
                             maskElementIndexRow(r(jk), c(jk), v(jk))=ind(ik,1);
                             maskElementIndexCol(r(jk), c(jk), v(jk))=ind(ik,2);
                        end    
                    end
                    temp=maskElementPos.*reshape(1:size(maskElementPos,1)*size(maskElementPos,2)*size(maskElementPos,3),size(maskElementPos,1), size(maskElementPos,2), size(maskElementPos,3));
                    temp=reshape(temp, [1 size(maskElementPos,1)*size(maskElementPos,2)*size(maskElementPos,3)]);
                    temp(temp~=0)=1:numel(temp(temp~=0));
                    maskElementPosStupidKwaveIndex=reshape(temp,size(maskElementPos,1), size(maskElementPos,2), size(maskElementPos,3));
                      
       end    

       function [ind]=get_all_elements_in_subArray(obj,kWaveSetup,ind1,ind2)
                   ind=kWaveSetup.KwavePatch.subArrayElementsPosIndex{ind1,ind2};
                    [A,B] = meshgrid(ind(1,:),ind(2,:));
                    ind=sortrows(reshape(cat(2,A',B'),[],2));           
       end   
       
        function blocked = check_if_blocked_by_ribs(obj, txAngle,r_max,saPosition,ribs)
            % check_if_blocked_by_ribs - check if the scan line is blocked
            % by ribs for the physical phantom
            %
            % Syntax: blocked = check_if_blocked_by_ribs(softwareOpt.txEvents(ii).txAngle,max(echoData.r),saPosition,ribs)
            %
            % Inputs
            %   txAngle - sectorial angle from softwareOpt
            %   r_max - max echoData depth
            %   saPosition -- position of the current subArray
            %   ribs -- triangulated surface representing the ribs with vertices and faces fields
            % Output
            %   blocked -- 1 if the scan line is blocked, 0 if not
            
            [xf,yf,zf] = PP_Utils.beam2cart(txAngle(1),txAngle(2),r_max);
            %Q1 and Q2 are two points that define the scan line
            Q1=saPosition;
            Q2=saPosition+[xf yf zf]; 
            %plot3([Q1(1) Q2(1)]*1000, [Q1(3) Q2(3)]*1000, [Q1(2) Q2(2)]*1000, 'g', 'LineWidth', 1);
 
            u = (Q2-Q1)/norm(Q2-Q1);  
            d = linspace(0,0.07,50)';
            %line defines 50 points along the scan line
            line = Q1 + d*u; 
            %D contains eucledian distances between scan line points and
            %vertices of the ribs surface. We are interested in the
            %ribs vertices, whose distances to scan line are smaller than
            %10mm (the threshold can be altered, this is just to speed it
            %up). ind are the indexes of such vertices
            D=pdist2(ribs.vertices, line); 
            [ind,~,~]=ind2sub(size(D),(find(D<0.01)));
            ind=unique(ind); ind1=[];
            % Now we find all the faces (ind1) of the ribs triangulation that
            % contain the vertices (with ind indexes). (because we do not want to check all faces, we had to narrow it down)
            for t=1:numel(ind)
                    [ri,~]=find(ribs.faces==ind(t));
                    ind1=[ind1;unique(ri)];
            end
            
            blocked=0;
            % For each of the faces from ind1, we find the plane equation
            % of that face. Now we check if that plane intersects scan line
            % Q2Q1. If it does intersect, we check if the point of intersection lies within the face triangle. 
            for t=1:numel(ind1)
                    P1=ribs.vertices(ribs.faces(ind1(t),1),:);
                    P2=ribs.vertices(ribs.faces(ind1(t),2),:);
                    P3=ribs.vertices(ribs.faces(ind1(t),3),:);                                       
                    N = cross(P2-P1,P3-P1); % Normal to the plane of the triangle
                    P0 = Q1 + dot(P1-Q1,N)/dot(Q2-Q1,N)*(Q2-Q1); % The point of intersection
                    if ( dot(cross(P2-P1,P0-P1),N)>=0 & ...
                      dot(cross(P3-P2,P0-P2),N)>=0 & ...
                        dot(cross(P1-P3,P0-P3),N)>=0)
                          blocked=1;
                          break;
                    end
           end
        end
        
        function  BfData = run_cole_unified_new(obj, softwareOpt, hardwareOpt, rawData, saPosition)
            % run_cole_unified - Uses the COLE algorithm for US simulation
            % in 2D or 3D
            % Syntax:  BfData = run_cole_unified(obj, softwareOpt, hardwareOpt, rawData)
            %
            % Inputs:
            %    softwareOpt - software options
            %    hardwareOpt - hardware options
            %    rawData - (optional) either LUT or FieldII raw data
            %    transmission
            %    saPosition - position of the current subArray
            %
            % Outputs:
            %    BfData - struct with fields data and timeAxis
            %
            % Example: see singleTest.m
            
            global c
            global plot_debug
            if nargin == 5
                echoData = rawData.echoData{1};
                impulseResponse = rawData.impulseResponse{1};
            else
                clear echoData rawData impulseResponse
            end
            
            %% simulation is 3D?
            threeD = all(any(obj.phantom.positionForCalc));
            if size(obj.phantom.positionForCalc,1) == 1
                threeD = all(obj.phantom.positionForCalc);
            end
            
            % save the ribs for checking later if they block the scan line
            if strcmp(obj.phantom.predefined,'physical_004')
                 index=find(strcmp(obj.phantom.segmentationClasses, 'ribs'));
                 ribs.vertices=obj.phantom.position(obj.phantom.segmentationIndex{index},:);
                 ribs.faces=obj.phantom.ribs; 
                 rMax=max(rawData.echoData{1}.r);
            end
            
            
            %% path to LUT
            folderName = strcat(pwd, '/Dataset/COLE/', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '/');
            if ~threeD
                index = find(softwareOpt.txEvents(1).txAngle(:,1)~=0);
                if isempty(index)
                    index = 1;
                end
                
                if index == 1
                    folderName = strcat(folderName, 'azimuth/');
                else
                    folderName = strcat(folderName, 'elevation/');
                end
            else
                folderName = strcat(folderName, '3D/');
                if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                     folderName = strcat(pwd, '\Dataset\COLE\', 'tx_on_rows', '_', 'rx_on_rows', '\3D\'); 
                end
            end
            
            %% first getting scatterers with zero amplitude out of the way
            indexNonZero = obj.phantom.amplitude ~=0;
            ph_position  = obj.phantom.positionForCalc(indexNonZero,:);
            ph_amplitude = obj.phantom.amplitude(indexNonZero,:);
            
            % convert to spherical coordinates
            [azPh, elPh, rPh] = PP_Utils.cart2beam(ph_position(:,1), ph_position(:,2), ph_position(:,3));
            
            % sorting;
            [rPh, ind] = sort(rPh);
            ph_amplitude = ph_amplitude(ind);
            
            %% phi and theta
            phiPh   =  azPh(ind);
            thetaPh =  elPh(ind);
            if ~threeD && index == 2
                phiPh = elPh;
            end
            index_end = 0;

            
            for ii = 1:length(softwareOpt.txEvents)
              %% check if the scan line is blocked by ribs
              if strcmp(obj.phantom.predefined, 'physical_004') 
                 blocked = obj.check_if_blocked_by_ribs(softwareOpt.txEvents(ii).txAngle,rMax,saPosition,ribs);
              else 
                 blocked=0;                        
              end   
              if ~blocked
                %% load LUT. Specify paths for 2D or 3D sim
                %if ~(exist('echoData', 'var') && exist('impulseResponse', 'var'))&& ~exist('rawData','var')
                if ~exist('rawData','var')
                    if threeD
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                            filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '.mat');
                        else    
                            filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '.mat');
                        end    
                    else
                        filename = strcat(folderName, 'LUT_angle_', num2str(abs((softwareOpt.txEvents(ii).txAngle(index)))), '.mat');
                    end
                    
                    if ~exist(filename, 'file')
                        error('COLE: LUT not found');
                    else
                        load(filename, 'echoData', 'impulseResponse');
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                              echoData.data = permute(echoData.data, [1 3 2]);
                        end    
                        if ~threeD
                            % LUTs consider symmetry. There are only LUTs
                            % for positive angles. For negative angles, flip
                            % the matrix
                            if sign(softwareOpt.txEvents(ii).txAngle(index)) == -1
                                echoData.data = fliplr(echoData.data);
                                
                                impulseResponse_orig = impulseResponse;
                                for jj = 1:length(impulseResponse.data)
                                    impulseResponse.data{jj} = impulseResponse_orig.data{end-jj+1};
                                end
                            end
                        end
                    end
                else %changes by Kate, for sectorial image for loading LUTs
                       echoData = rawData.echoData{ii};
                       impulseResponse = rawData.impulseResponse{ii};
                 end
                
                indexOfZero = find(echoData.angle == 0);
                
                %% looking for best match for radius -- this is done by solving a linear equation
                step = echoData.r(2) - echoData.r(1);
                indexRadius = max(min(round((rPh - echoData.r(1))/step) + 1, length(echoData.r)), 1);
                
                %% Calculating projection: sdfPosition is a Nx3 vector with [radiusPosition, anglePosition, echoData].
                % These 3 numbers are necessary for the composition of the
                % projections sdf
                if ~threeD
                    angleIndexPhi = round(phiPh)+indexOfZero;
                    angleIndexPhi = min(max(angleIndexPhi, 1), size(echoData.data, 2));
                    linearIndex = sub2ind(size(echoData.data), indexRadius, angleIndexPhi);
                    
                    sdfPosition = double([indexRadius, angleIndexPhi, echoData.data(linearIndex).*ph_amplitude]);
                else
                    angleIndexPhi = round(phiPh)+indexOfZero;
                    angleIndexPhi = min(max(angleIndexPhi, 1), size(echoData.data, 2));
                    
                    angleIndexTheta = round(thetaPh)+indexOfZero;
                    angleIndexTheta = min(max(angleIndexTheta, 1), size(echoData.data, 3));
                    
                    linearIndex_echoData = sub2ind(size(echoData.data), indexRadius, angleIndexPhi, angleIndexTheta);
                    linearIndex_impResp  = sub2ind(size(impulseResponse.data), angleIndexPhi, angleIndexTheta);
                    sdfPosition = double([indexRadius, linearIndex_impResp, echoData.data(linearIndex_echoData).*ph_amplitude]);
                end
                
                % sdf will contain the projection of the scatterers in
                % many directions. sdf is a
                % length(echoData.timeAxis)x numel(impulseResponse.data)
                % matrix. Each column contains the phantom projection for
                % one angle. Becuase most of the elements are zero, it is a
                % sparse matrix
                sdf = sparse(sdfPosition(:,1), sdfPosition(:,2), sdfPosition(:,3), length(echoData.timeAxis), numel(impulseResponse.data));
                
                %% plot the projection, debugging
                if plot_debug
                    figure(2);
                    plot(full(sdf));
                    title('Sdf');
                end
                
                %% doing convolutions
                % ignore sdf's that are below the peak threshold
                includeTheseImpulseResp = find(max(sdf) > softwareOpt.coleAmpThreshold);
                BfData.data(:,ii) = zeros(length(echoData.timeAxis), 1);
                for kk = includeTheseImpulseResp
                    sdfVec = full(sdf(:,kk));
                    BfData.data(:,ii) = BfData.data(:,ii) + conv(sdfVec, impulseResponse.data{kk}, 'same');
                end
                
                aux = find(BfData.data(:,ii));
                if index_end < (max(aux))
                    index_end = max(aux);
                end           
%                 if ~exist('rawData','var')
%                     clear echoData;
%                     clear impulseResponse;
%                 end
              else
                BfData.data(:,ii) = zeros(length(rawData.echoData{1}.timeAxis), 1); %change by Katya, if the scan line was blocked by ribs
              end    
                
            end
            
            %% Cutting data in time
            if ~isempty(softwareOpt.maxDepth)
                timeStamp = 2*softwareOpt.maxDepth/c;
                [~, indexTime] = min(abs(timeStamp - echoData.timeAxis));
                
                BfData.timeAxis = echoData.timeAxis(1:indexTime);
                BfData.data     = BfData.data(1:indexTime,:);
            else
                BfData.timeAxis = echoData.timeAxis(1:index_end);
                BfData.data     = BfData.data(1:index_end,:);
            end
            
            if plot_debug
                figure(1);
                hLegend = findobj(figure(1), 'Type', 'Legend');
                hLegend.String(2:end)=[];
                set(h_fake_phantom, 'Visible', 'off');
                set(h_center_subArray, 'Visible', 'off');
                set(h_fake_pressure_line, 'Visible', 'off');
                %set(h_projection_line, 'Visible', 'off');
                %set(h_sweep_lines, 'Visible', 'off');
            end
        end
        
        function  BfData = run_cole_splitTxRX(obj, ph_tx, ph_rx, softwareOpt, hardwareOpt)
            
            global c fs
            global plot_debug
            
            %% simulation is 3D?
            threeD = all(any(ph_tx));
            if size(obj.phantom.positionForCalc,1) == 1
                threeD = all(ph_tx) && all(ph_rx);
            end
            
            %% path to LUT
            folderName = strcat(pwd, '/Dataset/COLE_emittedField/', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '/');
            if ~threeD
                index = find(softwareOpt.txEvents(1).txAngle(:,1)~=0);
                if isempty(index)
                    index = 1;
                end
                
                if index == 1
                    folderName = strcat(folderName, 'azimuth/');
                else
                    folderName = strcat(folderName, 'elevation/');
                end
            else
                folderName = strcat(folderName, '3D/');
            end
            
            %% first getting scatterers with zero amplitude out of the way
            indexNonZero = obj.phantom.amplitude ~=0;
            ph_position_TX  = ph_tx(indexNonZero,:);
            ph_position_RX  = ph_rx(indexNonZero,:);
            ph_amplitude    = obj.phantom.amplitude(indexNonZero,:);
            
            % convert to spherical coordinates
            [azPh_tx, elPh_tx, rPh_tx] = PP_Utils.cart2beam(ph_position_TX(:,1), ph_position_TX(:,2), ph_position_TX(:,3));
            [azPh_rx, elPh_rx, rPh_rx] = PP_Utils.cart2beam(ph_position_RX(:,1), ph_position_RX(:,2), ph_position_RX(:,3));
            
            %% phi and theta
            index_end = 0;
            
            for ii = 1:length(softwareOpt.txEvents)
                
                %% load LUT. Specify paths for 2D or 3D sim
                if threeD
                    filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '.mat');
                else
                    filenameTX = strcat(folderName, 'LUT_angle_', num2str(abs((softwareOpt.txEvents(ii).txAngle(index)))), '.mat');
                    filenameRX = strcat(folderName, 'LUT_angle_', num2str(abs((softwareOpt.txEvents(ii).rxAngles(index)))), '.mat');
                end
                
                if ~exist(filenameTX, 'file') || ~exist(filenameRX, 'file')
                    error('COLE: LUT not found');
                else
                    load(filenameTX, 'emittedField', 'impulseResponse');
                    emittedFieldTx        = emittedField;
                    impulseResponseTx = impulseResponse;
                    
                    load(filenameRX, 'emittedField', 'impulseResponse');
                    emittedFieldRx        = emittedField;
                    impulseResponseRx = impulseResponse;
                    
                    clear emittedField impulseResponse;
                    
                    if ~threeD
                        % LUTs consider symmetry. There are only LUTs
                        % for positive angles. For negative angles, flip
                        % the matrix
                        if sign(softwareOpt.txEvents(ii).txAngle(index)) == -1
                            echoData.data = fliplr(echoData.data);
                            
                            impulseResponse_orig = impulseResponse;
                            for jj = 1:length(impulseResponse.data)
                                impulseResponse.data{jj} = impulseResponse_orig.data{end-jj+1};
                            end
                        end
                    end
                end
                
                indexOfZero = find(emittedFieldTx.angle == 0);
                
                %% looking for best match for radius -- this is done by solving a linear equation
                step = emittedFieldTx.r(2) - emittedFieldTx.r(1);
                indexRadius_Tx = max(min(round((rPh_tx - emittedFieldTx.r(1))/step) + 1, length(emittedFieldTx.r)), 1);
                indexRadius_Rx = max(min(round((rPh_rx - emittedFieldRx.r(1))/step) + 1, length(emittedFieldRx.r)), 1);
                
                %% Calculating projection: sdfPosition is a Nx3 vector with [radiusPosition, anglePosition, echoData].
                % These 3 numbers are necessary for the composition of the
                % projections sdf
                if ~threeD
                    angleIndexPhi_tx = min(max(round(azPh_tx)+indexOfZero, 1), size(emittedFieldTx.data, 2));
                    linearIndex_Tx   = sub2ind(size(emittedFieldTx.data), indexRadius_Tx, angleIndexPhi_tx);
                    sdfPosition_Tx = double([indexRadius_Tx, angleIndexPhi_tx, emittedFieldTx.data(linearIndex_Tx)]);

                    angleIndexPhi_rx = min(max(round(azPh_rx)+indexOfZero, 1), size(emittedFieldRx.data, 2));
                    linearIndex_Rx   = sub2ind(size(emittedFieldRx.data), indexRadius_Rx, angleIndexPhi_rx);
                    sdfPosition_Rx = double([indexRadius_Rx, angleIndexPhi_rx, emittedFieldRx.data(linearIndex_Rx).*ph_amplitude]);
                else
                    angleIndexPhi = round(phiPh)+indexOfZero;
                    angleIndexPhi = min(max(angleIndexPhi, 1), size(echoData.data, 2));
                    
                    angleIndexTheta = round(thetaPh)+indexOfZero;
                    angleIndexTheta = min(max(angleIndexTheta, 1), size(echoData.data, 3));
                    
                    linearIndex_echoData = sub2ind(size(echoData.data), indexRadius, angleIndexPhi, angleIndexTheta);
                    linearIndex_impResp  = sub2ind(size(impulseResponse.data), angleIndexPhi, angleIndexTheta);
                    sdfPosition = double([indexRadius, linearIndex_impResp, echoData.data(linearIndex_echoData).*ph_amplitude]);
                end
                
                %% plot the projection, debugging
                if plot_debug
                    figure(2);
                    plot(full(sdfPosition_Rx));
                    title('Sdf');
                end
                
                %% doing convolutions
                % ignore sdf's that are below the peak threshold
                BfData.data(:,ii) = zeros(2*length(emittedFieldTx.timeAxis)-1, 1);
               
                for kk = 1:size(sdfPosition_Rx,1)
                    sdf_Tx = zeros(length(emittedFieldTx.timeAxis), 1);
                    sdf_Tx(sdfPosition_Tx(kk,1)) = 1;
                    sdf_Rx = zeros(length(emittedFieldRx.timeAxis), 1);
                    sdf_Rx(sdfPosition_Rx(kk,1)) = 1;
                    
                    position = conv(sdf_Tx, sdf_Rx);
                    impulseR = conv(impulseResponseTx.data{sdfPosition_Tx(kk,2)}, impulseResponseRx.data{sdfPosition_Rx(kk,2)});
                    gain     = sdfPosition_Tx(kk,3)*sdfPosition_Rx(kk,3);
                    
                    result = gain*conv(position, impulseR, 'same');
                    BfData.data(:,ii) =  BfData.data(:,ii) + result;

                end
                
                aux = find(BfData.data(:,ii));
                if index_end < (max(aux))
                    index_end = max(aux);
                end
                
                if ii < length(softwareOpt.txEvents)
                    clear echoData;
                    clear impulseResponse;
                end
                
            end
            step = emittedFieldTx.timeAxis(2) - emittedFieldTx.timeAxis(1);
            timeAxis = (0:size(BfData.data,1)-1)/fs + 2*emittedFieldTx.timeAxis(1)-2*step;
            %% Cutting data in time
            if ~isempty(softwareOpt.maxDepth)
                timeStamp = 2*softwareOpt.maxDepth/c;
                
                [~, indexTime] = min(abs(timeStamp - timeAxis));
                
                BfData.timeAxis = timeAxis(1:indexTime);
                BfData.data     = BfData.data(1:indexTime,:);
            else
                BfData.timeAxis = timeAxis(1:index_end);
                BfData.data     = BfData.data(1:index_end,:);
            end
            
            if plot_debug
                figure(1);
                hLegend = findobj(figure(1), 'Type', 'Legend');
                hLegend.String(2:end)=[];
                set(h_fake_phantom, 'Visible', 'off');
                set(h_center_subArray, 'Visible', 'off');
                set(h_fake_pressure_line, 'Visible', 'off');
                %set(h_projection_line, 'Visible', 'off');
                %set(h_sweep_lines, 'Visible', 'off');
            end
        end
        
        function pulseEcho = calculate_echo_field_2D(obj, r, anglePosition, tx_focus, hardwareOpt)
            % calculate_echo_field_2D - calculates pulse-echo response for
            % positions described by r and anglePosition
            % r and anglePosition decribe the sweep. tx_focus contains the
            % information about the focus point
            % Syntax:  pulseEcho = calculate_echo_field_2D(obj, r, anglePosition, tx_focus, hardwareOpt)
            %
            % Inputs:
            %    r - vector; depth for the sweep
            %    anglePosition - Nx2 matrix with azimuth and elevation
            %    angles in degrees. Angles for the sweep
            %    tx_focus - 1x3 vector with focus point
            %    hardwarept - hardware options
            %
            % Outputs:
            %    pulseEcho - matrix with dimensions zeros(length(r),
            %    size(anglePosition,1)) with pulse-echo
            % Example:
            %    See generate_lookup_table_2D.m
            
            %% TX
            aux = obj.convert_cell_to_Fii_mat(obj.Tx.centralElementPos);
            if strcmp(hardwareOpt.mode_tx, 'tx_on_rows')
                txIndex = kron(eye(obj.Tx.subArraySize), ones( 1, obj.Tx.subArraySize));
                elementPos  = kron(txIndex*aux./sum(txIndex,2), ones(obj.Tx.subArraySize,1));
            else
                elementPos  = aux;
            end
            delays = obj.Tx.computeTxDelayLine(0, tx_focus, elementPos);
            apodizaton = obj.Tx.getApoMat(hardwareOpt);
            
            txPointer = obj.Tx.FiiPointer;
            
            xdc_times_focus(txPointer, 0, delays);
            xdc_apodization(txPointer, 0, apodizaton(:)');
            
            %% RX
            aux = obj.convert_cell_to_Fii_mat(obj.Tx.centralElementPos);
            if strcmp(hardwareOpt.mode_rx, 'rx_on_cols')
                rxIndex = repmat(eye(obj.Rx.subArraySize), 1, obj.Rx.subArraySize);
                elementPos  = repmat(rxIndex*aux./sum(rxIndex,2), obj.Rx.subArraySize,1);
            elseif strcmp(hardwareOpt.mode_rx, 'rx_on_rows')
                rxIndex = kron(eye(obj.Rx.subArraySize), ones(1,obj.Rx.subArraySize));
                elementPos  = kron(rxIndex*aux./sum(rxIndex,2), ones(obj.Rx.subArraySize,1));
            else
                elementPos  = aux;
            end
            delays = obj.Rx.computeTxDelayLine(0, tx_focus, elementPos);
            apodizaton = obj.Rx.getApoMat(hardwareOpt);
            
            rx_pointer = obj.Rx.FiiPointer;
            
            xdc_times_focus(rx_pointer, 0, delays);
            xdc_apodization(rx_pointer, 0, apodizaton(:)');
            
            pulseEcho = zeros(length(r), size(anglePosition,1));
            for nn = 1:length(r)
                points = PP_Utils.beam2cart(anglePosition(:,1), anglePosition(:,2), r(nn)*ones(size(anglePosition,1),1));
                p1 = calc_hhp(txPointer, rx_pointer, points);
                pulseEcho(nn,:) = max(abs(p1));
            end
            
        end
        
        function [emittedField, impulseResponse] = calculate_emmited_field_2D(obj, r, anglePosition, tx_focus, hardwareOpt)
            % calculate_emmited_field_2D - calculates emmited field for
            % positions described by r and anglePosition
            % r and anglePosition decribe the sweep. tx_focus contains the
            % information about the focus point. This function uses only
            % one probe. For the pulse-echo response, see calculate_echo_field_2D
            % Syntax:  emittedField = calculate_emmited_field_2D(obj, r, anglePosition, tx_focus, hardwareOpt)
            %
            % Inputs:
            %    r - vector; depth for the sweep
            %    anglePosition - Nx2 matrix with azimuth and elevation
            %    angles in degrees. Angles for the sweep
            %    tx_focus - 1x3 vector with focus point
            %    hardwarept - hardware options
            %
            % Outputs:
            %    pulseEcho - matrix with dimensions zeros(length(r),
            %    size(anglePosition,1)) with emmited filed
            %    impulseResponse - cell with dimension
            %    size(anglePosition,1))x1
            % Example:
            %    See generate_emmitefField_lookup_table_2D.m, generate_lookup_table_2D
            
            
            %% set tx aposization and delays
            aux = obj.convert_cell_to_Fii_mat(obj.Tx.centralElementPos);
            if strcmp(hardwareOpt.mode_tx, 'tx_on_rows')
                txIndex = kron(eye(obj.Tx.subArraySize), ones( 1, obj.Tx.subArraySize));
                elementPos  = kron(txIndex*aux./sum(txIndex,2), ones(obj.Tx.subArraySize,1));
            else
                elementPos  = aux;
            end
            delays = obj.Tx.computeTxDelayLine(0, tx_focus, elementPos);
            apodizaton = obj.Tx.getApoMat(hardwareOpt);
            
            txPointer = obj.Tx.FiiPointer;
            
            xdc_times_focus(txPointer, 0, delays);
            xdc_apodization(txPointer, 0, apodizaton(:)');
                    
            %% calculate emitted field - in contrast to calc_echo_pulse, notice that the function calc_hp is used
            emittedField = zeros(length(r), size(anglePosition,1));
            for nn = 1:length(r)
                points = PP_Utils.beam2cart(anglePosition(:,1), anglePosition(:,2), r(nn)*ones(size(anglePosition,1),1));
                p1 = calc_hp(txPointer, points);
                emittedField(nn,:) = max(abs(p1));
            end
            
            %% calculate impulse response - in contrast to calc_echo_pulse, notice that the function calc_hp is used
            points = PP_Utils.beam2cart(anglePosition(:,1), anglePosition(:,2), norm(tx_focus)*ones(length(anglePosition),1));
            [p1, tau1]  = calc_hp(txPointer, points);
            
            impulseResponse = cell(size(anglePosition,1),1);
            for kk = 1:size(anglePosition,1)
                p1(:,kk) = p1(:,kk)/max(p1(:,kk));
                index = find(abs(p1(:,kk)) > 1e-5);
                impulseResponse{kk} = p1(index(1):index(end),kk);
            end
        end
        
        function impulseResponse = calculate_impulse_response_2D(obj, focalDepth, anglePosition, tx_focus, hardwareOpt)
            % calculate_impulse_response_2D - calculates impulse reponse for
            % positions described by r and anglePosition
            % r and anglePosition decribe the sweep. tx_focus contains the
            % information about the focus point
            % Syntax:  impulseResponse = calculate_impulse_response_2D(obj, r, anglePosition, tx_focus, hardwareOpt)
            %
            % Inputs:
            %    focalDepth - scalar, depth for the sweep
            %    anglePosition - Nx2 matrix with azimuth and elevation
            %    angles in degrees. Angles for the sweep
            %    tx_focus - 1x3 vector with focus point
            %    hardwarept - hardware options
            %
            % Outputs:
            %    impulseResponse - cell with dimension
            %    size(anglePosition,1))x1
            % Example:
            %    See generate_lookup_table_2D.m
            
            %% TX
            aux = obj.convert_cell_to_Fii_mat(obj.Tx.centralElementPos);
            if strcmp(hardwareOpt.mode_tx, 'tx_on_rows')
                txIndex = kron(eye(obj.Tx.subArraySize), ones( 1, obj.Tx.subArraySize));
                elementPos  = kron(txIndex*aux./sum(txIndex,2), ones(obj.Tx.subArraySize,1));
            else
                elementPos  = aux;
            end
            delays = obj.Tx.computeTxDelayLine(0, tx_focus, elementPos);
            apodizaton = obj.Tx.getApoMat(hardwareOpt);
            
            txPointer = obj.Tx.FiiPointer;
            
            xdc_times_focus(txPointer, 0, delays);
            xdc_apodization(txPointer, 0, apodizaton(:)');
            
            %% RX
            aux = obj.convert_cell_to_Fii_mat(obj.Rx.centralElementPos);
            if strcmp(hardwareOpt.mode_rx, 'rx_on_cols')
                rxIndex = repmat(eye(obj.Rx.subArraySize), 1, obj.Rx.subArraySize);
                elementPos  = repmat(rxIndex*aux./sum(rxIndex,2), obj.Rx.subArraySize,1);
            elseif strcmp(hardwareOpt.mode_rx, 'rx_on_rows')
                rxIndex = kron(eye(obj.Rx.subArraySize), ones(1,obj.Rx.subArraySize));
                elementPos  = kron(rxIndex*aux./sum(rxIndex,2), ones(obj.Rx.subArraySize,1));
            else
                elementPos  = aux;
            end
            delays = obj.Rx.computeTxDelayLine(0, tx_focus, elementPos);
            apodizaton = obj.Rx.getApoMat(hardwareOpt);
            
            rx_pointer = obj.Rx.FiiPointer;
            
            xdc_times_focus(rx_pointer, 0, delays);
            xdc_apodization(rx_pointer, 0, apodizaton(:)');
            
            points = PP_Utils.beam2cart(anglePosition(:,1), anglePosition(:,2), focalDepth*ones(length(anglePosition),1));
            
            p1  = calc_hhp(txPointer, rx_pointer, points);
            
            impulseResponse = cell(size(anglePosition,1),1);
            for kk = 1:size(anglePosition,1)
                p1(:,kk) = p1(:,kk)/max(p1(:,kk));
                index = find(abs(p1(:,kk)) > 1e-5);
                impulseResponse{kk} = p1(index(1):index(end),kk);
            end
            
        end
        
        function [Y0, tStart] = applyTxScanSequenceToFMC(obj, Yin, tau, softwareOpt, hardwareOpt, plot_debug)
            % applyTxScanSequenceToFMC - This function takes the raw data and
            % outputs the equivalent beamformed channel data corresponding
            % to the software options applied. The output Yout are in the same format that would be
            % generated through experimental acquisition.
            %  [Yout, tStart] = applyTxScanSequenceToFMC(Yin, tau, softwareOpt, hardwareOpt).
            % Inputs:
            %    Yin - US raw data
            %    tau - start time of raw data
            %    softwareOpt - software options
            %    hardwareOpt - hardware options
            %
            % Outputs:
            %    Y0 - TX beamformed data
            %    tStart - start time of beamformed data
            
            global fs
            global plot_debug
            
            elementPos  = obj.convert_cell_to_Fii_mat(obj.Tx.centralElementPos);
            
            if hardwareOpt.PMUT_level
                vec = sqrt(hardwareOpt.PMUTPerElement):2*sqrt(hardwareOpt.PMUTPerElement):obj.Tx.subArraySize;
                vec = [ -vec(end:-1:1) vec ]';
                minEle = min(abs(elementPos(:,1)));
                elementPos = elementPos/minEle;
                [~, a] = min(abs(bsxfun(@minus, elementPos(:,1), vec')),[], 2);
                [~, b] = min(abs(bsxfun(@minus, elementPos(:,2), vec')),[], 2);
                
                elementPos = [vec(a) vec(b) zeros(length(a),1)]*minEle;
            end
            
            tx_apod_array = obj.Tx.getApoMat(hardwareOpt);
            
            if strcmp(hardwareOpt.mode_tx, 'tx_on_rows')
                txIndex = kron(eye(obj.Tx.subArraySize), ones( 1, obj.Tx.subArraySize));
                elementPos  = kron(txIndex*elementPos./sum(txIndex,2), ones(obj.Tx.subArraySize,1));
            elseif strcmp(hardwareOpt.mode_tx, 'tx_on_cols')
                rxIndex = repmat(eye(obj.Tx.subArraySize), 1, obj.Tx.subArraySize);
                elementPos  = repmat(rxIndex*elementPos./sum(rxIndex,2), obj.Tx.subArraySize,1);
            else
                tx_apod_array = tx_apod_array';
                tx_apod_array = tx_apod_array(:);
            end
            if plot_debug %changes by Kate
                figure(1);hold on;
                scatter3(elementPos(:,1)*1000,elementPos(:,3)*1000,elementPos(:,2)*1000,10.0,'filled','g');
            end
            
            nFrames = size(tx_apod_array,2);
            
            nElements_x  = obj.Tx.subArraySize;
            nElements_y  = obj.Tx.subArraySize;
            nTxEvents    = length(softwareOpt.txEvents);
            nTxAz        = softwareOpt.nTxAz;
            nTxEl        = softwareOpt.nTxEl;
            tStart       = tau;
            
            if hardwareOpt.regressionMode
                % Waitbar
                h_w = waitbar(0,'Restructuring Field II channel data ...');
                fprintf('\t Field II channel data (Tx: %gx%g, Packets: %g, Frames: %g) \t ... \t', nTxAz, nTxEl, 1, 1)
                t0 = tic;
            end
            
            nSamples  = size(Yin,1);
            nChannels = sqrt(size(Yin,2));
            Yin = reshape(Yin,[nSamples,nChannels,nChannels]);
            
            tx_delays = zeros(nChannels,nTxEvents);
            for tx_idx = 1:nTxEvents
                % Compute Tx delays
                tx_focus  = softwareOpt.txEvents(tx_idx).txFocus;
                tx_angle  = softwareOpt.txEvents(tx_idx).txAngle;
                tx_delays(:,tx_idx) = obj.Tx.computeTxDelayLine(tx_angle, tx_focus, elementPos);
                if plot_debug %changes by Kate, debug
                    a={softwareOpt.txEvents.txFocus};
                    figure(1);
                    scatter3(a{tx_idx}(1)*1000,a{tx_idx}(3)*1000,a{tx_idx}(2)*1000,10.0,'filled','g');
                    tx_apex = obj.Tx.centerOfAperture;
                    %{
                        %delayS plotting
                        figure(2); view(2);
                        hold on;
                        cc=rand(1,3);
                        plot(1:nChannels, tx_delays(:,tx_idx), 'Color',cc, 'Linewidth',3);view(2);
                        xlabel('Elements'); ylabel('Delays');
                    %}
                    hold on;
                    plot3([tx_apex(1)*1000 a{tx_idx}(1)*1000],[tx_apex(3)*1000 a{tx_idx}(3)*1000],[tx_apex(2)*1000 a{tx_idx}(2)*1000],'b');
                end
            end
            
            tMin = min(tx_delays(:));
            tx_delays = tx_delays - tMin;
            tStart = tStart + tMin;
            tx_delays_samples = max(floor(tx_delays * fs), 0);
            max_delay_samples = max(tx_delays_samples(:));
            nSamplesDep  = nSamples + max_delay_samples;
            
            Y0 = zeros(nSamplesDep, nElements_x*nElements_y, nTxEvents, nFrames);
            
            for tx_idx = 1:nTxEvents
                
                for ff = 1:nFrames
                    for tx = 1:nChannels
                        samplesBegin = tx_delays_samples(tx,tx_idx)+1;
                        samplesEnd = tx_delays_samples(tx,tx_idx)+nSamples;
                        Y0(samplesBegin:samplesEnd, :, tx_idx, ff) = Y0(samplesBegin:samplesEnd, :, tx_idx, ff) + + (tx_apod_array(tx, ff) .* Yin(:,:,tx));
                    end
                end
                
                if hardwareOpt.regressionMode
                    % update waitbar
                    waitbar((tx_idx)/(nTxEvents),h_w);
                end
                
            end
            
            % Reshape for [nRanges, nAzChannels, nElChannels, nTxAz, nTxEl, nPackets, nFrames]
            Y0 = reshape(Y0, nSamplesDep, nElements_x*nElements_y, nTxAz, nTxEl, nFrames);
            
            if hardwareOpt.regressionMode
                fprintf('Done! (%.2f sec)\n', toc(t0))
                delete(h_w), drawnow
            end
        end
        
        function BfData = offlineBeamforming(obj, Y, timeAxis, softwareOpt, hardwareOpt,varargin)
            % offlineBeamforming - offlineBeamforming beamforms simulation or experimental 2D data
            % Inputs:
            %    Y - big matrix; beamformed by the TX
            %    timeAxis - vector; time stamp
            %    softwareOpt - software options
            %    hardwareOpt - hardware options
            %
            % Outputs:
            %    BfData - full beamformed data (by TX and RX). Struct containing fields data and timeAxis
            %
            % Vangjush Komini, Pedro Santos (KU Leuven, 2016)
            
            global c
            global plot_debug
            

            elementPos  = obj.convert_cell_to_Fii_mat(obj.Rx.elementPos); 
            scatter3(elementPos(:,1).*10^3,elementPos(:,3).*10^3, elementPos(:,2).*10^3, 10.0, 'r', 'filled');
            scatter3(obj.Rx.centerOfAperture(:,1).*10^3,obj.Rx.centerOfAperture(:,3).*10^3,obj.Rx.centerOfAperture(:,2).*10^3, 10.0, 'r', 'filled');
            if hardwareOpt.PMUT_level
                vec = sqrt(hardwareOpt.PMUTPerElement):2*sqrt(hardwareOpt.PMUTPerElement):obj.Rx.subArraySize;
                vec = [ -vec(end:-1:1) vec ]';
                minEle = min(abs(elementPos(:,1)));
                elementPos = elementPos/minEle;
                [~, a] = min(abs(bsxfun(@minus, elementPos(:,1), vec')),[], 2);
                [~, b] = min(abs(bsxfun(@minus, elementPos(:,2), vec')),[], 2);
                
                elementPos = [vec(a) vec(b) zeros(length(a),1)]*minEle;
            end
            
            rx_apod = obj.Rx.getApoMat(hardwareOpt);
            
            % Add default values
            if strcmp(hardwareOpt.mode_rx, 'rx_on_cols')
                rxIndex = repmat(eye(obj.Rx.subArraySize), 1, obj.Rx.subArraySize);
                elementPos  = repmat(rxIndex*elementPos./sum(rxIndex,2), obj.Rx.subArraySize,1);
            elseif strcmp(hardwareOpt.mode_rx, 'rx_on_cols_v2')
                rxIndex = repmat(eye(obj.Rx.subArraySize), 1, obj.Rx.subArraySize);
                rxIndex1 = rxIndex(:,1:end/2);
                rxIndex2 = rxIndex(:,end/2+1:end);
                rxIndex =  [ [rxIndex1 zeros(size(rxIndex1))]; [zeros(size(rxIndex2)) rxIndex2]];
                elementPos  = rxIndex*elementPos./sum(rxIndex,2);
                S = obj.Rx.subArraySize*obj.Rx.subArraySize/size(elementPos,1);
                elementPos = [repmat(elementPos(1:end/2,:), S, 1);  repmat(elementPos(end/2+1:end,:), S, 1)];
            elseif strcmp(hardwareOpt.mode_rx, 'rx_on_rows')
                rxIndex = kron(eye(obj.Rx.subArraySize), ones(1,obj.Rx.subArraySize));
                elementPos  = kron(rxIndex*elementPos./sum(rxIndex,2), ones(obj.Rx.subArraySize,1));
            else
                rx_apod = rx_apod(:);
            end
            if plot_debug
                figure(1);
                hold on;
                scatter3(elementPos(:,1)*1000,elementPos(:,3)*1000,elementPos(:,2)*1000,10.0,'filled','r');
            end
            imgRanges     = timeAxis/2*c;
            
            nRX             = size(Y, 2);
            nAzTxBeams      = size(Y, 3);
            nElTxBeams      = size(Y, 4);
            nFrames         = size(Y, 5);
            nADC            = hardwareOpt.nADC;
            
            nRangesOut      = length(imgRanges);
            nMLAs           = softwareOpt.nMLAs; % take it from Reconstruction
            nMLAsAz         = nMLAs(1);
            nMLAsEl         = nMLAs(2);
            nMLTs           = [1  1];
            nMLTsAz         = nMLTs(1);
            nMLTsEl         = nMLTs(2);
            
            ch_time        = timeAxis';
            
            rx_depths      = imgRanges;
            
            % ADC indexes
            indexADC = cell(nADC,1);
            colrow = 1:obj.Rx.subArraySize:(obj.Rx.subArraySize*obj.Rx.subArraySize);
            iADC = 1;
            
            for couter = 1:obj.Rx.subArraySize
                
                indexADC{iADC} = union(indexADC{iADC}, colrow);
                
                colrow = colrow+1;
                if  mod(couter,obj.Rx.subArraySize/hardwareOpt.nADC ) == 0
                    iADC = iADC + 1;
                end
                
            end
            
            % Allocate space
            Beamformed       = zeros(nRangesOut, nMLTsAz*nMLAsAz*nAzTxBeams, nMLTsEl*nMLAsEl*nElTxBeams, nADC, nFrames, 'single');
            % Prepare interpolation grid
            [ChTimeGrid, ChannelsGrid] = ndgrid(single(ch_time), single(1:nRX));
            [BfTimeGrid, BeamformGrid] = ndgrid(single(imgRanges)*2/c, single(1:nRX));
            
            if nMLTsEl>1 || nMLAsEl > 1
                keyboard % 3D is not checked after last code refactoring
            end
            
            %% Start beamforming
            if hardwareOpt.regressionMode
                h = waitbar(0,'Beamforming, wait please');
                fprintf('\t Computing delay and sum (Image lines: %gx%g, Packets: %g, Frames: %g) \t ... \t', softwareOpt.nTxAz* softwareOpt.nTxEl, 1, 1)
            end
            
            t0 = tic;
            
            for tx_el_i = 1:nElTxBeams
                for tx_az_i = 1:nAzTxBeams
                    tx_i = (tx_el_i-1)*nAzTxBeams + tx_az_i; % current Tx event number including packets
                    for mlt_el = 1:nMLTsEl
                        for mlt_az = 1:nMLTsAz
                            mlt_i = (mlt_el-1)*nMLTsAz + mlt_az;
                            tx_focus   = softwareOpt.txEvents(tx_i).txFocus(mlt_i, :);
                            tx_angles  = softwareOpt.txEvents(tx_i).txAngle(:, mlt_i);
                            for mla_el = 1:nMLAsEl
                                rx_el      = (tx_el_i-1)*nMLAsEl*nMLTsEl + (mlt_el-1)*nMLAsEl + mla_el;
                                for mla_az = 1:nMLAsAz
                                    mla_i     = (mlt_el-1)*nMLTsAz*nMLAsEl*nMLAsAz + (mlt_az-1)*nMLAsEl*nMLAsAz + (mla_el-1)*nMLAsAz + mla_az;
                                    rx_az     = (tx_az_i-1)*nMLAsAz*nMLTsAz + (mlt_i-1)*nMLAsAz + mla_az;
                                    rx_angles = softwareOpt.txEvents(tx_i).rxAngles(:, mla_i);
                                    rx_apod   = reshape(rx_apod, [], nRX);
                                    
                                    % Compute delays
                                    if isfield(softwareOpt, 'Muxing') && isfield(softwareOpt.Muxing, 'TxChannelId') && ~isempty(softwareOpt.Muxing.TxChannelId)
                                        active_elements = reshape(softwareOpt.Muxing.TxChannelId(:,:,1)>0, [], 1);
                                        active_pos = reshape(elementPos, [], 3);
                                        active_pos = active_pos(active_elements, :);
                                        delay_line = computeRxDelayLine(tx_focus, rx_depths', rx_angles, active_pos, tx_angles);
                                    else
                                        delay_line = obj.Rx.computeRxDelayLine(tx_focus, rx_depths', rx_angles, elementPos, tx_angles);
                                    end
                                    delay_line = single(delay_line);
                                    
                                    % Beamform this beam for all frames
                                    for frame_i = 1:nFrames
                                        
                                        % Get channel date of current Tx event
                                        Y_txEvent = Y(:, :, tx_az_i, tx_el_i, frame_i);  % [nDep nCh]
                                        
                                        % Apply delays
                                        GI = griddedInterpolant(ChTimeGrid, ChannelsGrid, Y_txEvent); % create gridded interpolant directly
                                        alignedChLine = bsxfun(@times, rx_apod, GI(delay_line, BeamformGrid));
                                        
                                        for iADC = 1:nADC
                                            Beamformed(:, rx_az, rx_el, iADC, frame_i) = sum(sum(alignedChLine(:,indexADC{iADC}), 2), 3);
                                        end
                                        
                                    end
                                    
                                    % Update waitbar
                                    if hardwareOpt.regressionMode
                                        waitbar(((tx_i-1)*prod(nMLAs)*prod(nMLTs) + mla_i)/(prod(nMLAs)*prod(nMLTs)*nAzTxBeams*nElTxBeams*nPackets),h);
                                    end
                                end
                            end
                        end
                    end
                end
            end
            
            if hardwareOpt.regressionMode
                close(h); drawnow
                fprintf('Done! (%.2f sec)\n', toc(t0))
            end
            
            %% Take care of NaNs & pack datasets
            Beamformed(isnan(Beamformed)) = 0;
            BfData.data     = Beamformed;
            BfData.timeAxis = timeAxis;
            
        end
        
        function BfData = run_post_processing(obj, BfData)
            W = hadamard(obj.Tx.subArraySize);
            
            % Decoding Hadamard code
            unHadamard =  zeros(size(BfData.data));
            for nn = 1:size(BfData.data,1)
                for tx_idx = 1:size(BfData.data,3)
                    aux = squeeze(BfData.data(nn,:,tx_idx,:,:,:));
                    if iscolumn(aux)
                        aux = aux';
                    end
                    
                    unHadamard(nn, :,tx_idx,:,:) = aux*W/length(W);
                end
            end
            
            txIndex = kron(eye(length(W)), ones( 1, length(W)));
            elementPos = txIndex*cell2mat(obj.Tx.centralElementPos(:))./sum(txIndex,2);
            
            unHadamard = permute(unHadamard, [1 4 5 2 3]); % N x nADC x length(W) x (nEL*nAZ)
            unHadamard = reshape(unHadamard, size(unHadamard,1), length(W)*hardwareOpt.nADC, softwareOpt.nTxAz, softwareOpt.nTxEl);
            
            if hardwareOpt.nADC > 1
                rxIndex = repmat(eye(length(W)), 1, length(W));
                rxPos  = rxIndex*cell2mat(obj.Rx.centralElementPos)./sum(rxIndex,2);
                rxIndex2 = kron(eye(hardwareOpt.nADC), ones(1,size(rxIndex, 1)/hardwareOpt.nADC));
                rxPos =  rxIndex2*rxPos./sum(rxIndex2,2);
                elementPos = kron(elementPos, ones(size(rxPos,1),1));% + repmat(rxPos, size(elementPos,1),1);
            end
            
            BfData = obj.offlineBeamforming_post(unHadamard, timeAxis, softwareOpt, elementPos, hardwareOpt);
        end
        
        function BfData = offlineBeamforming_post(obj, Y, timeAxis, softwareOpt, elementPos, hardwareOpt)
            % offlineBeamforming_post - offlineBeamforming post-beamforms
            % simulation or experimental 2D data. offlineBeamforming uses
            % data generated by several consecutive transmission (e.g. with Hadamard coding)
            % Inputs:
            %    Y - big matrix; beamformed by the TX  and RX
            %    timeAxis - vector; time stamp
            %    softwareOpt - software options
            %    elementPos - position of all elements
            %    hardwareOpt - hardware options
            %
            % Outputs:
            %    BfData - full beamformed data (by TX and RX and post). Struct containing fields data and timeAxis
            
            global c
            
            imgRanges     = timeAxis/2*c;
            
            nRX             = size(Y, 2);
            nAzTxBeams      = size(Y, 3);
            nElTxBeams      = size(Y, 4);
            
            nRangesOut      = length(imgRanges);
            
            ch_time        = timeAxis';
            
            rx_depths      = imgRanges;
            rx_apex        = [0 0 0];
            
            % Allocate space
            Beamformed       = zeros(nRangesOut, nAzTxBeams, nElTxBeams, 'single');
            % Prepare interpolation grid
            [ChTimeGrid, ChannelsGrid] = ndgrid(single(ch_time), single(1:nRX));
            [BfTimeGrid, BeamformGrid] = ndgrid(single(imgRanges)*2/c, single(1:nRX));
            
            Nsaved             = obj.Rx.subArraySize;
            obj.Rx.nElements_x = hardwareOpt.nADC;
            rx_apod            = ones(size(obj.Rx.apoMat));
            obj.Rx.nElements_x = Nsaved;
            
            %% Start beamforming
            if hardwareOpt.regressionMode
                h = waitbar(0,'Beamforming, wait please');
                fprintf('\t Computing delay and sum (Image lines: %gx%g, Packets: %g, Frames: %g) \t ... \t', softwareOpt.nTxAz* softwareOpt.nTxEl, 1, 1)
            end
            t0 = tic;
            
            rx_apod   = reshape(rx_apod, [], nRX);
            
            for tx_el_i = 1:nElTxBeams
                for tx_az_i = 1:nAzTxBeams
                    tx_i = (tx_el_i-1)*nAzTxBeams + tx_az_i; % current Tx event number including packets
                    
                    tx_focus  = softwareOpt.txEvents(tx_i).txFocus;
                    rx_angles = [0;0]; % softwareOpt.txEvents(tx_i).rxAngles;
                    tx_angles = softwareOpt.txEvents(tx_i).txAngle;
                    
                    % Compute delays
                    delay_line = single(computeRxDelayLinePost(tx_focus, rx_depths', rx_angles, rx_apex, elementPos, c, tx_angles));
                    
                    % Beamform this beam for all frames
                    
                    % Get channel date of current Tx event
                    Y_txEvent = Y(:, :, tx_az_i, tx_el_i);  % [nDep nCh]
                    
                    % Apply delays
                    GI = griddedInterpolant(ChTimeGrid, ChannelsGrid, Y_txEvent); % create gridded interpolant directly
                    alignedChLine = bsxfun(@times, rx_apod, GI(delay_line, BeamformGrid));
                    Beamformed(:, tx_az_i, tx_el_i) = sum(sum(alignedChLine, 2), 3);
                    
                    % Update waitbar
                    if hardwareOpt.regressionMode
                        waitbar(((tx_i-1)*prod(nMLAs)*prod(nMLTs) + mla_i)/(prod(nMLAs)*prod(nMLTs)*nAzTxBeams*nElTxBeams*nPackets),h);
                    end
                end
            end
            
            
            if hardwareOpt.regressionMode
                close(h); drawnow
                fprintf('Done! (%.2f sec)\n', toc(t0))
            end
            
            %% Take care of NaNs & pack datasets
            Beamformed(isnan(Beamformed)) = 0;
            BfData.data     = Beamformed;
            BfData.timeAxis = timeAxis;
            
        end
        
        function obj = rotate_translate_phantom(obj, shift, angle)
            % rotate_translate_phantom - shifts and rotates phantom
            % according to patch curvature
            %
            % Syntax: obj = rotate_translate_phantom(obj, shift, angle)
            %
            % Inputs
            %   shift - 1x3 vector, in meters. Center for the rotation
            %   angle - 1x2 vector, in radians. Rotations angles for the
            %   azimuth and elevation directions.
            
            shift = shift.* (abs(shift)>1e-10);
            obj.phantom.positionForCalc = obj.phantom.position - shift;
            
            E = [angle(2) angle(1) 0]; % Euler angles
            
            %% rotation matrices per axis. Better to use the functions on PP_Utils
            Rotx = PP_Utils.rotx(E(1)); % X-Axis rotation
            Roty = PP_Utils.roty(E(2)); % Y-Axis rotation
            Rotz = PP_Utils.rotz(0);    % X-axis rotation
            
            %% full rotation matrix
            R    = Rotx*Roty*Rotz;
            
            obj.phantom.positionForCalc = (R*obj.phantom.positionForCalc')';
            
        end


    end
    
    methods (Static)
        function structDefault = getstructDefault()
            % structDefault - gets all the default properties for the patch.
            % Syntax: structDefault = getstructDefault()
            %
            % Output
            %   structDefault - struct with all the default values for the
            %   patch
            
            structDefault = struct( ...
                ... TX properties
                'Tx_shape',             'flat', ...
                'Tx_shapeNumber',       1, ...
                'Tx_nElements_x',       166, ...
                'Tx_nElements_y',       166, ...
                'Tx_width',             534e-6, ...
                'Tx_height',            534e-6, ...
                'Tx_kerf',              66e-6, ...
                'Tx_name',              'transmitter', ...
                'Tx_apodization',       'hanning', ...
                'Tx_impulseResponse',   gauspuls(-2/2.5e6:1/100e6:2/2.5e6, 2.5e6, 0.5), ...
                'Tx_excitation',        1, ...
                'Tx_nSub',              16, ...
                'Tx_focus',             [0, 0, 0.05], ...
                'Tx_centerOfAperture',  [0, 0, 0], ...
                ... RX properties
                'rx_same_as_tx',        true, ...
                'Rx_shape',             'flat', ...
                'Rx_shapeNumber',       1, ...
                'Rx_nElements_x',       166, ...
                'Rx_nElements_y',       166, ...
                'Rx_width',             534e-6, ...
                'Rx_height',            534e-6, ...
                'Rx_kerf',              66e-6, ...
                'Rx_name',              'transmitter', ...
                'Rx_apodization',       'hanning', ...
                'Rx_impulseResponse',   gauspuls(-2/2.5e6:1/100e6:2/2.5e6, 2.5e6, 0.5), ...
                'Rx_excitation',        1, ...
                'Rx_nSub',              16, ...
                'Rx_focus',             [0, 0, 0.05], ...
                'Rx_centerOfAperture',  [0, 0, 0], ...
                ... Compounding probe properties
                'compounding_shape',       'flat', ...
                'compounding_shapeNumber', 1, ...
                ... phantom properties
                'predefined',           'none', ...
                'nPoints',              1, ...
                'position',             [0, 0, 0.05], ...
                'amplitude',            1, ...
                'radius',               [0.01, 0.01], ...
                'ellipsoidData',        [1.33, 2, 0.03], ...
                'center',               [0,0,0], ...
                'frame',                0, ...
                'rotationAngle',        [0,0,0],...
                 'Tx_elementPos',          [],...
                 'Tx_subArrayPos',          [],...
                 'Tx_subArrayCurvature',    []);
        end
        
        function mat = convert_cell_to_Fii_mat(cell_mat)
            mat = flipud(cell_mat)';
            mat = cell2mat(mat(:));
        end
    end
end
