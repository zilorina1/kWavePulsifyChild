classdef post_processing
    % Post processing for Pulsify Patch simulator.
    %
    
    methods (Static)
        
        function PostData = main_post_processing(BfData, pPatch, softwareOpt, structIn)
            % main_post_processing - main function of the post-processing block. Performs
            % filtering, TGC, envelope detection, compression,
            % interpolation and conversion to image size
            %
            % Syntax:  PostData = main_post_processing(BfData, pPatch, softwareOptions, hardwareOpt)
            %
            % Inputs:
            %    BfData - struct; contains data and timeAxis fields
            %    pPatch - patch object
            %    softwareOptions - software options
            %    structIn - input to parser
            %
            % Outputs:
            %    PostData - struct; contains fields    envelope_depthSlice,
            %    envelope_angleSlice, depthSlice, angleSlice, bfRanges,
            %    bfAngles and bmode
            %
            % Example: see singleTest.m
            %
            %  Vangjush Komini,Pedro Santos (KU Leuven 2016)
            
            
            structIn = parser(struct( 'filterBmode',      1, ...
                'alpha',           0.5*1e-6, ...
                'filt_BW',         80, ...
                'filt_fc',         2.5e6, ...
                'debug_plots',     false, ...
                'outImgSize',      [1024 1024], ...
                'focusAngle',      0, ...
                'zCenter',         0.05, ...
                'xCenter',         0, ...
                'postBfInterp',    false, ...
                'use_post_pad',    false, ...
                'scanConv',        true, ...
                'maxBfData',        0), structIn);
            
            global fs c f0
            
            bf_angles   = unique([softwareOpt.txEvents(:).rxAngles]', 'rows')'; % Compounding had redundant rxAngles
            bf_ranges   = BfData.timeAxis/2*c;
            bf_range_dz = bf_ranges(2) - bf_ranges(1);
            rx_apex     = pPatch.Rx.centerOfAperture;
            n_bf_lines  = [size(BfData.data,2) 1];
            n_bf_ranges = length(bf_ranges);
            n_packets   = 1;
            n_frames    = 1;
            
            BfData = BfData.data;
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Before Post-Processing'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
            end
            
            %% -----------------Padd near field---------------------------------
            range_to_pad = rx_apex(3) : bf_range_dz : bf_ranges(1)-bf_range_dz;
            bf_ranges    = [range_to_pad, bf_ranges];       % update range vector
            padd         = squeeze(zeros(length(range_to_pad), n_bf_lines(1), n_bf_lines(2), n_packets, n_frames));
            if ~isempty(padd)
                BfData   = [padd; BfData];      % now pad the Bf image
            end
            
            % Padd also aftwerwards (to avoid filtering artifacts)
            if structIn.use_post_pad
                max_depth    = bf_ranges(end)*1.2;
                range_to_pad = bf_ranges(end)+bf_range_dz : bf_range_dz : max_depth;
                bf_ranges    = [bf_ranges, range_to_pad];       % update range vector
                padd         = zeros(length(range_to_pad), n_bf_lines(1), n_bf_lines(2), n_packets, n_frames);
                if ~isempty(padd)
                    BfData   = [BfData; padd];      % now pad the Bf image
                end
                nPaddingAfter = numel(range_to_pad);
                n_bf_ranges   = length(bf_ranges);
            end
            
            %% -----------------Filter---------------------------------
            if structIn.filterBmode
                
                filter_params = struct('fs', fs, 'f0', structIn.filt_fc, 'bw', structIn.filt_BW, 'filter_type', 'gaussian');
                BfData = post_processing.bandpass_filter(BfData, filter_params);
                
                if structIn.debug_plots
                    figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                    title('Filtered'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
                end
            end
            
            %% --------------------TGC----------------------------------------------
            tgc = exp(2*structIn.alpha*bf_ranges*f0)';
            BfData = bsxfun(@times, BfData, tgc);   % store intermediate data
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('TGC'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar
            end
            
            %% ----------------Envelope detection---------------------------------
            
            if isreal(BfData)  % if not IQ data
                BfData = abs(hilbert(reshape(BfData, numel(bf_ranges), [])));
            else  % if IQ data
                BfData = abs(reshape(BfData, numel(bf_ranges), []));
                BfData = reshape(BfData, numel(bf_ranges), n_bf_lines(1), n_bf_lines(2), 1, 1); % back to [nRanges, nAzLines, nElLines, nPackets, nFrames]
            end
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Envelope detection'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
            end
            
            envelope = BfData;
            %% ----------------Log compression---------------------------------
            if ~structIn.maxBfData
                BfData = BfData ./ max(BfData(:));
            else
                BfData = BfData ./ structIn.maxBfData;
            end
            BfData = 20*log10(BfData);
            
            if structIn.debug_plots
                figure, imagesc(-0.02:534e-6:0.02, 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Log compressed'), xlabel('Az angle [deg]'), ylabel('Range [mm]')
                grid on
                colorbar
            end
            
            %% ----------------Interpolation Beamspace---------------------------------------
            if structIn.postBfInterp
                if size(BfData,4) * size(BfData, 5) > 1
                    % not done for packets/frames
                    keyboard
                end
                
                if ndims(BfData) == 3 && ~any(n_bf_lines == 1) % 3-D data (no sinle-line in any dimension)
                    
                    dep_in = 1:size(BfData, 1);
                    az_in  = 1:size(BfData, 2);
                    el_in  = 1:size(BfData, 3);
                    
                    dep_out = linspace(1, dep_in(end), dep_in(end)*PostSetup.postBfInterp(1));
                    az_out  = linspace(1, az_in(end), az_in(end)*PostSetup.postBfInterp(2));
                    el_out  = linspace(1, el_in(end), el_in(end)*PostSetup.postBfInterp(3));
                    
                    BfData = interpn(dep_in, az_in', el_in, BfData, dep_out, az_out', el_out);   % store intermediate data
                    
                    % TODO: return new beam coordinates
                    
                elseif n_bf_lines(1) == 1 % Elevation slice
                    
                    ranges_out    = linspace(bf_ranges(1), bf_ranges(end), n_bf_ranges*PostSetup.postBfInterp(1));
                    el_angles_out = linspace(bf_angles(2,1), bf_angles(2,end), n_bf_lines(2)*PostSetup.postBfInterp(3));
                    
                    [dep_in, el_in]   = ndgrid(bf_ranges, bf_angles(2,:));
                    [dep_out, el_out] = ndgrid(ranges_out, el_angles_out);
                    
                    BfData(:,1,:,:,:) = interpn(dep_in, el_in, squeeze(BfData), dep_out, el_out);   % store intermediate data
                    
                    % Update dimensions
                    bf_angles   = [0*el_angles_out; el_angles_out];
                    bf_ranges   = ranges_out;
                    bf_range_dz = bf_ranges(2) - bf_ranges(1);
                    n_bf_lines  = [1 length(el_angles_out)];
                    n_bf_ranges = length(ranges_out);
                    
                    if structIn.debug_plots
                        figure, imagesc(el_angles_out, 1000*bf_ranges, squeeze(BfData(:,round(n_bf_lines(1)/2),:)))
                        title('Interpolated'), xlabel('El angle [deg]'), ylabel('Range [mm]'), grid on , colorbar
                    end
                    
                else
                    ranges_out    = linspace(bf_ranges(1), bf_ranges(end), n_bf_ranges*PostSetup.postBfInterp(1));
                    az_angles_out = linspace(bf_angles(1,1), bf_angles(1,end), n_bf_lines(1)*PostSetup.postBfInterp(2));
                    %         el_angles_out = linspace(bf_angles(2,1), bf_angles(2,end), n_bf_lines(2)*PostSetup.postBfInterp(3));
                    
                    [dep_in, az_in]   = ndgrid(bf_ranges, bf_angles(1,:));
                    [dep_out, az_out] = ndgrid(ranges_out, az_angles_out);
                    
                    BfData = interpn(dep_in, az_in, BfData, dep_out, az_out);   % store intermediate data
                    
                    % Update dimensions
                    bf_angles   = [az_angles_out; 0*az_angles_out];
                    bf_ranges   = ranges_out;
                    bf_range_dz = bf_ranges(2) - bf_ranges(1);
                    n_bf_lines  = [length(az_angles_out) 1];
                    n_bf_ranges = length(ranges_out);
                    
                    if structIn.debug_plots
                        figure, imagesc(az_angles_out, 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2)))
                        title('Interpolated'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on , colorbar
                    end
                    
                end
            end
            
            %% Remove padding now
            n_bf_ranges = length(bf_ranges);
            if structIn.use_post_pad
                BfData(end-nPaddingAfter+1:end, :, :, :, :) = [];
                bf_ranges(end-nPaddingAfter+1:end) = [];
                n_bf_ranges = length(bf_ranges);
            end
            
            %% angle and depth slices
            [~, bb] = max(max(BfData,[],2));
            
            index   = find(any(bf_angles'~=0));
            [~, cc] = min(abs(bf_angles(index, :) - structIn.focusAngle));
            
            PostData.envelope_depthSlice       = envelope(1:n_bf_ranges,cc);
            PostData.envelope_angleSlice       = envelope(bb,:);
            PostData.depthSlice     = BfData(:, cc);
            PostData.angleSlice     = BfData(bb, :);
            PostData.bfRanges       = bf_ranges;
            PostData.bfAngles       = bf_angles;
            
            BfDataSet.meta.PostProcessing.imgRanges = bf_ranges;
            BfDataSet.meta.PostProcessing.bf_angles = bf_angles;
            BfDataSet.meta.PostProcessing.outImgSize= structIn.outImgSize;
            
            %% -----------------Scan Conversion---------------------------------
            if structIn.scanConv
                if find(BfDataSet.meta.PostProcessing.bf_angles(:,1))==1 %changes by Kate 
                    image_plane='azimuth'; 
                elseif find(BfDataSet.meta.PostProcessing.bf_angles(:,1))==2 
                    image_plane='elevation';  
                else 
                    image_plane='azimuth';     
                end    
                BfData_out = post_processing.apply_scan_conversion(BfData, BfDataSet.meta, [],pPatch.Tx.centerOfAperture,[0 0],image_plane); %changes by Kate for compounding
                PostSetup.Bmode_mask    = BfData_out.Bmode_mask;
            else
                CENTER = pPatch.compoundingProbe.subArrayPos;
                for cc = 1:size(BfData,2)
                    saIndex = softwareOpt.shotIndex(cc,:);
                    saPosition = CENTER{saIndex(1), saIndex(2)};
                    disp_az_lims(cc) = saPosition(1);
                end
                disp_dep_lims = bf_ranges+pPatch.Tx.centerOfAperture(:,3);
                
                BfData_out.disp_dep_lims = linspace(disp_dep_lims(1), disp_dep_lims(end), structIn.outImgSize(2));
                BfData_out.disp_az_lims  = linspace(disp_az_lims(1), disp_az_lims(end), structIn.outImgSize(1));
                
                [TH, R] = meshgrid( disp_az_lims, disp_dep_lims);
                [TH_q, R_q] = meshgrid( BfData_out.disp_az_lims, BfData_out.disp_dep_lims);
                BfData_out.B_mode = interp2( TH, R , BfData, TH_q, R_q, 'linear');
                
            end
            
            PostData.Bmode         = BfData_out.B_mode;
            PostData.disp_az_lims  = BfData_out.disp_az_lims;
            PostData.disp_dep_lims = BfData_out.disp_dep_lims;
            
        end
        
        function PostData = main_post_processing_cells(BfData, pPatch, softwareOpt, structIn)
            % main_post_processing - main function of the post-processing block. Performs
            % filtering, TGC, envelope detection, compression,
            % interpolation and conversion to image size
            %
            % Syntax:  PostData = main_post_processing(BfData, pPatch, softwareOptions, hardwareOpt)
            %
            % Inputs:
            %    BfData - cell matrix; contains data and timeAxis fields
            %    pPatch - patch object
            %    softwareOptions - software options
            %    structIn - input to parser
            %
            % Outputs:
            %    PostData - struct; contains fields    envelope_depthSlice,
            %    envelope_angleSlice, depthSlice, angleSlice, bfRanges,
            %    bfAngles and bmode
            %
            % Example: see singleTest.m
            %
            %  Vangjush Komini,Pedro Santos (KU Leuven 2016)
            
            structIn = parser(struct( 'filterBmode',      1, ...
                'alpha',           0.5*1e-6, ...
                'filt_BW',         80, ...
                'filt_fc',         2.5e6, ...
                'debug_plots',     false, ...
                'outImgSize',      [1024 1024], ...
                'focusAngle',      0, ...
                'zCenter',         0.05, ...
                'xCenter',         0, ...
                'postBfInterp',    false, ...
                'use_post_pad',    false, ...
                'scanConv',        true, ...
                'maxBfData',        0), structIn);
            
            global fs c f0
            
            bf_angles   = unique([softwareOpt.txEvents(:).rxAngles]', 'rows')'; % Compounding had redundant rxAngles
            bf_ranges   = BfData{1}.timeAxis/2*c;
            bf_range_dz = bf_ranges(2) - bf_ranges(1);
            %figure(1); hold on; scatter3(rx_apex(1).*10^3, rx_apex(3).*10^3, rx_apex(2).*10^3, 50.0, 'r', 'filled')
            n_bf_lines  = [softwareOpt.nTxAz softwareOpt.nTxEl];
            n_bf_ranges = length(bf_ranges);
            n_packets   = 1;
            n_frames    = 1;
            
            BfData_orig = BfData;
            BfData = cellfun(@(x) x.data, BfData, 'UniformOutput', false);
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Before Post-Processing'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
            end
            
            %% -----------------Padd near field---------------------------------
%             range_to_pad = rx_apex(3) : bf_range_dz : bf_ranges(1)-bf_range_dz;
%             bf_ranges    = [range_to_pad, bf_ranges];       % update range vector
%             padd         = squeeze(zeros(length(range_to_pad), n_bf_lines(1), n_bf_lines(2), n_packets, n_frames));
%             if ~isempty(padd)
%                 BfData = cellfun(@(x) [padd; x], BfData, 'UniformOutput', false);
%             end
            
            % Padd also aftwerwards (to avoid filtering artifacts)
            if structIn.use_post_pad
                max_depth    = bf_ranges(end)*1.2;
                range_to_pad = bf_ranges(end)+bf_range_dz : bf_range_dz : max_depth;
                bf_ranges    = [bf_ranges, range_to_pad];       % update range vector
                padd         = zeros(length(range_to_pad), n_bf_lines(1), n_bf_lines(2), n_packets, n_frames);
                if ~isempty(padd)
                    cellfun(@(x) [x; padd], BfData, 'UniformOutput', false);
                end
                nPaddingAfter = numel(range_to_pad);
                n_bf_ranges   = length(bf_ranges);
            end
            
            %% -----------------Filter---------------------------------
            if structIn.filterBmode
                
                filter_params = struct('fs', fs, 'f0', structIn.filt_fc, 'bw', structIn.filt_BW, 'filter_type', 'gaussian');
                BfData = cellfun(@(x) post_processing.bandpass_filter(x, filter_params), BfData, 'UniformOutput', false);
                
                if structIn.debug_plots
                    figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                    title('Filtered'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
                end
            end
            
            %% --------------------TGC----------------------------------------------
            tgc = exp(2*structIn.alpha*bf_ranges*f0)';
            BfData = cellfun(@(x) bsxfun(@times, x, tgc), BfData, 'UniformOutput', false);   % store intermediate data
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('TGC'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar
            end
            
            %% ----------------Envelope detection---------------------------------
            BfData = cellfun(@(x) abs(hilbert(reshape(x, numel(bf_ranges), []))), BfData, 'UniformOutput', false);   % store intermediate data
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Envelope detection'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
            end
            
            %% ----------------Log compression---------------------------------
            if ~structIn.maxBfData
                BfData = cellfun(@(x) 20*reallog(x/max(x(:)))/reallog(10), BfData, 'UniformOutput', false);
            else
                %maxBfData   = max(max(cell2mat(cellfun(@(x) max(x(:)),BfData,'un',0))));  %calculate the normalizing constant
                BfData = cellfun(@(x) 20.*reallog(x./structIn.maxBfData)./reallog(10), BfData, 'UniformOutput', false);
            end
            
            if structIn.debug_plots
                figure, imagesc(-0.02:534e-6:0.02, 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Log compressed'), xlabel('Az angle [deg]'), ylabel('Range [mm]')
                grid on
                colorbar
            end
            
            %% ----------------Interpolation Beamspace---------------------------------------
            if structIn.postBfInterp
                if size(BfData,4) * size(BfData, 5) > 1
                    % not done for packets/frames
                    keyboard
                end
                
                if ndims(BfData) == 3 && ~any(n_bf_lines == 1) % 3-D data (no sinle-line in any dimension)
                    
                    dep_in = 1:size(BfData, 1);
                    az_in  = 1:size(BfData, 2);
                    el_in  = 1:size(BfData, 3);
                    
                    dep_out = linspace(1, dep_in(end), dep_in(end)*PostSetup.postBfInterp(1));
                    az_out  = linspace(1, az_in(end), az_in(end)*PostSetup.postBfInterp(2));
                    el_out  = linspace(1, el_in(end), el_in(end)*PostSetup.postBfInterp(3));
                    
                    BfData = interpn(dep_in, az_in', el_in, BfData, dep_out, az_out', el_out);   % store intermediate data
                    
                    % TODO: return new beam coordinates
                    
                elseif n_bf_lines(1) == 1 % Elevation slice
                    
                    ranges_out    = linspace(bf_ranges(1), bf_ranges(end), n_bf_ranges*PostSetup.postBfInterp(1));
                    el_angles_out = linspace(bf_angles(2,1), bf_angles(2,end), n_bf_lines(2)*PostSetup.postBfInterp(3));
                    
                    [dep_in, el_in]   = ndgrid(bf_ranges, bf_angles(2,:));
                    [dep_out, el_out] = ndgrid(ranges_out, el_angles_out);
                    
                    BfData(:,1,:,:,:) = interpn(dep_in, el_in, squeeze(BfData), dep_out, el_out);   % store intermediate data
                    
                    % Update dimensions
                    bf_angles   = [0*el_angles_out; el_angles_out];
                    bf_ranges   = ranges_out;
                    bf_range_dz = bf_ranges(2) - bf_ranges(1);
                    n_bf_lines  = [1 length(el_angles_out)];
                    n_bf_ranges = length(ranges_out);
                    
                    if structIn.debug_plots
                        figure, imagesc(el_angles_out, 1000*bf_ranges, squeeze(BfData(:,round(n_bf_lines(1)/2),:)))
                        title('Interpolated'), xlabel('El angle [deg]'), ylabel('Range [mm]'), grid on , colorbar
                    end
                    
                else
                    ranges_out    = linspace(bf_ranges(1), bf_ranges(end), n_bf_ranges*PostSetup.postBfInterp(1));
                    az_angles_out = linspace(bf_angles(1,1), bf_angles(1,end), n_bf_lines(1)*PostSetup.postBfInterp(2));
                    %         el_angles_out = linspace(bf_angles(2,1), bf_angles(2,end), n_bf_lines(2)*PostSetup.postBfInterp(3));
                    
                    [dep_in, az_in]   = ndgrid(bf_ranges, bf_angles(1,:));
                    [dep_out, az_out] = ndgrid(ranges_out, az_angles_out);
                    
                    BfData = interpn(dep_in, az_in, BfData, dep_out, az_out);   % store intermediate data
                    
                    % Update dimensions
                    bf_angles   = [az_angles_out; 0*az_angles_out];
                    bf_ranges   = ranges_out;
                    bf_range_dz = bf_ranges(2) - bf_ranges(1);
                    n_bf_lines  = [length(az_angles_out) 1];
                    n_bf_ranges = length(ranges_out);
                    
                    if structIn.debug_plots
                        figure, imagesc(az_angles_out, 1000*bf_ranges, BfData(:,:,round(n_bf_lines(2)/2)))
                        title('Interpolated'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on , colorbar
                    end
                    
                end
            end
            
            %% Remove padding now
            n_bf_ranges = length(bf_ranges);
            if structIn.use_post_pad
                BfData(end-nPaddingAfter+1:end, :, :, :, :) = [];
                bf_ranges(end-nPaddingAfter+1:end) = [];
                n_bf_ranges = length(bf_ranges);
            end
            
            BfDataSet.meta.PostProcessing.imgRanges = bf_ranges;
            BfDataSet.meta.PostProcessing.bf_angles = bf_angles;
            BfDataSet.meta.PostProcessing.outImgSize= structIn.outImgSize;
            
            %% -----------------Scan Conversion---------------------------------
            CENTER = pPatch.compoundingProbe.subArrayPos;
            ALPHA  = pPatch.compoundingProbe.subArrayCurvature;
            
            if find(BfDataSet.meta.PostProcessing.bf_angles(:,1))==[1 2]'
                image_plane='3D';
            elseif find(BfDataSet.meta.PostProcessing.bf_angles(:,1))==1 %changes by Kate
                image_plane='azimuth';
            elseif find(BfDataSet.meta.PostProcessing.bf_angles(:,1))==2
                image_plane='elevation';
            else
                image_plane='azimuth';
            end
            
            PostData = cell(length(BfData),1);
            
            h_w = waitbar(0,['Scan Conversion...']);
            for cc = 1:length(BfData)
                if structIn.scanConv
                    saIndex = softwareOpt.shotIndex(cc,:);
                    saPosition = CENTER{saIndex(2), saIndex(1)};
                    saAngle    = -ALPHA {saIndex(2), saIndex(1)};
                    if ~strcmp(image_plane, '3D')
                        
                        switch image_plane %ignore non-primary curvature for 2D image
                            case 'azimuth'
                                saAngle=[saAngle(1) 0];
                            case 'elevation'
                                saAngle=[0 saAngle(2)];
                        end
                        BfData_out = post_processing.apply_scan_conversion(BfData{cc}, BfDataSet.meta, [], ....
                            saPosition, saAngle, image_plane); %changes by Kate for compounding
                    else
                        sys.start_depth     = bf_ranges(1);
                        sys.end_depth       = bf_ranges(end);
                        sys.width           = bf_angles(1, end)*2/180*pi;
                        sys.width_elev      = bf_angles(2, end)*2/180*pi;
                        sys.res_axial       = numel(bf_ranges);
                        BfData{cc}=reshape(BfData{cc}, [size(BfData{cc},1),softwareOpt.nTxAz,softwareOpt.nTxEl]);
                        sys.nr_lines        = size(BfData{cc}, 2);
                        sys.nr_lines_elev   = size(BfData{cc}, 3);
                        
                        
                        az_min= bf_ranges(end)*sind(softwareOpt.txEvents(1).txAngle(1));
                        az_max= bf_ranges(end)*sind(softwareOpt.txEvents(end).txAngle(1));
                        if (saAngle(1)~=0)
                            az_max=az_max+sin(saAngle(1))*(bf_ranges(end)-bf_ranges(1));
                            az_min=az_min+sin(saAngle(1))*(bf_ranges(end)-bf_ranges(1));
                            if sign(az_max)==sign(az_min)
                                if sign(az_min)>0, az_min=0; else az_max=0; end
                            end
                        end  
                        
                        el_min= bf_ranges(end)*sind(softwareOpt.txEvents(1).txAngle(2));
                        el_max= bf_ranges(end)*sind(softwareOpt.txEvents(end).txAngle(2));
                        if (saAngle(2)~=0)
                            el_max=el_max+sin(saAngle(2))*(bf_ranges(end)-bf_ranges(1));
                            el_min=el_min+sin(saAngle(2))*(bf_ranges(end)-bf_ranges(1));
                            if sign(el_max)==sign(el_min)
                                if sign(el_min)>0, el_min=0; else el_max=0; end
                            end
                        end  
                        
                        FOI                 = [sys.start_depth sys.end_depth az_min az_max el_min el_max];
                        imageSize           = structIn.outImgSize;
                        
                        BfData_out.B_mode = ScanConvertCOLE(permute(BfData{cc}, [2 1 3]), sys, FOI, imageSize, saAngle);
                        BfData_out.B_mode = permute(BfData_out.B_mode, [2 1 3]);
                        BfData_out.disp_az_lims  = (0:1/(imageSize(2)-1):1).*(az_max-az_min) + az_min+ saPosition(1);
                        BfData_out.disp_el_lims  = (0:1/(imageSize(3)-1):1).*(el_max-el_min) + el_min+ saPosition(2);
                        BfData_out.disp_dep_lims = (0:1/(imageSize(1)-1):1).* (bf_ranges(end)-bf_ranges(1)) + bf_ranges(1)+ saPosition(3);

                    end
                else
                    
                    for ii = 1:length(softwareOpt.txEvents)
                        disp_az_lims(ii) = softwareOpt.txEvents(ii).txFocus(index);
                    end
                    disp_dep_lims = bf_ranges+pPatch.Tx.centerOfAperture(:,3);
                    
                    BfData_out.disp_dep_lims = linspace(disp_dep_lims(1), disp_dep_lims(end), structIn.outImgSize(2));
                    BfData_out.disp_az_lims  = linspace(disp_az_lims(1), disp_az_lims(end), structIn.outImgSize(1));
                    
                    [TH, R] = meshgrid( disp_az_lims, disp_dep_lims);
                    [TH_q, R_q] = meshgrid( BfData_out.disp_az_lims, BfData_out.disp_dep_lims);
                    BfData_out.B_mode = interp2( TH, R , BfData, TH_q, R_q, 'spline');
                    
                end
                
                PostData{cc}.Bmode         = BfData_out.B_mode;
                PostData{cc}.disp_az_lims  = BfData_out.disp_az_lims;
                PostData{cc}.disp_dep_lims = BfData_out.disp_dep_lims;
                if isfield(BfData_out, 'disp_el_lims')
                    PostData{cc}.disp_el_lims=BfData_out.disp_el_lims;
                end    
                waitbar((cc)/(length(BfData)),h_w);
            end
            delete(h_w), drawnow
        end
        
        function PostData = main_echo(echoData, softwareOpt, structIn)
            % main_echo - main function of the pulse-echo. Similar to
            % main_post_processing. Performs compressoin and conversion to
            % image size
            %
            % Syntax:  PostData = main_echo(BfData, softwareOpt, structIn)
            %
            % Inputs:
            %    echoData - struct; contains data and timeAxis fields
            %    softwareOpt - software structIn
            %    structIn - input to parser
            %
            % Outputs:
            %    PostData - contains fields bfRanges,
            %    bfAngles and bmode
            %
            % Example: see plot_echo_field.m
            
            structIn = parser(struct(  'debug_plots',     false, ...
                'outImgSize',      [1024 1024], ...
                'scanConv',        true), structIn);
            
            global c
            
            bf_angles   = unique([softwareOpt.txEvents(:).rxAngles]', 'rows')'; % Compounding had redundant rxAngles
            bf_ranges   = echoData.timeAxis/2*c;
            n_bf_lines  = [softwareOpt.nTxAz softwareOpt.nTxEl];
            echoData = echoData.data;
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, echoData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Before Post-Processing'), xlabel('Az angle [deg]'), ylabel('Range [mm]'), grid on; colorbar;
            end
            
            
            echoData = echoData ./ max(echoData(:));
            echoData = 20*log10(echoData);
            
            if structIn.debug_plots
                figure, imagesc(bf_angles(1,:), 1000*bf_ranges, echoData(:,:,round(n_bf_lines(2)/2),1,1))
                title('Log compressed'), xlabel('Az angle [deg]'), ylabel('Range [mm]')
                grid on
                colorbar
            end
            
            %% -----------------Scan Conversion---------------------------------
            if structIn.scanConv
                
                PostData.bfRanges       = bf_ranges;
                PostData.bfAngles       = bf_angles;
                
                BfDataSet.meta.PostProcessing.imgRanges = bf_ranges;
                BfDataSet.meta.PostProcessing.bf_angles = bf_angles;
                BfDataSet.meta.PostProcessing.outImgSize= structIn.outImgSize;
                
                echoData = post_processing.apply_scan_conversion(echoData, BfDataSet.meta, false, [0 0 0], [0 0], 'azimuth');
                
                PostData.data          = echoData.B_mode;
                
                PostData.disp_az_lims  = echoData.disp_az_lims;
                PostData.disp_dep_lims = echoData.disp_dep_lims;
            end
        end
        
        function Out = scan_conversion(scan_lines, steering_angles, depths, resolution, probe_origin, probe_rotation, image_plane)
            %SCANCONVERSION Convert scan-lines in polar coordinates to a B-mode ultrasound image.
            %
            % DESCRIPTION:
            %       scanConversion computes the remapping between a series of scan
            %       lines in polar coordinates (i.e., taken at different steering
            %       angles) to a B-mode ultrasound image in Cartesian coordinates
            %       suitable for display. The remapping is performed using bilinear
            %       interpolation via interp2.
            %
            % USAGE:
            %       b_mode = scanConversion(scan_lines, steering_angles, image_size, c0, dt)
            %       b_mode = scanConversion(scan_lines, steering_angles, image_size, c0, dt, resolution)
            %
            % INPUTS:
            %       scan_lines      - matrix of scan lines indexed as (time, angle)
            %       steering_angles - array of scanning angles [degrees]
            %       depths          - array of sample depths [metres]
            %       resolution      - optional input to set the resolution of the
            %                         output images in pixels (default = [256, 256])
            %
            % OUTPUTS:
            %       b_mode          - the converted B-mode ultrasound image
            %
            % ABOUT:
            %       author      - Bradley E. Treeby
            %       date        - 23rd February 2011
            %       last update - 12th September 2012
            %
            % This function is part of the k-Wave Toolbox (http://www.k-wave.org)
            % Copyright (C) 2009-2014 Bradley Treeby and Ben Cox
            %
            % See also interp2
            
            % This file is part of k-Wave. k-Wave is free software: you can
            % redistribute it and/or modify it under the terms of the GNU Lesser
            % General Public License as published by the Free Software Foundation,
            % either version 3 of the License, or (at your option) any later version.
            %
            % k-Wave is distributed in the hope that it will be useful, but WITHOUT ANY
            % WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
            % FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
            % more details.
            %
            % You should have received a copy of the GNU Lesser General Public License
            % along with k-Wave. If not, see <http://www.gnu.org/licenses/>.
            
            % Get image boundaries 
            
            dep_min = depths(1);
            dep_max = depths(end);
            if numel(steering_angles)>1
                x_resolution = resolution(1);
                y_resolution = resolution(end);
                lat_min = depths(end)*sind(steering_angles(1));
                lat_max = depths(end)*sind(steering_angles(end));
                % create regular Cartesian grid to remap t
                %%%%%%% checking
                if find(probe_rotation~=0)
                    lat_max=lat_max+sin(probe_rotation((probe_rotation~=0)))*(dep_max-dep_min);
                    lat_min=lat_min+sin(probe_rotation((probe_rotation~=0)))*(dep_max-dep_min);
                    if sign(lat_max)==sign(lat_min)
                        if sign(lat_min)>0, lat_min=0; else lat_max=0; end
                    end
                end
            else
                x_resolution = 1;
                y_resolution = resolution;
                lat_min=steering_angles; lat_max=steering_angles;
            end
            pos_vec_y_new = (0:1/(x_resolution-1):1).*(lat_max-lat_min) + lat_min;
            pos_vec_x_new = (0:1/(y_resolution-1):1).*(dep_max-dep_min) + dep_min;
            if numel(steering_angles)>1,
                [pos_mat_y_new, pos_mat_x_new] = ndgrid(pos_vec_y_new, pos_vec_x_new);
                [th_cart, r_cart] = cart2pol(pos_mat_x_new, pos_mat_y_new);
                if find((probe_rotation~=0))
                    th_cart=th_cart-probe_rotation((probe_rotation~=0));
                end
                b_mode = interp2(2*pi*steering_angles./360, depths, scan_lines, th_cart, r_cart, 'linear').';
            else
                b_mode = interp1(depths, scan_lines, pos_vec_x_new, 'linear').';
            end
            
            %% Changes by Kate for sub-aperture compounding
            if strcmp(image_plane, 'azimuth')
                pos_vec_y_new = pos_vec_y_new+probe_origin(1);
            else
                pos_vec_y_new = pos_vec_y_new+probe_origin(2);
            end
            pos_vec_x_new = pos_vec_x_new+probe_origin(3);
            
            Bmode_mask = ones(size(b_mode));
            Bmode_mask(isnan(b_mode)) = 0;
            b_mode(isnan(b_mode)) = min(min(b_mode));
            
            %figure; imagesc(pos_vec_y_new, pos_vec_x_new, b_mode); colormap(gray); axis equal tight ij;
            Out.B_mode = b_mode;
            Out.Bmode_mask = Bmode_mask;
            Out.disp_az_lims = pos_vec_y_new;
            Out.disp_dep_lims = pos_vec_x_new;
        end
        
        function bfData_out = coherent_compound(bfIn, pPatch, softwareOpt)
            % align_bfData - 
            global c;
            spatialResolution = (bfIn{1}.timeAxis(2)-bfIn{1}.timeAxis(1))/2*c;
            
            vec = size(bfIn);
            bfData_out.data     = squeeze(zeros(size(bfIn{1}.data,1), vec(1), vec(2)));
            bfData_out.data_raw = squeeze(zeros(size(bfIn{1}.data,1), vec(1), vec(2)));
            bfData_out.timeAxis = bfIn{1}.timeAxis;
            bfData_out.probePosition       = zeros(vec(1),3);
            bfData_out.compoundingPosition = zeros(vec(1),3);
            
            SHIFT = [75+1 75+1 1];
            
            count = zeros(size(bfData_out.data), 'uint16');
            
            CENTER = pPatch.compoundingProbe.subArrayPos;
            ANGLE  = pPatch.compoundingProbe.subArrayCurvature;
            
            for cc = 1:numel(bfIn)
                
                saIndex = softwareOpt.shotIndex(cc,:);
                saPosition = CENTER{saIndex(1), saIndex(2)};
                saAngle    = ANGLE{saIndex(1), saIndex(2)};
                
                Rotx = PP_Utils.rotx(-saAngle(2)); % X-Axis rotation
                Roty = PP_Utils.roty(-saAngle(1)); % Y-Axis rotation
                Rotz = PP_Utils.rotz(0);
                R    = Rotx*Roty*Rotz;
                spatialRFLine = [zeros(size(bfIn{cc}.timeAxis')) zeros(size(bfIn{cc}.timeAxis')) bfIn{cc}.timeAxis'/2*c ];
                origin =  spatialRFLine(1,:);
                spatialRFLine = (R*((spatialRFLine - origin)'))';
                spatialRFLine = spatialRFLine + saPosition;
                
                indexes = round(spatialRFLine/diag([pPatch.Tx.pitch pPatch.Tx.pitch spatialResolution]) + SHIFT);
                indexes(:,3) = max(min(indexes(:,3),  size(bfData_out.data,1)),1);
                indexes(:,1) = max(min(indexes(:,1),  size(bfData_out.data,2)),1);
                
                linearIndex = sub2ind(size(bfData_out.data), indexes(:,3), indexes(:,1));
                bfData_out.data(linearIndex) = bfData_out.data(linearIndex) + bfIn{cc}.data;
                count(linearIndex) = count(linearIndex) + 1;
                
                bfData_out.data_raw(:,cc) = bfIn{cc}.data;
                bfData_out.probePosition(cc,:)       = bfIn{cc}.position;
                bfData_out.compoundingPosition(cc,:) = saPosition;
                
            end
        end
        
        function [ff, PostDataSet] = plot_image(BfData, pPatch, softwareOpt, inOptions, hardwareOpt)
            % plot_image - get RF data, converts to Bmode and plots it
            %
            % Syntax:   plot_image(BfData, pPatch, softwareOpt, inOptions, hardwareOpt)
            %
            % Inputs:
            %    BfData - struct; contains data and timeAxis fields
            %    pPatch - patch object
            %    softwareOpt - software structIn
            %    inOptions - input to parser
            %    hardwareOpt - hardware options
            %
            % Outputs:
            %    ff - handle to figure
            %    PostData - struct; contains fields    envelope_depthSlice,
            %    envelope_angleSlice, depthSlice, angleSlice, bfRanges,
            %    bfAngles and bmode
            %
            % Example: see singleTest.m
            
            %% Post processing
            inOptions = parser(struct(...
                'newFigure',       true, ...
                'filterBmode',     1, ...                               % use filter
                'slice',           'azimuth', ...                       % azimuth or elevation
                'outImgSize',      [512 512], ...
                'alpha',           0.5*1e-6, ...                        % attenuation coefficient. input to TGC
                'debug_plots',     false, ...
                'transpose',        false, ...                          % transposes image. use it elevation slices
                'focusAngle',      0, ...
                'zCenter',         0.05, ...                            % if image is rotated, prior shift to zCenter
                'xCenter',         0, ...                               % if image is rotated, prior shift to xCenter
                'rotationAngle',   0, ...                               % if image is rotated, rotation angle
                'use_post_pad',    false, ...                           % post-pad flag
                'save_fig',        false, ...
                'save_png',        false, ...
                'scanConv',        true, ...                            % scan conversion flag
                'axisX',           [], ...                              % set axis for figure, x-axis
                'axisY',           [], ...                              % set axis for figure, y-axis
                'outFolder',       '../Experiments/', ...               % folder to save figures
                'maxBfData',        0, ...                              % number to normalize BfData with
                'fileName',        ''), inOptions);                     % overwrite automatic file name
            
            %%  run conversion, compression and filtering
            BfData.data =squeeze(BfData.data);
            PostDataSet = post_processing.main_post_processing(BfData, pPatch, softwareOpt, inOptions);
            
            xAxis = 1000*PostDataSet.disp_az_lims;
            zAxis = 1000*PostDataSet.disp_dep_lims;
            
            %% plot
            if ~inOptions.newFigure
                ff = figure(2); %changes by Kate
                hold on;
            else
                ff = figure;
                hold on
            end
            if ~inOptions.transpose
                p=imagesc((xAxis), zAxis, squeeze(PostDataSet.Bmode));
                ylabel('Depth [mm]')
                
                if strcmp(inOptions.slice, 'azimuth')
                    xlabel('Azimuth [mm]');
                else
                    xlabel('Elevation [mm]');
                end
                axis tight equal ij;
            else
                imagesc(zAxis, fliplr(xAxis), squeeze(PostDataSet.Bmode)');
                xlabel('Depth [mm]')
                
                if strcmp(inOptions.slice, 'azimuth')
                    ylabel('Azimuth [mm]');
                else
                    ylabel('Elevation [mm]');
                end
                axis tight equal;
            end
            
            colormap(gray), colorbar, caxis([-60 0]);
            
            %% hard coded axis
            if ~isempty(inOptions.axisX)
                set(gca,'xlim',inOptions.axisX)
            end
            
            if ~isempty(inOptions.axisY)
                set(gca,'ylim', inOptions.axisY);
            end
            
            %% title and name for saving
            titleString = ['TX: ', hardwareOpt.mode_tx, '; RX: ', hardwareOpt.mode_rx];
            fileName    = strcat(hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '_angle', num2str(inOptions.focusAngle));
            
            if  hardwareOpt.COLE
                titleString = strcat(titleString, '; COLE');
                fileName = strcat(fileName, '_COLE');
            end
            
            if  hardwareOpt.hadamard
                titleString = strcat(titleString, '; Hadamard coded');
                fileName = strcat(fileName, '_H1');
            end
            if  hardwareOpt.nADC > 1
                titleString = strcat(titleString, '; nADC: ', num2str(hardwareOpt.nADC));
                fileName = strcat(fileName, '_nADC ', num2str(hardwareOpt.nADC));
            end
            if  hardwareOpt.PMUT_level
                titleString = strcat(titleString, '; PMUT level');
                fileName = strcat(fileName, '_pmutLevel');
            end
            
            title(titleString, 'interpreter', 'none')
            
            %% overwrite automatic filename
            if ~isempty(inOptions.fileName)
                fileName = inOptions.fileName;
            end
            
            %% save?
            if inOptions.save_fig
                saveas(gcf,strcat(inOptions.outFolder, fileName, '.fig'))
            end
            
            if inOptions.save_png
                saveas(gcf,strcat(inOptions.outFolder, fileName, '.png'))
            end
            
            %% save?
            if inOptions.save_fig
                saveas(gcf,strcat(inOptions.outFolder, fileName, '.fig'))
            end
            
            if inOptions.save_png
                saveas(gcf,strcat(inOptions.outFolder, fileName, '.png'))
            end
        end
        
        function signal = gaussian_filter(signal, Fs, freq, bandwidth, plot_filter)
            %GAUSSIANFILTER   Filter signals using a frequency domain Gaussian filter.
            %
            % DESCRIPTION:
            %       gaussianFilter applies a frequency domain Gaussian filter with the
            %       specified center frequency and percentage bandwidth to the input
            %       signal. If the input signal is given as a matrix, the filter is
            %       applied to each matrix row.
            %
            % USAGE:
            %       signal = gaussianFilter(signal, Fs, freq, bandwidth)
            %       signal = gaussianFilter(signal, Fs, freq, bandwidth, plot_filter)
            %
            % INPUTS:
            %       signal      - signal/s to filter
            %       Fs          - sampling frequency [Hz]
            %       freq        - filter center frequency [Hz]
            %       bandwidth   - filter bandwidth [%]
            %
            % OPTIONAL INPUTS:
            %       plot_filter - Boolean controlling whether the filtering process is
            %                     plotted
            %
            % OUTPUTS:
            %       signal      - filtered signal/s
            %
            % ABOUT:
            %       author      - Bradley Treeby
            %       date        - 10th December 2011
            %       last update - 2nd September 2012
            %
            % This function is part of the k-Wave Toolbox (http://www.k-wave.org)
            % Copyright (C) 2009-2014 Bradley Treeby and Ben Cox
            %
            % See also applyFilter, filterTimeSeries, gaussian
            
            % This file is part of k-Wave. k-Wave is free software: you can
            % redistribute it and/or modify it under the terms of the GNU Lesser
            % General Public License as published by the Free Software Foundation,
            % either version 3 of the License, or (at your option) any later version.
            %
            % k-Wave is distributed in the hope that it will be useful, but WITHOUT ANY
            % WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
            % FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
            % more details.
            %
            % You should have received a copy of the GNU Lesser General Public License
            % along with k-Wave. If not, see <http://www.gnu.org/licenses/>.
            
            % suppress m-lint warnings for unused assigned values
            %#ok<*ASGLU>
            
            % compute the double-sided frequency axis
            N = length(signal(1, :));
            if mod(N, 2)==0
                % N is even
                f = (-N/2:N/2-1)*Fs/N;
            else
                % N is odd
                f = (-(N-1)/2:(N-1)/2)*Fs/N;
            end
            
            % compute gaussian filter coefficients
            mean = freq;
            variance = (bandwidth/100*freq / (2*sqrt(2*log(2))))^2;
            magnitude = 1;
            
            % create the double-sided Gaussian filter
            gauss_filter = max(post_processing.gaussian(f, magnitude, mean, variance), post_processing.gaussian(f, magnitude, -mean, variance));
            
            % store a copy of the central signal element if required for plotting
            if nargin == 5 && plot_filter
                example_signal = signal(ceil(end/2), :);
            end
            
            % apply filter
            signal = real(ifft(ifftshift(...
                bsxfun(@times, gauss_filter, ...
                fftshift(fft(signal, [], 2), 2) ...
                )...
                , 2), [], 2));
            
            % plot filter if required
            if nargin == 5 && plot_filter
                
                % compute amplitude spectrum of central signal element
                as = fftshift(abs(fft(example_signal))/N);
                as_filtered = fftshift(abs(fft(signal(ceil(end/2), :)))/N);
                
                % get axis scaling factors
                [f_sc, f_scale, f_prefix] = scaleSI(f);
                
                % produce plot
                figure;
                plot(f*f_scale, as./max(as(:)), 'k-');
                hold on;
                plot(f*f_scale, gauss_filter, 'b-');
                plot(f*f_scale, as_filtered./max(as(:)), 'r-');
                xlabel(['Frequency [' f_prefix 'Hz]']);
                ylabel('Normalised Amplitude');
                legend('Original Signal', 'Gaussian Filter', 'Filtered Signal');
                axis tight;
                set(gca, 'XLim', [0 f(end)*f_scale]);
                
            end
            
        end
        
        function [scanConvData] = apply_scan_conversion(dataIn, meta, debug_plots, probe_origin,probe_rotation,image_plane)
            
            
            % TODO: memory allocation
            
            
            if ~ exist('debug_plots', 'var') || isempty(debug_plots), debug_plots = 0; end
            
            nFrames = size(dataIn, 5);
            nPackets = size(dataIn, 4);
            
            
            % Setup waitbar
            if nFrames*nPackets > 1
                h_w = waitbar(0, 'Scan converting data. Please wait...');
                update_val = max(1,round(nFrames*nPackets/20)); % update every 20
                fprintf('\t Computing scan conversion (%gx%g pixels)\t ... \t', meta.PostProcessing.outImgSize);
            end
            
            
            % Loop through all frames and packets
            t0 = tic;
            for frame_i = 1:nFrames
                for packet_i = 1:nPackets
                    
                    
                    if (nFrames*nPackets > 1) && ~rem((frame_i-1)*nPackets+packet_i, update_val)
                        try waitbar(((frame_i-1)*nPackets+packet_i)/nFrames*nPackets, h_w); end
                    end
                    
                    
                    thisScanConv = post_processing.scan_convert_this(dataIn(:,:,1,packet_i, frame_i), meta, debug_plots, probe_origin,probe_rotation,image_plane); %changes by Kate : for compounding
                    
                    if packet_i == 1 && frame_i == 1
                        scanConvData = thisScanConv;
                        
                        % Allocate space for other frames
                        if nPackets*nFrames > 1
                            scanConvData.B_mode(:,:,:,nPackets,nFrames) = zeros(size(thisScanConv.B_mode));
                        end
                    else
                        scanConvData.B_mode(:,:,:,packet_i, frame_i) = thisScanConv.B_mode;
                    end
                    
                    
                    
                end
            end
            
            try
                waitbar(((frame_i-1)*nPackets+packet_i)/nFrames*nPackets, h_w);
                delete(h_w)
            end
            
            if nFrames*nPackets > 1, fprintf('Done! (%.2f sec)\n', toc(t0)); end
        end
        
        function [scanConvData] = scan_convert_this(dataIn, meta, debug_plots, probe_origin,probe_rotation, image_plane)
            
            if ~ exist('debug_plots', 'var') || isempty(debug_plots), debug_plots = 0; end
            
            % When no PostProcessing was done, look for imgRanges and bf_angles in Beamforming
            try
                bf_ranges = meta.PostProcessing.imgRanges;
                bf_angles = meta.PostProcessing.bf_angles;
            catch
                bf_ranges = meta.Beamforming.imgRanges;
                bf_angles = meta.Beamforming.bf_angles;
            end
            
            
            if isfield(meta.PostProcessing, 'forceBmodeAz') && ~isempty(meta.PostProcessing.forceBmodeAz)
                % Due to memory limitations, we need to reconstrcuct multiple subsectors in HD-Pulse. If we want to shwo them independently, we need to force Az limits
                bf_angles = [linspace(meta.PostProcessing.forceBmodeAz(1), meta.PostProcessing.forceBmodeAz(2), size(dataIn, 2));...
                    zeros(1, size(dataIn, 2))];
            end
            az_min_max = bf_ranges(end)*sind([bf_angles(1, 1) bf_angles(1, end)]);
            el_min_max = bf_ranges(end)*sind([bf_angles(2, 1) bf_angles(2, end)]);
            
            size_az    = az_min_max(2) - az_min_max(1);
            size_el    = el_min_max(2) - el_min_max(1);
            size_dep   = bf_ranges(end);
            dt         = mean(diff(bf_ranges)) /1540 *2;
            
            Bmode = single(dataIn);
            outImgSize  = meta.PostProcessing.outImgSize; % [dep az]
            
            index = find(bf_angles(:,1)~=0);
            
            if size(Bmode,2)>1 
                %                 fprintf('\t Computing scan conversion (%gx%g pixels)\t ... \t', outImgSize(1:2))
                scanConvData = post_processing.scan_conversion(Bmode, bf_angles(index,:), bf_ranges, outImgSize(1:2), probe_origin,probe_rotation, image_plane); % Scan convert, changes by Kate for compounding
                
            elseif size(Bmode, 2) == 1 % if single txAz, squeeze dimensions (tmp) 
                %                 fprintf('\t Computing scan conversion (%gx%g pixels)\t ... \t', outImgSize(1:2)) 
                 
                Bmode = squeeze(Bmode); 
                scanConvData = post_processing.scan_conversion(Bmode, bf_angles(2,:), bf_ranges, outImgSize([2]), probe_origin,probe_rotation, image_plane);  % Scan convert 
            else 
                %                 fprintf('\t Computing scan conversion from COLE (%gx%gx%g pixels)\t ... \t', outImgSize)
                
                sys.start_depth     = bf_ranges(1);
                sys.end_depth       = bf_ranges(end);
                sys.width           = bf_angles(1, end)*2/180*pi;
                sys.width_elev      = bf_angles(2, end)*2/180*pi;
                sys.res_axial       = numel(bf_ranges);
                sys.nr_lines        = size(Bmode, 2);
                sys.nr_lines_elev   = size(Bmode, 3);
                
                FOI                 = [sys.start_depth sys.end_depth az_min_max(1) az_min_max(2) el_min_max(1) el_min_max(2)];
                imageSize           = outImgSize;
                
                scanConvData.B_mode = ScanConvertCOLE(permute(Bmode, [2 1 3]), sys, FOI, imageSize);
                scanConvData.B_mode = permute(scanConvData.B_mode, [2 1 3]);
                scanConvData.disp_az_lims  = (0:1/(imageSize(2)-1):1).*size_az - size_az/2;
                scanConvData.disp_el_lims  = (0:1/(imageSize(3)-1):1).*size_el - size_el/2;
                scanConvData.disp_dep_lims = (0:1/(imageSize(1)-1):1).*size_dep;
            end
            
            if debug_plots
                x_pos = 1000*scanConvData.disp_az_lims;
                y_pos = 1000*scanConvData.disp_dep_lims;
                figure, imagesc(x_pos, y_pos, scanConvData.B_mode(:,:,round(end/2)))
                title('Scan Converted'), xlabel('Azimuth [mm]'), ylabel('Range [mm]'), axis equal tight, grid on, colorbar
            end
        end
        
        function dataOut = bandpass_filter(dataIn, params)
            
            %% Pre-process data
            dataOut = dataIn;
            sizeData = size(dataOut);
            
            % Crop initial samples? (e.g. saturation)
            if isfield(params, 'ncrop') && params.ncrop > 1
                ncrop = params.ncrop;
                dataOut = dataOut(ncrop+1:end,:,:,:,:,:,:);
            end
            
            % Zero-padd signal?
            if isfield(params, 'npadd')
                npadd = size(dataOut);
                npadd(1) = params.npadd;
                dataOut = [dataOut; zeros(npadd)];
            end
            
            % Window
            if isfield(params, 'tappering')
                tappering = params.tappering;
                dataOut = bsxfun(@times, dataOut, tukeywin(size(dataOut, 1), tappering)); % windowing to avoid ringing artifacts from the filter
            end
            
            %% Filter
            switch(params.filter_type)
                
                case 'gaussian'
                    for ii = 1:prod(sizeData(2:end))
                        dataOut(:,ii) = post_processing.gaussian_filter(dataOut(:,ii)', params.fs, params.f0, params.bw);
                    end
                    
                case 'fir1'
                    b = fir1(100, ([-.5 .5]*params.bw/100+1)*params.f0*2/params.fs);
                    dataOut = single(filtfilt(b, 1, double(dataOut)));
                otherwise
            end
            
            %% Post-process
            %remove padded positions
            if isfield(params, 'npadd')
                dataOut = dataOut(1:end-npadd(1), :, :, :,:,:,:);
            end
            
        end
        
        function gauss_distr = gaussian(x, magnitude, mean, variance)
            %GAUSSIAN   Create a Gaussian distribution.
            %
            % DESCRIPTION:
            %       gaussian returns a Gaussian distribution f(x) with the specified
            %       magnitude, mean, and variance. If these values are not specified,
            %       the magnitude is normalised and values of variance = 1 and mean = 0
            %       are used. For example running
            %           x = -3:0.05:3;
            %           plot(x, gaussian(x));
            %       will plot a normalised Gaussian distribution.
            %
            %       Note, the full width at half maximum of the resulting distribution
            %       can be calculated by FWHM = 2 * sqrt(2 * log(2) * variance).
            %
            % USAGE:
            %       gauss_distr = gaussian(x)
            %       gauss_distr = gaussian(x, magnitude)
            %       gauss_distr = gaussian(x, magnitude, mean)
            %       gauss_distr = gaussian(x, magnitude, mean, variance)
            %
            % INPUTS:
            %       x           - x-axis variable
            %
            % OPTIONAL INPUTS:
            %       magnitude   - bell height (default = normalised)
            %       mean        - mean or expected value (default = 0)
            %       variance    - variance ~ bell width (default = 1)
            %
            % OUTPUTS:
            %       gauss_distr - Gaussian distribution
            %
            % ABOUT:
            %       author      - Bradley Treeby
            %       date        - 4th December 2009
            %       last update - 25th August 2014
            %
            % This function is part of the k-Wave Toolbox (http://www.k-wave.org)
            % Copyright (C) 2009-2014 Bradley Treeby and Ben Cox
            
            % This file is part of k-Wave. k-Wave is free software: you can
            % redistribute it and/or modify it under the terms of the GNU Lesser
            % General Public License as published by the Free Software Foundation,
            % either version 3 of the License, or (at your option) any later version.
            %
            % k-Wave is distributed in the hope that it will be useful, but WITHOUT ANY
            % WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
            % FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
            % more details.
            %
            % You should have received a copy of the GNU Lesser General Public License
            % along with k-Wave. If not, see <http://www.gnu.org/licenses/>.
            
            % if the variance is not given, define as unity
            if nargin < 4
                variance = 1;
            end
            
            % if the mean is not given, center the distribution
            if nargin < 3
                mean = 0;
            end
            
            % if the magnitude is not given, normalise
            if nargin < 2
                magnitude = (2*pi*variance)^-0.5;
            end
            
            % compute the gaussian
            gauss_distr = magnitude*exp(-(x - mean).^2/(2*variance));
        end
        
        function comparison_plot(inOptions, postProcessed, hardwareOpt)
            % comparison_plot - out of several postProcessed data sets,
            % compares plots
            %
            % Syntax:  comparison_plot(inOptions, postProcessed, hardwareOpt)
            %
            % Inputs:
            %    inOptions - input for parser
            %    postProcessed - cell.
            %    hardwareOpt - hardware options
            
            inOptions = parser(struct('slice',           'azimuth', ...
                'save_fig',        false, ...
                'save_png',        false, ...
                'outFolder',       '../Experiments/'), inOptions);
            
            leg = cell(length(hardwareOpt),1);
            for ii = 1:length(hardwareOpt)
                leg{ii} = hardwareOpt{ii}.description;
            end
            
            figure(400)
            hold on
            
            figure(401)
            hold on
            
            figure(402)
            hold on
            
            figure(403)
            hold on
            
            for kk = 1:length(hardwareOpt)
                figure(400)
                index = find(postProcessed{kk}.bfAngles(:,1)~=0);
                plot(postProcessed{kk}.bfAngles(index, :), postProcessed{kk}.angleSlice)
                
                figure(401)
                plot(postProcessed{kk}.bfRanges*1000, postProcessed{kk}.depthSlice)
                
                figure(402)
                plot(postProcessed{kk}.bfAngles(index, :), postProcessed{kk}.envelope_angleSlice)
                
                figure(403)
                plot(postProcessed{kk}.bfRanges*1000, postProcessed{kk}.envelope_depthSlice)
            end
            
            figure(400)
            xlabel('angle, degree')
            ylabel('Normalized envelope, dB')
            ylim([-50 0])
            xlim([postProcessed{kk}.bfAngles(index, 1) postProcessed{kk}.bfAngles(index, end)])
            grid on
            legend(leg, 'interpreter', 'none')
            if inOptions.save_fig
                saveas(gcf, strcat(inOptions.outFolder, 'angleSlice', '.fig'))
            end
            if inOptions.save_png
                saveas(gcf, strcat(inOptions.outFolder, 'angleSlice', '.png'))
            end
            
            figure(401)
            xlabel('depth, mm')
            ylabel('Normalized envelope, dB')
            ylim([-50 0])
            xlim([0 postProcessed{1}.bfRanges(end)*1000])
            grid on
            legend(leg, 'interpreter', 'none')
            if inOptions.save_fig
                saveas(gcf, strcat(inOptions.outFolder, 'depthSlice', '.fig'))
            end
            if inOptions.save_png
                saveas(gcf, strcat(inOptions.outFolder, 'depthSlice', '.png'))
            end
            
            figure(402)
            set(gca, 'YScale', 'log')
            xlabel('angle, degree')
            ylabel('Envelope')
            xlim([postProcessed{kk}.bfAngles(index, 1) postProcessed{kk}.bfAngles(index, end)])
            grid on
            legend(leg, 'interpreter', 'none')
            if inOptions.save_fig
                saveas(gcf, strcat(inOptions.outFolder, 'envAngleSlice', '.fig'))
            end
            if inOptions.save_png
                saveas(gcf, strcat(inOptions.outFolder, 'envAngleSlice', '.png'))
            end
            
            figure(403)
            set(gca, 'YScale', 'log')
            xlabel('depth, mm')
            ylabel('Envelope')
            xlim([0 postProcessed{1}.bfRanges(end)*1000])
            grid on
            legend(leg, 'interpreter', 'none')
            if inOptions.save_fig
                saveas(gcf, strcat(inOptions.outFolder, 'envDepthSlice', '.fig'))
            end
            if inOptions.save_png
                saveas(gcf, strcat(inOptions.outFolder, 'envDepthSlice', '.png'))
            end
        end
        
        function make_movie(images, structIn)
            % make_movie - get many plot handles and makes a movie out of
            % it
            %
            % Syntax:  make_movie(images, structIn)
            %
            % Inputs:
            %    images - cell; each cell has an image handle
            %    structIn - input for parser
            
            structIn = parser(struct('outFile',          strcat(pwd,'\movie')), structIn);
            
            %% write the frames to the video
            for u=1:length(images)
                % convert the image to a frame
                frame = getframe(images{u});
                im = frame2im(frame);
                [imind,cm] = rgb2ind(im,256);
                
                if u == 1
                    imwrite(imind, cm, strcat(structIn.outFile), 'gif', 'Loopcount', inf);
                else
                    imwrite(imind, cm, strcat(structIn.outFile), 'gif', 'WriteMode','append', 'DelayTime', 0.125);
                end
            end
        end
        
        function add_frame_to_movie(image, first, structIn)
            % add_frame_to_movie - add one image to the last frame of a
            % movie
            %
            % Syntax:  add_frame_to_movie(image, first, structIn)
            %
            % Inputs:
            %    images - cell; each cell has an image handle
            %    first - flag; if true, create file
            %    structIn - input for parser
            
            structIn = parser(struct('outFile',          strcat(pwd,'\movie'), ...
                'delayTime',                0.125), structIn);
            
            %% write the frames to the video
            % convert the image to a frame
            frame = getframe(image);
            im = frame2im(frame);
            [imind,cm] = rgb2ind(im,256);
            
            if first
                % create file
                imwrite(imind, cm, strcat(structIn.outFile), 'gif', 'Loopcount', inf);
            else
                % append
                imwrite(imind, cm, strcat(structIn.outFile), 'gif', 'WriteMode','append', 'DelayTime', structIn.delayTime);
            end
        end
        
        function ff = plot_echo_field(echoData, softwareOpt, inOptions)
            % plot_echo_field - get pulse-echo data, converts and plots it
            %
            % Syntax:   plot_echo_field(echoData, softwareOpt, inOptions)
            %
            % Inputs:
            %    echoData - struct; contains data and timeAxis fields
            %    softwareOpt - software structIn
            %    inOptions - input to parser
            %
            % Outputs:
            %    ff - handle to figure
            %
            % Example: see generate_lookup_table_2D.m
            
            inOptions = parser(struct('filterBmode',     1, ...
                'slice',           'azimuth', ...
                'outImgSize',      [512 512], ...
                'alpha',           1, ...
                'debug_plots',     false, ...
                'focusAngle',       NaN, ...
                'use_post_pad',    false, ...
                'save_fig',        false, ...
                'save_png',        false, ...
                'outFolder',       '../Experiments/'), inOptions);
            
            %% 0
            echoData =squeeze(echoData);
            PostDataSet = post_processing.main_echo(echoData, softwareOpt, inOptions);
            
            xAxis = 1000*PostDataSet.disp_az_lims;
            zAxis = 1000*PostDataSet.disp_dep_lims;
            
            ff = figure;
            hold on;
            %[X, Z] =meshgrid(xAxis, zAxis);
            %mesh(X, Z, squeeze(PostDataSet.data));
            imagesc(xAxis, zAxis,  squeeze(PostDataSet.data));
            ylabel('Depth [mm]'),
            
            if strcmp(inOptions.slice, 'azimuth')
                xlabel('Azimuth [mm]');
            else
                xlabel('Elevation [mm]');
            end
            zlabel('dB')
            axis ij tight equal;
            caxis([-60 0]);
            colorbar
        end
        
        % methods (Static)
    end
end
