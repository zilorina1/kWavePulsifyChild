classdef subArrayPatch < handle
    % Pulsify Patch -- this class contains all the information and physical
    % dimensions of the Pulsify Patch. It contains three objects: Tx, Rx
    % and phantom.
    
    properties
        % all measures in meters
        Tx                                              % Tx probe. See probe class for details
        Rx                                              % Rx probe
    end
    
    properties (Access = private)
        rx_same_as_tx
    end
        
    
    methods
        function obj = subArrayPatch(inOptions)
            % pulsifyPatch - Pulsify Patch constructor
            % Syntax:  obj = PulsifyPatch(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify patch object
            
            
            %% TX
            s=struct(...
                'shape',            'subArray', ...
                'width',            inOptions.Whole_patch.Tx.width, ...
                'height',           inOptions.Whole_patch.Tx.height, ...
                'kerf',             inOptions.Whole_patch.Tx.kerf, ...
                'apodization',      inOptions.Whole_patch.Tx.apodization, ...
                'impulseResponse',  inOptions.Whole_patch.Tx.impulseResponse, ...
                'excitation',       inOptions.Whole_patch.Tx.excitation, ...
                'nElements_x',      inOptions.Whole_patch.Tx.subArraySize, ...
                'nElements_y',      inOptions.Whole_patch.Tx.subArraySize, ...
                'focus',            inOptions.Whole_patch.Tx.focus, ...
                'centerOfAperture', inOptions.saPosition, ...
                'subArraySize',     inOptions.Whole_patch.Tx.subArraySize,...
                'subArrayPos',       inOptions.saPosition,...
                'subArrayCurvature',       inOptions.saAngle);
            s.elementPos=inOptions.Tx_elementPos;
            obj.Tx  = probe(s);
            
            obj.Rx       = obj.Tx;
            obj.Rx.name  = 'receiver';
        end
      
        
        function BfData = offlineBeamforming(obj, Y, timeAxis, softwareOpt, hardwareOpt,varargin)
            % offlineBeamforming - offlineBeamforming beamforms simulation or experimental 2D data
            % Inputs:
            %    Y - big matrix; beamformed by the TX
            %    timeAxis - vector; time stamp
            %    softwareOpt - software options
            %    hardwareOpt - hardware options
            %
            % Outputs:
            %    BfData - full beamformed data (by TX and RX). Struct containing fields data and timeAxis
            %
            % Vangjush Komini, Pedro Santos (KU Leuven, 2016)
            
            if isempty(varargin)
              global c
            else
	      c=varargin{1};
	    end
            global plot_debug
            

            elementPos  = obj.convert_cell_to_Fii_mat(obj.Rx.elementPos); 
            %scatter3(elementPos(:,1).*10^3,elementPos(:,3).*10^3, elementPos(:,2).*10^3, 10.0, 'b', 'filled');
            %scatter3(obj.Rx.centerOfAperture(:,1).*10^3,obj.Rx.centerOfAperture(:,3).*10^3,obj.Rx.centerOfAperture(:,2).*10^3, 10.0, 'r', 'filled');
            if hardwareOpt.PMUT_level
                vec = sqrt(hardwareOpt.PMUTPerElement):2*sqrt(hardwareOpt.PMUTPerElement):obj.Rx.subArraySize;
                vec = [ -vec(end:-1:1) vec ]';
                minEle = min(abs(elementPos(:,1)));
                elementPos = elementPos/minEle;
                [~, a] = min(abs(bsxfun(@minus, elementPos(:,1), vec')),[], 2);
                [~, b] = min(abs(bsxfun(@minus, elementPos(:,2), vec')),[], 2);                
                elementPos = [vec(a) vec(b) zeros(length(a),1)]*minEle;
            end
            
            rx_apod = obj.Rx.getApoMat(hardwareOpt);
            
            % Add default values
            if strcmp(hardwareOpt.mode_rx, 'rx_on_cols')
                rxIndex = repmat(eye(obj.Rx.subArraySize), 1, obj.Rx.subArraySize);
                elementPos  = repmat(rxIndex*elementPos./sum(rxIndex,2), obj.Rx.subArraySize,1);
            elseif strcmp(hardwareOpt.mode_rx, 'rx_on_cols_v2')
                rxIndex = repmat(eye(obj.Rx.subArraySize), 1, obj.Rx.subArraySize);
                rxIndex1 = rxIndex(:,1:end/2);
                rxIndex2 = rxIndex(:,end/2+1:end);
                rxIndex =  [ [rxIndex1 zeros(size(rxIndex1))]; [zeros(size(rxIndex2)) rxIndex2]];
                elementPos  = rxIndex*elementPos./sum(rxIndex,2);
                S = obj.Rx.subArraySize*obj.Rx.subArraySize/size(elementPos,1);
                elementPos = [repmat(elementPos(1:end/2,:), S, 1);  repmat(elementPos(end/2+1:end,:), S, 1)];
            elseif strcmp(hardwareOpt.mode_rx, 'rx_on_rows')
                rxIndex = kron(eye(obj.Rx.subArraySize), ones(1,obj.Rx.subArraySize));
                elementPos  = kron(rxIndex*elementPos./sum(rxIndex,2), ones(obj.Rx.subArraySize,1));
            else
                rx_apod = rx_apod(:);
            end
            if plot_debug
                figure(1);
                hold on;
                scatter3(elementPos(:,1)*1000,elementPos(:,3)*1000,elementPos(:,2)*1000,10.0,'filled','r');
            end
            imgRanges     = timeAxis/2*c;
            
            nRX             = size(Y, 2);
            nAzTxBeams      = size(Y, 3);
            nElTxBeams      = size(Y, 4);
            nFrames         = size(Y, 5);
            nADC            = hardwareOpt.nADC;
            
            nRangesOut      = length(imgRanges);
            nMLAs           = softwareOpt.nMLAs; % take it from Reconstruction
            nMLAsAz         = nMLAs(1);
            nMLAsEl         = nMLAs(2);
            nMLTs           = [1  1];
            nMLTsAz         = nMLTs(1);
            nMLTsEl         = nMLTs(2);
            
            ch_time        = timeAxis';
            
            rx_depths      = imgRanges;%+obj.Tx.centerOfAperture(:,3); %changes by Katya
            
            % digital converters
%             % ADC indexes
%             indexADC = cell(nADC,1);
%             colrow = 1:max(obj.Rx.subArraySize):(obj.Rx.subArraySize(1)*obj.Rx.subArraySize(2));
%             iADC = 1;
%             
%             for couter = 1:max(obj.Rx.subArraySize)
%                 
%                 indexADC{iADC} = union(indexADC{iADC}, colrow);
%                 
%                 colrow = colrow+1;
%                 if  mod(couter,max(obj.Rx.subArraySize)/hardwareOpt.nADC ) == 0
%                     iADC = iADC + 1;
%                 end
%                 
%             end
            
            % Allocate space
            %Beamformed       = zeros(nRangesOut, nMLTsAz*nMLAsAz*nAzTxBeams, nMLTsEl*nMLAsEl*nElTxBeams, nADC, nFrames, 'single');
            Beamformed       = zeros(nRangesOut, nMLTsAz*nMLAsAz*nAzTxBeams, nMLTsEl*nMLAsEl*nElTxBeams, 1, nFrames, 'single');
            % Prepare interpolation grid
            [ChTimeGrid, ChannelsGrid] = ndgrid(single(ch_time), single(1:nRX));
            [BfTimeGrid, BeamformGrid] = ndgrid(single(imgRanges)*2/c, single(1:nRX));
            
            if nMLTsEl>1 || nMLAsEl > 1
                keyboard % 3D is not checked after last code refactoring
            end
            
            %% Start beamforming
            if hardwareOpt.regressionMode
                h = waitbar(0,'Beamforming, wait please');
                fprintf('\t Computing delay and sum (Image lines: %gx%g, Packets: %g, Frames: %g) \t ... \t', softwareOpt.nTxAz* softwareOpt.nTxEl, 1, 1)
            end
            
            t0 = tic;
            
            for tx_el_i = 1:nElTxBeams
                for tx_az_i = 1:nAzTxBeams
                    tx_i = (tx_el_i-1)*nAzTxBeams + tx_az_i; % current Tx event number including packets
                    for mlt_el = 1:nMLTsEl
                        for mlt_az = 1:nMLTsAz
                            mlt_i = (mlt_el-1)*nMLTsAz + mlt_az;
                            tx_focus   = softwareOpt.txEvents(tx_i).txFocus(mlt_i, :);
                            tx_angles  = softwareOpt.txEvents(tx_i).txAngle(:, mlt_i);
                            for mla_el = 1:nMLAsEl
                                rx_el      = (tx_el_i-1)*nMLAsEl*nMLTsEl + (mlt_el-1)*nMLAsEl + mla_el;
                                for mla_az = 1:nMLAsAz
                                    mla_i     = (mlt_el-1)*nMLTsAz*nMLAsEl*nMLAsAz + (mlt_az-1)*nMLAsEl*nMLAsAz + (mla_el-1)*nMLAsAz + mla_az;
                                    rx_az     = (tx_az_i-1)*nMLAsAz*nMLTsAz + (mlt_i-1)*nMLAsAz + mla_az;
                                    rx_angles = softwareOpt.txEvents(tx_i).rxAngles(:, mla_i);
                                    rx_apod   = reshape(rx_apod, [], nRX);
                                    
                                    % Compute delays
                                    if isfield(softwareOpt, 'Muxing') && isfield(softwareOpt.Muxing, 'TxChannelId') && ~isempty(softwareOpt.Muxing.TxChannelId)
                                        active_elements = reshape(softwareOpt.Muxing.TxChannelId(:,:,1)>0, [], 1);
                                        active_pos = reshape(elementPos, [], 3);
                                        active_pos = active_pos(active_elements, :);
                                        delay_line = computeRxDelayLine(tx_focus, rx_depths', rx_angles, active_pos, tx_angles);
                                    else
                                        delay_line = obj.Rx.computeRxDelayLine(tx_focus, rx_depths', rx_angles, elementPos, tx_angles, c);
                                    end
                                    delay_line = single(delay_line);
                                    
                                    % Beamform this beam for all frames
                                    for frame_i = 1:nFrames
                                        
                                        % Get channel date of current Tx event
                                        Y_txEvent = Y(:, :, tx_az_i, tx_el_i, frame_i);  % [nDep nCh]
                                        
                                        % Apply delays
                                        GI = griddedInterpolant(ChTimeGrid, ChannelsGrid, Y_txEvent); % create gridded interpolant directly
                                        alignedChLine = bsxfun(@times, rx_apod, GI(delay_line, BeamformGrid));
                                        %Beamformed(:, rx_az, rx_el, 1, frame_i) = sum(sum(alignedChLine, 2), 3);
                                        Beamformed(:, rx_az, rx_el, 1, frame_i) = sum(sum(alignedChLine(:,1:obj.Rx.subArraySize(1)*obj.Rx.subArraySize(2)), 2), 3);
%                                         for iADC = 1:nADC
%                                             Beamformed(:, rx_az, rx_el, iADC, frame_i) = sum(sum(alignedChLine(:,indexADC{iADC}), 2), 3);
%                                         end
                                        
                                    end
                                    
                                    % Update waitbar
                                    if hardwareOpt.regressionMode
                                        waitbar(((tx_i-1)*prod(nMLAs)*prod(nMLTs) + mla_i)/(prod(nMLAs)*prod(nMLTs)*nAzTxBeams*nElTxBeams*nPackets),h);
                                    end
                                end
                            end
                        end
                    end
                end
            end
            
            if hardwareOpt.regressionMode
                close(h); drawnow
                fprintf('Done! (%.2f sec)\n', toc(t0))
            end
            
            %% Take care of NaNs & pack datasets
            Beamformed(isnan(Beamformed)) = 0;
            BfData.data     = Beamformed;
            BfData.timeAxis = timeAxis;
            
        end
        
    end
    methods (Static)
        function mat = convert_cell_to_Fii_mat(cell_mat)
            %mat = cell_mat;%flipud(cell_mat); % there was ' before
            mat = cell2mat(reshape(cell_mat', [size(cell_mat,1)*size(cell_mat,2) 1]));
        end
    end    

end
