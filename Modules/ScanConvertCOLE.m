%% Scan Convertion for Geometric Display
function cart_image = ScanConvert(scan_image,sys,FOI,imageSize, probe_rotation)

if (nargin == 2)
    minx = sys.start_depth * cos(sys.width/2);
    maxx = sys.end_depth;
    miny = -sys.end_depth * sin(sys.width/2);
    maxy = sys.end_depth * sin(sys.width/2);
    minz = [];
    maxz = [];
elseif (nargin > 2)
    minx = FOI(1);
    maxx = FOI(2);
    miny = FOI(3);
    maxy = FOI(4);
    minz = [];
    maxz = [];
end    

if ~exist('imageSize', 'var') || isempty(imageSize), imageSize = [2048 2048 2048]; end


if length(size(scan_image))==2
    
    dx = (maxx - minx)/(sys.res_axial-1);
    xx = (minx:dx:maxx);
    yy = (miny:dx:maxy);

    [XX,YY] = meshgrid(xx,yy);
    clear xx yy;

    % Define the positions where the intensities are known
    r = linspace(sys.start_depth,sys.end_depth,size(scan_image,2));
    t = linspace(-sys.width/2,sys.width/2,sys.nr_lines);
    
    
        
    % Using griddata to do inverse scan conversion
%     [r,t]=meshgrid(r,t);
%     [Xout,Yout]=pol2cart(t,r);
%     cart_image= griddata(Xout,Yout,scan_image,XX,YY,'linear')';
    % Using interp2 to do scan conversion
    [t_i,r_i] = cart2pol(XX,YY);
    cart_image = interp2(r,t,scan_image,r_i,t_i);
 
elseif length(size(scan_image))==3
    if (nargin == 2)
        minx = min(sys.start_depth*cos(sys.width/2),sys.start_depth*cos(sys.width_elev/2));
        minz = -sys.end_depth * sin(sys.width_elev/2);
        maxz =  sys.end_depth * sin(sys.width_elev/2);
        
        dx = (sys.end_depth - minx)/(imageSize(1)-1);
        dy = dx;
        dz = dx;
        
    elseif (nargin == 3) || (nargin == 4)|| (nargin == 5)
        minz = FOI(5);
        maxz = FOI(6);
        
        dx = (sys.end_depth - minx)/(imageSize(1)-1);
        dy = (maxy - miny)/(imageSize(2)-1);
        dz = (maxz - minz)/(imageSize(3)-1);

    end
    
    % Define the Cartesian limits of the image
    x = (minx:dx:maxx);
    y = (miny:dy:maxy);
    z = (minz:dz:maxz);

    % Divide the volume in cubic (smaller) volumes in order to limit memory use
    box_size = 25;
    nr_boxes_x = floor(length(x)/box_size);
    nr_boxes_y = floor(length(y)/box_size);
    nr_boxes_z = floor(length(z)/box_size);

    end_box_x = [1:nr_boxes_x]*box_size;
    end_box_y = [1:nr_boxes_y]*box_size;
    end_box_z = [1:nr_boxes_z]*box_size;
    
    % modified at 8 Apr 2016 for each dimension Divide
    if mod(length(x), box_size)
        end_box_x = [0 end_box_x length(x)];
    else
        end_box_x = [0 end_box_x];
    end
    
    if mod(length(y), box_size)
        end_box_y = [0 end_box_y length(y)];
    else
        end_box_y = [0 end_box_y];
    end
    
    if mod(length(z), box_size)  
        end_box_z = [0 end_box_z length(z)]; 
    else
        end_box_z = [0 end_box_z];
    end
    % Define the positions where the intensities are known
    r = linspace(sys.start_depth,sys.end_depth,size(scan_image,2));
    t = linspace(-sys.width/2,sys.width/2,sys.nr_lines);
    phi = linspace(-sys.width_elev/2,sys.width_elev/2,sys.nr_lines_elev);

     if 0 % correction
        a = dDir.direction.a/180*pi;
        e = dPln.direction.e/180*pi;
        aNew  = bsxfun(@atan2, sin(a), cos(e)'*cos(a)).*180/pi;
        eNew  = asin(sin(e)'*cos(a)).*180/pi;
     end   
    
    % Interpolate box-by-box
    for x_step = 1:length(end_box_x)-1
        for y_step = 1:length(end_box_y)-1
            for z_step = 1:length(end_box_z)-1
                [xx,yy,zz] = meshgrid(x(end_box_x(x_step)+1:end_box_x(x_step+1)), y(end_box_y(y_step)+1:end_box_y(y_step+1)), z(end_box_z(z_step)+1:end_box_z(z_step+1)));
                [t_i,phi_i,r_i] = cart2sph(xx(:),yy(:),zz(:));
                if find((probe_rotation~=0))
                    t_i=t_i-probe_rotation(1);
                    phi_i=phi_i-probe_rotation(2);
                end
                % Define the relevant input range to limit excessive memory
                % usage
                min_r = min(r_i(:)); max_r = max(r_i(:));
                min_phi = min(phi_i(:)); max_phi = max(phi_i(:));
                min_t = min(t_i(:)); max_t = max(t_i(:));

                [dummy,min_r_index] = find(abs(r-min_r) == min(abs(r-min_r)));
                [dummy,max_r_index] = find(abs(r-max_r) == min(abs(r-max_r)));
                [dummy,min_phi_index] = find(abs(phi-min_phi) == min(abs(phi-min_phi)));
                [dummy,max_phi_index] = find(abs(phi-max_phi) == min(abs(phi-max_phi)));
                [dummy,min_t_index] = find(abs(t-min_t) == min(abs(t-min_t)));
                [dummy,max_t_index] = find(abs(t-max_t) == min(abs(t-max_t)));

                % Increase the size of these boxes just a bit in order to avoid
                % edge effects
                if (min_r_index > 2)
                    min_r_index = min_r_index - 2;
                elseif (min_r_index == 2)
                    min_r_index = 1;
                end
                if (min_phi_index > 2)
                    min_phi_index = min_phi_index - 2;
                elseif (min_phi_index == 2)
                    min_phi_index = 1;
                end
                if (min_t_index > 2)
                    min_t_index = min_t_index - 2;
                elseif (min_t_index == 2)
                    min_t_index = 1;
                end

                if (max_r_index < length(r)-2)
                    max_r_index = max_r_index + 2;
                elseif (max_r_index == length(r)-2)
                    max_r_index = length(r);
                end
                if (max_phi_index < length(phi)-2)
                    max_phi_index = max_phi_index + 2;
                elseif (max_phi_index == length(phi)-2)
                    max_phi_index = length(phi);
                end
                if (max_t_index < length(t)-2)
                    max_t_index = max_t_index + 2;
                elseif (max_t_index == length(t)-2)
                    max_t_index = length(t);
                end

                interp_data = interp3(r(min_r_index:max_r_index),t(min_t_index:max_t_index),phi(min_phi_index:max_phi_index),scan_image(min_t_index:max_t_index,min_r_index:max_r_index,min_phi_index:max_phi_index),r_i,t_i,phi_i,'cubic',NaN);
                cart_image(end_box_y(y_step)+1:end_box_y(y_step+1),end_box_x(x_step)+1:end_box_x(x_step+1),end_box_z(z_step)+1:end_box_z(z_step+1)) = reshape(interp_data,size(xx));
            end
        end
    end
    
end

return
