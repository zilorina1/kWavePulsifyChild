classdef transOptions < dynamicprops
    % transOptions -- this class contains the hardware description for
    % transmission
    properties
        mode_tx     = 'full_control'                                % Tx transmission mode
        mode_rx     = 'full_control'                                % Rx transmission mode
        PMUT_level  = false                                         % if true, substitute one elements by a PMUTPerElement^2 array of elements
        COLE        = false                                         % use COLE for simulation
        KWave       = false
        Experiment  = false
        splitTxRx   = false
        regressionMode = false                                      % if true, enables waiting bars
        PMUTPerElement = 9                                          % describes array of PMUTS that form one element
        nADC        =   1                                           % number of ADC at receiver
        description = 'full_control'                                % string with description. Used for plotting
        hadamard    = 0;                                            % activate Hadamard coding
    end
    
    methods
        function obj = transOptions(varArgIn)
            % transOptions - hardware options contructor
            % Syntax:  obj = transOptions(inOptions)
            %
            % Inputs:
            %    inOptions - struct with input options. This is the input
            %    to the parser
            % Outputs:
            %    obj - Pulsify transOptions object
            if nargin > 0
                %% parser
                names  = fieldnames(varArgIn);
                values = struct2cell(varArgIn);
                
                for ii = 1:length(values)
                    if ~isprop(obj, names{ii})
                        error(['transOptions: This object does not contain this property: ', names{ii}]);
                    end
                    
                    obj.(names{ii}) = values{ii};
                end
            end
            
            obj = validateTransopt(obj);
        end
        
        function obj = validateTransopt(obj)
            % validateTransopt - sanity check

            listOfModes = {'full_control', 'tx_on_cols', 'tx_on_rows', 'rx_on_cols', 'rx_on_rows', 'rx_on_cols_v2'};
 
            assert(any(strcmp(obj.mode_tx, listOfModes)), ['Error at transOptions. Transmittter mode not recognized: ', obj.mode_tx]);
            assert(any(strcmp(obj.mode_rx, listOfModes)), ['Error at transOptions. Receiver mode not recognized: ', obj.mode_rx]);
            assert(mod(sqrt(obj.PMUTPerElement),1) == 0 , ['Error at transOptions. PMUT per element needs to be a square: ', obj.PMUTPerElement])
        end
    end
end
    