function []=postprocessingKatyaCurved2D(active_path,kWavepath)

softwareOpt=PP_scanSequencer(); pPatch=pulsifyPatch(); hardwareOpt = transOptions(); kwaveInput=kwaveSimulation_Setup();
load([active_path 'data_for_kWave.mat']); %load(['data_for_kWave.mat']);
global f0 c
global parallelNodesKWave 
parallelNodesKWave=0;
global fs
fs=1/kwaveInput.kgrid.dt;
load([active_path 'nodesDistribution.mat']);%load(['nodesDistribution.mat'])
% if strcmp(choice, 'Subarrays')
%   N=1:size(softwareOpt.shotIndex,1);
% else
   N=1:numel(softwareOpt.txEvents);
%   shots=[];
%   check_existence=[active_path 'sensor_data_shot' num2str(taskPerNode{1}(1)) '-' num2str(taskPerNode{1}(end)) '.mat'];
%   load(check_existence, 'rfData', 'delays');
%   rfDataAll=zeros(size(rfData,1),size(rfData,2),numel(taskPerNode{1}(1):taskPerNode{numel(taskPerNode)}(end))); 
%   delaysAll=cell(1,numel(taskPerNode{1}(1):taskPerNode{end}(end)));
%   rfDataAll(:,:,taskPerNode{1}(1):taskPerNode{1}(end))=rfData;
%   delaysAll(taskPerNode{1}(1):taskPerNode{1}(end))=delays;
%   for ii=2:numel(taskPerNode)
%       check_existence=[active_path 'sensor_data_shot' num2str(taskPerNode{ii}(1)) '-' num2str(taskPerNode{ii}(end)) '.mat'];
%       load(check_existence, 'rfData', 'delays');
%       rfDataAll(:,:,taskPerNode{ii}(1):taskPerNode{ii}(end))=rfData(:,:,taskPerNode{ii}(1):taskPerNode{ii}(end));
%       delaysAll(taskPerNode{ii}(1):taskPerNode{ii}(end))=delays(taskPerNode{ii}(1):taskPerNode{ii}(end));
%   end 
%   
%   check_existence=[active_path 'sensor_data_shot' num2str(taskPerNode{1}(1)) '-' num2str(taskPerNode{numel(taskPerNode)}(end)) '.mat'];
%   rfData=rfDataAll; delays=delaysAll;
%   save(check_existence, 'rfData', 'delays', '-v7.3');
% end  
% 
% 
BfData_raw=pPatch.run_transmission(softwareOpt, hardwareOpt, [], kwaveInput, active_path, kWavepath,N,choice);
save([active_path 'BfData_raw.mat'], 'BfData_raw', '-v7.3')
load([active_path 'BfData_raw.mat'], 'BfData_raw')
BfData_raw=BfData_raw(1);
disp('############################# Starting PostProcessing ##############################################################')
BfData_temp = BfData_raw(~all(cellfun(@isempty, BfData_raw),2),:);
maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
clear BfData_temp;

if (softwareOpt.nTxAz>1)&(softwareOpt.nTxEl>1)
   sector_image_plane='3D';
elseif softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end   

%% PostProcessing and compounding
Axis    = [];
Bmode3D = [];
if ~strcmp(sector_image_plane, '3D')
  imageSize=[512 512];
else
  imageSize=[512 512 512];  
end    
outOptions = struct('filterBmode',     0, ...
    'slice',           'elevation', ...
    'outImgSize',      imageSize, ...
    'alpha',           0, ... % attenuation
    'debug_plots',     false, ...
    'use_post_pad',    false, ...
    'save_fig',        0, ...
    'save_png',        0, ...
    'outFolder',       active_path, ...
    'maxBfData',       double(maxBfData));

if hardwareOpt.KWave
 temp=cell2mat(reshape(kwaveInput.KwavePatch.elementPosCenters, [1 size(kwaveInput.KwavePatch.elementPosCenters,1)*size(kwaveInput.KwavePatch.elementPosCenters,2)])');
 kwaveInput.KwavePatch.Tx.apex=mean(temp);
 kwaveInput.KwavePatch.Tx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.Tx.pitch=pPatch.Tx.pitch;
 kwaveInput.KwavePatch.Rx.apex=mean(temp);
 kwaveInput.KwavePatch.Rx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.compoundingProbe.subArrayPos=kwaveInput.KwavePatch.subArrayPosCenters;
 kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature=cellfun(@(x) -x, kwaveInput.KwavePatch.subArrayCurvature, 'UniformOutput', false);
 clear temp;
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, kwaveInput.KwavePatch, softwareOpt, outOptions);
else
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, pPatch, softwareOpt, outOptions);   
end

disp('############################# Done with PostProcessing, start with Compounding ##############################################################')

if ~strcmp(sector_image_plane, '3D')
    if hardwareOpt.KWave
        CENTER = kwaveInput.KwavePatch.compoundingProbe.subArrayPos;
        ALPHA  = kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature;
    else
        CENTER = pPatch.compoundingProbe.subArrayPos;
        ALPHA  = pPatch.compoundingProbe.subArrayCurvature;
    end
    
    compPosition = zeros(length(BfData_raw),3);
    global plot_debug
    plot_debug=1;
    for ss = 1:length(BfData_raw)
        
        saIndex = softwareOpt.shotIndex(ss,:);
        compPosition(ss,:) = CENTER{saIndex(2), saIndex(1)};
        saAngle    = -ALPHA {saIndex(2), saIndex(1)};
        
        if size(BfData_raw{ss}.data,2)>1 %sectorial imaging
            switch sector_image_plane %ignore non-primary curvature for 2D image
                case 'azimuth'
                    saAngle=[0 saAngle(2)];
                case 'elevation'
                    saAngle=[saAngle(1) 0];
            end
        end
        inputOpt.imgSize=imageSize(2);
        inputOpt.sizeOfArray=[pPatch.Tx.subArraySize(2) pPatch.Tx.subArraySize(1)];
        inputOpt.sizeOfPatch=[pPatch.Tx.nElements_x pPatch.Tx.nElements_y];
        [xt,yt,zt,Bt]=prepareAxis(postProcessedPerSubArray{ss}, sector_image_plane, compPosition(ss,:), saAngle, inputOpt);
        Axis    = [Axis; [xt yt zt]];
        Bmode3D = [Bmode3D; Bt];
    end
    
    disp('############################# Done with preparing for Compounding ##############################################################')
    
    
    %% plotting
    if hardwareOpt.KWave
        step_3D_mesh=kwaveInput.kgrid.dx;%10^(-3)/2;%how precise the uniform 3D mesh is [m]
    else
        step_3D_mesh=10^(-3)/2;
    end
    
    [mesh3D, BmodeGlobal, pointsDistance] = define_global_3D_mesh(Axis,step_3D_mesh);
    [BmodeGlobal, pointsDistance]         = ThreeTwoDimensionalCompoundingv2 (Axis, Bmode3D, mesh3D, BmodeGlobal, pointsDistance);
    BmodeGlobal(~isnan(BmodeGlobal))      = BmodeGlobal(~isnan(BmodeGlobal))./pointsDistance((~isnan(BmodeGlobal)));
    BmodeGlobal(isnan(BmodeGlobal))   = -80;
else   
    BmodeGlobal=postProcessedPerSubArray{1, 1}.Bmode;
    [X_global_mesh, Y_global_mesh, Z_global_mesh]=meshgrid(postProcessedPerSubArray{1, 1}.disp_az_lims,postProcessedPerSubArray{1, 1}.disp_dep_lims,postProcessedPerSubArray{1, 1}.disp_el_lims);
    mesh3D.X_global_mesh=X_global_mesh;
    mesh3D.Y_global_mesh=Y_global_mesh;
    mesh3D.Z_global_mesh=Z_global_mesh;
end
disp('############################# Done with Compounding ##############################################################')

close all;
inOptions.plotsubArray=0;
inOptions.plotSequence=0;
inOptions.newFigure=1;
pPatch.plot_geometry(inOptions, softwareOpt);

%load('D:\kWavePulsifyChild\Results2D\to_edit_pics.mat');
threshold=-60;%-60;
ind=find(BmodeGlobal>=threshold);
ind=ind(1:100:end);
figure(1); hold on;
scatter3(mesh3D.X_global_mesh(ind).*10^3,mesh3D.Y_global_mesh(ind).*10^3,mesh3D.Z_global_mesh(ind).*10^3, 2.0,BmodeGlobal(ind), 'filled');
%colormap(flipud(hot)); 
colormap(gray); 
colorbar;
axis equal tight;colorbar;
caxis([threshold 0]); view(3);
zlabel('z'); ylabel('y'); xlabel('x');
saveas(gca, [active_path 'bmode_3Dview.png']);
save([active_path 'to_edit_pics.mat'], 'pPatch', 'mesh3D', 'BmodeGlobal', 'softwareOpt', '-v7.3');


disp('############################# Saved 3D ##############################################################')

%% projection Elevation
threshold=-60;ind=find(BmodeGlobal(:)>threshold);
[~,r]=min(abs(mesh3D.X_global_mesh(:)-mean(mesh3D.X_global_mesh(ind))));
Xval=mean(mesh3D.X_global_mesh(ind)); 
ind=find((mesh3D.X_global_mesh==mesh3D.X_global_mesh(r)));
x=mesh3D.Z_global_mesh(ind);
y=mesh3D.Y_global_mesh(ind);
Bmode_Az_Proj=BmodeGlobal(ind);
ind=find(Bmode_Az_Proj(:)>threshold);
x=x(ind); y=y(ind); Bmode_Az_Proj=Bmode_Az_Proj(ind);

[X,Y]=meshgrid(linspace(min(x),max(x),1024),linspace(min(y),max(y),1024));  
F = scatteredInterpolant(double(x),double(y),double(Bmode_Az_Proj));
Bmode=F(X,Y);     
figure; 
imagesc(linspace(min(x),max(x),1024).*10^3,linspace(min(y),max(y),1024).*10^3,Bmode');
% x1=Xval.*ones(size(linspace(min(y),max(y),1024)));
% y1=linspace(min(y),max(y),1024);
% z1=linspace(min(x),max(x),1024);
% [Y1,Z1]=meshgrid(y1,z1);
% X1=Xval.*ones(size(Y1));
% figure(1); hold on; 
% Bmode(Z1.*10^3<=-6.5)=-60;
% p=warp(X1.*10^3,Y1.*10^3,Z1.*10^3,Bmode');
colormap(gray);
caxis([-60 0]);colorbar; axis equal tight; zlabel('z [mm]'); ylabel('z [mm]'); xlabel('y [mm]');
set(gca,'Xdir','normal');
set(gca,'Ydir','normal');

saveas(gca, [active_path 'bmode_projectionEl.png']);

%% projection Azimuth
threshold=-90;ind=find(BmodeGlobal(:)>threshold);
[~,r]=min(abs(mesh3D.Z_global_mesh(:)-mean(mesh3D.Z_global_mesh(ind))));
ind=find((mesh3D.Z_global_mesh==mesh3D.Z_global_mesh(r)));
x=mesh3D.X_global_mesh(ind);
y=mesh3D.Y_global_mesh(ind);
Bmode_Az_Proj=BmodeGlobal(ind);
ind=find(Bmode_Az_Proj(:)>threshold);
x=x(ind); y=y(ind); Bmode_Az_Proj=Bmode_Az_Proj(ind);

[X,Y]=meshgrid(linspace(min(x(Bmode_Az_Proj>threshold)),max(x(Bmode_Az_Proj>threshold)),1024),linspace(min(y(Bmode_Az_Proj>threshold)),max(y(Bmode_Az_Proj>threshold)),1024));  
F = scatteredInterpolant(x,y,Bmode_Az_Proj);
Bmode=F(X,Y);     
figure; 
imagesc(linspace(min(x),max(x),1024),linspace(min(y),max(y),1024),Bmode); 
axis equal tight ij; colormap(gray);
caxis([-60 0]);colorbar;
saveas(gca, [active_path 'bmode_projectionAz.png']);

disp('############################# Saved projections ##############################################################')

disp('############################# Finished! ##############################################################')

end