close all
clc
if ~exist('rawData','var')||isempty(rawData)
     clearvars
else    
    clearvars -except rawData
end

profile clear
profile on

F = findall(0,'type','figure','tag','TMWWaitbar');
delete(F);

%% Preliminaries
global c fs f0
f0    = 1.5e6; % Transducer center frequency [Hz]
fs    = 100e6; % Sampling frequency [Hz]
c     = 1440; % Speed of sound [m/s]'; for COLE and FieldII

%%
tx_focal_depth  = 0.003; % metres
alpha           = 0.0; % db/cm/MHz attenuation for TGC
imageSize       = [512 512];
Y_level          = 0.01;

set_field ('att',  alpha*100/1e6*f0);
set_field ('c', c);
set_field ('fs',  fs);
set_field ('use_att',1);
set_field ('use_rectangles',1);

%% patch and software options

pPatch = pulsifyPatch(struct(...
   'Tx_shape',                'flat', ...
    'Tx_nElements_x',             32,...
    'Tx_nElements_y',             32, ....
    'Tx_subArraySize',            [16 16],...
      'nPoints',                 1e5, ...
     'predefined',     'ellipsoid', ...
      'center',          [-0.005  0  0.002], ...
      'ellipsoidData',   [1.33, 2, 0.03], ...
      'rotationAngle',  [0 0 0],...
      'radius',         [30/8e3, 25/8e3], ...
      'Tx_centerOfAperture',  [ 0  0   -0.005]));

%stepX=40;
%X = 1:stepX:130; %along elevation direction, from up to down
X=[8];%[50];
stepY=1;
Y =[1:17];%along azimuth direction, from left to right
[XX, YY] = meshgrid(X,Y);
shotPosition = [XX(:) YY(:)];

%% software options
softwareOpt = PP_scanSequencer(struct(...
'scanMode',         'custom', ...
    'shotIndex',     shotPosition, ...
    'initialPosition',  [1, 1], ...
    'txFocalDepth',     tx_focal_depth, ...
    'nTxAz'         ,   41, ...
    'nTxEl'         ,   1, ...
    'maxDepth'      ,   0.13, ...
    'coleAmpThreshold', 1e-28, ...
    'openingAngle'  ,   [abs(-20)+abs(20),0]));

if softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end    
    
%% hardware options
% hardwareOpt = transOptions(struct('mode_tx', 'tx_on_rows',...
%     'mode_rx',     'rx_on_rows',...
%     'COLE',        true));
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'KWave',        true));
% hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
%     'mode_rx',     'full_control',...
%     'COLE',        true));

if strcmp(hardwareOpt.mode_tx,'tx_on_rows')&strcmp(hardwareOpt.mode_rx,'rx_on_rows')&(softwareOpt.nTxAz>1)
    error('For row-row, we can do sectorial imaging only in the elevation plane');
end   
if strcmp(hardwareOpt.mode_tx,'tx_on_cols')&strcmp(hardwareOpt.mode_rx,'rx_on_cols')&(softwareOpt.nTxEl>1)
    error('For col-col, we can do sectorial imaging only in the azimuth plane');
end

if hardwareOpt.COLE
%% load LUT data once
% I save it to rawData variable, so if you want to load different LUTs, you
% would need to clean rawData variable (in the beginning of the file all variables are clened except it)
%index=round(cmpdImg.number_of_images_in_secondary_direction/2);
if (~exist('rawData','var'))
   if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
      folderName = strcat(pwd, '\Dataset\COLE\', 'tx_on_rows', '_', 'rx_on_rows', '\3D\'); 
   else    
      folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '\3D\');
   end
   h_w = waitbar(0,['I will load LUT tables forever...']);
   fprintf('I will load LUT tables forever...');
   t0 = tic; 
   for ii = 1:length(softwareOpt.txEvents)
                 if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '.mat');
                 else    
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '.mat');
                 end
                 if ~exist(filename, 'file')
                        error('COLE: LUT not found');
                 else
                        load(filename, 'echoData', 'impulseResponse');
                        rawData.echoData{ii}=echoData;
                        rawData.impulseResponse{ii}=impulseResponse;
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                            rawData.echoData{ii}.data = permute(rawData.echoData{ii}.data, [1 3 2]);
                        end    
                 end
                 waitbar((ii)/(length(softwareOpt.txEvents)),h_w);        
   end
   delete(h_w), drawnow
end
end

%% folder
outFolder_home = '/Experiments/2020.08.17 - Kate reorganization/';
outFolder = strcat(pwd, outFolder_home);
if ~exist(outFolder, 'dir')
    mkdir(outFolder)
end

inOptions.plotsubArray=0;
inOptions.plotSequence=0;
inOptions.newFigure=1;
pPatch.plot_geometry(inOptions, softwareOpt);

%% crop the phantom
ind=find((pPatch.phantom.position(:,2)>-3/10^(3))&(pPatch.phantom.position(:,2)<3/10^3));
ind_new=zeros(1,numel(pPatch.phantom.position));      
ind_new(ind)=1:numel(ind);
pPatch.phantom.segmentationIndex=cellfun(@(x) ind_new(intersect(x,ind)),pPatch.phantom.segmentationIndex, 'UniformOutput', false);    
center=mean(pPatch.phantom.position(ind, :));
pPatch.phantom.position=pPatch.phantom.position(ind, :)-center;
pPatch.phantom.positionForCalc=pPatch.phantom.position;
pPatch.phantom.amplitude=pPatch.phantom.amplitude(ind);
phantomSaved=pPatch.phantom;
save('currently_cropped_phantom.mat', 'phantomSaved');

pPatch = pulsifyPatch(struct(...
   'Tx_shape',                'flat', ...
    'Tx_nElements_x',             32,...
    'Tx_nElements_y',             32, ....
     'predefined',     'ellipsoid_cropped', ...
      'Tx_centerOfAperture', [ 0  0   -0.005]));%-center));      

inOptions.plotsubArray=0;
inOptions.plotSequence=0;
inOptions.newFigure=1;
pPatch.plot_geometry(inOptions, softwareOpt);




if hardwareOpt.KWave
 kWaveSetup=kwaveSimulation_Setup(struct('pPatch',   pPatch));
 kwaveInput.kgrid=kWaveSetup.kgrid;
 kwaveInput.medium=kWaveSetup.medium;
 kwaveInput.KwavePatch=kWaveSetup.KwavePatch;
 kwaveInput.input_signal=kWaveSetup.input_signal;

 kwaveInput.input_args=kWaveSetup.input_args;
 fs    = 1/kwaveInput.kgrid.dt; % Sampling frequency [Hz]
 c= kWaveSetup.c0; %background speed of sound
 %InOptions.plot_kgrid=1;InOptions.plot_subArrays=0;InOptions.plot_patch=1;
 %kWaveSetup.plot_geometry(InOptions, kwaveInput.KwavePatch,kwaveInput.kgrid,kwaveInput.medium);
 [active_path,~,~] = fileparts(which('start.m'));
 active_path=[active_path '\'];
 BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, [], kwaveInput, active_path);
else
 BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData,[]);   
end

BfData_temp = BfData_raw(~all(cellfun(@isempty, BfData_raw),2),:);
maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
clear BfData_temp;

%% PostProcessing and compounding
Axis    = [];
Bmode3D = [];

outOptions = struct('filterBmode',     1, ...
    'slice',           'elevation', ...
    'outImgSize',      [imageSize(1) imageSize(2)], ...
    'alpha',           0, ... % attenuation
    'debug_plots',     false, ...
    'use_post_pad',    false, ...
    'save_fig',        0, ...
    'save_png',        0, ...
    'outFolder',       outFolder, ...
    'maxBfData',       double(maxBfData));

if hardwareOpt.KWave
 temp=cell2mat(reshape(kwaveInput.KwavePatch.elementPosCenters, [1 size(kwaveInput.KwavePatch.elementPosCenters,1)*size(kwaveInput.KwavePatch.elementPosCenters,2)])');
 kwaveInput.KwavePatch.Tx.apex=mean(temp);
 kwaveInput.KwavePatch.Tx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.Tx.pitch=pPatch.Tx.pitch;
 kwaveInput.KwavePatch.Rx.apex=mean(temp);
 kwaveInput.KwavePatch.Rx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.compoundingProbe.subArrayPos=kwaveInput.KwavePatch.subArrayPosCenters;
 kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature=cellfun(@(x) -x, kwaveInput.KwavePatch.subArrayCurvature, 'UniformOutput', false);
 clear temp;
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, kwaveInput.KwavePatch, softwareOpt, outOptions);
else
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, pPatch, softwareOpt, outOptions);   
end

for ii=1:numel(postProcessedPerSubArray)
                              figure(3); hold on; 
                              bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
                              p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
                              p.AlphaData=0.7.*ones(size(bmode)); p.AlphaData((bmode==min(bmode(:))))=0;
                              set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
                              axis equal tight ij; colormap(gray); 
                              saveas(gca, [active_path 'bmode1.png']);
                              
                              figure(4);
                              bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
                              p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
                              axis equal tight ij; colormap(gray); caxis([-60 0]);colorbar;
                              set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
                              saveas(gca, [active_path 'bmode2.png']);
end                              
                              
if hardwareOpt.KWave                              
 CENTER = kwaveInput.KwavePatch.compoundingProbe.subArrayPos;
 ALPHA  = kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature;
else
 CENTER = pPatch.compoundingProbe.subArrayPos;
 ALPHA  = pPatch.compoundingProbe.subArrayCurvature;   
end    

compPosition = zeros(length(BfData_raw),3);
plot_debug=1;
for ss = 1:length(BfData_raw)
    
    saIndex = softwareOpt.shotIndex(ss,:);
    compPosition(ss,:) = CENTER{saIndex(1), saIndex(2)};
    saAngle    = -ALPHA {saIndex(1), saIndex(2)};
    
    if size(BfData_raw{ss}.data,2)>1 %sectorial imaging
            switch sector_image_plane %ignore non-primary curvature for 2D image
                      case 'azimuth'
                               saAngle=[0 saAngle(2)];
                      case 'elevation'
                               saAngle=[saAngle(1) 0];
            end
    end
    inputOpt.imgSize=imageSize(2);
    inputOpt.sizeOfArray=pPatch.Tx.subArraySize;
    inputOpt.sizeOfPatch=pPatch.Tx.nElements_x;
    [xt,yt,zt,Bt]=prepareAxis(postProcessedPerSubArray{ss}, sector_image_plane, compPosition(ss,:), saAngle, inputOpt);
    Axis    = [Axis; [xt yt zt]];
    Bmode3D = [Bmode3D; Bt];
end

%% plotting
if hardwareOpt.KWave 
  step_3D_mesh=kwaveInput.kgrid.dx;%10^(-3)/2;%how precise the uniform 3D mesh is [m]
else
 step_3D_mesh=10^(-3)/2;   
end    

[mesh3D, BmodeGlobal, pointsDistance] = define_global_3D_mesh(Axis,step_3D_mesh);
[BmodeGlobal, pointsDistance]         = ThreeTwoDimensionalCompoundingv2 (Axis, Bmode3D, mesh3D, BmodeGlobal, pointsDistance);
BmodeGlobal(~isnan(BmodeGlobal))      = BmodeGlobal(~isnan(BmodeGlobal))./pointsDistance((~isnan(BmodeGlobal)));
%BmodeGlobal(isnan(BmodeGlobal))   = -80;

% plot 3inOptions.plotsubArray=0;
inOptions.plotSequence=0;
inOptions.newFigure=1;
pPatch.plot_geometry(inOptions, softwareOpt);

threshold=-60;
ind=find(BmodeGlobal>threshold);
figure(3); hold on;
scatter3(mesh3D.X_global_mesh(ind).*10^3,mesh3D.Y_global_mesh(ind).*10^3,mesh3D.Z_global_mesh(ind).*10^3, 2.0,BmodeGlobal(ind), 'filled');
%scatter3(Y_global_mesh(ind).*10^3,X_global_mesh(ind).*10^3,Z_global_mesh(ind).*10^3, 50.0,BmodeGlobal(ind), 'filled');
colormap(flipud(hot)); colorbar;
axis equal tight;colorbar;
caxis([threshold 0]); view(3);
saveas(gca, [active_path 'bmode_3Dview.png']);

%plot azimuth projection
[~,r]=min(abs(mesh3D.Z_global_mesh(:)-mean(mesh3D.Z_global_mesh(ind))));
[r,cc,v]=ind2sub(size(mesh3D.Z_global_mesh),...
                             find(mesh3D.Z_global_mesh==mesh3D.Z_global_mesh(r)));
for jk=1:numel(r)
     x(r(jk)-min(r)+1, cc(jk)-min(cc)+1)=mesh3D.X_global_mesh(r(jk),cc(jk),v(jk));
     y(r(jk)-min(r)+1, cc(jk)-min(cc)+1)=mesh3D.Y_global_mesh(r(jk),cc(jk),v(jk));
     Bmode_Az_Proj(r(jk)-min(r)+1, cc(jk)-min(cc)+1)=BmodeGlobal(r(jk),cc(jk),v(jk));
end    
     
figure; 
ind=find(Bmode_Az_Proj>-60);
[Xq,Yq]=meshgrid(linspace(min(x(ind)),max(x(ind)),1024), linspace(min(y(ind)),max(y(ind)),1024));
Bmode_Az_Proj2 = interp2(x,y,Bmode_Az_Proj,Xq,Yq);
%imagesc(x(1,:),y(:,1),Bmode_Az_Proj); 
imagesc(Xq(1,:),Yq(:,1),Bmode_Az_Proj2); 
axis equal tight ij; colormap(gray);
caxis([-60 0]);colorbar;
saveas(gca, [active_path 'bmode_projection.png']);