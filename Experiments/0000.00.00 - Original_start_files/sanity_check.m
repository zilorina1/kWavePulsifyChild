% sanaty_check.m - a quick check if something is broken

%% preamble
close all
clearvars
profile on
F = findall(0,'type','figure','tag','TMWWaitbar');
delete(F);

%% constants
global c fs f0
f0    = 2.5e6; % Transducer center frequency [Hz]
fs    = 100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]';
alpha = 0.0; % db/cm/MHz attenuation for TGC

set_field ('att',  alpha*100/1e6*f0);
set_field ('c', c);
set_field ('fs', fs);
set_field ('use_att',1);
set_field ('use_rectangles',1);

%% TEST 1 -- azimuth transmission
tic
azimuth_transmission;
toc

%% TEST 2 - middle slice of ellipsoid
tic
middle_slice_of_ellipsoid;
toc

%% TEST 3 - middle slice of ellipsoid
tic
pulse_echo_pattern;
toc

%% functions %%

function azimuth_transmission
%  azimuth transmission -- part of the sanity_check. Gets azimuth slices
%  for full control array with fieldII and COLE

%% Preliminaries
azAngleSweep        = ((-15:15))*pi/180;
elAngleSweep        = 0;

%% transmission description
txFocalDepth  = 0.05; % metres
openingAngle    = [(max(azAngleSweep)-min(azAngleSweep))*180/pi  (max(elAngleSweep)-min(elAngleSweep))*180/pi];

%% define patch
pPatch = pulsifyPatch(struct(...
    'position',       [0.0, 0, 0.05], ...
    'amplitude',      1));

%% soft options: describes how to program the patch: what is the focus, how many shots and in which direction
softwareOpt = PP_scanSequencer(struct(...
    'scanMode',          'zero', ...
    'txFocalDepth',      txFocalDepth, ...
    'nTxAz',             length(azAngleSweep), ...
    'nTxEl',             length(elAngleSweep), ...
    'openingAngle',      openingAngle));

%% hard options: describes the hardware of the patch: TFT, number of ADC's, etc.
hardwareOpt{1} = transOptions(struct(...
    'mode_tx',     'tx_on_rows',...
    'mode_rx',     'rx_on_cols', ...
    'description', 'full_control'));

hardwareOpt{2} = transOptions(struct(...
    'mode_tx',     'full_control',...
    'mode_rx',     'full_control',...
    'COLE',        true, ...
    'description', 'full_control_COLE'));

%% initializing variables
BfData = cell(2,1);

%% start
for ii = 1:length(hardwareOpt)
    BfData{ii}  = pPatch.run_transmission(softwareOpt, hardwareOpt{ii});
    
    %% Post processing
    outOptions = struct('slice',           'azimuth', ...
        'alpha',           0*1e-6, ...
        'focusAngle',      0, ...
        'use_post_pad',    true, ...
        'save_fig',        false, ...
        'save_png',        false);
    
    %% plot image
    post_processing.plot_image(BfData{ii} , pPatch, softwareOpt, outOptions, hardwareOpt{ii});
end

pPatch.plot_geometry([], softwareOpt)

end % end function

function middle_slice_of_ellipsoid
% middle_slice_of_ellipsoid -- part of sanity_check. Plots middle slice of
% ellipsoid.
tx_focal_depth = 0.05;

pPatch = pulsifyPatch(struct(...
    'nPoints',        5e5, ...
    'coherentCompounding', true, ...
    'predefined',     'ellipsoid', ...
    'ellipsoidData',  [1.33, 2, 0.03], ...
    'rotationAngle',  [0, 0, 0], ...
    'center',         [0, 0 ,0.065], ...
    'radius',         [33/1e3, 25/1e3]));

%% softOpt
softwareOpt = PP_scanSequencer(struct(...
    'scanMode',       'azimuthSlice', ...
    'initialPosition',  [76, 1], ...
    'openingAngle'    , [0 0], ...
    'txFocalDepth',   tx_focal_depth, ...
    'nTxAz'         , 1, ...
    'nTxEl'         , 1 , ...
    'maxDepth'      , 0.13));

%% load data
folderName = strcat(pwd, '/Dataset/COLE/full_control_full_control/3D/');
filename = strcat(folderName, 'LUT_angleAz_0_angleEl_0.mat');
load(filename,'echoData', 'impulseResponse')
rawData.echoData        = echoData;
rawData.impulseResponse = impulseResponse;

%% hardware options
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'COLE',        true, ...
    'description', 'fullC'));


%% Transmission and align
BfData  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData);

%% Out
outOptions = struct('filterBmode',     1, ...
    'outImgSize',      [256 256], ...
    'slice',           'azimuth', ...
    'alpha',           0*1e-6, ...
    'scanConv',        false, ...
    'save_fig',        0, ...
    'save_png',        0);

%% plot and movie
post_processing.plot_image(BfData , pPatch, softwareOpt, outOptions, hardwareOpt);
pPatch.plot_geometry([], softwareOpt);

%% finish
xdc_free(pPatch.Tx.FiiPointer);
xdc_free(pPatch.Rx.FiiPointer);

end

function pulse_echo_pattern
% generate_lookup_table_2D.m - This script generates LUTs to be used in the
% new COLE algorithm
% there are focus angles and sweep angle. Each focus angle creates a .mat
% file. Each .mat file contains a sweep deterimned by the sweep angle and
% maximum and minimum depths. Impulse responses are also calculated and
% saved on the same .mat file

%% physical parameters
tx_focal_depth  = 0.05; % metres
alpha           = 0;
focusAngle      = 0; % delete

%% calculate LUT at these positions
angleSweep      = (-75:75)';
direction       = 'elevation';
minimumDepth    = 0.01;
maximumDepth    = 0.15;

%% Constants
global c fs f0
f0    = 2.5e6; % Transducer center frequency [Hz]
fs    = 100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]';

set_field ('att',  alpha*100/1e6*f0);
set_field ('c', c);
set_field ('fs',  fs);
set_field ('use_att',1);
set_field ('use_rectangles',1);

%% start
%%
angleSweep_forCalculation   = angleSweep(1:1:end);

nTxAz           = length(angleSweep);  % number of transmits in azimuth
nTxEl           = 1;   % number of transmits in elevation
openingAngle    = [(max(angleSweep)-min(angleSweep))  0];


%% patch
pPatch = pulsifyPatch;

%% soft options
softwareOpt = PP_scanSequencer(struct(...
    'txFocalDepth',   tx_focal_depth, ...
    'nTxAz'         , nTxAz, ...
    'nTxEl'         , nTxEl , ...
    'openingAngle'  , openingAngle));

%% hardware opt
hardwareOpt = transOptions(struct('mode_tx', 'tx_on_rows',...
    'mode_rx',     'rx_on_rows',...
    'description', 'row_row_el'));

outOptions = struct('filterBmode',     0, ...
    'slice',           direction, ...
    'alpha',           alpha*1e-6, ...
    'focusAngle',      0, ...
    'save_fig',        false, ...
    'save_png',        false);

for ff = focusAngle
    
    %% Calculate focus
    if strcmp(direction, 'azimuth')
        tx_focus = PP_Utils.beam2cart(ff, 0, tx_focal_depth);
    else
        tx_focus = PP_Utils.beam2cart(0, ff, tx_focal_depth);
    end
    
    %% calculate pulse echo response
    r_forCalculation   = ((round(minimumDepth*1e3):2:round(maximumDepth*1e3))*1e-3)';
    
    aux = unique(sort([angleSweep_forCalculation; ff]));
    if strcmp(direction, 'azimuth')
        twodAnglePosition = [ aux zeros(length(aux),1)];
    elseif strcmp(direction, 'elevation')
        twodAnglePosition = [zeros(length(aux),1) aux];
    end
    
    %% pulse echo field
    pulseEcho = pPatch.calculate_echo_field_2D(r_forCalculation, twodAnglePosition, tx_focus, hardwareOpt);
    
    %% interpolation
    [PHI, R] = meshgrid(aux, r_forCalculation);
    pulseEchoS = single(pulseEcho);
    
    timeAxis = (min(r_forCalculation)*2)/(c):1/fs:(max(r_forCalculation)*2)/(c);
    r = timeAxis*c/2;
    
    [PHI_q, R_q]      = meshgrid(angleSweep, r);
    echoData.data     = interp2(PHI,R, pulseEchoS, PHI_q, R_q, 'linear');
    echoData.r        = r;
    echoData.focusDepth = tx_focal_depth;
    echoData.angle    = angleSweep;
    echoData.timeAxis = timeAxis;
    
    %% impulse response
    impulseResponse.data = pPatch.calculate_impulse_response_2D(tx_focal_depth, twodAnglePosition, tx_focus, hardwareOpt);
    impulseResponse.angle = aux;
    impulseResponse.focusDepth = tx_focal_depth;
    
    post_processing.plot_echo_field(echoData, softwareOpt, outOptions);
end
end
