% singleTest.m - This is a simple test for the main functionalities of the
% PulsifySoft tool.
% This experiment calculates the PSF of a point scatterer at 5 cm from the
% Pulsify array. An azimuthal slice is taken. All the main functionality of
% the code is here, from the use of the PP_scansequencer and plotting to
% the the setting of different hardware options

%% preamble 
tic
close all
clearvars
profile clear
profile on
F = findall(0,'type','figure','tag','TMWWaitbar');
delete(F);

%% constants
global c fs f0 plot_debug
f0    = 2.5e6; % Transducer center frequency [Hz]
fs    = 100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]';
plot_debug =0;

%% Preliminaries
azAngleSweep        = (-75:75)*pi/180;
elAngleSweep        = 0;

%% transmission description
txFocalDepth  = 0.05; % metres
nTxAz           = length(azAngleSweep);  % number of transmits in azimuth
nTxEl           = length(elAngleSweep);   % number of transmits in elevation
openingAngle    = [(max(azAngleSweep)-min(azAngleSweep))*180/pi  (max(elAngleSweep)-min(elAngleSweep))*180/pi];
alpha           = 0.0; % db/cm/MHz attenuation for TGC

set_field ('att',  alpha*100/1e6*f0);
set_field ('c', c);
set_field ('fs', fs);
set_field ('use_att',1);
set_field ('use_rectangles',1);

%% define patch
pPatch = pulsifyPatch(struct(...
    'shape',          'flat', ...
    'position',       [0, 0, 0.05], ...
    'amplitude',      10));

%% soft options: describes how to program the patch: what is the focus, how many shots and in which direction
softwareOpt = PP_scanSequencer(struct(...
    'scanMode',          'zero', ...
    'txFocalDepth',      txFocalDepth, ...
    'nTxAz',             nTxAz, ...
    'nTxEl',             nTxEl, ... 
    'openingAngle',      openingAngle));

%% hard options: describes the hardware of the patch: TFT, number of ADC's, etc.
hardwareOpt{1} = transOptions(struct(... 
    'mode_tx',     'full_control',...
    'mode_rx',     'full_control', ...
    'description', 'full_control'));


%% initializing variables
BfData = cell(length(hardwareOpt), 1);
img     = BfData;
postProcessed     = BfData;

%% create folder
outFolder_home = '/Experiments/singleTest/';
outFolder = strcat(pwd, outFolder_home);

if ~exist(outFolder, 'dir')
    mkdir(outFolder)
end

%% start
for ii = 1:length(hardwareOpt)
    
    %% run transmission - simulate the interaction between TX, phantom and RX.
    BfData{ii}  = pPatch.run_transmission(softwareOpt, hardwareOpt{ii});
    
    %% Post processing
    outOptions = struct('slice',           'azimuth', ...
        'alpha',           alpha*1e-6, ...
        'focusAngle',      0, ...
        'use_post_pad',    true, ...
        'axisX',           [-55 55], ...
        'axisY',           [ 0 55], ...
        'save_fig',        true, ...
        'save_png',        true, ...
        'outFolder',       outFolder);
    
    %% plot image
    [img{ii}, postProcessed{ii}] = post_processing.plot_image(BfData{ii}{1} , pPatch, softwareOpt, outOptions, hardwareOpt{ii});
    
end

%% comparison
post_processing.comparison_plot(outOptions, postProcessed, hardwareOpt)

%% finish
xdc_free(pPatch.Tx.FiiPointer);
xdc_free(pPatch.Rx.FiiPointer);

pPatch.plot_geometry([], softwareOpt);
toc