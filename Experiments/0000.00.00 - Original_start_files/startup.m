%% startup
restoredefaultpath
folderNames = {'Common', 'Experiments', 'Modules', 'Dataset', 'FieldII', 'cvx', 'k_wave'};
for ii = 1:length(folderNames)
    addpath(genpath(strcat(pwd, '\', folderNames{ii})))
end
rmpath(strcat(pwd, '\Common\garbage'))
field_init(0)
clearvars