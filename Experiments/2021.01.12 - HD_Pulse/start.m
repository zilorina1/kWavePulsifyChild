close all
clc
clearvars %except newStream
profile clear
profile on

F = findall(0,'type','figure','tag','TMWWaitbar');
delete(F);

global newStream DWTitled DWSliding
fname='SinaSLT_UnderWater_3MHz_0.02_50V_4cycles';
ffolder='H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.12 - HD_Pulse\Experiment_2503201\Sina_New_transducer_3-6MHz_0.0002\';
load([ffolder...
     fname '.mat']);
newStream=FRD;
DWTitled=0;
DWSliding=0;
global Reconstruct
Reconstruct=0;
%% Preliminaries
global c fs f0 
%f0    = double(newStream.Probe.F0); % Transducer center frequency [Hz] 2.5e6
f0=6*10^6;
fs    = double(newStream.Fs); % Sampling frequency [Hz]
c     = 1500;%double(newStream.Probe.c0); % Speed of sound [m/s]'; for COLE and FieldII

%%
tx_focal_depth  = newStream.scanSequence.txFocalDepth_m; % metres
alpha           = 0.0; % db/cm/MHz attenuation for TGC
imageSize       = [512 512];

%% patch and software options

% pPatch = pulsifyPatch(struct(...
%     'Tx_shape',                'flat', ...
%     'Tx_nElements_x',             double(newStream.Probe.nElements(1)),... %azimuth
%     'Tx_nElements_y',             double(newStream.Probe.nElements(2)), ....%elevation
%     'Tx_subArraySize',              [double(newStream.scanSequence.nTxChanPerTxEvent(2)) double(newStream.scanSequence.nTxChanPerTxEvent(1))],...%[el az]
%     'Tx_width',                 double(newStream.Probe.elemWidth(1)),...
%     'Tx_height',            double(newStream.Probe.elemWidth(2)),...
%     'Tx_kerf',            double(newStream.Probe.kerf(1)),...
%     'predefined',    'none',...
%     'nPoints',              1, ...
%     'position',             [0, 0, 0.05], ...
%     'amplitude',            1));

pPatch = pulsifyPatch(struct(...
    'Tx_shape',                'flat', ...
    'Tx_nElements_x',             double(newStream.Probe.nElements(1)),... %azimuth
    'Tx_nElements_y',             double(newStream.Probe.nElements(2)), ....%elevation
    'Tx_subArraySize',              [double(newStream.scanSequence.nTxChanPerTxEvent(2)) double(newStream.scanSequence.nTxChanPerTxEvent(1))],...%[el az]
    'Tx_width',                 0.0002,...
    'Tx_height',            0.0002,...
    'Tx_kerf',            0,...
    'predefined',    'none',...
    'nPoints',              1, ...
    'position',             [0, 0, 0.05], ...
    'amplitude',            1,...
     'Tx_apodization',       'rectwin',...
     'Rx_apodization',       'rectwin'));


%stepX=40;
%X = 1:stepX:130; %along elevation direction, from up to down
aux=pPatch.convert_cell_to_Fii_mat(pPatch.Tx.subArrayPos); 
Indexes=[];
for i=1:size(newStream.scanSequence.TxApexes,1)
      [~,ind]=min(pdist2(aux,squeeze(newStream.scanSequence.TxApexes(i,1,:))'));
      [row,col]= ind2sub(size(pPatch.Tx.subArrayPos),ind);
      Indexes=[Indexes;row col];
end    
X=unique(Indexes(:,1))';
Y =[unique(Indexes(:,2))'];%along azimuth direction, from left to right
[XX, YY] = meshgrid(X,Y);
shotPosition = [XX(:) YY(:)];

if DWTitled||DWSliding, nMLAs=[90 1]; else nMLAs=[1 1]; end    
if DWSliding, TxEvents=[1 1]; else TxEvents=newStream.scanSequence.nTxEvents;  end
%% software options
softwareOpt = PP_scanSequencer(struct(...
'scanMode',         'custom', ...
    'shotIndex',     shotPosition, ...
    'initialPosition',  [1, 1], ...
    'txFocalDepth',     double(tx_focal_depth), ...
    'nTxAz'         ,   TxEvents(1), ...
    'nTxEl'         ,   TxEvents(2), ...
    'maxDepth'      ,   0.13, ...
    'coleAmpThreshold', 1e-28, ...
    'nMLAs',            nMLAs,...
    'openingAngle'  ,   double([newStream.scanSequence.openingAngle_deg(1), newStream.scanSequence.openingAngle_deg(2)])));

if softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end    
    
%% hardware options
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'Experiment',        true));


if (~hardwareOpt.KWave)&&(~hardwareOpt.Experiment)
    set_field ('att',  alpha*100/1e6*f0);
    set_field ('c', c);
    set_field ('fs',  fs);
    set_field ('use_att',1);
    set_field ('use_rectangles',1);
end

if strcmp(hardwareOpt.mode_tx,'tx_on_rows')&strcmp(hardwareOpt.mode_rx,'rx_on_rows')&(softwareOpt.nTxAz>1)
    error('For row-row, we can do sectorial imaging only in the elevation plane');
end   
if strcmp(hardwareOpt.mode_tx,'tx_on_cols')&strcmp(hardwareOpt.mode_rx,'rx_on_cols')&(softwareOpt.nTxEl>1)
    error('For col-col, we can do sectorial imaging only in the azimuth plane');
end

if hardwareOpt.COLE
%% load LUT data once
% I save it to rawData variable, so if you want to load different LUTs, you
% would need to clean rawData variable (in the beginning of the file all variables are clened except it)
%index=round(cmpdImg.number_of_images_in_secondary_direction/2);
if (~exist('rawData','var'))
   if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
      folderName = strcat(pwd, '\Dataset\COLE\', 'tx_on_rows', '_', 'rx_on_rows', '\3D\'); 
   else    
      folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '\3D\');
   end
   h_w = waitbar(0,['I will load LUT tables forever...']);
   fprintf('I will load LUT tables forever...');
   t0 = tic; 
   for ii = 1:length(softwareOpt.txEvents)
                 if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '.mat');
                 else    
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '.mat');
                 end
                 if ~exist(filename, 'file')
                        error('COLE: LUT not found');
                 else
                        load(filename, 'echoData', 'impulseResponse');
                        rawData.echoData{ii}=echoData;
                        rawData.impulseResponse{ii}=impulseResponse;
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                            rawData.echoData{ii}.data = permute(rawData.echoData{ii}.data, [1 3 2]);
                        end    
                 end
                 waitbar((ii)/(length(softwareOpt.txEvents)),h_w);        
   end
   delete(h_w), drawnow
end
end

%% folder
outFolder_home = '/Experiments/2020.08.17 - Kate reorganization/';
outFolder = strcat(pwd, outFolder_home);
if ~exist(outFolder, 'dir')
    mkdir(outFolder)
end


inOptions.plotsubArray=0;
inOptions.plotSequence=1;
inOptions.newFigure=1;
pPatch.plot_geometry(inOptions, softwareOpt);

% hold on;
% scatter3(pPatch.Tx.elementPos{1,1}(1)*10^3, pPatch.Tx.elementPos{1,1}(3)*10^3, pPatch.Tx.elementPos{1,1}(2)*10^3, 5.0, 'r', 'filled');
% scatter3(pPatch.Tx.elementPos{4,1}(1)*10^3, pPatch.Tx.elementPos{4,1}(3)*10^3, pPatch.Tx.elementPos{4,1}(2)*10^3, 5.0, 'r', 'filled');

[active_path,~,~] = fileparts(which('start.m'));
active_path=[active_path '\'];
 
if hardwareOpt.KWave
 kWaveSetup=kwaveSimulation_Setup(struct('pPatch',   pPatch));
 kwaveInput.kgrid=kWaveSetup.kgrid;
 kwaveInput.medium=kWaveSetup.medium;
 kwaveInput.KwavePatch=kWaveSetup.KwavePatch;
 kwaveInput.input_signal=kWaveSetup.input_signal;

 kwaveInput.input_args=kWaveSetup.input_args;
 fs    = 1/kwaveInput.kgrid.dt; % Sampling frequency [Hz]
 c= kWaveSetup.c0; %background speed of sound
%  InOptions.plot_kgrid=0;InOptions.plot_subArrays=0;InOptions.plot_patch=1;
%  kWaveSetup.plot_geometry(InOptions, kwaveInput.KwavePatch,kwaveInput.kgrid,kwaveInput.medium);
%  hold on;
%  scatter3(kwaveInput.KwavePatch.elementPosCenters{1,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(2)*10^3, 10.0, 'r', 'filled');
%  scatter3(kwaveInput.KwavePatch.elementPosCenters{4,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(2)*10^3, 10.0, 'r', 'filled');

 BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, [], kwaveInput, active_path);
else   
 if hardwareOpt.Experiment
    rawData= newStream.channelData;
 end    
 [BfData_raw,~]  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData,[], active_path);   
end

BfData_temp = BfData_raw(~all(cellfun(@isempty, BfData_raw),2),:);
maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
clear BfData_temp;

%% PostProcessing and compounding
Axis    = [];
Bmode3D = [];

outOptions = struct('filterBmode',     1, ...
    'slice',           'elevation', ...
    'outImgSize',      [1024 1024], ...
    'alpha',          2e-8,...%2e-6,4e-6 ... % attenuation
    'debug_plots',     false, ...
    'use_post_pad',    false, ...
    'save_fig',        0, ...
    'save_png',        0, ...
    'outFolder',       outFolder, ...
    'maxBfData',       double(maxBfData));

if hardwareOpt.KWave
 temp=cell2mat(reshape(kwaveInput.KwavePatch.elementPosCenters, [1 size(kwaveInput.KwavePatch.elementPosCenters,1)*size(kwaveInput.KwavePatch.elementPosCenters,2)])');
 kwaveInput.KwavePatch.Tx.apex=mean(temp);
 kwaveInput.KwavePatch.Tx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.Tx.pitch=pPatch.Tx.pitch;
 kwaveInput.KwavePatch.Rx.apex=mean(temp);
 kwaveInput.KwavePatch.Rx.centerOfAperture=mean(temp);
 kwaveInput.KwavePatch.compoundingProbe.subArrayPos=kwaveInput.KwavePatch.subArrayPosCenters;
 kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature=cellfun(@(x) -x, kwaveInput.KwavePatch.subArrayCurvature, 'UniformOutput', false);
 clear temp;
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, kwaveInput.KwavePatch, softwareOpt, outOptions);
else
 postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, pPatch, softwareOpt, outOptions);   
end

% for sectorial 2D compounding
% To better filter the background noise outside the object (non-essential, for not bright pixels <-60 and -40)
cmpdImg.filterPixels1=-60;
cmpdImg.filterPixels2=-40;

for ii=1:numel(postProcessedPerSubArray)
%                               figure(3); hold on; 
%                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
%                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
%                               p.AlphaData=0.7.*ones(size(bmode)); p.AlphaData((bmode==min(bmode(:))))=0;
%                               set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
%                               axis equal tight ij; colormap(gray); 
%                               saveas(gca, [active_path fname '_bmode1.png']);
%                               
%                               figure(4);
%                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
%                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
%                               axis equal tight ij; colormap(gray); caxis([-60 0]);colorbar;
%                               set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
%                               saveas(gca, [active_path fname '_bmode2.png']);
                              
                              [AzAxisPerSubArray,ElAxisPerSubArray, BmodePerSubArray]=TwoDimensionalCompounding(ii,postProcessedPerSubArray(1:ii),...
                                   outOptions, cmpdImg);
end                              

figure('Renderer', 'painters', 'Position', [500 100 900 650]); clf;
postProcessed.disp_az_lims=AzAxisPerSubArray{end};
postProcessed.disp_dep_lims=ElAxisPerSubArray{end};
postProcessed.Bmode=BmodePerSubArray{end};%.*maxN;
imagesc(postProcessed.disp_az_lims.*100, postProcessed.disp_dep_lims.*100,postProcessed.Bmode);
colormap(gray); colorbar; 
set(gca,'YDir','Reverse');
axis equal tight;
caxis([-60 0]); set(gca, 'Fontsize', 20);
xlabel('Azimuth [cm]','FontSize', 32); ylabel('Depth [cm]','FontSize',32); 
ylabel(get(gca,'colorbar'),'Brightness [dB]', 'FontSize', 32); colormap(gray);
%set(gca, 'XDir','reverse')

hold on;
init_pos=[0.7 2.7];%[-0.45 3.17];%[0.23 2.7];%[-0.45 3.17];
%scatter(init_pos(:,1), init_pos(:,2),5.0,'r','filled');
 
scatterers_pos=init_pos+[-4.6 -1.2; 11.5 0; -30.25 0; -35 0; -14.5 16.05;...
   -4.25 14.05; +26 14.05; +26 28.1; -14.5 42.16; -22 58.21]./10;
% scatterers_pos=init_pos+[0.45 3.17; -11.5 0; 30.25 0; 35 0; 14.5 16.05;...
%     4.25 14.05; -26 14.05; -26 28.1; +14.5 42.16; +35 56.21]./10;
scatter(scatterers_pos(:,1), scatterers_pos(:,2),5.0,'r','filled');
saveas(gca, [ffolder fname '_bmode.png']);
