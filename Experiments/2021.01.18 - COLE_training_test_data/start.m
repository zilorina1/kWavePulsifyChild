close all
clc

clear

profile clear
profile on

F = findall(0,'type','figure','tag','TMWWaitbar');
delete(F);

[active_path,~,~] = fileparts(which('start.m'));
active_path=[active_path '\'];

%% Preliminaries
global c fs f0
f0    = 6.25e6;%7.5e6; % Transducer center frequency [Hz]
fs    =32*f0;%f0*32;%100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]';
global DWSliding
DWSliding=0;
lambda=c/f0;

global PhantomIndex
global Reconstruct
Reconstruct=0;
global SaveKol
SaveKol=5723;
for PhantomIndex=5723:10000
%%
tx_focal_depth  =0.03; % metres
alpha           = 0.0; % db/cm/MHz attenuation for TGC
imageSize       = [512 512];
Y_level          = 0.01;

set_field ('att',  alpha*100/1e6*f0);
set_field ('c', c);
set_field ('fs',  fs);
set_field ('use_att',1);
set_field ('use_rectangles',1);

%% patch and software option
phantom_scatterers_number=1;%randi(3,1);
phantom_position=zeros(phantom_scatterers_number,3);
az_angle=100; range=1;
for jj=1:phantom_scatterers_number
    while ((abs(az_angle)>44)&(range>=0.06))
    phantom_position(jj,:)=[-0.02+0.04*rand(1,1), 0, 0.01+0.05*rand(1,1)];
    [az_angle, el_angle, range] = PP_Utils.cart2beam(phantom_position(jj,1),phantom_position(jj,2), phantom_position(jj,3));
    end    
end
pPatch = pulsifyPatch(struct(...
    'Tx_shape',                'flat', ...
    'Tx_nElements_x',             33,... %azimuth
    'Tx_nElements_y',            1, ....%elevation
    'Tx_subArraySize',              [1 33],...%[el az]
    'Tx_width',        2*lambda,...
    'Tx_height',      2*lambda,...
    'Tx_kerf',       0.05*2*lambda, ...
    'position',             phantom_position, ...
     'amplitude',           ones(phantom_scatterers_number,1), ...
    'Tx_apodization',       'rectwin',...
    'Rx_apodization',       'rectwin'));
    

X=1;
stepY=1;
Y =1;%[1:4:55];%along azimuth direction, from left to right
[XX, YY] = meshgrid(X,Y);
shotPosition = [XX(:) YY(:)];

%% software options
softwareOpt = PP_scanSequencer(struct(...
'scanMode',         'custom', ...
    'shotIndex',     shotPosition, ...
    'initialPosition',  [1, 1], ...
    'txFocalDepth',     tx_focal_depth, ...
    'nTxAz'         ,   91, ...
    'nTxEl'         ,   1, ...
    'maxDepth'      ,   0.13, ...
    'coleAmpThreshold', 1e-28, ...
    'openingAngle'  ,   [abs(-45)+abs(45),0]));

if softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end    
    
%% hardware options
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'COLE',        false));

if strcmp(hardwareOpt.mode_tx,'tx_on_rows')&strcmp(hardwareOpt.mode_rx,'rx_on_rows')&(softwareOpt.nTxAz>1)
    error('For row-row, we can do sectorial imaging only in the elevation plane');
end   
if strcmp(hardwareOpt.mode_tx,'tx_on_cols')&strcmp(hardwareOpt.mode_rx,'rx_on_cols')&(softwareOpt.nTxEl>1)
    error('For col-col, we can do sectorial imaging only in the azimuth plane');
end

if hardwareOpt.COLE
    %folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx,'\',sector_image_plane, '\', 'with_side_lobes_', num2str(pPatch.Tx.nElements_x), '_',...
    %    num2str(pPatch.Tx.nElements_y), '_elSizeby10^4_', num2str(pPatch.Tx.width*10^4), '\');
    %folderName= strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx,'\', sector_image_plane, '\', 'no_side_lobes_', num2str(pPatch.Tx.nElements_x), '_',...
    %    num2str(pPatch.Tx.nElements_y), '_elSizeby10^4_', num2str(pPatch.Tx.width*10^4), '\');
    folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx,'\',sector_image_plane, '\', 'apodized_', num2str(pPatch.Tx.nElements_x), '_',...
        num2str(pPatch.Tx.nElements_y), '_elSizeby10^4_', num2str(pPatch.Tx.width*10^4), '\');
    
%% load LUT data once
% I save it to rawData variable, so if you want to load different LUTs, you
% would need to clean rawData variable (in the beginning of the file all variables are clened except it)
%index=round(cmpdImg.number_of_images_in_secondary_direction/2);
if (~exist('rawData','var'))
%    if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
%       folderName = strcat(pwd, '\Dataset\COLE\', 'tx_on_rows', '_', 'rx_on_rows', '\3D\'); 
%    else    
%       folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '\3D\');
%    end
   %folderName=strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '\azimuth\with_side_lobes_16_1_elSizeby10^4_4.2857\');
   h_w = waitbar(0,['I will load LUT tables forever...']);
   fprintf('I will load LUT tables forever...');
   t0 = tic; 
   for ii = (length(softwareOpt.txEvents)-1)/2+1:length(softwareOpt.txEvents)
                 filename = strcat(folderName, 'LUT_angle_', abs(num2str(softwareOpt.txEvents(ii).txAngle(1))), '.mat');
                 if ~exist(filename, 'file')
                        error('COLE: LUT not found');
                 else
                        load(filename, 'echoData', 'impulseResponse');
                        rawData.echoData{ii-(length(softwareOpt.txEvents)-1)/2}=echoData;
                        rawData.impulseResponse{ii-(length(softwareOpt.txEvents)-1)/2}=impulseResponse;
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                            rawData.echoData{ii-(length(softwareOpt.txEvents)-1)/2}.data = permute(rawData.echoData{ii}.data, [1 3 2]);
                        end    
                 end
                 waitbar((ii-(length(softwareOpt.txEvents)-1)/2)/(length(softwareOpt.txEvents)),h_w);        
   end
   delete(h_w), drawnow
end
end

% inOptions.plotsubArray=0;
% inOptions.plotSequence=0;
% inOptions.newFigure=1;
% pPatch.plot_geometry(inOptions, softwareOpt);
% saveas(gca, ['H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.18 - COLE_training_test_data\training_data\data_' num2str(SaveKol+1) '_geometry.png']);

if hardwareOpt.KWave
    kWaveSetup=kwaveSimulation_Setup(struct('pPatch',   pPatch));
    kwaveInput.kgrid=kWaveSetup.kgrid;
    kwaveInput.medium=kWaveSetup.medium;
    kwaveInput.KwavePatch=kWaveSetup.KwavePatch;
    kwaveInput.input_signal=kWaveSetup.input_signal;
    kwaveInput.input_args=kWaveSetup.input_args;
    fs    = 1/kwaveInput.kgrid.dt; % Sampling frequency [Hz]
    c= kWaveSetup.c0; %background speed of sound
    InOptions.plot_kgrid=0;InOptions.plot_subArrays=0;InOptions.plot_patch=1;
    kWaveSetup.plot_geometry(InOptions, kwaveInput.KwavePatch,kwaveInput.kgrid,kwaveInput.medium);
    %  hold on;
    %  scatter3(kwaveInput.KwavePatch.elementPosCenters{1,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(2)*10^3, 10.0, 'r', 'filled');
    %  scatter3(kwaveInput.KwavePatch.elementPosCenters{4,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(2)*10^3, 10.0, 'r', 'filled');
    
    BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, [], kwaveInput, active_path);
else
   if (exist('rawData','var'))
      BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData,[]);
   else
      [BfData_raw, BfData_rawFiltered]  = pPatch.run_transmission(softwareOpt, hardwareOpt); 
   end    
end
% 
% BfData_temp = BfData_raw(~all(cellfun(@isempty, BfData_raw),2),:);
% %BfData_temp = BfData_rawFiltered(~all(cellfun(@isempty, BfData_rawFiltered),2),:);
% maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
% BfData_temp = BfData_rawFiltered(~all(cellfun(@isempty, BfData_rawFiltered),2),:);
% maxBfData2   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
% %maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
% clear BfData_temp;
% 
% %% PostProcessing and compounding
% Axis    = [];
% Bmode3D = [];
% 
% outOptions = struct('filterBmode',     0, ...
%     'slice',           'elevation', ...
%     'outImgSize',      [imageSize(1) imageSize(2)], ...
%     'alpha',           0, ... % attenuation
%     'debug_plots',     false, ...
%     'use_post_pad',    false, ...
%     'save_fig',        0, ...
%     'save_png',        0, ...
%     'outFolder',       active_path, ...
%     'maxBfData',       double(maxBfData));
% 
% outOptions2 = struct('filterBmode',     0, ...
%     'slice',           'elevation', ...
%     'outImgSize',      [imageSize(1) imageSize(2)], ...
%     'alpha',           0, ... % attenuation
%     'debug_plots',     false, ...
%     'use_post_pad',    false, ...
%     'save_fig',        0, ...
%     'save_png',        0, ...
%     'outFolder',       active_path, ...
%     'maxBfData',       double(maxBfData2));
% 
% global ChannelDataFiltering
% 
% ChannelDataFiltering=0;
% if hardwareOpt.KWave
%  temp=cell2mat(reshape(kwaveInput.KwavePatch.elementPosCenters, [1 size(kwaveInput.KwavePatch.elementPosCenters,1)*size(kwaveInput.KwavePatch.elementPosCenters,2)])');
%  kwaveInput.KwavePatch.Tx.apex=mean(temp);
%  kwaveInput.KwavePatch.Tx.centerOfAperture=mean(temp);
%  kwaveInput.KwavePatch.Tx.pitch=pPatch.Tx.pitch;
%  kwaveInput.KwavePatch.Rx.apex=mean(temp);
%  kwaveInput.KwavePatch.Rx.centerOfAperture=mean(temp);
%  kwaveInput.KwavePatch.compoundingProbe.subArrayPos=kwaveInput.KwavePatch.subArrayPosCenters;
%  kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature=cellfun(@(x) -x, kwaveInput.KwavePatch.subArrayCurvature, 'UniformOutput', false);
%  clear temp;
%  postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, kwaveInput.KwavePatch, softwareOpt, outOptions);
% else
%  postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, pPatch, softwareOpt, outOptions);   
%  postProcessedPerSubArrayFiltered = post_processing.main_post_processing_cells(BfData_rawFiltered, pPatch, softwareOpt, outOptions2);   ChannelDataFiltering=0;
% end

% close all; figure; hold on; 
% for ii=1:numel(postProcessedPerSubArray)
% %                               figure(3); hold on; 
% %                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
% %                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
% %                               p.AlphaData=0.7.*ones(size(bmode)); p.AlphaData((bmode==min(bmode(:))))=0;
% %                               set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
% %                               axis equal tight ij; colormap(gray); 
% %                               saveas(gca, [active_path 'bmode1.png']);
% %                               
%                               %subplot(1,2,1);
%                               figure(2);
%                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
%                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
%                               axis equal tight ij; colormap(gray); caxis([-60 0]);%colorbar;
%                               set(gca, 'XDir','Normal');set(gca, 'YDir','Reverse');
%                               %title('Regular');
%                               filename=['NotFiltered_' num2str(PhantomIndex)  '.png'];
%                               %saveas(gca, ['H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.18 - COLE_training_test_data\training_data\' filename]);
% 
%                               
%                               %subplot(1,2,2);
%                               figure(3);
%                               bmode=squeeze(postProcessedPerSubArrayFiltered{ii}.Bmode);
%                               p=imagesc(postProcessedPerSubArrayFiltered{ii}.disp_az_lims, postProcessedPerSubArrayFiltered{ii}.disp_dep_lims, bmode);
%                               axis equal tight ij; colormap(gray); caxis([-60 0]);%colorbar;
%                               set(gca, 'XDir','Normal');set(gca, 'YDir','Reverse');
%                               %title('Filtered');
%                               filename=['Filtered_' num2str(PhantomIndex)  '.png'];
%                               %saveas(gca, ['H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.18 - COLE_training_test_data\training_data\' filename]);
% end                              
% %title(t,['Phantom Id' num2str(PhantomIndex)]);
% %saveas(gca, [active_path '\pics\bmode_filtered_' num2str(PhantomIndex) '.png']);
close all;
end