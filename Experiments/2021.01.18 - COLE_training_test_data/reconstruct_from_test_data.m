close all
clc

clear

profile clear
profile on

F = findall(0,'type','figure','tag','TMWWaitbar');
delete(F);

[active_path,~,~] = fileparts(which('start.m'));
active_path=[active_path '\'];

%% Preliminaries
global c fs f0
f0    = 6.25e6;%7.5e6; % Transducer center frequency [Hz]
fs    =32*f0;%f0*32;%100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]';
global DWSliding
DWSliding=0;
lambda=c/f0;
global SaveKol
SaveKol=1;

tx_focal_depth  =0.03; % metres
alpha           = 0.0; % db/cm/MHz attenuation for TGC
imageSize       = [512 512];
Y_level          = 0.01;

set_field ('att',  alpha*100/1e6*f0);
set_field ('c', c);
set_field ('fs',  fs);
set_field ('use_att',1);
set_field ('use_rectangles',1);

pPatch = pulsifyPatch(struct(...
    'Tx_shape',                'flat', ...
    'Tx_nElements_x',             33,... %azimuth
    'Tx_nElements_y',            1, ....%elevation
    'Tx_subArraySize',              [1 33],...%[el az]
    'Tx_width',        2*lambda,...
    'Tx_height',      2*lambda,...
    'Tx_kerf',       0.05*2*lambda, ...
    'position',             [0, 0, 0.05], ...
     'amplitude',           1, ...
    'Tx_apodization',       'rectwin',...
    'Rx_apodization',       'rectwin'));

X=1;
stepY=1;
Y =1;%[1:4:55];%along azimuth direction, from left to right
[XX, YY] = meshgrid(X,Y);
shotPosition = [XX(:) YY(:)];


softwareOpt = PP_scanSequencer(struct(...
'scanMode',         'custom', ...
    'shotIndex',     shotPosition, ...
    'initialPosition',  [1, 1], ...
    'txFocalDepth',     tx_focal_depth, ...
    'nTxAz'         ,   91, ...
    'nTxEl'         ,   1, ...
    'maxDepth'      ,   0.13, ...
    'coleAmpThreshold', 1e-28, ...
    'openingAngle'  ,   [abs(-45)+abs(45),0]));

if softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end    
    
%% hardware options
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'COLE',        false));

global Reconstruct
Reconstruct=1;

SaveKolArray=[15];
for SaveKol=SaveKolArray
    [BfData_raw, ~]  = pPatch.run_transmission(softwareOpt, hardwareOpt);
    
    
    BfData_temp = BfData_raw(~all(cellfun(@isempty, BfData_raw),2),:);
    %BfData_temp = BfData_rawFiltered(~all(cellfun(@isempty, BfData_rawFiltered),2),:);
    maxBfData   = max(max(cell2mat(cellfun(@(x) max(x.data(:)),BfData_temp,'un',0))));  %calculate the normalizing constant
    clear BfData_temp;
    
    %% PostProcessing and compounding
    Axis    = [];
    Bmode3D = [];
    
    outOptions = struct('filterBmode',     0, ...
        'slice',           'elevation', ...
        'outImgSize',      [imageSize(1) imageSize(2)], ...
        'alpha',           0, ... % attenuation
        'debug_plots',     false, ...
        'use_post_pad',    false, ...
        'save_fig',        0, ...
        'save_png',        0, ...
        'outFolder',       active_path, ...
        'maxBfData',     double(maxBfData));
    
    global ChannelDataFiltering
    
    ChannelDataFiltering=0;
    if hardwareOpt.KWave
        temp=cell2mat(reshape(kwaveInput.KwavePatch.elementPosCenters, [1 size(kwaveInput.KwavePatch.elementPosCenters,1)*size(kwaveInput.KwavePatch.elementPosCenters,2)])');
        kwaveInput.KwavePatch.Tx.apex=mean(temp);
        kwaveInput.KwavePatch.Tx.centerOfAperture=mean(temp);
        kwaveInput.KwavePatch.Tx.pitch=pPatch.Tx.pitch;
        kwaveInput.KwavePatch.Rx.apex=mean(temp);
        kwaveInput.KwavePatch.Rx.centerOfAperture=mean(temp);
        kwaveInput.KwavePatch.compoundingProbe.subArrayPos=kwaveInput.KwavePatch.subArrayPosCenters;
        kwaveInput.KwavePatch.compoundingProbe.subArrayCurvature=cellfun(@(x) -x, kwaveInput.KwavePatch.subArrayCurvature, 'UniformOutput', false);
        clear temp;
        postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, kwaveInput.KwavePatch, softwareOpt, outOptions);
    else
        postProcessedPerSubArray = post_processing.main_post_processing_cells(BfData_raw, pPatch, softwareOpt, outOptions);
    end
    
    close all;
    for ii=1:numel(postProcessedPerSubArray)
        %                               figure(3); hold on;
        %                               bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
        %                               p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
        %                               p.AlphaData=0.7.*ones(size(bmode)); p.AlphaData((bmode==min(bmode(:))))=0;
        %                               set(gca, 'XDir','Normal');set(gca, 'YDir','Normal');
        %                               axis equal tight ij; colormap(gray);
        %                               saveas(gca, [active_path 'bmode1.png']);
        %
        %subplot(1,2,1);
        figure(2);
        bmode=squeeze(postProcessedPerSubArray{ii}.Bmode);
        p=imagesc(postProcessedPerSubArray{ii}.disp_az_lims, postProcessedPerSubArray{ii}.disp_dep_lims, bmode);
        axis equal tight ij; colormap(gray); caxis([-60 0]);%colorbar;
        set(gca, 'XDir','Normal');set(gca, 'YDir','Reverse'); axis tight equal; colorbar;
        %title('Regular');
        %saveas(gca, ['H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.18 - COLE_training_test_data\training_data\data_' num2str(SaveKol) '_filtered.png']);
        saveas(gca, ['H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.18 - COLE_training_test_data\training_data\data_' num2str(SaveKol) '_non_filtered.png']);
        %saveas(gca, ['H:\Kwave_Pulsify_Child\pulsify_us_soft\Experiments\2021.01.18 - COLE_training_test_data\training_data\data_' num2str(SaveKol) '_test.png']);
        
    end
end