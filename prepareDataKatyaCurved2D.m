function []=prepareDataKatyaCurved2D(active_path,kWavepath)
% F = findall(0,'type','figure','tag','TMWWaitbar');
% delete(F);

% [active_path,~,~] = fileparts(which('start.m'));
% active_path=[active_path '\'];

%% Preliminaries
global c fs f0 parallelNodesKWave
f0    = 6e6; % Transducer center frequency [Hz] 2.5e6
fs    = 100e6; % Sampling frequency [Hz]
c     = 1500; % Speed of sound [m/s]'; for COLE and FieldII
parallelNodesKWave = 0; %0 for no nodes parallelization
%%
tx_focal_depth  = 0.01; % metres
alpha           = 0.0; % db/cm/MHz attenuation for TGC
imageSize       = [512 512];
Y_level          = 0.01;

%% patch and software options
% pPatch = pulsifyPatch(struct(...
   % 'Tx_shape',                'MCLpatch', ...
    % 'Tx_nElements_x',            1,...
    % 'Tx_nElements_y',             100, ....
      % 'Tx_subArraySize',              [10 1],...%[el az]
      % 'Tx_width',       120e-6,...
      % 'Tx_height',       120e-6,...
       % 'Tx_kerf',       94e-6,...
      % 'nPoints',                 1e6, ...
      % 'predefined',     'knee_ligaments', ...
      % 'center',          [0  -0.02  -0.04], ...
      % 'rotationAngle',  [20 60 10],...
      % 'Tx_centerOfAperture',  [ 0.0049   -0.0141   -0.0519])); %[ 0.0059   -0.0141   -0.051]
% pPatch = pulsifyPatch(struct(...
%    'Tx_shape',                'flat', ...
%     'Tx_nElements_x',            10,...
%     'Tx_nElements_y',             10, ....
%       'Tx_subArraySize',              [10 10],...%[el az]
%       'Tx_width',       120e-6,...
%       'Tx_height',       120e-6,...
%        'Tx_kerf',       94e-6,...
%       'nPoints',                 1e6, ...
%       'predefined',     'knee_ligaments', ...
%       'center',          [0  -0.02  -0.04], ...
%       'rotationAngle',  [20 60 10],...
%       'Tx_centerOfAperture',  [ 0.0049   -0.0141   -0.0519])); %[ 0.0059   -0.0141   -0.051]

pPatch = pulsifyPatch(struct(...
   'Tx_shape',                'flat', ...
    'Tx_nElements_x',            10,...
    'Tx_nElements_y',             10, ....
      'Tx_subArraySize',              [10 10],...%[el az]
      'Tx_width',       120e-6,...
      'Tx_height',       120e-6,...
       'Tx_kerf',       94e-6,...
      'nPoints',                 1e6, ...
      'predefined',     'ellipsoid', ...
      'center',          [0  0  0.002], ...
      'ellipsoidData',   [1.33, 2, 0.03], ...
      'radius',         [30/8e3, 25/8e3], ...
      'rotationAngle',  [0 0 0],...
      'Tx_centerOfAperture',  [ 0  0   -0.005])); %[ 0.0059   -0.0141   -0.051]


X=1;
Y =[1];
[XX, YY] = meshgrid(X,Y);
shotPosition = [XX(:) YY(:)];

%% software options
softwareOpt = PP_scanSequencer(struct(...
'scanMode',         'custom', ...
    'shotIndex',     shotPosition, ...
    'initialPosition',  [1, 1], ...
    'txFocalDepth',     tx_focal_depth, ...
    'nTxAz'         ,   21, ...
    'nTxEl'         ,   21, ...
    'maxDepth'      ,   0.13, ...
    'coleAmpThreshold', 1e-28, ...
    'openingAngle'  ,   [-10, 10]));

if softwareOpt.nTxAz>1
   sector_image_plane='azimuth';
elseif   softwareOpt.nTxEl>1
   sector_image_plane='elevation';
else
   sector_image_plane='azimuth';
end    
    
%% hardware options
hardwareOpt = transOptions(struct('mode_tx', 'full_control',...
    'mode_rx',     'full_control',...
    'KWave',        true));

if strcmp(hardwareOpt.mode_tx,'tx_on_rows')&strcmp(hardwareOpt.mode_rx,'rx_on_rows')&(softwareOpt.nTxAz>1)
    error('For row-row, we can do sectorial imaging only in the elevation plane');
end   
if strcmp(hardwareOpt.mode_tx,'tx_on_cols')&strcmp(hardwareOpt.mode_rx,'rx_on_cols')&(softwareOpt.nTxEl>1)
    error('For col-col, we can do sectorial imaging only in the azimuth plane');
end

if hardwareOpt.COLE
%% load LUT data once
% I save it to rawData variable, so if you want to load different LUTs, you
% would need to clean rawData variable (in the beginning of the file all variables are clened except it)
%index=round(cmpdImg.number_of_images_in_secondary_direction/2);
if (~exist('rawData','var'))
   if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
      folderName = strcat(pwd, '\Dataset\COLE\', 'tx_on_rows', '_', 'rx_on_rows', '\3D\'); 
   else    
      folderName = strcat(pwd, '\Dataset\COLE\', hardwareOpt.mode_tx, '_', hardwareOpt.mode_rx, '\3D\');
   end
   h_w = waitbar(0,['I will load LUT tables forever...']);
   fprintf('I will load LUT tables forever...');
   t0 = tic; 
   for ii = 1:length(softwareOpt.txEvents)
                 if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '.mat');
                 else    
                     filename = strcat(folderName, 'LUT_angleAz_', num2str(softwareOpt.txEvents(ii).txAngle(1)), '_angleEl_', num2str(softwareOpt.txEvents(ii).txAngle(2)), '.mat');
                 end
                 if ~exist(filename, 'file')
                        error('COLE: LUT not found');
                 else
                        load(filename, 'echoData', 'impulseResponse');
                        rawData.echoData{ii}=echoData;
                        rawData.impulseResponse{ii}=impulseResponse;
                        if (strcmp(hardwareOpt.mode_tx, 'tx_on_cols')& strcmp(hardwareOpt.mode_rx, 'rx_on_cols'))
                            rawData.echoData{ii}.data = permute(rawData.echoData{ii}.data, [1 3 2]);
                        end    
                 end
                 waitbar((ii)/(length(softwareOpt.txEvents)),h_w);        
   end
   delete(h_w), drawnow
end
end


if ~exist([active_path 'geometry.png'])
    inOptions.plotsubArray=0;
    inOptions.plotSequence=0;
    inOptions.newFigure=1;
    pPatch.plot_geometry(inOptions, softwareOpt);
    saveas(gca, [active_path 'geometry.png']);
    close all;
end

%% crop the phantom
a=pPatch.convert_cell_to_Fii_mat(pPatch.Tx.elementPos);
gap_el=0.003;%0.002
gap_az=0.003;%0.0005;
ind=find(((pPatch.phantom.position(:,2)<max(a(:,2))+gap_el)&(pPatch.phantom.position(:,2)>min(a(:,2))-gap_el)&...
         (pPatch.phantom.position(:,1)>min(a(:,1))-gap_az)&(pPatch.phantom.position(:,1)<max(a(:,1))+gap_az)&...
         (pPatch.phantom.position(:,3)<mean(a(:,3))+1.5*tx_focal_depth)));
          %(pPatch.phantom.position(:,3)<mean(a(:,3))+1.8*tx_focal_depth)));
ind_new=zeros(1,numel(pPatch.phantom.position));      
ind_new(ind)=1:numel(ind);
pPatch.phantom.segmentationIndex=cellfun(@(x) ind_new(intersect(x,ind)),pPatch.phantom.segmentationIndex, 'UniformOutput', false);    
center=mean([pPatch.phantom.position(ind, :); pPatch.convert_cell_to_Fii_mat(pPatch.Tx.elementPos)]);
arr=[pPatch.phantom.position(ind, :); pPatch.convert_cell_to_Fii_mat(pPatch.Tx.elementPos)];
center=(max(arr)+min(arr))/2;
pPatch.phantom.position=pPatch.phantom.position(ind, :)-center;
pPatch.phantom.positionForCalc=pPatch.phantom.position;
pPatch.phantom.amplitude=pPatch.phantom.amplitude(ind);
phantomSaved=pPatch.phantom;
save('currently_cropped_phantom.mat', 'phantomSaved');
%save([active_path 'currently_cropped_phantom.mat'], 'phantomSaved');

pPatch = pulsifyPatch(struct(...
   'Tx_shape',                pPatch.Tx.shape, ...
    'Tx_nElements_x',           pPatch.Tx.nElements_x,...
    'Tx_nElements_y',           pPatch.Tx.nElements_y, ....
    'Tx_subArraySize',          pPatch.Tx.subArraySize,...
     'Tx_width',       pPatch.Tx.width,...
      'Tx_height',     pPatch.Tx.height,...
      'Tx_kerf',       pPatch.Tx.kerf,...
   'predefined',     'ellipsoid_cropped', ...%'predefined',     'knee_ligaments_cropped', ...
    'nPoints',                 5e5,...%5e5,...%1e4, ...
      'Tx_centerOfAperture', pPatch.Tx.centerOfAperture-center));      

%save([active_path 'cropped_field_patch_knee_ligaments.mat'], 'pPatch', '-v7.3');
%load('cropped_field_patch_knee_ligaments.mat', 'pPatch');
if ~exist([active_path 'geometry_zoom.png'])
    inOptions.plotsubArray=0;
    inOptions.plotSequence=0;
    inOptions.newFigure=1;
    pPatch.plot_geometry(inOptions, softwareOpt);
    saveas(gca, [active_path 'geometry_zoom.png']);
end

disp('############################# Projecting into kWave ##############################################################')

if hardwareOpt.KWave
    kWaveSetup=kwaveSimulation_Setup(struct('pPatch',   pPatch, ...
        'points_per_wavelength_x', 10,...
        'points_per_wavelength_y', 10,...
        'points_per_wavelength_z', 10));
    kwaveInput.kgrid=kWaveSetup.kgrid;
    kwaveInput.medium=kWaveSetup.medium;
    kwaveInput.KwavePatch=kWaveSetup.KwavePatch;
    kwaveInput.input_signal=kWaveSetup.input_signal;
    kwaveInput.input_args=kWaveSetup.input_args;
    fs    = 1/kwaveInput.kgrid.dt; % Sampling frequency [Hz]
    c= kWaveSetup.c0; %background speed of sound
    %InOptions.plot_kgrid=0;InOptions.plot_subArrays=0;InOptions.plot_patch=1;
    %kWaveSetup.plot_geometry(InOptions, kwaveInput.KwavePatch,kwaveInput.kgrid,kwaveInput.medium);
    %  hold on;
    %  scatter3(kwaveInput.KwavePatch.elementPosCenters{1,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{1,1}(2)*10^3, 10.0, 'r', 'filled');
    %  scatter3(kwaveInput.KwavePatch.elementPosCenters{4,1}(1)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(3)*10^3, kwaveInput.KwavePatch.elementPosCenters{4,1}(2)*10^3, 10.0, 'r', 'filled');
    save([active_path 'data_for_kWave.mat'], 'softwareOpt', 'hardwareOpt', 'kwaveInput','f0','c','pPatch','active_path','kWavepath', '-v7.3');
    disp(['############################# Saved data prepared to go into kWave ##############################################################'])
else
    BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData,[],[],[]);
end

end