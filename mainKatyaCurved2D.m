function []=mainKatyaCurved2D(active_path,kWavepath,current_path,Node)

softwareOpt=PP_scanSequencer(); pPatch=pulsifyPatch(); hardwareOpt = transOptions();
load([active_path 'data_for_kWave.mat'], 'softwareOpt', 'hardwareOpt','f0','c','kwaveInput','pPatch','active_path'); %load(['data_for_kWave.mat']);
global f0 c 
fs    = 1/kwaveInput.kgrid.dt; 
global fs
load([active_path 'nodesDistribution.mat']);%load(['nodesDistribution.mat'])
global parallelNodesKWave
parallelNodesKWave=0; %the parallel version for local computer, not vsc!
disp('############################# Ready to start calculating kWave ##############################################################')
disp(['############################# I am' Node '##############################################################'])
disp(['############################# Calculating shots ' num2str(taskPerNode{str2num(Node)}) '##############################################################'])

if hardwareOpt.KWave
    tStart=tic;
%     inOptions.plotsubArray=0;
%     inOptions.plotSequence=0;
%     inOptions.newFigure=1;
%     pPatch.plot_geometry(inOptions, softwareOpt);

    pPatch.run_transmission(softwareOpt, hardwareOpt, taskPerNode{str2num(Node)}, active_path);% kwaveInput, active_path, kWavepath,taskPerNode{str2num(Node)},choice);
    disp(['############################# The time for kWave calculation was: ' num2str(toc(tStart))  '##############################################################'])
else
    BfData_raw  = pPatch.run_transmission(softwareOpt, hardwareOpt, rawData);
end


delete(fullfile(current_path,['output2D_' Node '.txt']))
delete(fullfile(current_path,['jobMain2D_' Node '.pbs']))
disp('############################# Done with kWave ##############################################################')


kiy